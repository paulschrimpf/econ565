using RData # install with Pkg.add("RData") if you haven't yet
using DataFrames

dt = load("dfr.rda",convert=true)
# using dialysisFacilityReports.Rdata failed here, I'm guessing it's
# because load() is overloaded and dispatch is based on the file
# extension, so I renamed the data to dfr.rda. It works, my reasoning
# may be wrong, but whatever.

dialysis = dt["dialysis"]
clear!(:dt)

sort!(dialysis, cols = [order(:provfs), order(:year)])

"
    panelLag(x::Symbol,  i::Symbol, t::Symbol, df::DataFrame, lag=1)

Returns column x lagged by from DataFrame df lagged by lag period. i
Should index individuals and t index time. The DataFrame must be sored
by i, t prior to usage, e.g.

    sort!(df, cols = [order(:i), order(:t)])

lag may be positive or negative.
"
function panelLag(x::Symbol,  i::Symbol, t::Symbol, df::DataFrame, lag=1)
    @assert(issorted(df, cols=[order(i), order(t)]))
    if (lag>0)
        xLag = df[1:(nrow(df)-lag),x]
        xLag[df[1:(nrow(df)-lag),i] .!= df[(1+lag):nrow(df),i]] =
            missing
        xLag[(df[1:(nrow(df)-lag),t]+lag) .!= df[(1+lag):nrow(df),t]] =
            missing
        val = cat(1, fill(missing, lag), xLag)
    elseif (lag<0)
        lag = -lag
        xLag = df[(1+lag):nrow(df),x]
        xLag[df[1:(nrow(df)-lag),i] .!= df[(1+lag):nrow(df),i]] =
            missing
        xLag[(df[1:(nrow(df)-lag),t]+lag) .!= df[(1+lag):nrow(df),t]] =
            missing
        val = cat(1, xLag, fill(missing, lag))
    else
        val = df[:,x]
    end
    return(val)
end

"
    mapMiss(f,x,y)

Returns f(x,y) if matching elements of x and y are
non-missing. Returns missing otherwise.
"
function mapMiss(f, x, y)
    return(map((a,b) ->
    if (ismissing(a) || ismissing(b))
        missing
    else
        f(a,b)
    end
    , x, y))
end

"
    mapMiss(f,x)

Returns f(x) if matching elements x is
non-missing. Returns missing otherwise.
"
function mapMiss(f, x)
    return(map((a) ->
    if (ismissing(a))
        missing
    else
        f(a)
    end
    , x))
end


dialysis[:,:invest] = panelLag(:stations, :provfs, :year, lag=-1) -
    dialysis[:,:stations]
dialysis[:,:labor] = (dialysis[:, :nurseFT] + 0.5*dialysis[:,:nursePT]+
                      dialysis[:,:ptcareFT] + 0.5*dialysis[:,:ptcarePT])
dialysis[:,:hiring] = panelLag(:labor, :provfs, :year, lag=-1) -
    dialysis[:,:labor]
dialysis[:,:for_profit] = dialysis[:,:profit_status].=="For Profit"
dialysis[:,:inspected_this_year] =
    mapMiss(&&, dialysis[:,:days_since_inspection].>=0,
            dialysis[:,:days_since_inspection].<365)
stateRates = by(dialysis, :state, df ->
                mean(skipmissing(df[:,:inspected_this_year])))
rename!(stateRates, :x1 => :state_inspection_rate)
dialysis = join(dialysis, stateRates, on = :state)


# clean up chain_name

# assuming REGIONAL-XXX is same as XXX, not certain this is correct,
# but seems rights
foo = dialysis[:,:chain_name]
foo = map(x -> replace(x,r"REGIONAL-",""), foo)
foo = map(x -> replace(x,r"  "," "), foo)
foo = map(x -> replace(x,r"(,|\\.) ",""), foo)
foo = map(x -> replace(x,r"( LLC| INC) ",""), foo)
for name in ["FRESENIUS",
             "DAVITA",
             "NATIONAL RENAL INSTUTES",
             "RENAL ADVANTAGE",
             "LIBERTY DIALYSIS",
             "PACIFIC SOUTH BAY DIALYSIS",
             "DIALYSIS NEWCO",
             "TRC TEXAS",
             "NORTH CENTRAL P",
             "AMERICAN RENAL ASSOCIATES",
             "BELMONT COURT DIALYSIS",
             "DIALYSIS CLINIC INC",
             "DREILING MEDICAL",
             "HCA HEALTH SERVICES OF OK",
             "IDF",
             "MEMORIAL HERMANN H",
             "NORTHERN MI"]
    foo = map(x -> ifelse(ismatch(Regex(name),x), name, x), foo)
end
foo = map(x -> replace(x,r"RENAL CAREPARTNERS","RENAL CARE PARTNERS"), foo)
foo = map(x -> replace(x,r"UNIV OF UTAH DIAL PRG","UNIVERSITY OF UTAH DIALYSIS PROGRAM"), foo)

dialysis[:,:chain_name] = foo
dialysis[:,:fresenius] = foo.=="FRESENIUS"
dialysis[:,:davita] = foo.=="DAVITA"

## calculate number of other centers with positive patients in same city
dialysis[:,:city] = map(uppercase, dialysis[:,:city])
dialysis[:,:patient_years] = dialysis[:,:patient_months]./12

comps = by(dialysis,[:city,:year],
           df -> mapreduce((x) -> ifelse(ismissing(x),0,1*(x>0)), +, df[:patient_months])
           )
rename!(comps, :x1 => :competitors)
dialysis = join(dialysis, comps, on = [:city,:year])

## Summary statistics
describe(dialysis) # similar to summary() in R
# skipping making a nice table


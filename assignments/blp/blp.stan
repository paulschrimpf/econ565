data {
  int<lower=1> T; # number of markets
  int<lower=1> N[T]; # number of firms in each market
  int<lower=1> J[T]; # number of products in each market
  int<lower=1> K; # number of product attributes
  int<lower=1> S; # number of simulation draws for integrals

  vector[J] share[T]; # shares
  matrix[K,]
    
}

parameters {

}
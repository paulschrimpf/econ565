## R code to reproduce results from Rust & Rothwell (1995 & 1996)
rm(list=ls())

## load data
source("loadRRdata.R",echo=TRUE)

################################################################################
## Summary statistics and figures
# Table I
plantData$percent.time.operating <- plantData$hrs.operating/plantData$hrs.total
plantData$percent.time.refueling <- plantData$hrs.refuel/plantData$hrs.total
plantData$percent.time.forced.out <- plantData$hrs.forced.out/plantData$hrs.total
plantData$percent.time.planned.out <- plantData$hrs.plan.out/plantData$hrs.total
df <- plantData
df$era <- "1975-79"
df$era[df$year>=80] <- "1980-83"
df$era[df$year>=84] <- "1984-93"
tab1 <- aggregate(. ~ era, df[,c("era", "percent.time.operating",
                         "percent.time.refueling", "percent.time.forced.out",
                         "percent.time.planned.out")], FUN=mean)
tab1 <- cbind(tab1,aggregate(. ~ era, df[,c("era", "hrs.total")],
                             FUN=sum)[,2],
              aggregate(. ~ era, df[df$spell.type!="exit",c("era", "hrs.total")],
                        FUN=length)[,2])
colnames(tab1)[6:7] <- c("hours","months")
tab1

# Figure 3
library(ggplot2)
fontFamily <- "Palatino"
fdims <-c(6, 3) ## figure dimensions, c(5.03937, 3.77953)=128mm x 96mm = dimensions of beamer slide
printDev <- function(filename, width=fdims[1], height=fdims[2], family=fontFamily) {
  cairo_pdf(filename,width=width,height=height,family=family)
  #pdf(filename,width,height)
}

df <- plantData
fig3 <- ggplot(data=aggregate(. ~ year, plantData, mean),
               aes(x=year,y=num.forced.out)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("number of forced outages per month")
printDev("figures/fig3.pdf")
fig3
dev.off()

fig4 <- ggplot(data=aggregate(. ~ year, df,mean),
               aes(x=year,y=100*percent.time.operating)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("availability factor")
printDev("figures/fig4.pdf")
fig4
dev.off()

fig7a <- ggplot(data=aggregate(. ~ year, df, mean),
               aes(x=year,y=100*percent.time.planned.out)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("% time in planned outages")
printDev("figures/fig7a.pdf")
fig7a
dev.off()


fig7b <- ggplot(data=aggregate(. ~ year, df,mean),
               aes(x=year,y=100*percent.time.forced.out)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("% time in forced outages")
printDev("figures/fig7b.pdf")
fig7b
dev.off()

################################################################################
## Re-code major problem spells
nop <- plantData$hrs.operating==0
tmp <- (nop[-1]!=nop[-length(nop)] | plantData$ID[-1] != plantData$ID[-nrow(plantData)])
tmp[is.na(tmp)] <- FALSE
op.id <- cumsum(c(TRUE,tmp))
op.length <- ave(op.id,factor(op.id), FUN=length)
tmp <- op.length>9 & (nop==TRUE)
tmp[is.na(tmp)] <- FALSE
plantData$major.problem.spell <- tmp
df <- subset(plantData,action!="exit")
df$era <- "1975-79"
df$era[df$year>=80] <- "1980-83"
df$era[df$year>=84] <- "1984-93"
tab1 <- aggregate(. ~ era, df[,c("era", "percent.time.operating",
                         "percent.time.refueling", "percent.time.forced.out",
                         "percent.time.planned.out")], FUN=mean)
tab1 <- cbind(tab1,aggregate(. ~ era, df[,c("era", "hrs.total")],
                             FUN=sum)[,2],
              aggregate(. ~ era, df[df$spell.type!="exit",c("era", "hrs.total")],
                        FUN=length)[,2])
colnames(tab1)[6:7] <- c("hours","months")
tab1

## re-create figures without major problem spells
df <- subset(plantData,major.problem.spell==FALSE & action!="exit")
fig3 <- ggplot(data=aggregate(. ~ year, plantData, mean),
               aes(x=year,y=num.forced.out)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("number of forced outages per month")
printDev("figures/fig3fix.pdf")
fig3
dev.off()

fig4 <- ggplot(data=aggregate(. ~ year, df,mean),
               aes(x=year,y=100*percent.time.operating)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("availability factor")
printDev("figures/fig4fix.pdf")
fig4
dev.off()

fig7a <- ggplot(data=aggregate(. ~ year, df, mean),
               aes(x=year,y=100*percent.time.planned.out)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("% time in planned outages")
printDev("figures/fig7afix.pdf")
fig7a
dev.off()


fig7b <- ggplot(data=aggregate(. ~ year, df,mean),
               aes(x=year,y=100*percent.time.forced.out)) + geom_line() +
  theme_minimal() + xlab("year") + ylab("% time in forced outages")
printDev("figures/fig7bfix.pdf")
fig7b
dev.off()


################################################################################
## Estimate transition probabilities

# fix durations to drop major problem spells
st <- as.character(plantData$spell.type)
st[plantData$major.problem.spell] <- "major.problem"
st <- as.factor(st)
spell.id <-
  cumsum(c(TRUE,st[-1]!=st[-length(st)] | plantData$ID[-1] != plantData$ID[-nrow(plantData)]))
duration <- ave(rep(1,length(spell.id)),spell.id,FUN=cumsum)
plantData$duration <- duration

# estimate P(forced outage | duration)
pof.glm <- glm( (npp.signal=="forced.outage") ~ duration + I(duration^2),
               family=binomial(link=logit) ,
               data=subset(plantData,npp.signal!="cont.refuel" &
                   major.problem.spell==FALSE & spell.type=="operating"))
# plot estimated probability
md <- max(subset(plantData,npp.signal!="cont.refuel")$duration)
df <- data.frame(duration=1:md)
tmp <- predict(pof.glm,newdata=df,type="response", se.fit=TRUE)
df$p <- tmp$fit
df$se.p <- tmp$se.fit
df$lo <- df$p - df$se.p*1.96
df$hi <- df$p + df$se.p*1.96
fig10.of <- ggplot(data=df, aes(x=duration, y=p)) +
  geom_line() + geom_line(aes(y=lo),linetype="dashed") +
  geom_line(aes(y=hi),linetype="dashed") +
  scale_x_continuous(limits=c(0,30)) +
  xlab("Month of operating spell") +
  scale_y_continuous(limits=c(0.3,0.55)) +
  ylab("Probability of forced outage") +
  theme_minimal()
rm(df,tmp)

# estimate P(refueling ends | duration)
pro.glm <- glm( (npp.signal!="cont.refuel") ~
               #duration,
               I(duration==1) +
               I(duration==2) +
               I(duration==3) +
               I(duration==4) +
               I(duration==5) +
               I(duration==6) +
               I((duration>=7)*(duration-6)),
               family=binomial(link=logit) ,
               data=subset(plantData,spell.type=="refueling" &
                   major.problem.spell==FALSE ))
# plot estimated probability
md <- max(subset(plantData,spell.type=="refueling")$duration)
df <- data.frame(duration=1:md)
tmp <- predict(pro.glm,newdata=df,type="response", se.fit=TRUE)
df$p <- tmp$fit
df$se.p <- tmp$se.fit
df$lo <- df$p - df$se.p*1.96
df$hi <- df$p + df$se.p*1.96
fig10.ro <- ggplot(data=df, aes(x=duration, y=p)) +
  geom_line() + geom_line(aes(y=lo),linetype="dashed") +
  geom_line(aes(y=hi),linetype="dashed") + theme_minimal() +
  xlab("Month of refueling spell") +
  ylab("Probability of resuming operation")
rm(df,tmp)


printDev("figures/fig10of.pdf")
fig10.of
dev.off()

printDev("figures/fig10ro.pdf")
fig10.ro
dev.off()

max.duration <- 24
plantData$duration.orig <- plantData$duration
plantData$duration <- pmin(plantData$duration.orig,max.duration)

# create function that given array of states (or a single state) returns predicted
# probabilities
pof.fn <- function(state) {
  p <- predict(pof.glm,newdata=state,type="response")
  p[state$npp.signal=="cont.refuel"] <- 0
  p[state$spell.type=="exit"] <- 0
  return(p)
}
pro.fn <- function(state) {
  p <- predict(pro.glm,newdata=state,type="response")
  p[state$spell.type!="refueling"] <- 0
  return(p)
}


action.vars <- c("action")
state.vars <- c("spell.type","npp.signal","duration")
# Find out how many states and actions there are
n.states <- prod(apply(plantData[,state.vars], 2,
                       function(x) {sum(!is.na(unique(x))) } ))
n.actions <- sum(!is.na(unique(plantData[,action.vars])))

# Given vector of variables that define a state, create a function
# that returns an index for each unique combination of them, and a
# function that given an index returns a state vector.
# The two resulting functions are inverses of one another
vector.index.converter <- function(data, state.vars) {
  nv <- rep(NA,length(state.vars))
  state.levels <- list()
  for (s in 1:length(state.vars)) {
    state.levels[[s]] <- sort(unique(data[,state.vars[s]]))
    nv[s] <- length(state.levels[[s]])
  }
  si <- function(state) {
    stopifnot(length(state)==length(nv))
    sn <- as.numeric(state)
    fac <- 1
    index <- 0
    for (i in 1:length(nv)) {
      index <- index + (sn[i]-1)*fac
      fac <- fac*nv[i]
    }
    return(index+1)
  }

  sv <- function(index) {
    stopifnot(index<=n.states)
    state <- data[1,state.vars]
    li <- rep(NA,length(state.vars))
    fac <- prod(nv)
    index <- index - 1
    for (i in 1:length(nv)) {
      li[i] <- index %% nv[i] + 1
      index <- index %/% nv[i]
      state[[i]] <- state.levels[[i]][li[i]]
    }
    state[[1]] <- state.levels[[1]][li[1]]
    return(state)
  }
  return(list(index=si, vector=sv))
}

state.fn <- vector.index.converter(plantData,state.vars)
action.fn <- vector.index.converter(plantData, action.vars)
for (s in 1:n.states) {
  stopifnot(state.fn$index(state.fn$vector(s))==s)
}

# Given functions pof and pro return an X by X by A array of
# transition probabilities. pof(state) is the probability of receiving
# a forced outage signal. pro(state) is the probability of not
# receiving a cont.refuel signal while refueling.
transition.prob <- function(pof, pro) {
  P <- array(0,dim=c(n.states, n.states, n.actions))
  state <- plantData[1,state.vars]
  action <- plantData$action[1]
  for (i in 1:n.states) {
    old.state <- state.fn$vector(i)
    new.state <- old.state

    # choose to exit
    new.state$spell.type <- factor("exit",levels=levels(old.state$spell.type))
    action <- factor("exit",levels=levels(action))
    ia <- action.fn$index(action)
    P[state.fn$index(new.state),i,ia] <- 1

    if (old.state$spell.type!="exit") {
      # choose to refuel
      if (old.state$spell.type=="refueling") {
        new.state$duration <- min(old.state$duration+1,
                                  max.duration)
      } else {
        new.state$duration <- 1
      }
      new.state$spell.type <- factor("refueling",levels=levels(old.state$spell.type))
      action <- factor("refuel", levels=levels(action))
      ia <- action.fn$index(action)
      new.state$npp.signal <- factor("none", levels=levels(old.state$npp.signal))
      P[state.fn$index(new.state), i, ia] <-
        pro(old.state)*(1-pof(old.state))
      new.state$npp.signal <- factor("forced.outage", levels=levels(old.state$npp.signal))
      P[state.fn$index(new.state), i, ia] <-
        pro(old.state)*pof(old.state)
      new.state$npp.signal <- factor("cont.refuel", levels=levels(old.state$npp.signal))
      P[state.fn$index(new.state), i, ia] <-
        (1-pro(old.state))

      # choose to operate
      if (old.state$npp.signal!="cont.refuel") {
        new.state <- old.state
        new.state$spell.type <- factor("operating",levels=levels(old.state$spell.type))
        if (old.state$spell.type=="refueling") {
          new.state$duration <- 1
        } else {
          new.state$duration <- min(old.state$duration+1, max.duration)
        }
        for (a in c("shutdown", "run1.25", "run26.50", "run51.75",
                    "run76.99", "run100")) {
          a <- factor(a, levels=levels(action))
          ia <- action.fn$index(a)
          new.state$npp.signal <- factor("none", levels=levels(old.state$npp.signal))
          P[state.fn$index(new.state), i, ia] <- (1-pof(old.state))
          new.state$npp.signal <- factor("forced.outage", levels=levels(old.state$npp.signal))
          P[state.fn$index(new.state), i, ia] <- pof(old.state)
        }
      }
    }
    cat(".")
    if (i %% 50 == 0) cat(" ",i," of ",n.states," transitions completed\n")
  }
  return(P)
}
P <- transition.prob(pof.fn,pro.fn)

feasible.action <- matrix(TRUE,nrow=n.states,ncol=n.actions)
for(s in 1:n.states) {
  state <- state.fn$vector(s)
  if (state$npp.signal=="cont.refuel") {
    feasible.action[s,] <- FALSE
    feasible.action[s,2] <- TRUE
  }
  if (state$spell.type=="exit") {
    feasible.action[s,] <- FALSE
    feasible.action[s,1] <- TRUE
  }
}


## flow utility
phi.names <-
  c("exit","refuel","f.r","duration","shutdown","run1.25","run26.50","run51.75","run76.99","run100",
    "f.s","f.100")
# parameter estimates from Rust & Rothwell (1995)
phi.pre <- c(0,-1.82, -2.33, -0.05, -0.04, -1.82, -0.96, -0.15, 1.52,
             2.93, -4.03, -3.44)
names(phi.pre) <- phi.names
phi.post <- c(0, -3.44, -3.09, -0.06, -0.54, -2.12, -1.58, -0.74,
              0.54, 2.93, -4.04, -5.89)
names(phi.post) <- phi.names

flow.utility <- function(phi) {
  u <- matrix(0,nrow=n.states,ncol=n.actions)
  colnames(u) <- levels(plantData$action)
  if (is.null(names(phi))) names(phi) <- phi.names
  for (a in c("exit", "refuel", "shutdown", "run1.25", "run26.50",
              "run51.75", "run76.99", "run100")) {
    u[,a] <- phi[a]
  }
  for(s in 1:n.states) {
    state <- state.fn$vector(s)
    if (state$spell.type=="exit") {
      u[s,] <- phi["exit"]
    } else {
      if (state$spell.type=="operating") {
        for (a in c("run1.25", "run26.50", "run51.75",
                    "run76.99", "run100")) {
          u[s,a] <- u[s,a] + state$duration*phi["duration"]
        }
      }
      if (state$npp.signal=="forced.outage") {
        u[s,"refuel"] <- u[s,"refuel"] + phi["f.r"]
        u[s,"shutdown"] <- u[s,"shutdown"] + phi["f.s"]
        u[s,"run100"] <- u[s,"run100"] + phi["f.100"]
      }
    }
  }
  return(u)
}

## Code for estimating a dynamic discrete decision problem
#
# Each period agents choose a \in {1, ..., A}.
# There is a state x \in {1, ..., X }.
# Flow utility is u(x,a) + \epsilon(a), where e(a) ~ Type-I
# extreme value
# x follows a controlled Markov process with transition probability
# P[newx, oldx, a]
# u is represented by a X by A matrix

## Calculate choice specific value function
## v(t,x,a) =  u(x,a) + beta*E[max_{a'} v(t+1,x',a') + e(a') | x, a]
choice.value <- function( u, discount, p.x, T) {
  v <- array(dim=c(T,nrow(u),ncol(u)))
  v[T,,] <- u[,]
  vtp1 <- v[T,,]
  for(t in (T-1):1) {
    for (a in 1:ncol(u)) {
      v[t,,a] <- u[,a] + discount*log( rowSums(exp(vtp1)*feasible.action) ) %*% p.x[,,a]
    }
    vtp1 <- v[t,,]
  }
  return(v)
}


discount <- 0.999 # monthly discount factor


## Figures 13 & 14
u <- flow.utility(phi.post)
T <- 480
v <- choice.value(u,discount,P,T)
df <- data.frame(duration=1:max.duration, refuel=NA, u0=NA, u13=NA,
                 u88=NA, u100=NA)
state <- state.fn$vector(1)
state$npp.signal <- factor("none",levels=levels(state$npp.signal))
state$spell.type <-
  factor("operating",levels=levels(state$spell.type))
t <- 366
dimnames(v)[[3]] <- levels(plantData$action) # so we can index by action name
for(d in df$duration) {
  state$duration <- d
  si <- state.fn$index(state)
  df$refuel[d] <- v[t,si,"refuel"]
  df$u0[d] <- v[t,si,"shutdown"]
  df$u13[d] <- v[t,si,"run1.25"]
  df$u88[d] <- v[t,si,"run76.99"]
  df$u100[d] <- v[t,si,"run100"]
}
library(reshape2)
df <- melt(df,id.vars="duration")
fig13 <- ggplot(data=df,aes(x=duration,y=value,colour=variable)) +
  geom_line() + theme_minimal()
printDev("figures/fig13.pdf")
fig13
dev.off()

df <- data.frame(age=1:480, refuel=NA, u0=NA, u13=NA,
                 u88=NA, u100=NA, exit=NA)
state <- state.fn$vector(1)
state$npp.signal <- factor("none",levels=levels(state$npp.signal))
state$spell.type <-
  factor("operating",levels=levels(state$spell.type))
state$duration <- 5
si <- state.fn$index(state)
df$refuel <- v[,si,"refuel"]
df$u0 <- v[,si,"shutdown"]
df$u13 <- v[,si,"run1.25"]
df$u88 <- v[,si,"run76.99"]
df$u100 <- v[,si,"run100"]
df$exit <- v[,si,"exit"]
df <- melt(df,id.vars="age")
fig14 <- ggplot(data=df,aes(x=age,y=value,colour=variable)) +
  geom_line() + theme_minimal()
printDev("figures/fig14.pdf")
fig14
dev.off()


# create indices of observed state and actions
# this is slow, but it only needs to be done once
for (i in 1:nrow(plantData)) {
  plantData$state.index[i] <- state.fn$index(plantData[i,state.vars])
  plantData$action.index[i] <- action.fn$index(plantData[i,action.vars])
  if (i %% 100 == 0) {
    cat(i, " of ", nrow(plantData),"\n")
  }
}

likelihood <- function(phi) {
  u <- flow.utility(phi)
  v <- choice.value(u, discount, P, T)
  #v <- choiceValue(u,feasible.action, discount, P, 480)
  sumExpV <- matrix(NA,nrow=dim(v)[1],ncol=dim(v)[2]) # T by S
  for(t in 1:dim(v)[1]) {
    sumExpV[t,] <- rowSums(exp(v[t,,])*feasible.action)
  }
  ccp <- exp(v[cbind(plantData$age, plantData$state.index,
                     plantData$action.index)]) /
          sumExpV[cbind(plantData$age,plantData$state.index)]
  return(sum(log(ccp),na.rm=TRUE))
}

Rprof("rrLike.prof", line.profiling=TRUE)
likelihood(phi.post)
Rprof(NULL)

summaryRprof("rrLike.prof")

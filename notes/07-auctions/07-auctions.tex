\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Auctions}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \cite{hendricks2007} (working paper version \cite{hendricks2000})
    \item \cite{reiss2007structural} section 8 relates auctions to
      other structural models
    \item \cite{athey2007} focuses on identification
    \item \cite{klemperer1999} and \cite{klemperer2004} for theory
    \end{itemize}
  \end{itemize} 
\end{frame}

\begin{frame}
  \tableofcontents  
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame}[allowframebreaks]
  \frametitle{Introduction}
  \begin{itemize}
  \item Auctions widely used
    \begin{itemize}
    \item Historical:
      \begin{itemize}
      \item Babylonian wives
      \item Greek mines \& slaves
      \item Roman war booty, debtors' goods, etc        
      \end{itemize}
    \item Governments:
      \begin{itemize}
      \item Finance: treasury bills, foreign exchange
      \item Procurement
      \item Privatization
      \item Natural resources: oil, gas, fishing, timber, pollution,
        wireless spectrum
      \end{itemize}
    \item Private commerce:
      \begin{itemize}
      \item Art
      \item Houses
      \item eBay
      \end{itemize}
    \end{itemize}
  \item Purpose of auctions: efficiently allocate goods and maximize
    seller revenue when there is asymmetric information about the
    value of the good
  \item Rich theory of auctions - results depends on:
    \begin{itemize}
    \item Nature of information \& values: independent or affliated
      (correlated)
    \item Single or multi-unit 
    \item Risk aversion
    \end{itemize}
  \item Close connection between theoretic models and estimable
    empirical models
  \item Empirical work on auctions:
    \begin{itemize}
    \item Rich environment allows us to estimate a lot (mainly
      bidders' values) under plausible assumptions
    \item Informs auction design \citep{klemperer2002} 
      \begin{itemize}
      \item Private or common values
      \item Independent or affiliated values
      \item Collusion
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Theory}

\begin{frame} \frametitle{Auction theory}
  \begin{itemize}
  \item Reference: \cite{klemperer2004} and references therein
  \item Very brief overview, will return to some of these issues as
    needed when we look at empirical papers 
  \item Begin with simplest case: single unit auction with independent
    private values and risk neutrality
  \item Generalize to allow
    \begin{itemize}
    \item Common values
    \item Affliated values
    \item Risk averse buyers/seller/both
    \item Multi-unit
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Values and information}
  \begin{itemize}
  \item Use notation of \cite{athey2007}
  \item Bidders $i = 1,...,n$
  \item Uncertain value of $u_i$
  \item Private information: scalar signal $x_i$
    \begin{itemize}
    \item Assume $\Er[u_i | x_i, x_{-i}]$ increasing in $x_i$
    \item Usually normalize $\Er[u_i|x_i] = x_i$
    \end{itemize}
  \item Private values $\equiv$ $\Er[u_i | x_1, ..., x_n] = \Er[u_i |
    x_i]$
  \item Common values $\equiv$ $\Er[u_i | x_1, ..., x_n]$ increasing
    in $x_j$ for all $i$, $j$
  \item Pure common values $\equiv$ $u_i = u_j$ $\forall$ $i,j$
  \item Independent private values (IPV) $\equiv$ $x_i \indep x_j$
  \item Affiliated private values $\sim$ non-independent private
    values 
    \begin{itemize}
    \item Affiliated means $f(x \vee y) f(x \wedge y) \geq f(x) f(y)$
    \item Affiliated implies non-negatively correlated
    \item Affiliated implies $\frac{f(x_i|x_j)}{f(x_i'|x_j)}$ for $x_i >
      x_i'$ is increasing in $x_j$ (monotone likelihood ratio) 
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{Common types of auctions}
  \begin{center}
    \begin{tabular}{c|cc}
      & First-price & Second-price \\
      \hline
      Open bid & & Ascending/English \\
      Sealed bid & Descending/Dutch & Vickrey 
    \end{tabular}
  \end{center}
  \begin{itemize}
  \item Equivalence of these forms of auctions is for IPV single-unit
    auctions 
  \item Bidding with risk neutral independent private values:
    \begin{itemize}
    \item Open second price $=$ ascending/English : bid $x_i$
    \item Sealed first price $=$ descending/Dutch: bids increasing in
      $x_i$ 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] 
  \frametitle{Revenue equivalence theorem}
  \begin{theorem}
    In a single unit auction with risk neutral bidders, assume
    identically distributed independent values with the distribution
    of $x_i$ strictly increasing and atomless. Then any auction
    mechanism in which
    \begin{enumerate}
    \item the bidder with the highest $x_i$ always wins, and
    \item\label{reserve} any bidder with the lowest-feasible signal expects zero
      surplus,
    \end{enumerate}
    yields the same expected revenue (and results in each bidder making
    the same expected payment as a function of her signal).
  \end{theorem}
  \begin{itemize}
  \item Vickrey (1961), Myerson (1981), Rogers and Samuelson (1981)
  \item Empirical implications:
    \begin{itemize}
    \item IPV is important -- Do we believe it? Is is testable?
    \item If we believe IPV, is \ref{reserve} satisfied? 
      \begin{itemize}
      \item Can meet \ref{reserve} by choosing optimal reserve price
      \item Optimal reserve price is independent of number of bidders 
      \end{itemize} 
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Relaxing assumptions}

\begin{frame}[allowframebreaks]
  \frametitle{Relaxing assumptions}
  \begin{itemize}
  \item Risk aversion:
    \begin{itemize}
    \item Risk averse buyers
      \begin{itemize}
      \item Revenue equivalence no longer holds
      \item Expected revenue of sealed first price $>$
        expected revenue of sealed second price
      \end{itemize}
    \item Risk averse seller: 
      \begin{itemize}
      \item Revenue equivalence holds
      \item Seller prefers sealed first price to sealed second price 
      \end{itemize}
    \end{itemize}
  \item Affiliated private values
    \begin{itemize}
    \item Optimal auction extracts full surplus, but is unlike any
      observed auction 
    \item Ascending $>$ sealed second $>$ sealed first
    \item Optimal reserve price decreases with number of bidders
    \end{itemize}
  \item Non-identically (non-symmetric) distributed $x_i$
    \begin{itemize}
    \item Revenue maximizing auction not necessarily allocatively
      efficient (i.e. bidder with highest $x_i$ might not win)
    \end{itemize}
  \item Common values
    \begin{itemize}
    \item ``Winner's curse'' winner likely to have signal that is
      higher than value
    \end{itemize}
  \item Endogenous entry of bidders
  \item Collusion
  \item Multi-unit: few general results on efficiency or revenue  
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Identification}

\begin{frame}[allowframebreaks]\frametitle{Identification}
  \begin{itemize}
  \item Reference: \cite{athey2007} 
  \item Identification: given the observed distribution of bids is
    there a unique distribution of information and values that could
    have generated it?
  \item Data:
    \begin{itemize}
    \item Bidders $\mathcal{N} = \{1,...,n\}$
    \item Bids $b_i$, $b=(b_1,...,b_n) \sim G_b(\cdot;\mathcal{N})$
    \item If do not observe identities of bidders or only observe
      winning bid, then observe some order statistics
      \[ b^{(k:n)} = \text{ $k$th order statistic from $n$
        observations} \]
      $b^{(k:n)}$ is the $k$th smallest bid
      \begin{itemize}
      \item $b^{(n:n)}$ is maximum bid
      \item In ascending auction $b^{(n:n)} = x^{(n-1:n)}$ is the winning bid
      \end{itemize}
      $G_b^{(k:n)}$ is the CDF of the $b^{(k:n)}$
    \end{itemize}
  \item Model: 
    \begin{itemize}
    \item Uncertain value of $u_i$ (will assume private values
      throughout this section)
    \item Private information: scalar signal $x_i$
    \item Normalization $\Er[u_i|x_i] = x_i$, with risk neutrality,
      wlog can assume $u_i = x_i$
    \item $x,u \sim F_{x,u}(\cdot; \mathcal{N})$ is common knowledge
    \item Assumptions (e.g.\ IPV) restrict $F_{x,u} \in \mathbb{F}$
    \item Bayesian Nash equilibrium with bidding strategies
      $\beta_i(x_i; \mathcal{N})$ gives mapping from model to data,
      $\gamma \in \Gamma$
    \end{itemize}
  \item Formal definition of identification: $(\mathbb{F},\Gamma)$ is
    identified if for all $F, \tilde{F} \in \mathbb{F}$ and
    $\gamma, \tilde{\gamma} \in \Gamma$, $\gamma(F) =
    \tilde{\gamma}(\tilde{F})$ implies $(F,\gamma) =
    (\tilde{F},\tilde{\gamma})$ 
  \end{itemize}
\end{frame}

\subsection{Ascending}

\begin{frame}[allowframebreaks]
  \frametitle{Ascending}
  \begin{itemize}
  \item Private values: bidders exit at $u_i$
  \item Winning bid $= u^{(n-1):n}$
  \item $u_i$ i.i.d. implies
    \[ F^{(k:n)}_u(s) = \frac{n!}{(n-k)!(k-1)!} \int_0^{F_{u}(s)}
    t^{k-1}(1-t)^{n-K} dt \]
    so observing only the number of bidders and the winning bid identifies $F_u$
  \item If $u_i$ not identically distributed (i.e.\ ``asymmetric
    independent private values''') then need to observe winning bid,
    identity of the winner, and set of bidders to identify $F_u$
  \item Without independence $F_u$ is not identified \citep{athey2002}
  \item Partial identification: \cite{haile2003}
    \begin{itemize}
    \item Assume:
      \begin{enumerate}
      \item Bidders do not bid more than they are willing to pay.
      \item Bidders do not allow an opponent to win at a price they are
        willing to beat.
      \end{enumerate}
    \item Can estimate bounds on $F_u$, see \cite{haile2003} 
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{First-price sealed-bid}

\begin{frame}[allowframebreaks]
  \frametitle{First price sealed bid}
  \begin{itemize}
  \item Key result due to \cite{gpv2000} (and refined by \cite{li2002},
    \cite{campo2003})  
  \item 
  \end{itemize}
  \begin{theorem}
    \begin{enumerate}
    \item Suppose all bids are observed in first-price sealed-bid
      auctions. Then the symmetric affiliated private values model is
      identified. 
    \item Suppose all bids and bidder identitites are observed in
      first-price sealed-bid auctions. Then the asymmetric affiliated
      private values model is identified.
    \end{enumerate}
  \end{theorem}  
  \begin{itemize}
  \item Proof: (with bids and identities observed)
    \begin{itemize}
    \item Bidder $i$'s problem:
      \[ \max_{b} \Er\left[u_i - b| x_i, \max_{j \in \mathcal{N}_{-i}} b_j
        \leq b \right] \Pr\left(\max_{j \in \mathcal{N}_{-i}} b_j
        \leq b | x_i \right) \]
    \item Value conditional on highest competing bid $= m_i$
      \[ \tilde{v}_i(x_i,m_i;\mathcal{N}) = \Er\left[u_i| x_i, \max_{j \in \mathcal{N}_{-i}} b_j
        = m_i \right] \]
    \item Note that $G_{m_i|b_i}(m_i|b_i;\mathcal{N}) = \Pr\left(\max_{j
          \neq i} b_j \leq m_i | b_i, \mathcal{N} \right)$ is observed
    \item Increasing strategies implies conditioning on $b_i$ is same
      as conditioning on $x_i$
    \item Rewrite bidder's problem:
      \[ 
      \max_{b} \int_{-\infty}^b \left[
        \tilde{v}_i(x_i,m_i;\mathcal{N}) - b \right]
      g_{m_i|b_i}(m_i|\beta_i(x_i;\mathcal{N});\mathcal{N}) dm_{i} \]
    \item First order condition:
      \[ 
      \tilde{v}_i\left(x_i,\beta_i(x_i;\mathcal{N});\mathcal{N}\right)
      = b_i +
      \frac{G_{m_i|b_i}(b_i|b_i;\mathcal{N})}{g_{m_i|b_i}(b_i|b_i;\mathcal{N})}
      \equiv \xi_i(b_i;\mathcal{N}) 
      \]
      RHS observable
    \item Private values implies 
      \begin{align*}
        \tilde{v}_i\left(x_i,\beta_i(x_i;\mathcal{N});\mathcal{N}\right)
        = & \Er\left[u_i| x_i, \max_{j \in \mathcal{N}_{-i}}
          \beta_j(x_j;\mathcal{N}) = \beta_i(x_i;\mathcal{N}) \right]
        \\
        = & \Er[u_i | x_i] \text{ (private values)} \\
        = & x_i  \text{ (normalization)} \\
        = & u_i  \text{ (risk neutrality)} 
      \end{align*}
    \item So $F_u$ identified from observed distribution of $\xi_i(b_i;\mathcal{N})$
    \end{itemize}
  \item Extensions:
    \begin{itemize}
    \item Incomplete bid data: often only observe $b^{(n:n)}$ results
      depend on independent or affiliated values, whether we observe
      identity of winner, see \cite{athey2007} section 3.3
    \item Unobserved heterogeneity: \cite{krasnokutskaya2011}
    \item Risk aversion: \cite{guerre2009}     
    \end{itemize}
  \end{itemize}
\end{frame}

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Estimation}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Data: auction with  $n = \underline{n} , ... , \overline{n}$
    bidders, each observed $T_n \to \infty$ times
  \item Focus on first-price sealed-bid auctions
  \item Parametric: parametrically specify $F_{u,x}(\cdot;\theta)$,
    estimate $\theta$ by MLE or GMM
    \begin{itemize}
    \item Examples: \cite{paarsch1992a}, 
      \cite{donald1993}, \cite{laffont1995} , \cite{paarsch1997}
    \item Challenge: equilibrium bid function only has closed form for
      some distributional families
    \end{itemize}
  \item 2-step semi parametric: estimate distribution of bids
    non-parametrically but make parametric assumption about
    distribution of values (or vice-versa)
    \begin{itemize}
    \item[Step 1]: use bids to estimate 
      \[ \hat{u}_i = b_i
      +\frac{\hat{G}_{m_i|b_i}(b_i|b_i;\mathcal{N})}{\hat{g}_{m_i|b_i}(b_i|b_i;\mathcal{N})} \]
    \item[Step 2]: estimate $F_u(\hat{u};\theta)$ by MLE or GMM
    \item Examples: \cite{jbp2003}, \cite{athey2011}, \cite{campo2011}
    \end{itemize}
  \item Non parametric: 
    \begin{itemize}
    \item[Step 1]:  use bids to estimate 
      \[ \hat{u}_i = b_i
      +\frac{\hat{G}_{m|b}(b_i|b_i;\mathcal{N})}{\hat{g}_{m|b}(b_i|b_i;\mathcal{N})} \]
      E.g.\ kernel estimates
      \begin{itemize}
      \item $\hat{G}_{m|b}(b|b;n) = \frac{1}{nT_n h_G}
        \sum_{i=1}^n \sum_{t=1}^T K\left(\frac{b - b_{it}}{h_G} \right) 1\{m_{it} < b,
        n_t = n \}$
      \item $\hat{g}_{m|b}(b|b;n) = \frac{1}{nT_n h_g^2}
        \sum_{i=1}^n \sum_{t=1}^T K\left(\frac{b - b_{it}}{h_g}, \frac{b-m_{it}}{h_g} \right)$
      \end{itemize}
    \item[Step 2]: Estimate $F_u$
      \begin{itemize}
      \item E.g.\ kernel estimate
        \[ \hat{f}_u(u_1,...,u_n) = \frac{1}{T_n h_f^n} \sum_{t=1}^T
        K_f\left( \frac{u_1 - \hat{u}_{1t}}{h_f}, ..., \frac{u_n -
            \hat{u}_{nt}}{h_f} \right) 1\{n_t = n\} \]
      \end{itemize}
    \item Data-driven choice of bandwidth is an open question
    \item Inference for $\hat{f}_u$ is tricky because $\hat{u}_{it}$
      non parametrically estimated
      \begin{itemize}
      \item Asymptotic distribution of $\hat{f}_u$ is not known 
      \item \cite{marmer2012} gives alternative non parametric
        estimator and proves asymptotic normality
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Applications}

\begin{frame}\frametitle{Applications: timber auctions}
  \begin{itemize}
  \item In many countries (U.S., Canada, France, Russia, etc) auctions
    are used to allocate logging rights on government owned land
  \item Variation across countries and over time in auction format
  \item Trade dispute between U.S.\ and Canada
  \item Foresty $\approx$ 30\% of BC exports and 2\% of BC GDP in 2009 
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Foresty in BC}
  \begin{center}
    \includegraphics[height=0.75\textheight,keepaspectratio=true]{foresty-exportShare}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Foresty in BC}
  \begin{center}
    \includegraphics[height=0.75\textheight,keepaspectratio=true]{foresty-gdpShare}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Questions}
  \begin{itemize}
  \item Modeling: 
    \begin{itemize}
    \item Common or private values (e.g.\ \cite{paarsch1992})
    \item Resale (e.g.\ \cite{haile2001})
    \item Risk aversion (e.g.\ \cite{lu2008})
    \end{itemize}
  \item Auction design:
    \begin{itemize}
    \item Reserve price (e.g.\ \cite{paarsch1997}, \cite{haile2003})
    \item Format (e.g.\ \cite{athey2011})
    \end{itemize}
  \item Collusion: 
    \begin{itemize}
    \item E.g.\ \cite{baldwin1997}, \cite{list2007}, \cite{price2008}
    \end{itemize}    
  \end{itemize}
\end{frame}

\subsection{\cite{paarsch1997}}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{paarsch1997}}
  \begin{itemize}
  \item BC timber auctions 1984-1987
    \begin{itemize}
    \item Small Business Forest Enterprise Program (SBFEP) auctions
      rights to timber on Crown land to independent loggers
    \item Combination of ascending (English) and first-price
      sealed-bid auctions (choice between seems to be random)
    \item Only uses ascending auctions in estimation
    \end{itemize}
  \item Main question: what is the optimal reserve price?
  \end{itemize}
\end{frame}
\begin{frame}%[allowframebreaks]
  \frametitle{\cite{paarsch1997} - method}
  \begin{itemize}
  \item Method: 
    \begin{itemize}
    \item Estimate IPV model of ascending timber auction
    \item Use model estimates and assumptions about Crown valuation of
      timber to calculate optimal reserve price
    \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}%[allowframebreaks]
  \frametitle{\cite{paarsch1997} - issues}
  \begin{itemize}
  \item Issues:
    \begin{itemize}
    \item Why is IPV a good assumption?
      \begin{itemize}
      \item Forest service provides information about common component
        of value: volume and type of timber, terrain, roads
      \item Private information from inspection, idiosyncratic costs
        (labor, capital, transportation)        
      \item Knowledge of others' bids unlikely to provide any
        information about each private value
      \end{itemize}
    \item Auctions have a reserve price, so observed bids are a
      selected sample of valuations
      \begin{itemize}
      \item Parametrically specify distributions and estimate by MLE         
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{paarsch1997} - model}
  \begin{itemize}
  \item Valuations:
    \[ v = \left((\sum_{j=1}^k p_j \lambda_j ) - a\right)q \]
  \item $j$ indexes species, prices $p_j$, portions $\lambda_j$
  \item Harvesting cost $a \sim F(a)$
    \[ a = \gamma_{q1} + \gamma_{q2}q + \gamma_{q3}q^2 +
      \gamma_{q0} q^{-1} + \gamma_{qd} d \]
    with $F_\gamma(c) = 1-\exp(-\delta_1 c^{\delta_2})$      
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{p-tab4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{p-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{p-fig3}
\end{frame}


\begin{frame}%[allowframebreaks]
  \frametitle{\cite{paarsch1997} - results}
  \begin{itemize}
  \item Results: 
    \begin{itemize}
    \item Optimal reserve price $>$ observed reserve price
    \item \$2.39 per cubic metre $=$ observed reserve price
    \item \$8.59 per cubic metre $=$ optimal reserve if government
      value of timber is 0 
    \item \$10.43 per cubic metre $=$ optimal reserve if government
      value of timber is observed reserve price
    \end{itemize}
  \end{itemize}
\end{frame}

%\subsection{\cite{haile2003}}
%\begin{frame}\frametitle{\cite{haile2003}}
  
%\end{frame}

\subsection{\cite{athey2011}}
\begin{frame}%[allowframebreaks] 
  \frametitle{\cite{athey2011}}
  \begin{itemize}
  \item Compares open and sealed bid timber auctions
    \begin{itemize}
    \item Revenue, welfare, collusion
    \end{itemize}
  \item Data: 1982-1990 Idaho-Montana border and California
  \item Basic data facts:
    \begin{itemize}
    \item Sealed bids induce more participation by small firms (loggers)
    \item Large firms (mills) equally likely to enter either 
    \item Sealed bid auctions more likely to be won by loggers
    \item Winning bids 10\% higher in sealed bid auctions
    \end{itemize}
  \item Construct model of auction participation to explain basic
    findings
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks] 
  \frametitle{\cite{athey2011} - model}
  \begin{itemize}
  \item Model features:
    \begin{itemize}
    \item Participation cost of acquiring information
    \item Heterogeneous value distributions
    \item Possible collusion
    \end{itemize}
  \item Model properties:
    \begin{itemize}
    \item Sealed bid auctions favor weaker bidders
    \item No clear implication for revenue (depends on model
      primitives) 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks] 
  \frametitle{\cite{athey2011} - estimation}
  \begin{itemize}
  \item Estimation of model:
    \begin{itemize}
    \item Use sealed bid auctions and \cite{gpv2000} method
    \item Take estimates and use to predict open auctions (in model
      with \& without collusion) and compare
      with data
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks] 
  \frametitle{\cite{athey2011} - results}
  \begin{itemize}
  \item Some evidence of collusion by mills in Idaho-Montana border
    region (predictions of competitive model do not fit as well)
  \item Welfare calculations:
    \begin{itemize}
    \item Sealed bid auctions raise more revenue and distort the
      allocation away from efficiency and in favor of loggers, but
      the effects are small (less than 1\%)
    \item Mild degree of cooperative bidding by the mills at open
      auctions—the behavioral assumption most consistent with the
      observed outcomes in the Northern forests—results in much more
      substantial revenue differences (on the order of 5–10\%)
    \end{itemize}
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\cite{haile2001}}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{haile2001}}
  \begin{itemize}
  \item Auction followed by resale opportunity
    \begin{itemize}
    \item Seller effect: auction winner has option to sell the
      contract, so value of winning higher
    \item Buyer effect: auction loser could buy later, so value of
      winning lower
    \item More bidders $\Rightarrow$ more competition among buyers in
      resale market $\Rightarrow$ higher seller effect, lower buyer
      effect 
    \item Private use value, but endogenous common willingness to pay
      from resale value
    \end{itemize}    
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{haile2001} - data}
  \begin{itemize}
  \item Context \& data: U.S.\ Forest Service timber auctions
    \begin{itemize}
    \item English auctions
    \item Bid on price per-unit, pay based on amount harvested
    \item Resale $\approx$ subcontracting of harvesting and/or
      milling, some transfers
    \item 1981 legislative changes affecting resale
      \begin{itemize}
      \item Pre-1981: on average 55 months to harvests 
      \item Post-1981: 33 months (so less motive to subcontract),
        transfers mostly forbidden 
      \item Data 1974-1989 
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{haile2001} - model}
  \begin{itemize}
  \item Theoretic Model: 
    \begin{itemize}
    \item[1] Initial auction by Forest Service     
    \item[2] All bidders learn use values, winner re-auctions 
    \item Willingness to pay in first stage $\geq$ use value because
      of resale option
    \item Resale option $\Rightarrow$ other players' bids in first
      stage affect willingness to pay
    \item Key result: each bidders' willingness to pay in first stage
      increases with the number of bidders
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{haile2001} - econometric specification}
  \begin{itemize}
  \item Econometric specification:
    \begin{itemize}
    \item Oral English auction, so only observe winning bid $=$ 2nd
      highest willingness to pay
      \begin{align*}
        \log b_{t}^{(n:n)} = & 
        \underbrace{w_t \theta +
          h_t}_{\text{common use value}} +
        \underbrace{\Omega_{2t}}_{\text{info from other bids}} +
        \underbrace{\epsilon_{t}^{(n-1:n)}}_{\text{private
            value}} 
      \end{align*}
    \item Instruments $z_t$ independent of $h, \Omega, \epsilon$, so 
      \begin{align*}
        \Er[  \log b_{t}^{(n:n)} - w_t \theta | z_t ] =
        \Er[\epsilon^{(n-1:n)}_t | n_t ] 
      \end{align*}
    \item Assume $\epsilon \sim N(0,\sigma^2_t)$
    \item $w_t  =$ auction characteristics, number of bidders
    \item $z_t = $ auction characteristics, number of nearby
    \item Estimate by GMM
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}%[allowframebreaks]
  \frametitle{\cite{haile2001} - results}
  \begin{itemize}
  \item Results:
    \begin{itemize}
    \item Estimate for full sample, separately for pre \& post 1981
    \item Significant difference in $\frac{\partial v}{\partial n}$ pre and post
      \begin{itemize}
      \item $\frac{\partial v}{\partial n} \approx 50$ before 1981
      \item $\frac{\partial v}{\partial n} \approx 20$ after 1981
      \end{itemize}
    \item Interpretation: resale important determinant of bids
    \item Robust to: assumed distribution of $\epsilon$, information
      in others' bids, region
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Detecting collusion}

\begin{frame}\frametitle{Detecting collusion}
  \begin{itemize}    
  \item \cite{baldwin1997}
  \item \cite{list2007}
  \item \cite{price2008}
  \end{itemize}
\end{frame}

\subsubsection{baldwin1997}

\begin{frame}\frametitle{\cite{baldwin1997}}
  \begin{itemize}
  \item Data: ascending auctions in U.S.\ Pacific NW 1975-1981 
    \begin{itemize}
    \item Allegations of collusion
    \end{itemize}
  \item Question: was there collusion?
    \begin{itemize}
    \item Collusion $\Rightarrow$ low prices
    \item High supply (many auctions) $\Rightarrow$ low prices
    \end{itemize}
  \item Method: estimate model with and without collusion and with and
    without supply 
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{baldwin1997}}
  \begin{itemize}
  \item Results: evidence of collusion
    \begin{itemize}
    \item Adding collusion or supply increase likelihood substantially
    \item After adding collusion, allowing supply effects as well does
      not increase likelihood
    \item Loss in revenue from collusion: 7.9\%
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{\cite{list2007}}
\begin{frame}\frametitle{\cite{list2007}}
  \begin{itemize}
  \item Question: is there collusion in BC timber auctions?
  \item Motivation: auctions are central to current resolution to
    US-Canada Softwood Lumber Dispute 
    \begin{itemize}
    \item Ongoing dispute since 1820s
      \begin{itemize}
      \item Canada exports a lot of lumber to U.S.
      \item Canadian forests 94\% publicly owned, U.S.\ $\approx$
        27\%
      \end{itemize}
    \item First-price sealed-bid auctions for some plots, use to estimate
      \[ \text{winning bid} = x\beta + e \]
    \item Non-auctioned plots: 
      \[ \text{price} = x\hat{\beta} \]
    \item Collusion in auctions would distort prices in many plots
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{list2007}}
  \begin{itemize}
  \item Data: 1996-2000 Small Business Forest Enterprise Program
    (SBFEP) auctions in BC
    \begin{itemize}
    \item First-price sealed-bid
    \item MoF announces upset rate (reserve price) and estimated
      volume of timber (NCV)
    \item Bidders evaluate plot, submit bids
    \item Identities and bids of all bidders revealed
    \end{itemize}    
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{list2007}}
  \begin{itemize}
  \item Model:
    \begin{itemize}
    \item Assume IPV $\eta$ conditional on characteristics $Z$
    \item Bidding function $b_i = \phi(Z_i,\eta_i)$
    \item Collusion (vs no-collusion) implies: 
      \begin{enumerate}
      \item Cartel members make systematically lower bidders and have
        different bidding function than non-members 
      \item Cartel member bids likely correlated conditional on $Z$
      \end{enumerate}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{list2007}}
  \begin{itemize}
  \item Empirical approach: infer treatment assignment given outcomes
    \begin{itemize}
    \item Collusion indicator $D_{it}$
    \item Bids:
      \[ y_{it} = D_{it}[ X_{it} \beta_1 ] + (1-D_{it})[X_{it} \beta_0] +
      \epsilon_{it} \]
    \item Collusion implies:
      \begin{itemize}
      \item $\beta_1 \neq \beta_0$, and $X_{it} \beta_1 \leq X_{it}
        \beta_0$
      \item $\mathrm{Cov}(\epsilon_{it},\epsilon_{jt} | D_i = 0) = 0$, but
        $\mathrm{Cov}(\epsilon_{it},\epsilon_{jt} | D_i = 1, D_j = 1) \neq 0$ 
      \end{itemize}
    \end{itemize}
  \item Problem: $D_{it}$ is unobserved
    \begin{itemize}
    \item Assume constant over time, $D_{it} = D_i$
    \item Use proxy $\tilde{D}_{it}$ (geographic proximity of bidders)
    \end{itemize}
  \end{itemize}
\end{frame}  

\begin{frame}\frametitle{\cite{list2007} - results}
  \begin{itemize}
  \item Results:
    \begin{itemize}
    \item Fixed effects: evidence of collusion
    \item Proxy: mixed evidence (not always expected sign, limited
      statistical significance)
    \end{itemize}
  \item \cite{price2008} focuses on geographic proximity, finds
    ``Firms located within the same town as one another submit bids
    that are on average 12.77 percent below predicted levels. As the
    distance between firms increases, the difference between estimated
    and predicted bids declines. In fact, there is no discernable
    difference in the bids submitted by suspected cartel pairs located
    21–100 miles from each other and the control group of bids
    submitted in auctions that are assumed competitive.''
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Comparison}
  \begin{itemize}
  \item \cite{baldwin1997} 
    \begin{itemize}
    \item Highly structured model - strong behavioral and parametric
      assumptions
    \item More precise results
    \item Estimates of magnitude 
    \end{itemize}
  \item \cite{list2007}  
    \begin{itemize}
    \item Less structured model - weaker behavioral assumptions, no
      distributional assumptions, so perhaps more credible
    \item Less precise results
    \item No estimate of magnitude
    \end{itemize}
  \end{itemize}
\end{frame}

\subsubsection{\cite{schurter2017}}

\begin{frame}\frametitle{\cite{schurter2017}}
  ``Identification and Inference in First-Price Auctions
  with Collusion''
  \begin{itemize}
  \item Main idea : collusive bidders' and competitives bidders'
    strategies depend on competitiveness of auction in different ways
    \begin{itemize}
    \item Under no collusion, GPV estimates of bidder valuations
      should be independent of exogenous shifts in auction
      competitiveness 
      \begin{itemize}
      \item Number of bidders, reserve price, etc
      \end{itemize}
    \item With collusion, GPV estimates of valuations will depend on
      auction competitiveness
    \end{itemize}
  \item Construct test to identify which bidders are colluding
  \item Given set of collusive bidders, modification of GPV identifies
    distribution of valuations
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Setting and data}
  \begin{itemize}
  \item BC SBFEP 1996-2000
  \item Same as \cite{list2007}
  \item Instrument : reserve price
    \begin{itemize}
    \item 1999 : change from reserve prices set based on appraised
      price of timber, a Ministry revenue target, and silviculture \&
      development costs to reserve prices equal to 70\% of appraised value
    \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\subsection{\cite{haile2003}}

\begin{frame}\frametitle{Inference with an incomplete model of English
    auctions --- \cite{haile2003}}
  \begin{itemize}
  \item Motivation:
    \begin{itemize}
    \item Idealized theoretical English auction: bid rises continuously,
      bidders hold down button until bid exceeds value
    \item Actual English auctions: bidders call out bids 
    \end{itemize}
  \item Assume:
    \begin{enumerate}
    \item Bidders do not bid more than they are willing to pay.
    \item Bidders do not allow an opponent to win at a price they are
      willing to beat.
    \end{enumerate}
  \item Identifies bounds on CDF of values
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Bidders $i \in \{1, ..., M\}$
  \item Value $V_i$ independently $\sim F_0$, support $[\underline{v}, \overline{v}]$
  \item Seller value $v_0$
  \item Minimum bid increment $\Delta$
  \item Reserve price $r$
  \item $N$ of $M$ bidders participate
  \item Value distribution conditional on participating $(v \geq r)$
    \[ F(v) = \frac{F_0(v) - F_0(r)}{1 - F_0(r)} \]
  \item Bids $B_j$, order statistics $B_{j:n}$ with CDF $G_{j:n}$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Upper bound}
  \begin{columns}[T]
    \begin{column}{.6\textwidth}
      \begin{itemize}
      \item Assumption 1, $b_i \leq v_i$ , implies
        \begin{itemize}
        \item $G(v) \geq F(v)$
        \item $b_{i:n} \leq v_{i:n}$
        \item $G_{i:n}(v) \geq F_{i:n}(v)$
        \end{itemize}
      \item Identical distributions implies $F(v) = \phi(F_{i:n}(v); i, n)$
      \item Upper bound
        \[ F(v) \leq F_U(v) \equiv \min_{n, i} \phi(G_{i:n}(v): i,n) \]
      \end{itemize}
    \end{column}
    \begin{column}{.4\textwidth}
      \includegraphics[width=\textwidth]{ht-fig1}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}\frametitle{Lower bound}
  \begin{itemize}
  \item Assumption 2: $v_i \leq v_i^u \equiv \begin{cases}
      \bar{v} &  b_i = b_{n:n} \\
      b_{n:n} + \Delta & b_i < b_{n:n}
    \end{cases}$
  \item Implies $v_{n-1:n} \leq b_{n:n} + \Delta$, i.e. $F_{n-1:n}(v)
    \leq G^\Delta_{n:n}(v)$
  \item Lower bound:
    \[ F(v) \geq F_L(v) \equiv \max_n \phi(G^\Delta_{n:n}(v): n-1,
    n) \]
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Observe $T$ auctions, subscript by $t$
  \item Plug in empirical CDFs
    \begin{align*}
      \hat{G}_{i:n}(v) = & \frac{1}{T_n} \sum_{t=1}^T
      \mathbf{1}[n_t=n,b_{i:n} \leq v]  \\
      \hat{G}^\Delta_{n:n}(v) = & \frac{1}{T_n} \sum_{t=1}^T
      \mathbf{1}[n_t=n,b_{n:n} +\Delta_t \leq v]  \\
      \hat{F}_U(v) = & \min_{n, i} \phi(\hat{G}_{i:n}(v): i,n) \\
      \hat{F}_L(v) = & \max_n \phi(\hat{G}^\Delta_{n:n}(v): n-1, n)
    \end{align*}
  \item Uniformly consistent as $T_n \to \infty$, $T_n / T \to
    \lambda_n \in (0,1)$
  \item Large finite sample bias from min \& max, so smooth instead
    \[ \mu(y_1, ..., y_J;\rho_T) = \sum_{j=1} y_j \frac{e^{y_j
        \rho_T}}{\sum_{k=1}^J e^{y_k \rho_T}} \]
    $\min = \lim_{\rho_T \to -\infty}$ and $\max = \lim_{\rho_T \to
      \infty}$
    \begin{itemize}
    \item $|\rho_T|/\log\sqrt{T} \to \infty$ for consistency
    \item Inference is tricky: see \cite{hirano2012} and
      \cite{clr2013}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Optimal reserve price}
  \begin{itemize}
  \item Assume $(p-v_0) [1 - F_0(p)]$ is strictly pseudo-concave in $p$
  \item Optimal reserve price solves (assuming observed $r < p^*$
    \[ p^* \in \argmax_p (p - v_0) [1-F(p)] \]
  \item Let
    \begin{align*}
      \pi_1(p) = &(p-v_0) [1 - F_U(p)] \\
      \pi_2(p) = &(p-v_0) [1 - F_L(p)] 
    \end{align*}
    note:
    \[ \pi_1(p) \leq (p - v_0) [1-F(p)] \leq \pi_2(p) \]
  \item Bounds:
    \begin{align*}
      p_L \equiv & \sup\{p < \argmax \pi_1(\tilde{p}) : \pi_2(p) <
      \sup \pi_1(\tilde{p}) \} \\
      p_U \equiv & \inf \{p > \argmax \pi_1(\tilde{p}) : \pi_2(p) <
      \sup \pi_1(\tilde{p}) \} \\
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=0.85\textwidth]{ht-fig2}
\end{frame}
\begin{frame}
  \includegraphics[width=0.85\textwidth]{ht-fig3}
\end{frame}

\begin{frame}\frametitle{Simulations}
  \begin{itemize}
  \item Bidders randomly selected, either drop out or raise bid by
    $\Delta$
  \item With probability $\lambda$ bid uniformly between $b+\Delta$
    and valuation
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{ht-fig4}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{ht-fig5}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{ht-fig6}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{ht-tab1}
\end{frame}


\begin{frame}\frametitle{Forest Service timber auctions}
  \begin{itemize}
  \item Data: Washington \& Oregon, 1982-1990
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{ht-tab3}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{ht-fig10}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{ht-tab4}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{ht-tab5}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\begin{frame}[allowframebreaks]
\nocite{laffont1996}
\nocite{klemperer1999}
\nocite{hendricks2000}
\nocite{gpv2000}
\nocite{li2002}
\nocite{campo2011}
\nocite{athey2001}
\nocite{athey2002}
\nocite{athey2006}
\nocite{athey2011}
\nocite{agarwal2009}
\nocite{larsen2013}
\nocite{pesendorfer2000}
\nocite{psd2008}
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
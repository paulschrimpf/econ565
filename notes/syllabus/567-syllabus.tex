\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=.9in,right=.9in,top=.7in,bottom=.7in]{geometry}
%\usepackage[left=1.1in,right=1.1in,top=.7in,bottom=.7in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage[small,compact]{titlesec}
\usepackage{color}
\usepackage{ulem}
%\usepackage{harvard}
%\renewcommand{\cite}{\citeasnoun}
\usepackage{natbib}
\renewcommand{\cite}{\citet}

%\usepackage[screen,nopanel,gray]{pdfscreen}
%\screensize{4in}{5.33in}
%\margins{0.75in}{0.75in}{0.75in}{0.75in}


%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{newpxmath}
\usepackage{newpxtext}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\newcommand{\myTable}[1]{\begin{table}[b]\caption{#1}
    \begin{minipage}{\linewidth} \begin{center}}
\newcommand{\myTableEnd}{\end{center}\end{minipage}\end{table}}

\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,}
\def\inprob{\,{\inprobHIGH}\,}
\def\indist{\,{\buildrel d \over \rightarrow}\,}

\newtheorem{theorem}{Theorem}

%\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}

%\fancyhead[LE,LO]{\thepage}
%\fancyhead[CE,CO]{\rightmark}
%\fancyfoot[C]{}

\title{Economics 567 } %-- Market Structure and Business Behaviour}
\date{2025}
\author{Paul Schrimpf \\ \href{mailto:schrimp@mail.ubc.ca}{schrimpf@mail.ubc.ca}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    {\large \textbf{\@title} \par \normalsize \@date \par \@author}%
  \end{center}%
  \par}
\makeatother



\begin{document}

\maketitle

This course is about empirical industrial organization. It focuses on
the use of structural econometric techniques to study specific markets. Notes
and assignments will be posted on the main course web page at
\url{http://faculty.arts.ubc.ca/pschrimpf/567/index.html}. UBC Canvas \url{https://canvas.ubc.ca/}
will be also be used to post grades and share material that should not
be publicly posted on the web. Class will be held in person.

\section{Schedule}
\begin{tabular}{l c c l}
  \hline
  & \textbf{Day(s)} & \textbf{Time} & \textbf{Location} \\
  Lecture & Tuesday \& Thursday & 9:30am-11:00am  & Buchanan B218 \\
  Office hours & Tuesday \& Thursday 11:00am-11:30am & Iona 112 & \\
  \hline
\end{tabular} \\
Office hours are subject to change. Any changes in office hours will
be posted on the course web page. If you cannot come to my office
hours, feel free to drop by anytime or email me to schedule an
appointment.

\section{Course Work}

Course work will consist of a presentation, problem
sets, and a research proposal. Required
reading should be completed before each class. Participation in
class discussion is expected, especially during student
presentations.

We will use Canvas for online discussion. Whenever
possible, please ask questions in the Canvas discussion instead of
through email or a Canvas message.

\subsection{Presentation}
Each student will present a paper. The paper should be related to or
on the reading list. Please email me with your intended paper to
present at least one week before your presentation.
Presentations will occur throughout the
term. Each presentation should last for 30 minutes including
questions. The presentation should:
\begin{enumerate}
\item Summarize the paper
\item Identify the paper's contributions
\item Discuss weaknesses of the paper
\item Make suggestions for further research
\end{enumerate}


\subsection{Problem Sets}
There will three to five problem sets that will each involve
reproducing some results from a paper. Programming will be required
for all problem sets. The problem sets will include instructions and
example code using Julia. You may use another programming language if
you wish, but I expect that will be more difficult. I encourage you to
work together on problem sets. Sharing code is acceptable, as long you
clearly indicate with whom you worked. Using AI for coding assistance
is allowed and encouraged, but you are responsible for the correctness
of all code. Please disclose whatever AI tools you use.

\subsection{Research Proposal}

A research proposal will be due on April 23rd. I encourage sending a
rough draft of your proposal by April 9th. I will give feedback on
rough drafts within three business days. Your proposal should clearly state a
research question. It should include a related literature review. It
should also include a some of the following: institutional background,
data, and empirical approach.

\subsection{Grading}
The grade for this course will be 20\% presentation, 45\% problem sets
/ replication, 30\% research proposal, and 5\%
participation. Participation includes attending lecture, contributing
to discussion (both in class and online), and attending office hours.

\section{Course Outline}

The topics of the course and some related readings are listed
below. The slides for each topic contain the most up to date
references. I will announce required readings at the end of each
lecture.  The order of topics may be changed depending on class
interest. We are unlikely to cover all of the topics.  The books
\cite{Aguirregabiria2012} and \cite{hortacsu2023} and the {\slshape{Handbook}} chapters by
\cite{reiss2007structural}, \cite{ackerberg2007econometric},
\cite{berry2021},
\cite{gandhi2021}, \cite{aguirregabiria2021}, and \cite{einav2021}
provide
good overviews of many of these topics. The references therein are
good sources of further reading.
\begin{enumerate}
\item Introduction
  \begin{itemize}
  \item Recommended: \cite{Aguirregabiria2012} chapter 1,
    \cite{hortacsu2023} chapter 1
  \item Suggested: \cite{einav2010empirical}, \cite{reiss2007structural}
  \end{itemize}
\item Estimation of production functions
  \begin{itemize}
  \item Recommended: \cite{ackerberg2007econometric} section 2,
    \cite{olley1996dynamics},
  \item Suggested: \cite{levinsohn2003},
    \cite{ackerberg2006structural}, \cite{Aguirregabiria2012} chapter 2,
    \cite{gandhi2009identifying}, \cite{wooldridge2009},
    \cite{grieco2017}, \cite{deloecker2021}
  \end{itemize}
\item Static demand and supply of differentiated products
  \begin{itemize}
  \item Recommended: \cite{berry2021},\cite{gandhi2021}, \cite{ackerberg2007econometric} section 1, \cite{berry1995}
  \item Suggested: \cite{berry1994}, \cite{Aguirregabiria2012} chapter
    3, \cite{nevo2000practitioner}, \cite{nevo2001}
  \end{itemize}
\item Market entry
  \begin{itemize}
  \item Recommended: \cite{Aguirregabiria2012} chapter 5, \cite{br1991},
  \item Suggested: \cite{br1990}, \cite{seim2006empirical},
    \cite{sweeting2009}, \cite{jia2008happens}
  \end{itemize}
\item Single-agent dynamic structural models
  \begin{itemize}
  \item Recommended: \cite{rust_chapter_1994}
  \item Suggested: \cite{Aguirregabiria2012} chapters 7, \cite{rust1987},
    \cite{hotz1993}, \cite{timmins2002}, \cite{am2002}
  \end{itemize}
\item Dynamic oligopoly
  \begin{itemize}
  \item Recommended: \cite{aguirregabiria2021}, \cite{ackerberg2007econometric} section 3, \cite{am2010}
  \item Suggested: \cite{Aguirregabiria2012} chapters 6, 9,
    \cite{magnacThesmar2002}, \cite{psd2008}, \cite{bbl2007},
    \cite{pob2007}, \cite{am2007}, \cite{bajari_nonparametric_2009},
    \cite{ryan2012costs}
  \end{itemize}
\item Auctions \cite{hortascu2021}
\item Contracting and asymmetric information
\item Search and matching
\item Networks
\end{enumerate}

\clearpage
\bibliographystyle{jpe}
\bibliography{../565}

\end{document}

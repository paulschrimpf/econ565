\input{../slideHeader}

\usepackage[pdf]{graphviz}
\usepackage{annotate-equations}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Vertical Relationships}
\author{Paul Schrimpf}
\institute{UBC \\ Vancouver School of Economics \\ 567}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}\frametitle{Vertical Relationships}
  \begin{itemize}
  \item Firm to firm transactions
  \item Overview: \cite{lee2021}
  \item Insurers and hospitals: \cite{ghili2022}, \cite{ho2019},
    \cite{ho2017a}, \cite{prager2020}
  \item Suppliers and assemblers: \cite{fox2018t}
  \item Retailers and wholesalers: \cite{hristakeva2022}
  \item Foundations for Nash-in-Nash model: \cite{collard2019} and
    Horn \& Wolinksy (198?)
  \end{itemize}
\end{frame}


\section{\cite{ho2017a}}
\begin{frame} \frametitle{\cite{ho2017a}}
``Insurer Competition in Health Care Markets''

\begin{itemize}
\item Employer sponsored private health insurance in US (60\% of
  non-elderly)
\item Model premium and hospital prices with Nash bargaining between
  employer and insurer and insurer and hospital
\item Bargaining leads to novel implications for effect of removal of
  an insurer
\end{itemize}
\end{frame}

\begin{frame}
  % run `dot -T pdf -o eih.pdf eih.dot` to create the graph (or call
  % latex with --shell-escape)
  \digraph[width=\textwidth,height=\textheight,keepaspectratio=true]{eih}{
  e1 -> k;
  c -> k;
  c -> bs;
  c -> bc;
  e3 -> bs;
  e4 -> bc;
  e5 -> bc;
  k -> hk;
  bs -> ha;
  bs -> hb;
  bs -> hc;
  bc -> ha;
  bc -> hb;
  e1 [label=<Employer 1>];
  c [label=<CalPERS>];
  e3 [label=<Employer 2>];
  e4 [label=<Employer 3>];
  e5 [label=<Employer 4>];
  k [label=<Kaiser>];
  bs [label=<Blue Shield>];
  bc [label=<Blue Cross>];
  ha [label=<Hospital A>];
  hb [label=<Hospital B>];
  hc [label=<Hospital C>];
  hk [label=<Kaiser Hospitals>];
}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-fig1}
\end{frame}

\subsection{Data}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab2}
\end{frame}


\subsection{Model}

\begin{frame}\frametitle{Notation}
  \begin{itemize}
  \item $\mathcal{M} = \{$Kaiser, Blue Cross, Blue Shield $\}$ set of
    insurers offered by CalPERS
  \item insurance premiums $\phi_j$
  \item $\mathcal{G} = $ hospitals covered by each insurer
  \item price of hospital $i$ for insurer $j$ $p_{ij}$
  \item Insurance demand $D_j(\mathcal{G},\phi)$
  \item Hospital demand $D_{ij}^H(\mathcal{G},\phi)$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item[1a.] Employer and insurers bargain over $\phi$
  \item[1b.] Insurers and hospitals bargain over $p$
  \item[2.] Households choose insurance plans $\rightarrow$ $D_j(\mathcal{G},\phi)$
  \item[3.] Sick individuals choose hospitals $\rightarrow$ $D_{ij}^H(\mathcal{G},\phi)$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Payoffs}
  \begin{itemize}
  \item MCO/insurer $j$:
    $$
    \pi_j^M(\mathcal{G},p,\phi) = D_j(\cdot)(\phi_j - \eta_j) -
    \sum_{h \in \mathcal{G}_j^M} D^H_{hj}(\cdot) p_{hj}
    $$
  \item Hospital $i$:
    $$
    \pi_i^H(\mathcal{G},p,\phi) = \sum_{n \in \mathcal{G}^H_i}
    D_{in}^H(\cdot)(p_{in} - c_i)
    $$
  \item Employee welfare:
    $$
    W(\mathcal{M}, \phi)
    $$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Premium Bargaining}
  \begin{itemize}
  \item Nash bargaining
  \item $\tau^\phi =$ bargaining weight of insurer for premiums
  \begin{align*}
    \phi_j = \argmax_{\varphi} & \pi_j^M(\mathcal{G},p,(\varphi,
                                 \phi_{-j}))^{\tau^\phi} \times \\
    & \times \left[\underbrace{ W(\mathcal{M},(\varphi, \phi_{-j})) -
      W(\mathcal{M} \setminus j, \phi_{-j})}_{GFT_j^E(\cdot)}\right]^{(1-\tau^\phi)}
  \end{align*}
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Premium First Order Condition}
  \includegraphics[width=\textwidth]{hl17-eq5}
\end{frame}


\begin{frame}\frametitle{Hospital Price Bargaining}
  \begin{align*}
    p_{ij} = \argmax_p & \left[\pi_j^M(\mathcal{G},(p,p_{-ij}),\phi) -
                         \pi_j^M(\mathcal{G}\setminus ij,p_{-ij},\phi) \right]^{\tau_j} \\
                       & \times \left[
                         \pi_i^H(\mathcal{G},(p,p_{-ij}),\phi) -
                         \pi_i^H(\mathcal{G}\setminus ij,p_{-ij},\phi)
                         \right]^{1-\tau_j}
  \end{align*}
  \begin{itemize}
  \item Equilibrium effect of insurer competition on negotiated prices
    \& premiums is complicated and cannot be signed a priori
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Hospital Price First Order Condition}
  \includegraphics[width=\textwidth]{hl17-eq6}
\end{frame}


\begin{frame}\frametitle{Hospital Demand \& Consumer Surplus}
  \begin{itemize}
  \item Discrete choice model of hospitals
    \begin{equation*}
      u_{k,i,l,m}^H = \tikzmarknode{di}{\delta_i} + \tikzmarknode{zi}{z_i} \tikzmarknode{vk}{\nu_{k,l}}
      \beta^z + \tikzmarknode{dik}{d_{i,k}} \beta^d_m + \tikzmarknode{ei}{\epsilon_{k,i,l,m}^H }
    \end{equation*}
    \annotate[yshift=-0.5em]{below,left}{di}{hospital}
    \annotate[yshift=0.5em]{above,left}{zi}{hospital characteristics}
    \annotate[yshift=0.5em]{above,right}{vk}{person $k$ \&  diagnosis
      $l$}
    \annotate[yshift=-0.5em]{below,left}{dik}{distance}
    \annotate[yshift=1em]{below,right}{ei}{T1EV}
  \item Willingness to pay:
    {\footnotesize{
    \begin{equation*}
      WTP_{k,jm}(\mathcal{G}) = \eqnmark[red]{pa}{\gamma^a_{\kappa(k)}} \sum_{l \in
          \mathcal{L}} \eqnmark[blue]{pd}{\gamma_{\kappa(k),l}} \eqnmarkbox[purple]{EU}{
      \log \left(\sum_{h \in \mathcal{G}} \exp(\delta_h + z_h
        \nu_{k,l}\beta^z + d_{h,k} \beta^d_m) \right)}
    \end{equation*}
    \annotate[yshift=0.5em]{above,left}{pa}{P(admission)}
    \annotate[yshift=1em]{above}{pd}{P(diagnosis|admit)}
    \annotate[yshift=-0.5em]{below}{EU}{$EU(\mathcal{G})$}
    }}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Insurance Plan Demand}
  \begin{itemize}
  \item Family $f$ chooses among plans $j$ offered in market $m$:
  \item
  \end{itemize}
  {\footnotesize{
    \begin{equation*}
      u^M_{f,j,m} = \tikzmarknode{djm}{\delta_{j,m}} +
      \alpha_f^\phi \eqnmark[purple]{prem}{(0.2 \phi_j
        \Phi_{\lambda(f)})} + \eqnmark[blue]{kappa}{\sum_{\kappa}
        \alpha_{\kappa}^W}
      \eqnmark[red]{fam}{\sum_{k \in f: \kappa(k) = \kappa}}
        WTP_{k,j,m}
      + \epsilon^M_{f,j,m}
    \end{equation*}
    \annotate[yshift=0.5em]{above,left}{djm}{insurer $\times$ market}
    \annotate[yshift=0.5em]{above}{prem}{premium paid by household}
    \annotate[yshift=-0.5em]{below,left}{kappa}{age-sex categories}
    \annotate[yshift=-0.5em]{below,right}{fam}{family members}
    }}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-fig2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab6}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab7}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab8}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl17-tab9}
\end{frame}


\section{\cite{ho2019}}

\begin{frame}
  \frametitle{\cite{ho2019}}
  ``Equilibrium provider networks:
  bargaining and exclusion in health care markets''
  \begin{itemize}
  \item ``narrow network'' health insurance plans annoy consumers,
    concern policy makers
    \begin{itemize}
    \item Insurers with market power underproviding quality?
    \item Provider network design as a mechanism to ``cream skim''
    \end{itemize}
  \item Model of provider network formation
    \begin{itemize}
    \item Bargaining between insurer and hospitals
    \item Use to simulate effect of proposed ``network adequacy''
      regulation
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item[1a] Network formation \& rate determination : MCOs (insurers)
    bargain with hospitals
  \item[1b] Premium setting : MCOs and employers bargain over premiums
  \item[2] Insurance demand : households choose insurance plans
  \item[3] Hospital demand : sick households choose hospitals
  \end{itemize}
  \footnote{1b-3 similar to \cite{ho2017a}, 1a new to this paper}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-fig1}
\end{frame}


\begin{frame}[allowframebreaks]\frametitle{Model : rate determination}
  \begin{itemize}
  \item MCOs $\mathcal{M}$ index $j$, hospitals $\mathcal{H}$, network
    $G$
  \item Profits
    \[ \pi_j^M(G,p) \equiv \tilde{\pi}_j^M(G) - \sum_{i \in G}
      D_{ij}^H(G)p_{ij} \]
    \[ \pi_i^H(G,p) \equiv \tilde{\pi}_i^H(G) + \sum_{n \in \mathcal{M}}
      D_{in}^H(G)p_{in} \]
  \item Gains from trade
    \[ \Delta_{ij} \pi^M_j(G,p) \equiv \pi_j^M(G,p) -
      \pi_j^M(G\setminus i,p_{-ij}) \]
    \[ \Delta_{ij} \pi^H_i(G,p) \equiv \pi_i^H(G,p) -
      \pi_i^H(G\setminus i,p_{-ij}) \]
    \framebreak
  \item Nash-in-Nash with Threat of Replacement (NNTR)
    \[ p^*_{ij}(G) = \min\{p_{ij}^{Nash}(G,p^*_{-ij}),
      p_{ij}^{OO}(G,p_{-ij}^*)\} \]
    where
    \[ p_{ij}^{Nash}(G,p^*_{-ij}) \argmax_p \left[\Delta_{ij}
        \pi^M_j(G,p,p_{-ij}^*)\right]^\tau \left[\Delta_{ij}
        \pi^H_i(G,p,p_{-ij}^*)\right]^{(1-\tau)} \]
    and
    \[ \pi^M_j(G,p_{ij}^{OO},p_{-ij}) = \max_{k \not\in G}
      \pi_j^M(G\setminus i \cup k, p_{kj}^{res}, p_{-ij}) \]
    with
    \[ \pi^H_k(G\setminus i \cup k, p_{kj}^{res}, p_{-ij}) = \pi^H_k(G
      \setminus i, p_{-ij}) \]
  \item Show that equilibrium prices exist for any $G$
  \item First order conditions for $p$ given observed $G$ used to
    estimate $\tau$
  \item Model used to say what prices would be under counterfactual
    $G$
  \item Formation of observed $G$ not used in estimation --- observed
    $G$ constrained by regulators
  \end{itemize}
\end{frame}

\subsection{Data}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item California Public Employees’ Retirement System (CalPERS) in
    2004
  \item Three MCOs : Kaiser (vertically integrated HMO), Blue Cross
    (PPO), Blue Shield (HMO)
  \item Focus on Blue Shield : in 2004 had close to full networks in
    markets considered (forced to do so by regulation), but then
    reduced network
  \item Observe premiums, enrollemnt, admissions, demographics, prices
    paid by insurers to hospitals
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-tabc1}
\end{frame}


\subsection{Estimation}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item See \cite{ho2017a}
  \item Hosptial demand and insurance demand by MLE
  \item Insurer non-inpatient hospital costs $(\eta_j)$ and bargaining
    weights from first order conditions for Nash bargaining
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-tabc2}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-tab3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-fig2a}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hl-fig2b}
\end{frame}


\section{\cite{dorn2024s}}

\begin{frame}\frametitle{Six Stylized Facts From Ten Years of Vertical
    Market Contract Data}
  \cite{dorn2024s}
  \begin{itemize}
  \item Novel data of hospital-insurer contracts from WV 2005-2015
    \begin{itemize}
    \item Payment rates, contract formation, scale
    \end{itemize}
  \item Document six stylized facts
  \end{itemize}
\end{frame}

\subsection{Regulatory Environment and Data}

\begin{frame}\frametitle{WV Regulatory Environment}
  \begin{itemize}
  \item ``corridor'' system regulating hospital list prices
  \item Hospital specific price cap on list price increases
    \begin{itemize}
    \item Lower costs, list prices allow large list price increases
    \item Excessive list prices lead to reduction in future approve
      list prices
    \item Private insurer contracts required to pay more than hospital
      average costs (generally not a binding constraint)
    \item WV Health Care Authority (HCA) made contracts public
      (unusual)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \includegraphics[width=\textwidth]{ds-fig1}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item Hospital discount contract lists (DCL) 2006-2015
    \begin{itemize}
    \item \% each insurer paid below list price in previous year
    \item Separates smaller (top panel) and larger payees (bottom panel)
    \end{itemize}
  \item Hospital Discount Contract forms 2010-2015
    \begin{itemize}
    \item For larger payers
    \item Includes revenue, contract acceptance \& expiration dates, etc
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \includegraphics[width=\textwidth]{ds-fig2}
\end{frame}

\subsection{Six Facts}

\subsubsection{Largest Insurer Paid the Lowest Prices}

\begin{frame}\frametitle{Largest Insurer Paid the Lowest Prices}
  \includegraphics[width=\textwidth]{ds-tab2}
\end{frame}

\subsubsection{Largest Insurer Formed Multiyear Contracts}

\begin{frame}\frametitle{Largest Insurer Formed Multiyear Contracts}
  \includegraphics[width=\textwidth]{ds-fig5}
\end{frame}

\subsubsection{Smaller Insurers Generally Formed Auto-Renew Contracts}

\begin{frame}\frametitle{Smaller Insurers Generally Formed Auto-Renew Contracts}
  \includegraphics[width=\textwidth]{ds-fig7}
\end{frame}



\subsubsection{Auto-Renew Contracts Generally Renewed}

\begin{frame}\frametitle{Auto-Renew Contracts Generally Renewed}
  \includegraphics[width=\textwidth]{ds-fig9}
\end{frame}

\begin{frame}\frametitle{Auto-Renew Contracts Generally Renewed,
    Especially for Smaller Insurers}
  \includegraphics[width=\textwidth]{ds-fig11}
\end{frame}


\subsubsection{Short-Term Data May Underestimate Small Insurer Bargain-
ing Power}

\begin{frame}\frametitle{Short-Term Data May Underestimate Small Insurer Bargain-
ing Power}

\includegraphics[width=0.6\textwidth]{ds-fig12}

\includegraphics[width=0.6\textwidth]{ds-fig13}

\end{frame}


\subsubsection{Contract Formation Was Staggered}

\begin{frame}\frametitle{Contract Formation Was Staggered}
  \includegraphics[width=\textwidth]{ds-fig14}
\end{frame}

\section{\cite{dorn2024}}

\begin{frame}\frametitle{Dynamic Bargaining between Hospitals and
    Insurers}
  \cite{dorn2024}
  \begin{itemize}
  \item Vertical market bargaining over multi-year contracts
  \item Motivated by stylized fact that small insurers agree to long
    term contracts that set price to 100+X\% of Medicare reimbursement
    rate (or other benchmark price)
  \item Negotiators are forward looking and adjust negotiations based
    on expected benchmark price growth
  \item Results imply 1\% increase in Medicare reimbursement rate (to
    match hospital cost growth) would increase national hospital
    spending by \$5 billion
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}
  \frametitle{Model}
  Each period:
  \begin{enumerate}
  \item Hospital and insurer demand revealed
  \item Auto-renew decisions
  \item Contract bargaining and premium setting
  \item Profits realized
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Demand}
  \begin{itemize}
  \item Insurer:
    \begin{align*}
      D_{nt}^M(\eqnmark{G}{\mathcal{G}}, \eqnmark{phi}{\phi})
    \end{align*}
    \annotate[yshift=0.5em]{above,left}{G}{network}
    \annotate[yshift=0.5em]{above,right}{phi}{premiums}
  \item Hospital $h$ demand from enrollees in insurer $n$:
    \begin{align*}
      D_{hnt}^H(\mathcal{G}, \phi)
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Auto-renew}
  \begin{itemize}
  \item Contracts specify price as proportion of benchmark (either
    Medicare or list price)
  \item Choose to renew or not at beginning of year
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{New Contract Bargaining}
  \begin{itemize}
  \item Contract = $($ benchmark $b_{hnt_0}$, length $\ell_{hnt_0}$,
    initial price $p_{hnt_0})$
    \begin{itemize}
    \item After $t_0$, $p_{hn{t+1}} = b_{hnt+1}
      \frac{p_{hnt}}{b_{hnt}}$
    \end{itemize}
  \item Bellman equation
    \begin{align*}
      V(\eqnmark{C}{\mathbb{C}_t},\eqnmark{S}{S_t}) =
      \eqnmark{pi}{\pi(\mathbb{C}_t, S_t)} + \beta E\left[
      V(\mathbb{C}_{t+1}, S_{t+1}) | S_t \right]
    \end{align*}
    \annotate[yshift=0.3em]{above,left}{C}{contract state}
    \annotate[yshift=-0.3em]{below, right}{S}{bargaining state (includes demand, benchmark prices)}
    \annotate[yshift=0.3em]{above, right}{pi}{flow profits}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Kalai Bargaining}
  \begin{itemize}
  \item Gains from trade:
    \begin{equation*}
      GFT_{ij}(\mathbb{C}_{t}, S_t) = V(\mathbb{C}_{t}, S_t) -
      V(\eqnmark{Cij}{\mathbb{C}_{t}/ij}, S_t)
    \end{equation*}
    \annotate[yshift=-0.5em]{below,left}{Cij}{contracts with $i,j$
      contract removed}
  \item Kalai bargaining
    \begin{align*}
      \frac{GFT^M_{ij}(\mathbb{C}_{t},
      S_t)}{GFT^H_{ij}(\mathbb{C}_{t}, S_t)} = \frac{\tau_{ij}}{1-\tau_{ij}}
    \end{align*}
    \begin{itemize}
    \item Dynamic Nash bargaining faces technical challenge --
      disagreement now affects all future bargaining
    \item Axiomatic, intuitive, and lab evidence for Kalai bargaining
      (see references in paper)
    \item Generalizes static Nash-in-Nash bargaining
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Flow Profits}
  \begin{align*}
    \pi_{it}^H = & \sum_{n \in \mathcal{G}_{it}^H}
                   D_{int}^H(\mathcal{G}_t,\phi_t)(p_{int} - c_i) - r_i^H R_{int}  \\
    \pi_{jt}^M = & D_{jt}^M(\mathcal{G}_t, \phi_t) (\phi_{jt} -
                   \eta_j)  - \sum_{h \in \mathcal{G}_{jt}^M}
                   D_{hnt}^H(\mathcal{G}_t, \phi_t) p_{hjt} - r_j^M
                   R_{hjt}
  \end{align*}
  $rR$ negotiation costs
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{d-thm1}
\end{frame}

\subsection{Empirical Specification}

\begin{frame}\frametitle{Empirical Specification}
  \begin{itemize}
  \item Bargaining weights as function of hospital size
    \begin{align*}
      \log(\tau_{ij}/(1-\tau_{ij})) = \log(\tau_j/(1-\tau_j)) +
      \tau^{Size} \log(hospSize_i/\overline{size})
    \end{align*}
  \item Hospital $h$ demand from person $i$ with diagnosis $\ell$
    \begin{align*}
      u_{i,h,\ell} = \delta_{h, \ell} + \nu_{i, h, \ell} \rho  +
      \epsilon_{i,h,l}
    \end{align*}
  \item Insurer demand $j$ from person $i$ in county $c$, market $m$,
    age $k$
    \begin{align*}
      u_{i,j,c,m}^M = \tilde{\delta}_{j,m}^M + \gamma_k WTP_{j,k,c} +
      \xi_{j,k,c} + \epsilon_{i,j,c,m}
    \end{align*}
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}\frametitle{Hospital Demand}
  \includegraphics[width=\textwidth]{d-tab2}
\end{frame}

\begin{frame}\frametitle{Insurer Demand}

  \includegraphics[width=\textwidth]{d-tab3}

  \noindent\rule{\textwidth}{0.4pt}

  \includegraphics[width=\textwidth]{d-tab6}
\end{frame}


\begin{frame}\frametitle{Bargaining}
  \includegraphics[width=\textwidth]{d-tab4}
\end{frame}

\begin{frame}\frametitle{Increasing Benchmark Price Growth by 1\%}
  \includegraphics[width=\textwidth]{d-fig4}
\end{frame}

\begin{frame}\frametitle{Increasing Benchmark Price Growth by 1\%}
  \includegraphics[width=\textwidth]{d-fig6}
\end{frame}

\begin{frame}\frametitle{Increasing Benchmark Price Growth by 1\%}
  \includegraphics[width=\textwidth]{d-fig13}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}


\end{document}

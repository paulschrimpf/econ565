\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}

\title{Demand and supply of differentiated products}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}

\part{Overview of demand estimation}

\begin{frame}
  \tableofcontents
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \cite{gandhi2021}
    \item \cite{berry2021}
    \item \cite{Aguirregabiria2012} chapter 2
    \item \cite{hortacsu2023} 2.1-2.2 and chapter 3
    \item \cite{ackerberg2007econometric} section 1 (these slides use their notation)
    \item \cite{reiss2007structural} sections 1-7, especially 7
    \end{itemize}
  \item Classic papers:
    \begin{itemize}
    \item \cite{berry1994}
    \item \cite{berry1995}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Typical market for consumer goods has many differentiated, but
    similar products, e.g.\
    \begin{itemize}
    \item Cars
    \item Cereal
    \end{itemize}
  \item Differentiated products are a source of market power
  \item Having many products can result in many parameters creating
    estimation difficulties and requiring departures from textbook
    demand and supply models
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Motivation}
  \begin{itemize}
  \item Counterfactuals that do not change production technology
    \begin{itemize}
    \item Mergers
    \item Tax changes
    \end{itemize}
  \item Effects of new goods
  \item Cost-of-living indices
  \item Product differentiation and market power
    \begin{itemize}
    \item Cross-price elasticities
    \end{itemize}
  \end{itemize}
\end{frame}

% inverse logit demand
% https://www.openicpsr.org/openicpsr/project/194501/version/V1/view

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Demand in product space}
\begin{frame}[allowframebreaks]
  \frametitle{Demand in product space}
  \begin{itemize}
  \item $J$ products, each treated as separate good
  \item Classical demand,
    \begin{align*}
      q_{1} = & D_1(p_1,...,p_J, z_1, \eta_1; \beta_1) \\
      \vdots =  \vdots \\
      q_{J} = & D_J(p_1,...,p_J, z_J, \eta_J; \beta_J), \\
    \end{align*}
    and supply (firms' first-order conditions for
    prices):
    \begin{align*}
      p_1 = & g_1(q_1,...,q_J, w_1, \nu_1; \theta_1) \\
      \vdots =  \vdots \\
      p_{J}^d = & g_J(q_1,...,q_J, w_J, \nu_J; \theta_J), \\
    \end{align*}
    where
    \begin{itemize}
    \item $p_j=$ price
    \item $q_j= $ quantity
    \item $z_j =$ observed demand shifter
    \item $\eta_j =$ unobserved demand shock
    \item $\beta_j=$ demand parameters
    \item $w_j =$ observed supply shifter
    \item $\nu_j=$ unobserved supply shock
    \item $\theta_j=$ supply parameters
    \end{itemize}
  \item $D_j$ typically parametrically specified, e.g.\
    \[
    \ln q_j = \beta_{j0} + \beta_{j1} p_1 + \cdots + \beta_{jJ}
    p_J + \beta_{jy} \ln y + Z_1 \gamma + \nu_j
  \]
\end{itemize}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Demand in product space}
  \begin{itemize}
  \item Use reduced form to find instruments
    \begin{align*}
      q_1 = & \Pi^q_{1}(Z,W,\nu,\eta;\beta,\theta) \\
      \vdots = & \vdots \\
      q_J = & \Pi^q_J(Z,W,\nu,\eta;\beta,\theta) \\
      p_1 = & \Pi^p_1(Z,W,\nu,\eta;\beta,\theta) \\
      \vdots = & \vdots \\
      p_J = & \Pi^p_J(Z,W,\nu,\eta;\beta,\theta)
    \end{align*}
    \begin{itemize}
    \item Cost shifters of product $j$ excluded from demand and supply
      of product $k$, but in reduced form
      \begin{itemize}
      \item Cost data often not available
      \item If available, unlikely to be product specific
      \end{itemize}
    \item Attributes of other products
      \begin{itemize}
      \item \cite{hausman1996} uses prices of other products
      \item Hard to justify, especially with prices
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Demand in product space}
  \begin{itemize}
  \item Advantages of product space:
    \begin{itemize}
    \item Flexible substitution patterns
    \item Does not require detailed product attribute data
    \end{itemize}
  \item Problems with product space:
    \begin{enumerate}
    \item Representative agent and aggregation issues
      \begin{itemize}
      \item With heterogeneous preferences, aggregate market demand
        need not meet restrictions on individual demand derived from
        economic theory
      \item Cannot use restrictions easily to improve estimates
      \item Can use simulation to aggregate \citep{pakes1986}
      \end{itemize}
    \item Too many parameters, $O(J^2)$
      \begin{itemize}
      \item Can limit by restricting cross-price elasticities, e.g.\
        \cite{pinkse2002}
      \end{itemize}
    \item Too many instruments needed, $J$
    \item Cannot analyze new goods
    \end{enumerate}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Demand in characteristic space}

\begin{frame}\frametitle{Demand in characteristic space}
  \begin{itemize}
  \item Why do firms differentiate products?
    \pause
  \item Because consumers have heterogeneous tastes for product
    characteristics
    \begin{itemize}
    \item E.g.\ cars: tastes for size, safety, fuel efficiency, etc
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Demand in characteristic space}
  \begin{itemize}
  \item Model consumer preferences for characteristics and
    treat products as bundles of characteristics
    \pause
  \item Reduces number of parameters
  \item Predict demand for new goods
  \item Demand system consistent with utility maximization
    \pause
  \item Early work: \cite{lancaster1971}, \cite{mcfadden1973}
  \item Key extension to early work: \cite{berry1995}
  \end{itemize}
\end{frame}


\subsection{Early work}

\begin{frame}
  \frametitle{Early work in characteristic space}
  \begin{itemize}
  \item Consumer chooses one or none of $J$ products
  \item Utility of consumer $i$ from product $j$
    \[ u_{ij} = x_j \beta + \epsilon_{ij} \]
    with $\epsilon_{ij}$ iid across $i$ and $j$ (usually Type I
    extreme value)
  \item Implies aggregate demand (for Type I extreme value)
    \[ q_j = \frac{\exp(x_j \beta) }{1 + \sum_{k=1}^J \exp(x_k
        \beta)} \]
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Downside of Logit}
  \begin{itemize}
  \item Problem: restrictive substitution ``independence of irrelevant
    alternatives''
    \begin{itemize}
    \item Two goods with the same shares have the same cross price
      elasticities with any third good (think about a luxury and
      bargain good with equal shares)
    \item Goods with same shares should have same markups
    \end{itemize}
  \item Solution: add heterogeneity in $\beta$ and/or allow correlation
    across $j$ in $\epsilon_{ij}$
  \end{itemize}
\end{frame}

\subsection{Model}
\begin{frame}[allowframebreaks] \frametitle{Model}
  \begin{itemize}
  \item Consumers $i$, goods $j$, markets $t$
  \item Utility: (include good $0 =$ buy nothing)
    \[ u_{ijt} = U\left(\overbrace{\underbrace{p_{jt}, \tilde{x}_{jt}}_{\text{observed}},
    \underbrace{\xi_{jt}}_{\text{unobserved}}}^{\text{product characteristics}} ,
  \overbrace{\underbrace{z_{it}}_{\text{observed}},
    \underbrace{\nu_{it}}_{\text{unobserved}}}^{\text{consumer characteristics}};
  \theta\right)
\]
    \begin{itemize}
    \item $x_{jt} = (\tilde{x}_{jt}, p_{jt})\in \R^K$, $z_{it} \in \R^R$, $\nu_{it} \in \R^L$
    \end{itemize}
  \item Choose $j$ if $u_{ijt} > u_{ikt}$ $\forall k \neq j$
  \end{itemize}
\end{frame}

\subsection{Model}
\begin{frame}[allowframebreaks] \frametitle{Model}
  \begin{itemize}
  \item Usually $U(\cdot)$ linear:
    \begin{align*}
      u_{ijt} = & \overbrace{x_{jt}}^{1 \times K}
      \overbrace{\underbrace{\theta_{it}}_{=\bar{\theta} + \theta^o z_{it} + \theta^u
          \nu_{it}}}^{K \times 1} + \overbrace{\xi_{jt}}^{1 \times 1} + \epsilon_{ijt}   \\
    \end{align*}
    for $j=1...J$ and normalize $u_{i0t} = 0$
  \item Assume $\epsilon_{ijt}$ i.i.d. double exponential
  \item Assume $\nu_{it} \sim f_\nu(\cdot;\theta)$, e.g. independent
    normal
  \item Write as product specific $+$ observed interactions $+$
    unobserved interactions
    \begin{align*}
      u_{ijt}= & \underbrace{\delta_j}_{= x_{jt} \bar{\theta} + \xi_{jt}} +
      x_{jt} \underbrace{\theta^o}_{K \times R} z_{it} + x_{jt}
      \underbrace{\theta^u}_{K \times L} \nu_{it} + \epsilon_{ijt}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Endogeneity}
  \begin{itemize}
  \item Usually assume $\Er[\nu_{it} | x_{jt}, z_{it} ]  = 0$ and
    $\Er[\epsilon_{ijt} | x_{jt}, z_{it}]=0$
    \begin{itemize}
    \item Not interested in counterfactuals with respect to changes
      in $z_{it}$, so can treat as residual, i.e.\
      \[ \nu_{it} = \theta_{it} - \Er[\theta_{it} | z_{it}] \]
    \item Market average $\nu_{it}$ or $\epsilon_{ijt}$ plausibly
      correlated with $p_{jt}$ or other product characteristics, but
      this correlation absorbed into $\xi_{jt}$ and/or market fixed
      effects
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Endogeneity}
  \begin{itemize}
  \item Problem is $\xi_{jt}$
    \begin{itemize}
    \item Prices and other flexible product characteristics will be
      correlated with $\xi_{jt}$
    \item If $\xi_{jt}$ serially correlated, then possibly also
      correlated with inflexible product characteristics
    \item Need instrument(s), $w_{jt}$ such that $\Er[\xi_{jt}|w_{jt}]
      = 0$
      \begin{itemize}
      \item Cost shifters
      \item Characteristics of other products
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Estimation and identification}

\subsubsection{Aggregate product data}
\begin{frame}[allowframebreaks]
  \frametitle{Aggregate data}
  \begin{itemize}
  \item Often only have data on product characteristics and market
    shares
  \item Maybe also distribution of some individual characteristics for
    each market (e.g.\ income and education from CPS or census)
  \item Instrument $w$ such that $\Er[\xi_j|w] = 0$
  \item Distribution of $\nu \sim f_\nu(\cdot;\theta_\nu)$
    \begin{itemize}
    \item Combination of estimated market level distribution of
      observed individual characteristics and parametric distributions of
      unobserved individual characteristics
    \item e.g. $\nu_{it} = (educ_{it}, income_{it}, e_{it})$
      \[
      F_{\nu,t}(s,y,e;\theta_\nu) =
      \underbrace{\hat{F}_t(s,y)}_{\text{empirical distribution}}
      \Phi\left(\frac{e - \theta_\nu^\mu}{\theta_\nu^\sigma}\right)
      \]
      $\hat{F}_t(s,y)$ estimated from CPS or other similar data set
    \end{itemize}
  \item Assume $\epsilon_{ijt} \sim $ double exponential (aka Gumbel
    or type I extreme value) as in logit
    \begin{itemize}
    \item Computationally convenient, but other distributions
      feasible too
    \end{itemize}
  \end{itemize}
\end{frame}

\subsubsection{Estimation steps}
\begin{frame}\frametitle{Estimation outline}
  \begin{itemize}
  \item Estimate $\theta$ from moment condition
    \[ \Er[ \xi(\cdot;\theta) | w ] = 0 \]
  \item Where $\xi(\cdot;\theta)$ is such
    that model predicted market shares $=$ observed market shares\footnote{In this
    slide $\cdot$ means the data. I will leave the $\cdot$ out of the
    notation in subsequent slides. I will also leave out $t$ subscripts.}
    \begin{enumerate}
    \item Compute shares given $\theta$, $\sigma(\cdot;\theta,\delta)$
    \item Find
      $\delta(\cdot;\theta) = x_{jt}\bar{\theta} + \xi(\cdot;\theta)$
      such that observed shares, $s_{jt}$ $=$ model shares,
      $\sigma(\cdot;\theta,\delta)$, then
      \[ \xi(\cdot;\theta) = \delta(\cdot;\theta) - x_{jt}
      \bar{\theta} \]
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Computing model shares}
  \begin{itemize}
  \item Integrate over $\nu$
    \[
    \sigma_j(\theta,\delta) = \int \frac{ \exp( \delta_j +
      x_j \theta^u \nu) } { 1 + \sum_{k=1}^j \exp( \delta_k +
      x_k \theta^u \nu) } dF_\nu(\nu)
    \]
  \item Integral typically has no closed form, so compute numerically,
    usually by Monte Carlo integration
    \[ \sigma_j(\theta,\delta) =  \sum_{r=1}^{N_s} \frac{ \exp( \delta_j +
      x_j \theta^u \nu_r) } { 1 + \sum_{k=1}^j \exp( \delta_k +
      x_k \theta^u \nu_r) } \]
    where $\nu_r$ are $N_s$ random draws from $f_\nu$
    \begin{itemize}
    \item Issues about how best to compute integral --- simulation vs
      quadrature, type of simulation \citep{skrainka2011}
    \item Simulation (more generally approximation) of integral
      affects distribution of estimator
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Solving for $\delta$ and $\xi$}
  \begin{itemize}
  \item Want $\delta$ s.t. $\sigma_j(\theta,\delta) = \hat{s}_j$
  \item \cite{berry1995} show
    \[ T(\delta) = \delta + \log(\hat{s}_j) -
    \log(\sigma_j(\theta,\delta)) \]
    is a contraction
    \begin{itemize}
    \item Unique fixed point $\delta$ such that $\delta = \delta + \log(\hat{s}_j) -
      \log(\sigma_j(\theta,\delta)) $, i.e. $\hat{s}_j =
      \sigma_j(\theta,\delta)$
    \item Can compute $\delta(\theta)$ by repeatedly applying
      contraction (in theory and practice often faster to use other
      method)
    \end{itemize}
  \item $\xi_j(\theta) = \delta_j(\theta) - x_j \bar{\theta}$
  \item Important identifying assumption: only $\theta$
    s.t. $\xi_j(\theta) = \xi_j^0$ is true $\theta_0$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimating $\theta$}
  \begin{itemize}
  \item Conditional moment restriction $\Er[\xi_j(\theta)|w] = 0$
  \item Empirical unconditional moments:
    \[ G_{J,T,N,N_s} = \frac{1}{JT} \sum_{j=1}^J \sum_{t=1}^T
    \xi_{jt}(\theta) f(w_{t})  \]
    where
    \begin{itemize}
    \item $f(w) = $ vector of function of $w$
    \item $J =$ number of products
    \item $T=$ number of markets
    \item $N=$ number of observations in each market underlying
      $\hat{s}_j$
    \item $N_s=$ number of simulations
    \end{itemize}
  \item Asymptotic properties (consistency, distribution), depend on
    which of $J,T,N$, and $N_s$ are $\to \infty$, see
    \cite{bplinton2004}
  \item \cite{reynaert2014}: using optimal instruments greatly
    improves efficiency and stability
  \end{itemize}
\end{frame}

% \subsubsection{}
% \begin{frame}
% \end{frame}

\subsubsection{Pricing equation}
\begin{frame}[allowframebreaks]\frametitle{Pricing equation}
  \begin{itemize}
  \item More moments give more precise estimates
  \item Assumption about form of equilibrium allows use of firm first
    order condition (pricing equation) as additional moment
  \item Nash equilibrium in prices
  \item Log linear marginal cost
    \[ \log mc_j = r_j \theta^k + \omega_j \]
    \begin{itemize}
    \item $r_j=$ observed product characteristics, input prices, maybe
      quantity, etc
    \item $\omega_j=$ unobserved productivity, possibly endogenous
    \end{itemize}
  \item Firm $f$ producing set of product $\J_f$,
    \[ \max_{p_j:j \in \J_f} \sum_{j \in \J_f} \left(p_j - C_j(\cdot)
    \right) M s_j(\cdot, p) \]
  \item First order condition:
    \[ \sigma_j(\cdot) + \sum_{l \in \J_f} (p_l - mc_l) \frac{\partial
      \sigma_l(\cdot)}{\partial p_j} = 0 \]
  \item Collect as
    \[ s + (p-mc) \Delta = 0 \]
  \item Rearrange and use log linear marginal cost
    \[ \log(p - \Delta^{-1} \sigma) - r\theta^c = \omega(\theta) \]
  \item Conditional moment restriction $\Er[\omega(\theta)|w] = 0$
  \item Add empirical moments to $G$, $\frac{1}{JT} \sum_{jt}
    \omega_{jt}(\theta) f(w_t)$
  \end{itemize}
\end{frame}


\subsubsection{Micro data}
\begin{frame}\frametitle{Micro data}
  \begin{itemize}
  \item \cite{blp2004}
  \item Data on individual choices and characteristics
    \[ u_{ijt}= \underbrace{\delta_j}_{= x_{jt} \bar{\theta} + \xi_{jt}} +
    x_{jt} \underbrace{\theta^o}_{K \times R} z_{it} + x_{jt}
    \underbrace{\theta^u}_{K \times L} \nu_{it} + \epsilon_{ijt}  \]
  \item Random coefficients discrete choice model, so can identify and
    estimate $\delta$, $\theta^o$, and $\theta^u$ without assumptions
    about $\xi$ and $x$
    \begin{itemize}
    \item \cite{ichimura1998} give conditions for nonparametric
      identification of random coefficients binary choice models
    \item Estimate by MLE or (usually) GMM
    \end{itemize}
  \item Still need $\bar{\theta}$ for price elasticities, etc
    \[ \delta_j = x_{jt} \bar{\theta} + \xi_{jt} \]
    \begin{itemize}
    \item Use IV
    \item Use IV with a pricing equation
    \end{itemize}
  \end{itemize}
\end{frame}

%\subsection{Limitations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{References}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}

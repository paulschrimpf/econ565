\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}

\title{Demand and supply of differentiated products}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\part{Identification}

\begin{frame}\frametitle{References}
  \begin{itemize}
  \item Review paper: \cite{berry2015} --- summarizes
    \cite{berry2009}, \cite{berry2012}, and \cite{berry2013}
  \item Alternative approach :
    \cite{bonnet2017}, \cite{chiong2017}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Motivation}
  \begin{itemize}
  \item Can numerically check local parametric identification
  \item Parametric identification not enough
    \begin{itemize}
    \item Functional form assumptions in BLP are somewhat arbitrary
      and mostly chosen for convenience
    \item Do not want our conclusions to be driven by arbitrary
      assumptions
    \end{itemize}
  \item Non parametric identification shows what assumptions are
    essential for results
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Market level data: \cite{berry2012}}

\begin{frame}\frametitle{Market level data: \cite{berry2012}}
\end{frame}

\begin{frame}
  \frametitle{Model}
  \begin{itemize}
  \item Market characteristics $\chi_t = (x_t, p_t, \xi_t)$, $x_t$
    exogenous, $p_t$ endogenous
  \item Random utilities with distribution $F_v(v_{i1t},...,v_{iJt} |
    \chi_t)$
  \item Shares
    \[ s_{jt} = \sigma_j(\chi_t) = \Pr(\argmax_k v_{ikt} = j|
    \chi_t) \]
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Rationale for setup}
  \begin{itemize}
  \item Parametric models:
    \begin{itemize}
    \item Logit random utility:
      \[ u_{ijt} =  x_{jt} \beta - \alpha p_{jt} + \xi_{jt} +
        \epsilon_{ijt}  \]
      implies
      \[ x_{jt}^{(1)} + \tilde{\xi}_{jt} = \frac{1}{\beta^{(1)}}(\ln(s_{jt}
        - \ln(s_{0t})) + \frac{\alpha}{\beta^{(1)}} p_{jt} -
        \frac{1}{\beta^{(1)}} x_{jt}^{(-1)} \beta^{(-1)} \]
    \item BLP implies:
      \[ x_{jt}^{(1)} + \tilde{\xi}_{jt} =
        \frac{1}{\bar{\theta}^{(1)}} \left( \delta_j(s_t,p_t,\theta) -
          x_{jt}^{(-1)} \bar{\theta}^{(-1)} \right) \]
    \end{itemize}
  \item In each case:
    \[ x_{jt}^{(1)} + \xi_{jt} = \text{function of}
      s_t,p_t,x_{t}^{(-1)} \]
  \end{itemize}
\end{frame}


\begin{frame}[shrink]\frametitle{Assumptions}
  \begin{enumerate}
  \item\label{ma1} \alert{Index restriction}: partition $x_{jt} = (x_{jt}^{(1)},
    x_{jt}^{(2)})$, define
    \[ \delta_{jt} = x_{jt}^{(1)} + \xi_{jt} \]
    then
    \[ F_v(\cdot | \chi_t) = F_v(\cdot | \delta_t, x_t^{(2)}, p_t) \]
  \item\label{ma2} \alert{Connected substitutes}: (see \cite{berry2013})
    \begin{enumerate}
    \item $\sigma_k(\delta_t,p_t, x_t^{(2)})$ is nonincreasing in
      $\delta_{jt}$ and $-p_{jt}$ for $k \neq j$
    \item Among any subset $\mathcal{K} \subseteq \mathcal{J}$,
      $\exists j,k \in \mathcal{K}$ and $j \not\in \mathcal{K} $
      s.t. $\sigma_k(\delta_t, p_t, x_t^{(2)})$ is decreasing in
      $\delta_{jt}$ and $-p_{jt}$
    \end{enumerate}
  \item \label{ma3} \alert{IV exogeneity} $\Er[\xi_{jt} | z_t, x_t] = 0$
  \item \label{ma4} \alert{Rank condition / completeness}:
    $\forall B(s_t,p_t)$ if $\Er[B(s_t,p_t) | z_t, x_t] = 0$ a.s., then
    $B(s_t,p_t) = 0$ a.s.
  \end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks] \frametitle{Identification of demand}
  \begin{itemize}
  \item If \ref{ma1}-\ref{ma4}, then $\xi_{jt}$ and $\sigma_j(\chi_t)$
    are identified [Theorem 1]
  \item Connected substitute (\ref{ma1}) $\Rightarrow$ demand
    invertible
    \[ \delta_{jt} = \sigma^{-1}(s_t,p_t) \]
  \item IV \& rank condition:
    \begin{align*}
      \Er[x_{t}^{(1)} - \sigma^{-1}(s_t,p_t) | z, x ] = 0
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Implications for instruments}
  \begin{itemize}
  \item 2J endogenous variables $(s,p)$, so need at least $2J$ instruments
    \begin{itemize}
    \item $J$ instruments from exogenous characteristic $x^{(1)}$
    \item Need $J$ instruments that shift price and are excluded from
      demand
    \item Need variation in price and variation in share conditional on price
    \end{itemize}
  \item Types of instruments that have been used:
    \begin{itemize}
    \item ``BLP instruments'' $=$ characteristics of competing
      products
      \begin{itemize}
      \item Needed, but not sufficient (without more restrictions)
      \end{itemize}
    \item Cost shifters:
      \begin{itemize}
      \item ``Hausman instruments'' $=$ price of same good in other
        markets
      \item Consumer characteristics in nearby markets
        (e.g. \cite{fan2012})
      \end{itemize}
    \end{itemize}
  \item Functional form restrictions can reduce needed number of
    instruments, e.g. if
    \[ \delta_{jt} = x_{jt}^{(1)} - \alpha p_{jt} + \xi_{jt} \]
    then only need 1 instrument for price
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Marginal costs}
  \begin{itemize}
  \item More assumptions:
    \begin{enumerate}
    \item\label{ma6} $\sigma_j(\delta_t,p_t)$ continuously differentiable wrt
      $p_t$
    \item\label{ma7} Known form of competition so know $\psi_j$ such that
      \[ mc_{jt} = \psi_j(s_t,M_t, \{\frac{\partial \sigma}{\partial
        p}\},p_t) \]
    \end{enumerate}
  \item Then $mc_{jt}$ is identified [Theorem 3]
  \item If want to do counterfactuals that change quantities, need to
    know marginal cost function, not just $mc_{jt} = $ marginal cost
    at observed quantity
    \begin{itemize}
    \item If $mc_{jt} = \tilde{c}_j(Q_{jt},w_{jt}) + \omega_{jt}$ and have
      instruments $y_{jt}$ such that $\Er[\omega|w,y]=0$, then $c_j$
      and $\omega_{jt}$ identified [Theorem 4]
    \end{itemize}
  \item If $\psi_j$ is unknown, then with stronger assumptions about
    marginal cost function and cost instruments, can still identify
    $\omega_{jt}$ [Theorem 5]
    \begin{itemize}
    \item Can then test for different forms of $\psi_j$ [Theorem 9]
    \item Require index restriction on $c_j$,
      \[ mc_{jt} = c_j(Q_{jt}, w_{jt}^{(1)} \gamma_j + \omega_{jt},
      w_{jt}^{(2)}) \]
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Simultaneous equations approach}
  \begin{itemize}
  \item Use demand and supply equations together and can replace
    completeness conditions with regularity conditions about demand
    and supply equations
  \item Will need no external instruments (but do need exclusions)
  \item Index assumptions imply
    \begin{align}
      \underbrace{x_{jt} + \xi_{jt}}_{=\delta_{jt}} = & \sigma^{-1}(s_t,p_t) \label{demand}\\
      \underbrace{w_{jt} + \omega_{jt}}_{=\kappa_{jt}} = & \pi^{-1}(s_t,p_t) \label{supply}
    \end{align}
  \item Assume $x,w \indep \xi,\omega$ and $\mathrm{supp}(x,w) = \R^{2J}$
  \item Assume conditions (including for each $\delta,\kappa$ there is
    unique $s,p$) such that can make change of variables so
    \begin{align}
      f_{s,p} & (s_t,p_t|x_t,w_t) = \notag \\
      & = f_{\xi,\omega}\left(\sigma^{-1}(s_t,p_t) -
      x_t,  \pi^{-1}(s_t,p_t) - w_t\right) \abs{\mathcal{J}(s_t,p_t)} \label{fsp}
    \end{align}
    where $\mathcal{J}(s,p) = $ Jacobian wrt $s,p$ of (\ref{demand})
    and (\ref{supply})
  \item Then can identify $\xi$, $\sigma$, $\omega$ [Theorems 6 and 7]
    \begin{itemize}
    \item Integrating (\ref{fsp}) wrt $x,w$ identifies
      $\abs{\mathcal{J}(s,p)}$, then dividing gives $f_{\xi,\omega}$
    \item Integrating $f_{\xi,\omega}$ give $F_\xi$
    \item Location normalization: assume $\sigma^{-1}(s^0,p^0)
      - x^0) = 0$, then know $F_\xi(0)$
    \item For other $s,p$, can find $x^*$ such that
      $F_\xi(\sigma^{-1}(s,p) - x^\ast) = F_\xi(0)$, i.e.\
      $\sigma^{-1}(s,p) = x^\ast$, so $\sigma^{-1}$ identified
    \item (\ref{demand}) identifies $\xi_{jt}$
    \item Same argument for $\omega$
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Micro data: \cite{berry2024}}

\begin{frame}\frametitle{Micro data: \cite{berry2024}}
  Working paper was \cite{berry2009}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Consumer $i$, markets $t$, products $j \in \mathcal{J}_t$
  \item Consumer observations $z_{it} = (z_{i1t}, ..., z_{iJ_t t})$
  \item Observed product characteristics $x_t = (x_{1t}, ..., x_{J_t t})$
    (includes prices, may include product dummies)
  \item Scalar product unobservable $\xi_{jt}(z_{it})$
  \item Random utility function $u_{it}: \mathcal{X} \to \R$
    \begin{itemize}
    \item Formally, there's a probability space, $(\Omega,\mathcal{F},
      \mathbb{P})$ and
      \[ v_{ijt} = u_{it}(x_{jt},\xi_{jt}(z_{it}), z_{ijt}) =
      u(x_{jt},\xi_{jt}(z_{it}), z_{ijt}, \omega_{it}) \]
      where $\omega_{it} \in \Omega$ and $u$ is measurable in
      $\omega_{it}$ with $\omega_{it} \indep (x_{jt},z_{it},\xi_{jt}(z_{it}))$
    \item E.g. random coefficients
      \[ u(x_{jt},\xi_{jt}(z_{it}), z_{ijt}, \omega_{it}) = x_{jt}
      \theta_{it} + z_{ijt} \gamma + \xi_{jt} + \epsilon_{ijt} \]
      $\omega_{it} = (\theta_{it}, \epsilon_{i1t}, ..., \epsilon_{iJ_t
        t})$
    \item Allows distribution of $\theta$ and $\epsilon$ to
      depend on $z, x, \xi$
    \end{itemize}
  \item Special regressor: $z_{ijt}^{(1)} \in \R$ s.t.
    \[ v_{ijt} = \phi(\omega_{it}) z_{ijt}^{(1)} +
    \tilde{\mu}\left(x_{jt}, \xi_{jt}(z_{it}^{(2)}), z_{ijt}^{(2)},
      \omega_{it} \right) \]
    Restrictions:
    \begin{enumerate}
    \item \alert{invariance} of $\xi_{jt}(z_{it})$ to $z_{it}^{(1)}$
    \item Additive \alert{separability}
    \item $\tilde{\mu}$ \alert{monotonic} in $\xi_{jt}$
    \end{enumerate}
    Identifies mapping from choice probabilities to utilities
  \item Henceforth, all argument conditional on $z_{it}^{(2)}$, so
    leave out of notation
  \item Normalizations
    \begin{itemize}
    \item $\xi_{jt} \sim U(0,1)$
    \item Location of utilities
      \[ v_{i0t} = 0 \]
    \item Scale of utilities $\phi_{it} = 1$,
      \[ v_{ijt} = z_{ijt}^{(1)}  + \underbrace{\mu(x_{jt}, \xi_{jt}(z_{it}^{(2)}), z_{ijt}^{(2)},
      \omega_{it} )}_{\equiv \mu_j(x_{jt}, \xi_{jt}, \omega_{it})} \]
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]\frametitle{Identification definition}
  \begin{itemize}
  \item Data: $(t, y_{it}, \{x_{jt}, \tilde{w}_{jt}, z_{ijt}\}_{j \in
      \mathcal{J}_t} )$
  \item Conditional choice probabilities
    \[ p_{ijt} = \Pr\left(y_{it} = j | t, \{x_{kt}, \tilde{w}_{kt}, z_{ikt}\}_{k \in
        \mathcal{J}_t} \right) \]
  \item \alert{Full identification of random utility model}: means
    that for any given conditional choice probabilities there is a
    unique distribution of $\xi_{jt}$ (given $z_{ijt}^{(2)}$) and a
    conditional distribution of utilities $\{v_{ijt}\}_{j \in \mathcal{J}_t}$
    given $\{x_{jt}, \tilde{w}_{jt}, z_{ijt}\}_{j \in
      \mathcal{J}_t} $ that generates the given choice probabilities
  \item \alert{Identification of demand}: for any conditional choice
    probabilities there is a unique distribution of $\xi_{jt}$ (given
    $z_{ijt}^{(2)}$) and unique structural choice probabilities
    \[ \rho_j\left(\{x_{jt}, \xi_{jt}, z_{ijt}\}_{j \in \mathcal{J}_t}
    \right) = \Pr\left(y_{it}=j| \{x_{kt}, \xi_{kt}, z_{ikt}\}_{k \in
        \mathcal{J}_t} \right) \]
    that match the choice probabilities
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Assumptions}
  \begin{enumerate}
  \item\label{a3} Large support: $\mathrm{supp} \{z_{ijt} \}_{j \in \mathcal{J}_t} |
    \{x_{jt} \}_{j \in \mathcal{J}_t} = \R^{|\mathcal{J}_t|-1}$
  \item\label{a4} Independence of instruments: $\xi_{jt} \indep (w_{jt},z_{ijt}) \forall j,t$
  \item\label{a5} Rank condition: the quantile version of bounded
    completeness from \cite{chernozhukov2005} applies to $D_j(x_{jt}, \xi_{jt})$
  \end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Identification proof}
  \begin{itemize}
  \item $\mu_{ijt}  =\mu(x_{jt}, \xi_{jt}(z_{it}^{(2)}),
    z_{ijt}^{(2)}, \omega_{it} )$
  \item Large support:
    \begin{align*}
      \lim_{z_{ikt}^{(1)} \to -\infty \forall k \neq
        j} & \Pr\left(y_{it} = j | t, \{x_{kt}, \tilde{w}_{kt}, z_{ikt}\}_{k
          \in \mathcal{J}_t} \right) =  \\
      = &
      \lim_{z_{ikt}^{(1)} \to -\infty \forall k \neq
        j} \Pr\begin{pmatrix} z_{ijt} + \mu_{ijt} \geq z_{ikt} + \mu_{ikt}
        \forall k \neq j \cap \\ \cap z_{ijt} + \mu_{ijt} \geq 0 | \\
        | t, \{x_{kt}, \tilde{w}_{kt}, z_{ikt}\}_{k
          \in \mathcal{J}_t}
      \end{pmatrix}
      \\
      = & \Pr\left(z_{ijt} + \mu_{ijt} \geq 0 | t, \{x_{kt}, \tilde{w}_{kt}, z_{ikt}\}_{k
          \in \mathcal{J}_t} \right)
    \end{align*}
  \item Independence of $z_{ijt}$ and $(\xi_{jt}, \omega_{it})$
    \begin{align*}
      \Pr\left(z_{ijt} + \mu_{ijt} \geq 0 |t, \{x_{kt}, z_{ikt}\}_{k
          \in \mathcal{J}_t} \right ) = &
      \Pr\left(z_{ijt} + \mu_{ijt} \geq 0 |t, x_{jt}, z_{ijt} \right)
      \\
      = & 1 - F_{\mu_{ijt}|t}(-z_{ijt} |x_{jt}, t)
    \end{align*}
    averaging over $x_{jt}$ identifies $F_{\mu_{ijt}|t}$, and so identifies conditional
    quantiles, e.g.
    \begin{align*}
      \delta_{jt} \equiv & \textrm{median}[\mu_j(x_{jt}, \xi_{jt},
      \omega_{it}) | t ]
    \end{align*}
    Define
    \begin{align*}
      \textrm{median}[\mu_j(x_{jt}, \xi_{jt},
      \omega_{it}) | x_{jt}, \xi_{jt} ]  = D_j(x_{jt}, \xi_{jt})
    \end{align*}
  \item Independence of $w_{jt}$ and $\xi_{jt}$, and $\xi_{jt} \sim
    U(0,1)$ implies that
    \[ \Pr\left(\delta_{jt} \leq D_j(x_{jt},\tau) | w_{jt}\right) = \tau \]
  \item Nonparametric IV quantile regression of
    \cite{chernozhukov2005} shows that with the bounded completeness
    condition $D_j$ is unique identified, and so is $\xi_{jt} =
    D_j^{-1}(x_{jt},\delta_{jt})$
  \item Joint distribution of $\mu$ from
    \[ p_{i0t} = \Pr(z_{ijt} + \mu_{ijt} \leq 0 \forall j \neq 0 | t,
    z_{it}) \]
  \item $F_{\mu|t} = F_{\mu|x_t,z_{it}, \xi_t}$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Further remarks}
  \begin{itemize}
  \item To summarize if (\ref{a3})-(\ref{a5}) then the random utility
    model is identified
  \item If large support fails, then can still identify demand
  \item Identifying random utility model is not exactly the same as
    identifying random coefficients
    \[  v_{ijt} = x_{jt} \theta_{it} + z_{ijt} \gamma + \xi_{jt} +
    \epsilon_{ijt} \]
    \begin{itemize}
    \item This paper identifies $F_{v|x,z,\xi}$, further conditions
      needed for distribution of $\theta$, $\epsilon$
    \item Given results in this paper, we can treat $v_{ijt}$ as
      observed and use standard results to identify distribution of
      coefficients (see conclusion of \cite{berry2009} for references)
    \item Most economic quantities that we might care about depend on
      the random utility model not the random coefficients
    \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{bonnet2017}}

\begin{frame}\frametitle{\cite{bonnet2017}}
  Yogurts choose consumers? identification of random
  utility models via two-sided matching

  \begin{itemize}
  \item Inversion of demand (market shares to mean utilities) for
    nonadditive models
  \item Connection between demand and two-sided matching
  \item Algorithm for computing identified set
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Setup}
  \begin{itemize}
  \item Products $\mathcal{J}_0$
  \item Nonadditive random utility model for consumers
    \[ u_{\epsilon_i} = \max_{j \in \mathcal{J}_0}
      \mathcal{U}_{\epsilon_i,j}(\delta_j) \]
    with $\epsilon_i
    \in \Omega$ with distribution $P$
    known
  \item $\mathcal{U}_{\epsilon_i}$ and distribution of $\epsilon_i$ known
  \item Econometrician observes market shares
    \[ s_j = \sigma_j(\delta) = \Pr\left(\mathcal{U}_{\epsilon
          j}(\delta_j) \geq \max_{j'} \mathcal{U}_{\epsilon
          j'}(\delta_j')\right) \]
  \item Will characterize the identified set for $\delta$ given $s$
    \[ \sigma^{-1}(s) = \{ \delta \in \R^{|\mathcal{J}_0|}:
      \sigma(\delta) = s \} \]
  \item All results could be conditional on individual covariates
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Examples}
  \begin{itemize}
  \item Additive model:
    \[ \mathcal{U}_{\epsilon j}(\delta_j) = \delta_j + \epsilon_j \]
    and could have $\delta_j = x_j \beta$ as further restriction
  \item Risk aversion
    \[ \mathcal{U}_{\epsilon j}(\delta_j) = \Er_\eta\left[
        \frac{(\delta_j - p_j +\eta_j)^{1-\epsilon}}{1-\epsilon}
      \right]
    \]
  \item Income shock
    \[ \mathcal{U}_{\epsilon j}(\delta_j) = \max_{x} V(x) + \delta_j
      \,:\, x'p \leq y_j + \epsilon_i \]
  \end{itemize}
\end{frame}

\begin{frame}[shrink] \frametitle{Matching game}
  \begin{itemize}
  \item Preferences and feasibility described by transfers :
    \begin{itemize}
    \item $f_{\epsilon j}(u) =$ transfer needed by consumer $\epsilon$
      for utility $u$ when matched with product $j$ (increasing function)
    \item $g_{\epsilon j}(\delta) =$ transfer needed by product $j$ to
      reach utility $\delta$ (increasing function)
    \end{itemize}
  \item Equilibrium consists of :
    \begin{itemize}
    \item Matching probibility distribution $\pi$ on $\Omega \times
      \mathcal{J}_0$
    \item Realized utilities $u_\epsilon$ and $\delta_j$
    \end{itemize}
    such that
    \begin{itemize}
    \item No blocking pair : $f_{\epsilon j}(u_\epsilon) + g_{\epsilon
        j} (v_j) \geq 0$ for all $\epsilon, j$
    \item Feasibility : if $\pi(\epsilon, j)>0$, then $f_{\epsilon
        j}(u_\epsilon) + g_{\epsilon j}(v_j) = 0$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Equivalence}
  Theorem 1 : the following are equivalent
  \begin{enumerate}
  \item $\delta \in \sigma^{-1}(s)$
  \item In matching game with $f_{\epsilon j}(u) = u$ and
    $g_{\epsilon_j}(-\delta) = -\mathcal{U}_{\epsilon_j}(\delta)$

    Let $u_\epsilon = \max_{j \in \mathcal{J}_0}
    \mathcal{U}_{\epsilon,j}(\delta_j)$, and $v_j = -\delta_j$

    Then $\exists \pi$ such that $(\pi, u, -\delta)$ is an equilibrium
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Implications}
  \begin{itemize}
  \item $\sigma^{-1}(s)$ is a connected lattice
    \begin{itemize}
    \item Partially order : $\delta \leq \delta'$ iff
      $\delta_j \leq \delta_{j}$ for all $j$
    \item $\delta \wedge \delta' \equiv $ greatest lower bound $ = $
      coordinate wise min, $\delta \vee \delta'$ exist
    \item[$\Rightarrow$] Has minimal and maximal elements
    \end{itemize}
  \item Minimal and maximal elements $ = $ equilibrium matchings most
    preferred by consumers and products, respectively
  \item Modified versions of algorithms from matching literature can
    compute min and max
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Combining with BLP framework}
  \begin{itemize}
  \item This paper identifies set of mean utilities, still need
    assumption about how mean utilities relate to product
    characteristics to identify demand over characteristics
  \item Assume $\delta_j = p_j \alpha + \xi_j$ (or similar)
  \item Use algorithm to compute $\delta_{min}$ and $\delta_{max}$
  \item Identified set for $\alpha$ :
    \[ \{\alpha : \Er[\delta - p_j \alpha | Z ] = 0 \text{ for some }
      \delta \in [\delta_{min}, \delta_{max}] \} \]
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Other identification results}

\begin{frame}\frametitle{Other identification references}
  \begin{itemize}
    \item \cite{berry2013} show connected substitutes is sufficient
      for invertibility of demand
  \item \cite{foxGandhi2011} identification of demand for any dimension $\xi$
  \item \cite{chiappori2009} identification through conditional
    independence instead of special regressor
  \item \cite{fox2012rc} shows random coefficients logit (without
    endogeneity) is identified
  \end{itemize}
\end{frame}

\section{References}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}


\end{document}

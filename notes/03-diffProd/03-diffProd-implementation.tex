\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}

\title{Demand and supply of differentiated products}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}

\part{Implementation}

\frame{\partpage}

%\section{General guidelines}

% \begin{frame}[allowframebreaks]\frametitle{Implementing an estimator}
%   \begin{itemize}
%   \item Suppose we want to estimate a BLP model
%   \item Anytime we want to estimate a model we should:
%   \end{itemize}
%   \begin{enumerate}
%   \item\label{o1} Find and use a \textbf{popular} existing implementation
%     \begin{itemize}
%     \item Do not waste time rewriting code
%     \item Widely used code is more likely to be correct than our own
%     \item Code written by skilled programmers will be faster than
%       our own
%     \end{itemize}
%   \item\label{o2} If \ref{o1} does not exist or is not suitable for our
%     problem, take an existing implementation and
%     \begin{enumerate}
%     \item\label{2a} Verify it runs
%       \begin{itemize}
%       \item Runs on your computer
%       \item Fast enough for your problem
%       \item Flexible enough for your problem
%       \end{itemize}
%     \item\label{2b} Test that it produces correct results
%       \begin{itemize}
%       \item Read and check the code
%       \item Estimate with simulated data
%       \end{itemize}
%     \end{enumerate}
%   \item\label{o3} If \ref{o2} fails, try again with another
%     implementation, read literature about implementations so you know
%     what the issues are
%   \item\label{o4} If no existing implementation works for your
%     problem, see if you can modify one
%     \begin{itemize}
%     \item It may be easier to start from scratch; writing code
%       that others can understand and maintain is something
%       professional programmers struggle with, economists usually
%       fail completely
%     \item More likely to be a good idea if:
%       \begin{itemize}
%       \item You know the language well
%       \item Clear, localized problem, examples: fails to run because
%         of missing library; too slow but with a single bottleneck;
%         small change needed to accommodate your problem
%       \end{itemize}
%     \item Probably bad idea if you do not know the language well and
%       the problem is that the code produces incorrect results
%     \end{itemize}
%   \item\label{o5} Write your own implementation
%     \begin{itemize}
%     \item Write down the steps involved, break into separate
%       functions, identify abstractions
%       \begin{itemize}
%       \item Abstraction barriers: code calling a function does not
%         need to how the function is implemented
%       \item Allows pieces of code to be modified and tested separately
%         from one another
%       \item Anything you think you might want to change should be
%         behind an abstraction barrier; e.g.\ how you integrate over
%         $\nu$, how you solve for $\delta$, how you minimize the
%         moments, how the data is stored
%       \end{itemize}
%     \item Test your code thoroughly
%     \item Do not optimize unnecessarily; clarity more important than
%       speed; use a profiler to identify bottlenecks
%     \end{itemize}
%   \end{enumerate}
% \end{frame}

% \section{Available implementations}

% \begin{frame}
%   \frametitle{Available implementations}
%   \begin{itemize}
%   \item \cite{nevo2000practitioner} (Matlab)
%     \begin{itemize}
%     \item \url{http://faculty.wcas.northwestern.edu/~ane686/supplements/rc_dc_code.htm}
%     \item \cite{rasmusen2006} update of code
%       \url{http://www.rasmusen.org/zg604/lectures/blp/frontpage.htm}
%     \end{itemize}
%   \item \cite{dube2012} (Matlab)
%     \begin{itemize}
%     \item
%       \url{http://faculty.chicagobooth.edu/jean-pierre.dube/research/MPECcode.html}
%     \end{itemize}
%   \item \cite{vincent2012} (Stata)
%   \item
%     \href{https://bitbucket.org/paulschrimpf/econ565/src/master/notes/03-diffProd/blp/ampl?at=master}
%     {My AMPL code} (this likely contains bugs)
%   \item Random coefficient logit
%     \url{http://cran.r-project.org/web/packages/mlogit/} (R)
%   \end{itemize}
% \end{frame}

\begin{frame}[fragile]
  \frametitle{Computational issues}
  \begin{itemize}
  \item Non-convex optimization problems are almost always difficult
    to solve, this is no exception
  \item Nested iteration can be problematic
    \begin{itemize}
    \item Solve for $\delta(theta)$:
\begin{verbatim}
while norm(T(delta) - delta) > tolerance1 { delta = T(delta) }
\end{verbatim}
    \item Minimize
\begin{verbatim}
while norm(theta - thetaOld) > tolTheta && norm(f - fold) > tolf {
  thetaOld = theta
  fold = f
  // update theta by e.g. newton's method, set f = f(theta, xi(delta;theta))
}
\end{verbatim}
    \item Error in $\delta$ can lead to error in minimization
    \item Error in $\delta$ is not a continuous with respect to
      $\theta$ (where changing $\theta$ changes number of iterations)
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{pyBLP}

\begin{frame}\frametitle{pyBLP}
  \begin{itemize}
  \item \cite{pyBLP}, \cite{microPyBLP}
  \item Most used and feature complete implementation
  \item \url{https://pyblp.readthedocs.io/en/stable/introduction.html}
  \end{itemize}
\end{frame}

\subsection{Nevo}

\begin{frame}[fragile]
  \frametitle{Nevo}
  \begin{itemize}
  \item Popular code provided by \cite{nevo2000practitioner}
  \item Requires: Matlab, optimization toolbox
  \item Nevo's code does not run in current version of Matlab, but
    \cite{rasmusen2006} update does
  \item Code runs in Octave after changing {\texttt{fminsearch}} to
    another optimization routine
  \item Worked on by three people
  \item Used by at least six other papers (see \cite{knittel2012}
    footnote 5 for list)
  \item Fast for data provided
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Nevo - issues}
  \begin{itemize}
  \item Minimization difficult and not robust
    \begin{itemize}
    \item Starting value
    \item Algorithm
    \item Tolerance for finding $\delta$ (\cite{dube2012} show loose
      tolerance affects estimates)
    \item \cite{knittel2012} algorithms often stop at point where
      first and/or second order conditions fail
    \item \cite{knittel2012} differences among convergence points
      economically significant
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{\cite{dube2012}}
\begin{frame}[allowframebreaks]\frametitle{\cite{dube2012}}
  \begin{itemize}
  \item Fixed point iteration to compute $\delta$ messes up
    GMM minimization; also is not best method for finding $\delta$
    \begin{itemize}
    \item Table 1: shows problem is too large a tolerance. NFP gives
      good estimates when tolerances are tight
    \end{itemize}
  \item Can recast problem as constrained minimization
    \begin{align*}
      \min_{\theta, \delta} & \sum_\ell \left(\frac{1}{JT} \sum_{j=1}^J \sum_{t=1}^T
        \xi_{jt}(\theta,\delta) f_\ell(w_{t})\right)^2   \\
      & \text{subject to } \\
      & \hat{s} = \sigma(\cdot;\theta,\delta)
    \end{align*}
  \item \cite{su2012}: ``mathematical programming with equilibrium
    constraints'' (MPEC)
  \item Use state of the art algorithm to solve constrained
    minimization
  \item Solvers work best with accurate (i.e.\ not finite
    difference) derivatives---supplying 1st and 2nd order
    derivatives makes algorithm take approximately 1/3 as long as
    with just 1st order \citep{dube2012supplement}
  \item Gains from exploiting sparsity of Jacobian of constraints
    and Hessian of objective function
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{dube2012} code}
  \begin{itemize}
  \item Code requires: Matlab, KNITRO
  \item KNITRO proprietary, free version limited to 300 variables \&
    constraints
  \item KNITRO can be replaced with other optimization algorithm,
    but others do not seem to work as well:
    \begin{itemize}
    \item \href{https://projects.coin-or.org/Ipopt}{IPOPT} uses
      similar algorithm, but I had trouble installing
    \item
      \href{http://ab-initio.mit.edu/wiki/index.php/NLopt}{NLOPT}
      has no interior point algorithm, its algorithms do not seem to
      deal with nonlinear constraints very well
    \item \cite{skrainka2012} uses SNOPT, which is similar algorithm
      to NLOPT's SLSQP
    \end{itemize}
  \item Runs in Octave with KNITRO replaced by NLOPT
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Observations}
  \begin{itemize}
  \item High quality commercial solver appears necessary; my attempts with NLOPT fail and
    take longer
    \begin{itemize}
    \item \cite{skrainka2012} uses SNOPT instead of KNITRO
    \end{itemize}
  \item KNITRO and SNOPT not perfect
    \begin{itemize}
    \item Still sensitive to starting values
    \item MPEC replaces a contraction --- a problem we know we can
      solve --- with constraints that may make the optimization harder
      \begin{itemize}
      \item \cite{reynaerts2012} give method to improve accuracy and
        speed of computing $\delta$
      \item \cite{dube2012supplement} using nested fixed point
        requires fewer solver iterations than MPEC, but takes as long
        or longer because of time spend solving for $\delta$ (can be
        much longer if contraction mapping is slow)
      \end{itemize}
    \end{itemize}
  \item \cite{reynaert2014}: using optimal instruments makes
    optimization more robust
  \end{itemize}
\end{frame}

% \subsection{\cite{vincent2012}}

% \begin{frame}\frametitle{\cite{vincent2012}}
%   \begin{itemize}
%   \item Requires Stata, {\texttt{ssc install blp}}
%   \item Easiest to use
%   \item I did not look at this in detail but would be surprised if it
%     resolves the minimization difficulties above
%   \end{itemize}
% \end{frame}

% \subsection{My AMPL code}
% \begin{frame}\frametitle{My AMPL code}
%   \begin{itemize}
%   \item Same approach as \cite{dube2012}
%   \item May not be correct
%   \item AMPL not free; trial version limited to 300 variables and
%     constraints
%   \item AMPL memory usage can become prohibitively large
%   \end{itemize}
% \end{frame}


\begin{frame}
  \frametitle{References about implementation}
  \begin{itemize}
  \item Overviews
    \begin{itemize}
    \item \cite{nevo2000practitioner}
    \item \cite{dube2012}, \cite{dube2012supplement}
    \item \cite{knittel2012}
    \item \cite{skrainka2012}
    \end{itemize}
  \item Particular issues
    \begin{itemize}
    \item \cite{skrainka2011}: integration
    \item \cite{reynaerts2012}: solving for $\delta$
    \end{itemize}
  \item Course on discrete choice models with simulation by Kenneth Train
    \url{http://elsa.berkeley.edu/users/train/distant.html}
  \item Bayesian: \cite{jiang2009}, \cite{viard2014}, \cite{sun2013}
  %\item \cite{fox2011} change parameterization to make convex ()
  \item Overview of optimization methods and software
    \cite{leyffer2010}
  \end{itemize}
\end{frame}

\section{\cite{fosgerau2024}}

\begin{frame}\frametitle{The Inverse Product Differentiation Logit
    Model}
  \begin{itemize}
  \item \cite{fosgerau2024}
  \item Flexible demand model with closed from inverse share
  \item Generalization of nested logit
  \item Faster, more stable computation
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Setup}
  \begin{itemize}
  \item Typical demand models (logit, nest logit, random coefficients
    logit) have:
    \begin{enumerate}
    \item Linear index of average product desirability:
      \begin{equation*}
        \delta_{\tikzmarknode{j}{j}\tikzmarknode{t}{t}} = \tikzmarknode{x}{x_{jt}}\beta  - \alpha \tikzmarknode{p}{p_{jt}} + \tikzmarknode{xi}{\xi_{jt}}
      \end{equation*}
      \begin{tikzpicture}[overlay,remember
        picture,>=stealth,nodes={align=left,inner ysep=1pt},<-]
        \path (t.south) ++ (0,-0.2em) node[anchor=north east,color=gray]
        (scalep){market};
        \draw (t.south) |- (scalep.north);

        \path (j.north) ++ (0,0.5em) node[anchor=south east,color=gray]
        (scalep){product};
        \draw (j.north) |- (scalep.south);

        \path (x.north) ++ (0,0.5em)  node[anchor=south west,color=gray]
        (scalep){product characteristics};
        \draw (x.north) |- (scalep.south);

        \path (p.south) ++ (0,-1em)  node[anchor=north east,color=gray]
        (scalep){price};
        \draw (p.south) |- (scalep.north);


        \path (xi.south) ++ (0,-0.2em) node[anchor=north west,color=gray]
        (scalep){demand shock};
        \draw (xi.south) |- (scalep.north);
      \end{tikzpicture}

    \item Share equations:
      \begin{equation*}
        s_{jt} = \tikzmarknode{sigma}{\sigma_j}(\mathbf{\delta}_t; \tikzmarknode{theta2}{\theta_2})
      \end{equation*}
      \begin{tikzpicture}[overlay,remember
        picture,>=stealth,nodes={align=left,inner ysep=1pt},<-]
        \path (sigma.south) ++ (0,-0.2em) node[anchor=north east,color=gray]
        (scalep){known share function};
        \draw (sigma.south) |- (scalep.north);

        \path (theta2.south) ++ (0,-0.2em) node[anchor=north west,color=gray]
        (scalep){additional parameters};
        \draw (theta2.south) |- (scalep.north);
      \end{tikzpicture}
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Example Share Functions}
  \begin{itemize}
  \item Logit:
    $$
    s_{jt} = \frac{e^{\delta_{jt}}}{\sum_{k=0^J} e^{\delta_{kt}}}
    $$
  \item Random coefficients logit:
    $$
    s_{jt} = \int \frac{e^{\delta_{jt} + x_{jt} \Sigma
        \nu}}{\sum_{k=0^J} e^{\delta_{kt} + x_{kt} \Sigma \nu} dF(\nu)}
    $$
  \item Nested logit:
    $$
    s_{jt} = \frac{\exp\left(\mu \log\left(\sum_{k \in \mathcal{G}(j)}
            e^{\delta_{kt}}\right)\right)}
    {\sum_{\mathcal{G}} \exp\left(\mu \log\left(\sum_{k \in \mathcal{G}}
          e^{\delta_{kt}}\right)\right)}
    \frac{e^{\delta_{jt}}}{\sum_{k \in \mathcal{G}(j)} e^{\delta_{kt}}}
    $$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Nest(ed) Logit is Invertible}
  \begin{itemize}
  \item Logit inverse share
    $$
    \delta_{jt} = \log(s_{jt}) - \log(s_{0t})
    $$
  \item Mixed logit inverse share
    $$
    \delta_{jt} = (1-\mu)\log(s_{jt}) + \mu \log(\sum_{k \in
      \mathcal{G}(j)} s_{kt}) - \log(s_{0t})
    $$
  \item Can estimate by linear IV
    $$
    \log(s_{jt}/s_{0t}) = x_{jt} \beta  - p_{jt}\alpha + \mu \log(\sum_{k \in
      \mathcal{G}(j)} s_{kt}) + \xi_{jt}
    $$
  \end{itemize}
\end{frame}

\begin{frame}[shrink]{Inverse Product Differentiation Logit}
  \begin{itemize}
  \item $D$ discrete product characteristics define partitions /
    groups
  \item $d(j) \subseteq \{1,...,J\} = $ products in same group as $j$
    in partition $d$
  \item Specify inverse share
    {\small{
    \begin{align*}
    \sigma_{j}^{-1}(\mathbf{s}_t;\theta_2) = \left(1 -
      \sum_{d=1}^D \mu_d \right) \log(s_{jt})  + \sum_{d=1}^D  \mu_d
    \log(\overbrace{s_{d(j)t}}^{\equiv \sum_{k \in d(j)} s_{kt}}) -
      \log(s_{0t})
    \end{align*}
    }}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{IPDL Properties}
  \begin{itemize}
  \item Inverse share has closed form, but share does not
  \item Can be micro founded from model of consumers choosing shares
    (not discrete choice)
    \begin{itemize}
    \item Allows complements
    \end{itemize}
  \item Simulations indicate it ``accommodates similar consumer heterogeneity as the RCL
    model with independent normal random coefficients on dummies for groups defined
    by market segmentation''
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{IPDL Estimation}
  \begin{equation*}
    \log(s_{jt}/s_{0t}) = x_{jt} \beta  - p_{jt}\alpha + \sum_{d=1}^D \mu_d \log\left(\frac{s_{jt}}{s_{d(j)t}}\right) + \xi_{jt}
  \end{equation*}
  \begin{itemize}
  \item Need instruments for price and share terms (at least $1 + D$
    instruments)
  \item
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Simulations}
  \begin{itemize}
  \item See paper
  \end{itemize}
\end{frame}

\section{References}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}


\end{document}

\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}

\title{Demand and supply of differentiated products}
\subtitle{Applications}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{gv2003}}

\begin{frame}\frametitle{\cite{gv2003} ``Competition Among Hospitals''}
  \begin{itemize}
  \item California hospitals
  \item Structural model of demand \& pricing
  \item Merger simulation
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Motivation}
  \begin{itemize}
  \item Many hospital mergers, 900 from 1994-2000 (among 6100
    hospitals)
  \item Profit vs non-profit plays role in antitrust decisions
    \begin{itemize}
    \item 1993-2002: 6 federal anti-trust cases, one initially won
      (but lost on appeal)
    \item Non-profit hospitals have argued that they will not raise
      prices --- court reaction mixed, generally sympathetic
    \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=0.9\textwidth]{hospitalMergers}
  \footnotetext{\href{http://www.nytimes.com/interactive/2013/08/13/business/A-Wave-of-Hospital-Mergers.html}{Source.} }
\end{frame}

\begin{frame}\frametitle{Continued relevance}
  \begin{itemize}
  \item
    \href{https://www.nytimes.com/2015/12/19/business/regulators-tamp-down-on-mergers-of-hospitals.html}{``Regulators
      Tamp Down on Mergers of Hospitals'' NYTimes
      Dec 18, 2015}
  \item
    \href{https://www.nytimes.com/2016/11/21/business/health-care-mergers-under-trump.html}{``The
      Future of Health Care Mergers Under Trump'' NYTimes Nov 20,
      2016}
  \item
    \href{https://www.nytimes.com/2023/01/25/podcasts/the-daily/nonprofit-hospitals-investigation.html}{``How
    Nonprofit Hospitals Put Profits Over Patients'' NYTimes The Daily
    Jan 25, 2023}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Prior literature}
  \begin{itemize}
  \item Structure-conduct-performance
    \begin{itemize}
    \item Regress market performance (price) on market structure
      \[ price_{mt} = \beta concentration_{mt} + \epsilon_{mt} \]
    \item Typically find $\beta>0$
    \item Results mixed when concentration interacted with non-profit
    \end{itemize}
  \item Other contemporaneous (in 2003) structural work
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Utility of consumer $i$ from hospital $j$
    \[ V_{ij} = -\alpha_i^P \underbrace{p_j}_{\text{price}}
      \underbrace{q_i}_{\text{quantity}} + v(q_i,
      \underbrace{R_i}_{\text{consumer}},
      \underbrace{S_j}_{\text{hospital}})
    \]
  \item Aggregate to get demand, $D_j(p)$
  \item Hospital profits:
    \[ \pi_j = p_j D_j(p) - C(D_j(p); Z_j, \zeta_j, W) \]
  \item For-profit pricing: $\max_{p_j} \pi_j $
    \[ p_j = \frac{\partial C_j}{\partial D_j} - \frac{D_j}{\partial
        D_j / \partial p_j} \]
  \item Non-profit pricing: $\max_{p_j} U_j(\pi_j, D_j) s.t. \pi_j
    \geq \pi_L$
    \[  p_j = \frac{\partial C_j}{\partial D_j} - \frac{\partial
        U_j/\partial D_j}{\partial U_j/\partial \pi_j + \mu_j} - \frac{D_j}{\partial
        D_j / \partial p_j} \]
  \item Merged hospital systems maximize sum of profits or utility
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item California OSHPD
    \url{https://www.oshpd.ca.gov/HID/Find-Hospital-Data.html}
  \item annual discharge, annual financial, \& quarterly financial
    data for 1995
  \item 913,660 discharges ($i$) and 374 hospitals
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=0.8\textwidth]{gv-tab2}
\end{frame}

\begin{frame}[shrink]\frametitle{Econometric model}
  \begin{itemize}
  \item Micro-BLP
  \item[Step 1]: use individual choice data to estimate $\delta_j$
    \begin{itemize}
    \item Specification of $V_{ij}$
      \begin{align*}
        V_{ij} = & -\tilde{\alpha}_i^P p_j E[q_i] + \tilde{\alpha}_i^d
                   d_{i \to j} + \tilde{\alpha}_i^{d^2} d_{i\to j}^2 + \sum_k
                   Z_{jk} \tilde{\alpha}_{ik} + \xi_j + \epsilon_{ij}
      \end{align*}
      where
      \begin{align*}
        q_i = & \exp\left(\sum_\ell X_{i\ell} \beta_\ell + \nu_i \right)
         &
            \tilde{\alpha}_i^P = & \exp\left(\alpha_0^P + \sum_\ell X_{i\ell}
                                   \alpha_\ell^p\right) \\
        \tilde{\alpha}_i^d = & \rho + \sum_\ell X_{i\ell} \rho_\ell^X
        &
           \tilde{\alpha}_i^{d^2} = & \rho^2 + \sum_\ell X_{i\ell}
                                      \rho_\ell^{2X}
      \end{align*}
      \begin{align*}
      \tilde{\alpha}_{ik} = & \alpha_0 + \sum_\ell X_{i\ell}
                                \alpha_{\ell k} + \rho_k^Z d_{i \to j} +
                                \rho_k^{2Z} d_{i \to j}^2
      \end{align*}
    \item Rearrange as hospital mean, $\delta_j$, plus deviations
      \begin{align*}
        V_{ij} = & \underbrace{\sum_{k=0}^K Z_{jk} \bar{\alpha}_k +
                   \xi_j}_{= \delta_j} + (X_i - \bar{X}) \alpha Z_j +
                   \text{quadratic distance} + \epsilon_{ij}
      \end{align*}
    \item Estimate by MLE with individual choice data -- gives estimates
      of $\hat{\delta}_j$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Econometric model}
  \begin{itemize}
  \item[Step 2:] estimate $\bar{\alpha}$ (include $\alpha^p$) by 2SLS
    \[ \delta_j  = Z_j \bar{\alpha} + \xi_j \]
    \begin{itemize}
    \item Instruments: wages, exogenous product characteristics,
      consumer characteristics
      \begin{itemize}
      \item Functional form of instruments: from FOC,
        \[ p_j = \frac{\partial C_j}{\partial D_j} - \frac{D_j}{\partial
            D_j / \partial p_j} \]
        use estimate of $D_j$ and $\frac{D_j}{\partial
          D_j / \partial p_j}$ (with $\alpha^p=0$ and $\xi =0$)
      \item $D_j$ depends on coefficients first assume $0$,
        get initial estimates, then redo to get final estimates
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Econometric model}
  \begin{itemize}
  \item[Step 3]: estimate marginal cost function by 2SLS
    \[ P + \left(\Theta \cdot\times \frac{\partial D}{\partial p}
      \right)^{-1} D = \omega_0 + D \omega_D + W  \omega_W + Z
      \omega_Z + \zeta \]
    \begin{itemize}
    \item D endogenous, same instruments as step 2
    \item Steps 2 \& 3 often combined for efficiency, but not
      necessary for consistency
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}\frametitle{}
  \begin{columns}[T] % align columns
    \begin{column}{.48\textwidth}
      \begin{itemize}
      \item Results as expected
      \item How to do inference?
        \begin{itemize}
        \item 913,660 patients
        \item 374 hospitals
        \item 413 parameters
        \end{itemize}
      \end{itemize}
    \end{column}
    \begin{column}{.48\textwidth}
      \includegraphics[width=\linewidth]{gv-tab3}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}\frametitle{First stage}
  \begin{columns}[T] % align columns
    \begin{column}{.38\textwidth}
      \begin{itemize}
      \item This paper was written at same time the weak
        identification literature was developing
      \end{itemize}
    \end{column}
    \begin{column}{.58\textwidth}
      \includegraphics[width=\linewidth]{gv-tabA1}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}\frametitle{Demand}
  \begin{columns}[T] % align columns
    \begin{column}{.38\textwidth}
      \begin{itemize}
      \item Average elasticity -4.85 (2.03)
      \end{itemize}
    \end{column}
    \begin{column}{.58\textwidth}
      \includegraphics[width=\linewidth]{gv-tab4}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}\frametitle{}
  \begin{columns}[T] % align columns
    \begin{column}{.38\textwidth}
      \begin{itemize}
      \item For-profit prices \$248 (187) higher
        \begin{itemize}
        \item Behavioral marginal cost \$592 (329) higher
        \item Markup 1183 (587) for profit, 948 (345) non-profit
        \end{itemize}
      \item First-stage F-stat p-value $<0.01$
      \item What is being assumed about dependence of $\xi_j$ when
        calculating standard errors?
      \end{itemize}
    \end{column}
    \begin{column}{.58\textwidth}
      \includegraphics[width=\linewidth]{gv-tab5}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}\frametitle{Cross-price elasticities}
  \includegraphics[height=0.45\textheight]{gv-fig1}

  \includegraphics[height=0.45\textheight]{gv-fig2}
\end{frame}

\subsection{Merger simulation}

\begin{frame}\frametitle{Merger simulation}
  \begin{itemize}
  \item Tenet \& Ornda merged in 1997
  \item FTC required Tenet divest French Hospital (bought by Vista)
  \item Simulate assuming:
    \begin{itemize}
    \item No divestiture of French
    \item With divestiture of French
    \item No divestiture, but assuming non-profit
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Merger simulation}
  \includegraphics[height=0.45\textheight]{gv-tab6}

  \includegraphics[height=0.45\textheight]{gv-tab7}
\end{frame}

\begin{frame}\frametitle{Merger simulation}
  \includegraphics[width=\textwidth]{gv-tab8}
\end{frame}

\begin{frame}\frametitle{Merger simulation}
  \includegraphics[width=\textwidth]{gv-tab9}
\end{frame}


\begin{frame}
  \frametitle{Related papers}
  \begin{itemize}
  \item \cite{gnt2015}: BLP model of hospital demand, but hospital
    prices set through negotiations with MCOs
  \item \cite{bundorf2012}, \cite{starc2014}: BLP model of insurance demand
  \item \cite{goto2016}: BLP model of flu vaccine demand
  \end{itemize}
\end{frame}

\section{\cite{gnt2015}}

\begin{frame}\frametitle{\cite{gnt2015} ``Mergers When Prices Are
    Negotiated: Evidence from the Hospital Industry''}
  Slides: \url{http://www.u.arizona.edu/~gowrisan/pdf_papers/hospital_merger_negotiated_prices_slides.pdf}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{goolsbee2004}}

\begin{frame}\frametitle{\cite{goolsbee2004}}
  \begin{itemize}
  \item In U.S. in 1996 cable television deregulated
    \begin{itemize}
    \item Hope was that multiple cable operators would enter each area
      and compete
    \item Did not happen, but direct broadcast satellite (DBS)
      companies did enter
    \end{itemize}
  \item Questions:
    \begin{itemize}
    \item How much did competition from DBS lower cable prices?
    \item How much did consumers gain from DBS?
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Consumers $n$, products $j$, markets $m$
  \item Utility:
    \begin{align*}
      U_{nj} = & \alpha_0 p_{mj} + \underbrace{\sum_{g=2}^5 \alpha_g
        p_{mj} d_{gn}}_{\text{income effects}} + \beta^x x_{mj} + z_{n}
      \beta^z_j + (\xi_{mj} + \epsilon_{nj}) \\
      = & \underbrace{\delta_{mj}}_{=\alpha_0 p_{mj} \beta^x x_{mj} + \xi_{mj}} + \sum_{g=2}^5 \alpha_g
      p_{mj} d_{gn}  +  z_{n} \beta^z_j + \epsilon_{nj}
    \end{align*}
  \item $\epsilon_{n} \sim $ multivariate normal with unrestricted
    covariance across $j$ (avoids IIA problem)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Estimation}
  \begin{itemize}
  \item Similar to micro-BLP
  \item Use micro data to estimate $\delta_{mj}$, $\beta^z$
  \item Use estimated $\delta$, instruments for price to estimate
    $\alpha_0$, $\beta^x$
    \begin{itemize}
    \item Uses local tax on cable revenues as instrument for price
    \end{itemize}
  \item Effect of entry, need to know price as function of model
    primitives
    \begin{itemize}
    \item Could fully specify costs and form of competition
    \item Instead estimate reduced form pricing equation,
      \[ p_{mj} = f(\text{observables}) \]
    \end{itemize}
  \item Use pricing equation to predict prices without DBS, calculate
    compensating variation as measure of consumer welfare
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Results: demographics and demand}
  \includegraphics[width=\textwidth]{gp-margEffects}
\end{frame}

\begin{frame}
  \frametitle{Results: demand elasticities}
  \includegraphics[height=0.9\textheight]{gp-elasticities}
\end{frame}

\begin{frame}\frametitle{Results: welfare}
  \begin{itemize}
  \item No DBS would increase cable prices by \$4.17 per month
  \item Monthly consumer gains from DBS:
    \begin{itemize}
    \item \$10.57 in consumer surplus for DBS subscribers
    \item \$4.17 per month for cable subscribers from lower prices
    \item \$1 per month for cable subscribers from increased quality
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{fan2012}}

\begin{frame}\frametitle{\cite{fan2012}}
  \begin{itemize}
  \item Question: effect of mergers on product characteristics
    \begin{itemize}
    \item Merged firm will generally produce different product(s) than
      two separate firms
    \item Need to endogenize choice of product characteristics
    \end{itemize}
  \item Setting: U.S. daily newspapers
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item BLP style demand with endogenous price and other product
    characteristics, $x_{jt} =$ quality index, local news ratio
    (share of local news staff), news variety (HHI of staff shares
    across sections)
  \item Demand for advertising:
    \[ \log a_{jt} = \eta + \underbrace{\lambda_0 \log
      H_{jt}}_{\text{market size}} + \underbrace{\lambda_1 \log
      q_{jt}}_{\text{circulation}} + \underbrace{\lambda_2 \log
      r_jt}_{\text{advertising price}} + \iota_{jt} \]
    Note: no cross price elasticities, i.e.\ no competition
  \item Variable profits:
    \[ \pi_j^{II} = (p_j q_j - ac_j^{(q)} q_j) + (r_j a_j - mc_j^{(a)}
    a_j) + (\mu_1 q_j + \mu_2/2 q_j^2) \]
    where
    \begin{itemize}
    \item $ac_j^{(q)}$ is average cost of producing quantity $q$ and
      has some parametric form
    \item $mc_j^{(q)}$ is marginal cost of advertising sales and has
      some parametric form
    \end{itemize}
  \item Definition of market:
    \begin{itemize}
    \item Newspapers compete in many overlapping local markets, so
      local paper in Portland, Maine potentially competes with local
      paper in Portland, Oregon
    \item Define market for newspaper $j$ as the counties where 85\% of
      circulation for newspaper $j$ is contained
    % \item It is unclear what the author intends this to mean. It
    %   clearly means that demand model assumes consumers do not buy
    %   newspapers from outside their market. Does
    %   it also mean that in equilibrium when firm $j$ calculates best
    %   responses of other firms to find $\frac{\partial
    %     p_j^\ast}{\partial x_k}$ it only takes into account behavior
    %   of firms in overlapping markets? Does it mean that we define
    %   markets this way and then assume they are independent when
    %   calculating standard errors?
    \end{itemize}
  \item Equilibrium: solving backward
    \begin{itemize}
    \item[3] Given $Q_{jt}$, advertising rate chosen to equalize marginal
      cost and marginal revenue of advertising
      \begin{itemize}
      \item No competition in advertising rates
      \end{itemize}
    \item[2] Given characteristics, prices chosen in simultaneous Nash
      equilibrium
    \item[1] Characteristics chosen simultaneous Nash equilibrium
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Data}
  \begin{itemize}
  \item 1997-2005, market level data on newspaper quantity, price, and
    characteristics, and
    advertising quantity and price
  \item County demographics (education, age, income, urbanization)
  \item 5843 newspaper-year observations of newspaper characteristics
    and prices
  \item 11203 newspaper-county-year observations of quantity
  \item 422 newspaper-year also with advertising information
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Estimation}
  \begin{itemize}
  \item Moment conditions
    \begin{itemize}
    \item Consumer demand: $\Er[\xi_{jt} | w_{jt}] = 0$
    \item Advertiser demand: $\Er[\iota_{jt} | w_{jt}] = 0$
    \item Advertising first order condition: $\Er[\zeta_{jt} | w_{jt}]
      = 0$
    \item Price first order condition: $\Er[\omega_{jt} | w_{jt}] = 0$
    \item Characteristics first order condition: $\Er[\nu_{jt} |
      w_{jt}] = 0$
    \end{itemize}
  \item Instruments from overlapping markets
    \begin{itemize}
    \item Suppose newspaper A is only in county 1, but newspaper B is
      in counties 1 and 2
    \item Demographics in county 2 affect prices and characteristics
      of newspaper B, which in turns affects newspaper A's price and
      characteristics
    \item Use demographics in county 2 to instrument for newspaper A's
      price
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Results}
  \begin{itemize}
  \item Parameter estimates
  \item Simulation of merger of {\slshape{Minneapolis Star Tribune}} and
    {\slshape{St. Paul Pioneer}}
    \begin{itemize}
    \item In reality: owner of {\slshape Pioneer} bought {\slshape
        Star}, DOJ filed antitrust complaint 3 months later, owner of
      {\slshape Pioneer} sold {\slshape Star} 2 months later
    \end{itemize}
  \item Simulate with and without characteristic adjustment, compare
    results
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fan-starMarket}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fan-pioneerMarket}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fan-cf1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fan-cf2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{fan-welfare}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{gandhi2014}}

\begin{frame}[allowframebreaks]
  \frametitle{``Demand Estimation with Scanner Data: Revisiting the Loss-Leader
    Hypothesis'' gandhi2014}
  \begin{itemize}
  \item Motivation:
    \begin{itemize}
    \item Frequent price discounts (sales) in scanner data
    \item \cite{chevalier2003}: loss-leader model implies prices can
      fall when demand increases because of promotional effect;
      evidence that prices fall during seasonal peak
      demand (e.g. tuna during Lent)
    \item \cite{nh2006}: prices could also fall during high demand
      because elasticity of demand could increase (if buying more quantity,
      makes more sense to search for lower price)
    \end{itemize}
  \item Methodology: estimate BLP demand model, see if demand
    elasticity is different during seasonal peak
  \item Data: Dominick's scanner data (grocery store)
  \item Difficulty: many product categories have hundreds of products,
    so many products have $0$ observed share in some markets
  \item Solution: optimally shift observed shares away from $0$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Data}
  \begin{itemize}
  \item Dominick's scanner data (grocery store)
  \item Estimate separately for each product category
  \item Market $=$ store $\times$ week (all stores in Chicago,
    1989-1997, gives $\approx 400,000$ markets)
  \item Many products in each category (Table 4) --- 283 cheese, 537
    soft drinks, 820 shampoos, 118 canned tuna, etc
  \item Sales concentrated among top 20\% of products in each category
    (Table 4) --- approximately 80\%
  \item High percent (20-80) of products with $0$ sales (Table 4) ---
    35\% for canned tuna
  \item Distribution of sales approximately follows Zipf's law ---
    $k$th most popular product has sales proportional to $1/k^s$ for
    some $s>1$
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model and zero share problem}
  \begin{itemize}
  \item BLP setup (but empirical results are without random
    coefficients)
  \item Zero share problem, $0 = \sigma(\delta)$ implies $\delta =
    -\infty$
    \begin{itemize}
    \item Cannot just drop goods with $0$ share because that creates
      selection ($0$ share implies low $\xi$)
    \end{itemize}
  \item Laplace: when observe zero share, add 1 sale to each product
    \[ s_{jt}^LS = \frac{n_t s_{jt} + 1}{n_t +J_t + 1} \]
    Optimal Bayes estimator under uniform prior
  \item Could use Laplace transformation here, but what is optimal for
    estimating shares might not be optimal for estimating demand
  \item Choose transformation $\pi^*(s_t,n_t)$ that minimizes
    asymptotic (slowly growing $n_t$) MSE
    \[ \pi^*(s_t,n_t) = \sigma\left( \Er\left[\sigma^{-1}(\pi_t) |
        s_t, n_t \right] \right) \]
  \item $F_{\pi_t|s_t,n_t}$ unknown, show that if assume Zipf's law,
    can estimate it
  \item Use estimated $F_{\pi_t|s_t,n_t}$ to estimate optimal
    transformation
  \item Estimate rest of model using BLP with transformed shares
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}\frametitle{Zero share correction reduces bias}
  \begin{table}
    \caption{Table 6: Average Bias for a Repeated Simulation}
    \begin{tabular}{c cccc}
      Fraction of Zeros & 16.48\% & 36.90\% & 49.19\% & 63.70\% \\
      Using Empirical Share & .3833 & .6589 & .7965 & .9424 \\
      Using Laplace Rule & .2546 & .5394 & .6978 & .8476 \\
      Inverse Demand EB & -.0798 & -.0924 & -.0066 & .0362 \\
    \end{tabular}
    Note: $T = 500$, $n = 10,000$, Number of Repetitions $=1,000$.
  \end{table}
\end{frame}


\begin{frame}
  \includegraphics[width=\linewidth]{gandhiTable7}
\end{frame}

\begin{frame}
  \includegraphics[width=\linewidth]{gandhiTable8}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{References}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}

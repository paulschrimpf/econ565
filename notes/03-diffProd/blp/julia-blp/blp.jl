nChar = 2
nCost = 2

type Data
    productCharacteristics::Matrix # first characteristic should be
                                   # price
    #costShifters::Matrix
    shares::Matrix
    instruments::Matrix
    xi::Vector
    function Data(numberProducts::Int, numberMarkets::Int,
                  numberIV::Int, pm::Parameters )
        # Simulates data
        # Prices are created artificially and do not satisfy firm
        # first order condition
        instruments = randn(numberMarkets,numberIV)
        xi = randn(numberMarkets,numberProducts)
        price = sum(instruments,2)+xi+randn(numberMarkets,1)
        price[price<1] = exp(price[price<1])
        productCharacteristics = zeros(numberMarkets,nChar)
        productCharacteristics(:,1) = price
        productCharacteristics(:,2:(nChar)) = instruments(:,1:(nChar-1))

        ## simulate to find shares
        sims = 10000
        delta = productCharacteristics*pm.thetaBar
        thetai = randn(sims,nChar)*chol(pm.sigma,:U)
        for t=1:numberMarkets
            shares[t,] = marketShares(delta + xi[t,], productCharacteristics, thetai)
        end
        new(productCharacteristics, # costShifters,
            shares, instruments, xi)
    end
end

type Parameters
    thetaBar::Vector # tastes for characteristics
    thetaC::Matrix   # cost coefficients
    sigma::Matrix    # taste heterogeneity ~ N(0,sigma)

    function Parameters(vec::Vector)
        j = 1
        thetaBar = vec[j:(j+nChar)]
        j += nChar

        #thetaC = vec[j:(j+nCost)]
        #j += nCost

        # sc = vec[j:+nChar*(nChar+1)/2))]
        # Assume sigma diagonal
        sigma = exp(diag(vec[j:(j+nChar)]))
        new(thetaBar,thetaC,sigma)
    end
end


#  Given product dummies, delta, product characteristics x, draws from
# characteristic test distribution, thetai, returns a vector of
# market shares for each product
function marketShares(delta::Vector, x::Matrix, thetai::Matrix)
    edtn = exp(delta + nu*theta)
    return(mean(
                edtn./
                (1+sum(edtn,2)*ones(1,size(edtn,2)))
                ,1))
end
function gradientShares(delta::Vector, x::Matrix, thetai::Matrix)
    edtn = exp(delta + nu*theta)
    shareij = edtn./ (1+sum(edtn,2)*ones(1,size(edtn,2)))
    return(mean(shareij - shareij.^2,1))
end

# Given product dummies delta, product average tastes, thetaBar, and
# product characteristics, x, returns the unobserved product
# attribute, xi
function unobservedCharacteristic(delta::Vector, thetaBar::Vector, x::Matrix)
    return(delta - x*thetaBar)
end

# Given characteristics x, product average tastes, thetaBar, and
# product taste distribution, thetai, cost coefficeint thetaC, and
# marginal cost shifters r, return the unobserved cost shock
function mcShock(x::Matrix, thetaBar::Vector, thetai::Matrix,
                 thetaC::Matrix, r::Matrix)
    delta = x*thetaBar
    share = marketShares(delta, x, thetai)
    # We assume that x[,1] is price and each firm only sells one product
    dShare = diagm(gradientShares(delta,x,thetai).*thetaBar[1])
    return(log(x[,1] - dShare \ share) - r*thetaC)
end

nsim = 100
nx = 2
nu = randn(nsim,nx)
function thetai(sigma::Matrix)
    return(nu*chol(sigma,:U))
end

# Use contraction to solve for delta(theta)
contractionTolerance = 1e-10
function deltaContract(thetaBar::Vector, share::Matrix, x::Matrix, thetai::Matrix)
    del = zeros(size(share))
    delOld = ones(size(share))
    iterations = 0
    while norm(del-delOld,Inf) / max(norm(del,Inf),1)  > contractionTolerance
        delOld = del
        del = del + log(share) - log(marketShares(del,x,thetai))
        iterations = iterations + 1
    end
    print("Contraction took ",iterations," iterations")
    return(del)
end


function moments(pm::Parameters, data::Data, delta::Matrix)
    xiHat = zeros(size(delta))
    for t=1:size(delta,1)
        xiHat[t,] = unobservedCharacteristic(delta[t,], pm.thetaBar,
                                             data.productCharacteristics)
    end
    m = zeros(size(data.shares,2),size(data.instruments,2))
    # m is number of products X number of instruments
    m = xiHat' * data.instruments / size(data.shares,1);
    return(mean(m,1)') # average across characteristics
end



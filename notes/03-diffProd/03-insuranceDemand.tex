\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}

\title{Models of insurance demand}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews: \cite{efl2010}, \cite{ef2011}, \cite{ght2015} sections
    6 \& 7, \cite{einav2021}, \cite{handel2021}
  \item BLP models of insurance demand: \cite{bundorf2012}, \cite{starc2014}
  \item Expected utility models of insurance demand: \cite{cardon2001}, \cite{efrsc2013}
  \item Behavioral: \cite{handel2013}, \cite{barseghyan2013}, \cite{handel2015}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{starc2014}}

\begin{frame}[allowframebreaks]
  \frametitle{\cite{starc2014}}
  \begin{itemize}
  \item Health insurance industry concentrated
  \item Mergers often blocked by antitrust
    \begin{itemize}
    \item
      \href{http://www.politico.com/story/2017/01/judge-block-aetna-humana-merger-234043}
      {Aetna \& Humana}
    \item
      \href{http://fortune.com/2017/01/19/anthem-cigna-merger-block/}
      {Anthem \& Cigna}
    \end{itemize}
  \item What are the sources and consequences of insurer market power?
  \item Medigap insurance
  \item Estimate model of demand and firm pricing
  \item Results
    \begin{itemize}
    \item Low demand elasticity, strong brand preferences
    \item Average cost pricing would decrease premiums by 17\%
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Medigap}

\begin{frame}[allowframebreaks] \frametitle{Medigap}
  \begin{itemize}
  \item Medicare has high deductibles \& copays
    \begin{itemize}
    \item Part A (hospitalizatoin) deductible $\approx$\$1000
    \item Part B (outpatient) copays 20\%, no maximum
    \end{itemize}
  \item Medigap provides extra coverage
  \item Set of plans regulated (price [and branding] is only
    characteristic chosen by firms)
  \item Open-enrollment period (within 6 months of enrolling in
    Medicare) price only based on age, gender, state, \& smoking
  \item Minimum Loss Ratio: at least 65\% of premiums must be used to
    cover claims
  \item Taxes vary within consumer state based on insurer state
  \item Data:
    \begin{itemize}
    \item NAIC: insurer premiums, quantities, claims
    \item \href{https://www.cms.gov/Research-Statistics-Data-and-Systems/Research/MCBS/}{MCBS}: individual demographics, whether have any Medigap (but
      not which insurer \& plan), claims
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab1}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab2}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab3}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab4}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-fig1}
\end{frame}

\subsection{Model}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Firm pricing:
    \begin{align*}
      \max_{p_{jfm}} & \sum_j \left[ \left( p_{jfm} -
                       \underbrace{\gamma_{jfm}(\mathbf{p}_m)}_{\text{claims}} -
                       \underbrace{a_{jfm}(\mathbf{p_m})}_{\text{commissions}} \right)
                       s_{jfm}(\mathbf{p}_m) M_m \right]  \\
      s.t. & \gamma_{jgm}(\mathbf{p}_m) \geq 0.65 p_{jfm}
    \end{align*}
  \item Demand
    \begin{itemize}
    \item Consumer valuations:
      \[ v_{ijm} = x_j \beta_1 + b_f \beta_2 + x_m \beta_3 + \xi_{jfm}
        + \alpha p_{jfm} + \mu_{ijfm} + \epsilon_{ijfm} \]
    \item $\mu_{ijfm}=$ interactions between $x_j$ and $(z_i, \omega_i)$
    \item Claims:
      \[ \gamma_{ijfm} = \theta_0 + x_j \theta_1 + \underbrace{\omega_i \theta_2}_{\text{income}} +
        \underbrace{z_i \theta_3}_{\text{SRH}} + \varepsilon_{jm} +
        \eta_i \]
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Demand estimation moments:
    \begin{itemize}
    \item BLP market level data: $\Er[\xi_{jfm} | \text{instruments}]
      = 0$
      \begin{itemize}
      \item Retaliatory taxes
      \item Average $p_{jf(-m)t}$
      \end{itemize}
    \item Expected claims given plan:
      \[ \Er[\gamma_{ifm}|J=j] = \theta_0 + x_j \theta_1 + \Er[\omega_i|J=j] \theta_2
        \Er[z_i |J=j] \theta_3 + \varepsilon_{jm} \]
    \item Individual P(any Medigap), premium
    \end{itemize}
  \item Pricing FOC used to estimate marginal costs (commissions)
    \begin{itemize}
    \item Equality if MLR slack, inequality if binding or violated
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab5}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab6}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab7}
\end{frame}

\begin{frame}\frametitle{Consequences of market power}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab8}
\end{frame}

\begin{frame}\frametitle{Consequences of market power}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab9}
\end{frame}

\begin{frame} \frametitle{Source of market power}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tabA7}

    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tabA8}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{saltzman2019}}
\begin{frame}\frametitle{\cite{saltzman2019} ``Demand for health
    insurance: Evidence from the California and Washington ACA
    exchanges''}
  \begin{itemize}
  \item Estimate insurance demand
  \item Simulate impact of subsidies, mandate penalty, and mandate
    existence
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{ACA Exchanges}
  \begin{itemize}
  \item Regulated state insurance exchanges
  \item Plan tiers based on expected percentage of health care costs
    covered
    \begin{itemize}
    \item Bronze 60\%, Silver 70\%, Gold 80\%, Platinum 90\%
    \item In California, plans standardized, elsewhere insurers can
      choose deductible, copay, etc
    \end{itemize}
  \item Restrictions on price discrimination
    \begin{itemize}
    \item Age: 64 year-old at most 3$\times$ 21 year old
    \item Smoking: 50\% more than non (prohibited in California)
    \item Same price within geographic areas defined by states
    \end{itemize}
  \item Mandatory to have some health insurance
    \begin{itemize}
    \item Penalty: increased from $\max\{\$95, 1\% \mathrm{income}\}$
      to $\max\{\$625, 2.5\% \mathrm{income}\}$ from 2014-2018, then
      $\$0$ after
    \item Some exemptions
    \end{itemize}
  \item Premium subsidies if income less than $400\%$ of federal
    poverty level (price after subsidy is a max percentage of income
    ranging from 2\%-9.5\%)
  \end{itemize}
\end{frame}

\begin{frame}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab11}
\end{frame}

\begin{frame}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-ca}
\end{frame}

\begin{frame}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-or}
\end{frame}

\begin{frame}[shrink]\frametitle{Model}
  \begin{align*}
    U_{ij} = & \overbrace{\alpha_i \underbrace{p_{ij}}_{\mathrm{premium}}  +
               \underbrace{x_j}_{\text{plan characteristics}} \beta +
               \underbrace{d_i}_{\text{household characteristics}} \varphi + \xi_j}^{V_{ij}} +
               \epsilon_{ij} \\
    \underbrace{U_{i0}}_{\text{uninsured}} = & \alpha'_i
                                               \underbrace{\rho_i}_{\text{penalty}}
                                               + \epsilon_{i0}
  \end{align*}
  \begin{itemize}
  \item $d_i$ includes $d_{mi}=$ whether $i$ subject to mandate, with
    coefficient $\varphi_m$
  \item Nested logit for $\epsilon_i$ with all plans in one nest, and
    uninsured in other ($\epsilon_{ij}$ are correlated with one
    another for $j\geq 1$ with correlation $\sqrt{1-\lambda}$)
    \begin{align*}
      \Pr(i \text{ chooses } j) = \frac{e^{V_{ij}/\lambda} \left(
      \sum_j e^{V_{ij}/\lambda} \right)^{\lambda - 1} }{ 1 +
      \left(\sum_j e^{V_{ij}/\lambda} \right)^{\lambda - 1}}
    \end{align*}
    \footnote{This is the equation in the paper, but I think it's
      incorrect. It would be fine if $V_{i0} = 0$, but not with
      $V_{i0} = \alpha'_i \rho_i$.}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab2}
\end{frame}

\begin{frame}\frametitle{Data}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab3}
\end{frame}

\begin{frame}\frametitle{Results}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab4}
\end{frame}

\begin{frame}\frametitle{Results}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab5}
\end{frame}

\begin{frame}\frametitle{Results}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab6}
\end{frame}

\begin{frame}\frametitle{Results}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {saltzman-tab10}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{tebaldi2023}}

\begin{frame}
  \frametitle{\cite{tebaldi2023} ``Nonparametric estimates of demand
    in the California health insurance exchanges''}
  \begin{itemize}
  \item How much do logit / mixed logit assumptions influence demand
    estimates?
  \item Setting: California ACA exchange
  \item Nonparametric partially identified demand estimates
  \end{itemize}
\end{frame}

\subsection{Setting}

\begin{frame}\frametitle{Covered California}
  \begin{itemize}
  \item 19 rating regions (premiums vary across regions and are
    constant within)
  \item 4 tiers of insurance coverage
  \item Region, tier, \& age specific premium $=$ insurer chosen
    region, tier premium $\times$ federal age adjustment
  \item Premium subsidies and cost-sharing reductions for low income
    individuals
  \item Mandated participation with tax penalty (penalty repealed in 2017)
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Covered California}
  \includegraphics[width=\textwidth]{tty-tab1}
\end{frame}

\begin{frame}\frametitle{Premium Variation}
  \includegraphics[width=\textwidth]{tty-fig2}
\end{frame}

\subsection{Model}

\begin{frame}[shrink]
  \frametitle{Model}
  \begin{itemize}
  \item Individual $i$, plans $j \in \{0, 1, ..., J\}$
  \item Valuations $V_{ij}$ with premiums $P_{ij}$, with utility
    additively separable in premium
    $$
    \max_{j} V_{ij} - P_{ij}
    $$
  \item Want to recover $f(V_{ij} | P_{ij}, M_i, X_i)$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Price Variation}
  \begin{itemize}
  \item Premiums depend on market, $M_i$, and individual
    characteristics, $X_i$ (age \& income)
  \item Price variation within market will be used in estimation
  \item Price variation within market not present in typical demand
    estimation
  \item Appendix discusses modifications to use when there is not
    within market price variation
  \end{itemize}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Target Parameters}
  \begin{itemize}
  \item Density of valuation given observables $f(v|p, m, x)$
  \item Target parameters are functionals of this density,
    $\theta: \mathcal{F} \to \R^{d_\theta}$, e.g.
    \begin{itemize}
    \item Fraction that choose plan $j$ if premiums were $p^\ast$
      {\footnotesize{
      $$
      P(j|p^\ast, m, x) = \int \mathbf{1}\{v_j - p_j^\ast \geq v_k - p_k^\ast \,\forall k
      \}f(v|m,x)dv
      $$
      }}
    \item Change in consumer surplus from changing $p$ to $p^\ast$
      {\footnotesize{
      $$
      \Delta CS(p^\ast | m, x) = \int \max_{j} (v_j - p_j^\ast)
      f(v|m,x) dv  - \int \max_{j} (v_j - p_j) f(v|m,x) dv
      $$
      }}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Assumptions}
  \begin{itemize}
  \item $W_i, Z_i$ subvectors of $M_i, X_i$
    \begin{itemize}
    \item In application $W_i$ is $M_i$ and course age and income
      bins, $Z_i$ is variation in age and income within bins
    \end{itemize}
  \item $Z_i$ is instrument
    \begin{itemize}
    \item Exogenous:
      \begin{equation}
        f_{V|W,Z}(v|w,z) = f_{V|W,Z}(v|w,z') \label{exo}
      \end{equation}
    \item No relevance or rank assumption required, but size of
      identified will depend on instrument variation and relevance
    \end{itemize}
  \item Support restrictions
    \begin{equation}
      \int_{\mathcal{V}^\text{\textbullet}(w)} f_{V|W,Z}(v|w,z) dz = 1 \label{sup}
    \end{equation}
    e.g. at same prices, consumers prefer higher tier plan
    $\mathcal{V}^\text{\textbullet}(w) = \{v : v_4 \geq v_1 \}$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Identified Set}
  \begin{itemize}
  \item Define $\mathcal{V}_j(p) = \{v: v_j - p_j \geq v_k - p_k \, \forall
    k\}$
  \item Observed shares $=$ model shares:
    \begin{equation}
      s_j(p,m,x;f) = \int_{\mathcal{V}_j(p)} f(v|p,m,x) dv
      \label{share}
    \end{equation}
  \item Identified set $\mathcal{F}^\ast \equiv \{ f \in \mathcal{F} : \ref{exo},
    \ref{sup}, \ref{share} \}$
  \item Identified set for target parameter $\Theta^\ast \equiv
    \{\theta(f) : f \in \mathcal{F}^\ast \}$
  \item Goal : characterize and then estimate $\Theta^\ast$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-fig1}
\end{frame}

\begin{frame}[shrink]\frametitle{Identified Set}
  \begin{itemize}
  \item Observe $p^a$, counterfactual $p^\ast$, want $\theta(f) = $
    share of good 2 at $p^\ast$
  \item Partition support of $v$ into minimal relevant partition (c)
  \item We observe
    \begin{align*}
      s_0(m, p^a) = & \int_{\mathcal{V}_1} f(v|m, p^a) dv \\
      s_1(m, p^a) = & \int_{\mathcal{V}_5 \cup \mathcal{V}_6} f(v|m, p^a) dv \\
      s_2(m, p^a) = & \int_{\mathcal{V}_2 \cup \mathcal{V}_3 \cup
                      \mathcal{V}_4} f(v|m, p^a) dv
    \end{align*}
  \item Assume $p$ exogenous, so $f(v|m, p^a) = f(v|m,p^\ast) =
    f(v|m)$ (i.e. $Z = p$)
  \item Let $\phi_\ell = \int_{\mathcal{V}_\ell} f(v|m) dv$, note that
    $s_2(m,p^\ast) = \phi_3$ is the parameter of interest
  \item Upper bound: $\max_\phi \phi_3$ s.t. observed shares
    \begin{align*}
      t^\ast_{ub} = \max_{\phi} \phi_3 & \text{ s.t. } & \phi_1 = & s_0(m,p^a) \\
                                       && \phi_2 + \phi_3 + \phi_4 = & s_2(m,p^a) \\
                                       && \phi_5 + \phi_6  = & s_1(m,p^a) \\
                                       && \phi_\ell & \geq \; \forall \ell
    \end{align*}
  \item $t^\ast_{lb} = \min \phi_3$ gives lower bound, paper shows
    $[t^\ast_{lb}, t^\ast_{ub}]$ is the identified set
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Notation:
    \begin{itemize}
    \item $\mathbb{V}$ is minimal relevant partition
    \item $\mathbb{V}_j(p)$ is subset of $\mathbb{V}$ that
      rationalizes choice $j$ given prices $p$
    \item $\phi(\mathcal{V}|m,x) = \int_{\mathcal{V}} f(v|m,x) dv$
    \item $\phi(\mathcal{V}|w,z) = \int_{\mathcal{V}} f(v|w,z) dv$
    \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Just replace unknown population shares with observed market
    shares:
    {\footnotesize
    \begin{align*}
      \min_{\phi \geq 0} \bar{\theta}(\phi) & \text{s.t.} & \hat{s}_j(m,x) & =
                                                     \sum_{\mathcal{V}
                                                     \in
                                                     \mathbb{V}_j(p(m,x)}
                                                     \phi(\mathcal{V}|m,x)
      \, \forall j \\
      && \phi_{\mathbb{V}|WZ}(\mathcal{V}|w,z) & =
                                                 \phi_{\mathbb{V}|WZ}(\mathcal{V}|w,z')
                                                 \, \forall z,z',w,
                                                 \mathcal{V} \\
      && \sum_{\mathcal{V} \in \mathbb{V}^{\text{\textbullet}}(w)}
         \phi_{\mathbb{V}|WZ}(\mathcal{V}|w,z) = 1 \, \forall w, z
    \end{align*}
    }
    but might have no solution\footnote{I think this is the reason,
      but the paper says
      ``The purpose of this tuning parameter is to  smooth  out potential
      discontinuities  caused by set  convergence.''}
  \item Define:
    {\footnotesize
    \begin{align*}
      \hat{Q}(\phi) = \sum_{j,m,x} \hat{\mathbb{P}}(m,x) \abs{\hat{s}_j(m,x) -
      \sum_{\mathcal{V} \in \mathbb{V}_j(p(m,x))}
      \phi(\mathcal{V}|m,x) }
    \end{align*}
    }
    and $\hat{Q}^\ast = \min_\phi \hat{Q}(\phi)$
  \item Relax problem to
    {\footnotesize
    \begin{align*}
      \hat{t}^\ast_{lb} = \min_{\phi \geq 0} \bar{\theta}(\phi) & \text{s.t.} &
                                                            \hat{Q}(\phi)
                                                            & \leq
                                                            \hat{Q}^\ast
                                                            + \eta
                                                              \\
      && \phi_{\mathbb{V}|WZ}(\mathcal{V}|w,z) & =
                                                 \phi_{\mathbb{V}|WZ}(\mathcal{V}|w,z')
                                                 \, \forall z,z',w,
                                                 \mathcal{V} \\
      && \sum_{\mathcal{V} \in \mathbb{V}^{\text{\textbullet}}(w)}
         \phi_{\mathbb{V}|WZ}(\mathcal{V}|w,z) & = 1 \, \forall w, z
    \end{align*}
  }
  \item Inference based on \cite{deb2021}
  \end{itemize}
\end{frame}

\subsection{Application}

\begin{frame}\frametitle{Identifying Assumptions}
  \begin{itemize}
  \item California ACA pricing
    \begin{itemize}
    \item 19 rating regions (premiums vary across regions and are
      constant within)
    \item 4 tiers of insurance coverage
    \item Region, tier, \& age specific premium $=$ insurer chosen
      region, tier premium $\times$ federal age adjustment
    \item Premium subsidies and cost-sharing reductions for low income
      individuals
    \end{itemize}
  \item So price variation within a region due to age and income
    should be exogenous to demand shocks
  \item Assume that preferences for insurance do not depend on age or
    income within ``coarse bins'' (defined by 5 years and 50
    percentage points of FPL)
  \item Support restriction: at equal prices, consumers prefer plan
    with more coverage
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-tab2}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-tab3}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-tab4}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-fig3}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-tab5}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-fig4}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-tab6}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-fig5ab}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-fig5c}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-fig6}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{tty-tab7}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{efrsc2013}}

\begin{frame}
  \frametitle{\cite{efrsc2013}}
  \url{http://faculty.arts.ubc.ca/pschrimpf/565/efrsc-slides_2012_05_18.pdf}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
\input{../slideHeader}
\providecommand{\folder}{medD}
\providecommand{\omth}{\frac{\omega}{\theta}} 
\providecommand{\clm}{{\mathrm{claim}}}
\providecommand{\jm}{{\iota}}
\providecommand{\type}{{m}}
\providecommand{\typeSet}{{\mathcal{M}}}
\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Single agent dynamic models --- applications in health economics}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}


\begin{frame}
  \tableofcontents  
\end{frame}

\section{\cite{fang2015}}

\begin{frame}\frametitle{\cite{fang2015}}
  \begin{center}
    \textbf{``Estimating dynamic discrete choice models with hyperbolic
      discounting, with an application to mammography decisions''}
  \end{center}
  \begin{itemize}
  %\item Last week's presentations --- choice of discount factor?
  \item This paper: identification of discount factor (and more)
    \begin{itemize}
    \item Key restriction: variables that shift expectations, but do
      not enter current payoff
    \end{itemize}
  \item Find substantial present bias and naivety in mammography
    screening 
  \end{itemize}  
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Dynamic discrete choice, $i \in \{0, ..., I\}$
    \[ u_i(x,{\epsilon})  = u_i(x) + \epsilon_i \]
  \item Infinite time horizon, hyperbolic discounting
    \[ U_t(u_t, u_{t+1}, ...) = u_t + \beta \sum_{k=t+1}^\infty
      \delta^{k-1} u_k \]
    \begin{itemize}
    \item Present bias $\equiv \beta$
    \item Discount factor $\equiv \delta$
    \end{itemize}
  \item Time $t$ self believes that future selves will make choices
    with present bias $\tilde{\beta} \in [\beta,1]$ and discounting
    $\delta$
    \begin{itemize}
    \item Completely naive if $\tilde{\beta}=1$
    \item Sophisticated if $\tilde{\beta} = \beta$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Continuation strategy profile
    ${\sigma}_t^+ = \{\sigma_{k}\}_{k=t^\infty}$,
  \item Continuation utility:
    \[
      V_t(x_t,{\epsilon}_t; {\sigma}_t^+) =
      u_{\sigma_t(x_t,{\epsilon}_t)}(x_t,\epsilon_{\sigma_t(x_t,{\epsilon}_t)t})
        + \delta \Er\left[V_{t+1}(x_{t+1}, {\epsilon}_{t+1};
        {\sigma}_{t+1}^+) | x_t,
        \sigma_t(x_t,{\epsilon}_t)\right]
    \]
  \item Perceived continuation strategy
    \[
      \tilde{\sigma}_t(x_t,{\epsilon_t}) = \argmax_i u_{i}(x_t,\epsilon_{it})
        + \tilde{\beta} \delta \Er\left[V_{t+1}(x_{t+1}, {\epsilon}_{t+1};
        {\tilde{\sigma}}_{t+1}^+) | x_t,
        i \right]
    \]
  \item Perception-perfect strategy with partial naivety
    \[
      \sigma^*_t(x_t,{\epsilon_t}) = \argmax_i u_{i}(x_t,\epsilon_{it})
        + \beta \delta \Er\left[V_{t+1}(x_{t+1}, {\epsilon}_{t+1};
        {\tilde{\sigma}}_{t+1}^+) | x_t,
        i \right]
    \]
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Identification}
  \begin{itemize}
  \item Assumptions:
    \begin{itemize}
    \item Stationarity
    \item Conditional independance:
      $\pi(x_{t+1},\epsilon_{t+1}|x_t,\epsilon_t,d_t) = q(\epsilon_t)
      \pi(x_{t+1}|x_{t},d_t)$
    \item $\epsilon_t$ iid extreme value
    \end{itemize}
  \item Perceived long-run choice-specific value function:
    \[ V_i(x) = u_i(x) + \delta \sum_{x'} V(x') \pi(x'|x,i) \]
    where   $V(x) =$ expected value from following perceived continuation
    strategy
    \[ V(x) = \Er[V_{\tilde{\sigma}(x,\epsilon)}(x) +
      \epsilon_{\tilde{\sigma}(x,\epsilon)}] \]
  \item Current choice-specific value function
    \[ W_i(x) = u_i(x) + \beta \delta \sum_{x'} V(x') \pi(x'|x,i) \]
  \item Naively perceived next-period choice-specific value function
    \[ Z_i(x) = u_i(x) + \tilde{\beta} \delta \sum_{x'} V(x')
      \pi(x'|x,i) \]
  \item Definition of $W$, $Z$
    \[ Z_i(x) - u_i(x) = \frac{\tilde{\beta}}{\beta} [W_i(x) -
      u_i(x)] \]
  \item Distribution of $\epsilon$ implies
    \[ P_i(x) = \frac{e^{W_i(x)}}{\sum_j e^{W_j(x)}} \]
    and
    \[ V(x) = \log\left(\sum_i e^{Z_i(x)} \right) + (1-\tilde{\beta})
      \delta \sum_j \frac{e^{Z_j(x)}}{\sum_k e^{Z_k(x)}} \sum_{x'}
      V(x')\pi(x'|x,j) \]
  \item Given $\delta, \beta, \tilde{\beta}$, variant of usual
    inversion of choice probabilities identifies $u_i(x) - u_0(x)$
  \item Assumption: $\exists x_1 \neq x_2$ such that (i) $u_i(x_1) =
    u_i(x_2) \forall i$ and $\pi(x'|x_1,i) \neq \pi(x'|x_2,i)$ for
    some $i$ and $(I+1) \times |\mathcal{X}| \geq 4$
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Identification of $\delta, \beta, \tilde{\beta}$}
  \begin{itemize}      
  \item Collect equations relating $u, \beta,\tilde{\beta},\delta$
    to observed $\pi$, $P$
    \[ G(\underbrace{u,\beta,\tilde{\beta},\delta}_{x, \text{ dim } n = (I+1)|\mathcal{X}| + 3}; \underbrace{\pi, P}_{
        s=|\mathcal{X}|(|\mathcal{X}|-1)(I+1) + (I+1)|\mathcal{X}|} )
      = \underbrace{0}_{b, \text{ dim } m = (I+1)|\mathcal{X}| +
        |\mathcal{X}_e||\mathcal{X}_r|(I+1)}  \]
  \item Transversality theorem: let $G: \mathbb{R}^n \times \mathbb{R}^s \to \mathbb{R}^m$ by
    $\max\{n-m,0\}$ times continuously differentiable. Suppose $0$
    is a regular value of $G$, i.e. $G(x,b) = 0$ implies rank
    $DG_{x,b} = m$, then generically in $b$, $G(\cdot, b) : \mathbb{R}^n \to \mathbb{R}^m$
    has 0 as a regular value, i.e. rank $D_x G_{x,b} = m$
  \item $G$ satisfies these conditions, but $n < m$, so 0 is never a
    regular value, so generically in $b$, $G(x,b) \neq 0$
  \item Paper says, so generically $G(x,b) = 0$ has no solution,
    except at true $x^*$, but ...
  \item Theorem says except at true $b^*$, given true $b^*$, theorem
    says nothing about how many $x$ satisfy equation ...
  \item Introduce obviously non-identified reparameterization,
    e.g. $\beta = \beta_1 + \beta_2$, where does argument breakdown?
  \item Theorem does imply that model is falsifiable
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Identification of $\delta, \beta, \tilde{\beta}$}
  \begin{itemize}      
  \item I believe discount factors are identified here, but proof
    appears incomplete
  % \item Possible approaches to prove: either construct them from
  %   observables, or locally show $D_xG$ has rank $n$
  \item See \cite{ad2019n} for details of the problem
  \item \cite{ad2017} and \cite{adi2018} identification results for similar models
  \item Related identification results:
    \begin{itemize}
    \item \cite{magnacThesmar2002}
    \item \cite{bajari2013}
    \item \cite{an2014}
    \item \cite{dube2014}
    \item \cite{yao2012}
    \item \cite{ksss2018}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Estimation}
  \begin{itemize}
  \item Maximum pseudo-likelihood
    \begin{enumerate}
    \item Estimate $\hat{u}_i(x;\beta,\tilde{\beta},\delta)$ using
      choice probabilities and Bellman equations
    \item Maximize pseudo likelihood $\hat{P}_i(x; 
      \hat{u}_i(x;\beta,\tilde{\beta},\delta),
      \beta,\tilde{\beta},\delta)$
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Empirical application: mammography}
  \begin{itemize}
  \item Data from HRS, women 51-64
  \item Instantaneous utility from getting mammogram:
    \[ u_1(x) - u_0(x) = \alpha_0 + \alpha_1 BadHealth + \alpha
      LogIncome \]
  \item Demographics excluded from payoffs, but enter transition
    probabilities
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-tab2}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-fig1}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-tab3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-tab4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-tab5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-fig2}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-tab6}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{fw-tab7}
\end{frame}

\nocite{kennet1994,rust1987,adda2000,kasahara2009}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{\cite{efs2015}}

\subsection{Introduction}

\begin{frame}
  The Response of Drug Expenditure to Nonlinear Contract Design:
  Evidence from Medicare Part D
\end{frame}

\begin{frame}\frametitle{Medicare part D}

  \begin{itemize}
  \item Medicare Part D
    
    \begin{itemize}
    \item The largest expansion of Medicare since inception
      
    \item 32 million beneficiaries, 11\% of Medicare spending
      
    \item Typical coverage highly non-linear
    \end{itemize}
    
  \item Many planned and potential changes
    
    \begin{itemize}
    \item Under ACA, \textquotedblleft donut hole\textquotedblright \ will
      be \textquotedblleft filled\textquotedblright \ by 2020
    \end{itemize}
    
  \item Main objectives:
    
    \begin{itemize}
    \item Assess the contract design impact on drug spending\ (\textquotedblleft moral hazard\textquotedblright )
      
    \item Estimate the spending effects of the proposed changes in
      Part D\ contracts
      
    \item Conceptually: analyze healthcare utilization
      under non-linear contracts
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Standard Coverage in Medicare part D (in 2008)}
  \begin{center}    
    \includegraphics[width=\textwidth,keepaspectratio=true]{\folder/standard}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Approach}
  \begin{itemize}
  \item We use \textquotedblleft moral hazard\textquotedblright to mean the effect of
    out-of-pocket price on health care spending.
  \end{itemize}
  \begin{enumerate}
  \item Descriptive analysis of moral hazard
    \begin{itemize}
    \item Prescription demand is responsive the out-of-pocket price
    \item Individuals are forward looking when choosing prescriptions
    \end{itemize}
  \item Structural dynamic model of prescription demand to quantify
    descriptive results and for counterfactual analysis of alternative
    contract designs
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Related literatures}
  \begin{itemize}
  \item Large recent literature on Medicare Part D, mostly focusing on the
    quality of plan choice ({\small Heiss et al. 2010, 2012; Abaluck and Gruber
      2011; Ketcham et al. 2012)}    
  \item Large venerable literature on response of healthcare spending to
    insurance contracts (\textquotedblleft moral hazard\textquotedblright )    
    \begin{itemize}
    \item Only recently has attention focused on non-linear nature of contract (%
      {\small Bajari et al. 2011; Kowalski 2011; Marsh 2011; Aron-Dine et al. 2012)%
      }
    \end{itemize}    
  \item \textquotedblleft Bunching\textquotedblright \ response to progressive
    income tax ({\small Saez 2010; Chetty et al. 2011; Chetty 2012)}    
    \begin{itemize}
    \item In our context, need to account for dynamics
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Setting}
  
  \begin{itemize}
  \item Part D introduced in 2006, covering approximately 30M eligible
    individuals
  \item Government sets standard plan, but actual plans\ often provide
    different coverage
    
  \item Individuals eligible the month they turn 65, and then make plan
    choices prior to every calendar year
  \end{itemize}  
\end{frame}

\mode<all>
\inputslide{slides_Minnesota_091714}{7}
\inputslide{slides_Minnesota_091714}{8}
\mode<all>{\usebackgroundtemplate{}}
\mode*

\subsection{Descriptive evidence}

\begin{frame}\frametitle{Actual contracts}
  \begin{center}    
    \includegraphics[width=\textwidth,keepaspectratio=true]{\folder/actualContracts}
  \end{center}  
\end{frame}

\begin{frame}
  \frametitle{Static price response:\ bunching at the kink}
  
  \begin{itemize}
  \item Sharp increase in price when go into donut hole
    
    \begin{itemize}
    \item On average price rises from 34 to 93 cents for every dollar
    \end{itemize}
    
  \item Standard economic theory: with convex preferences smoothly distributed
    in population, should see bunching at the convex kink
  \end{itemize}
\end{frame}


\mode<all>

\inputslide{slides_Minnesota_091714}{13}
\inputslide{slides_Minnesota_091714}{14}
\inputslide{slides_Minnesota_091714}{16}
\inputslide{slides_Minnesota_091714}{17}
\inputslide{slides_Minnesota_091714}{18}

\mode<all>{\usebackgroundtemplate{}}
\mode*


\begin{frame}\frametitle{Forward looking moral hazard}
  \begin{itemize}
  \item Individuals become eligible when they turn 65; contract resets
    on January 1st 
  \item Compare spending in first month of eligibility for people who
    turn 65 in February versus October --- same spot price, but
    very different expected future prices
  \end{itemize}  
\end{frame}


\begin{frame}\frametitle{Forward looking moral hazard}
  \begin{center}
    \includegraphics[width=\textwidth,keepaspectratio=true]{\folder/futurePrice}
  \end{center}
\end{frame}

\subsection{Model}

\begin{frame}[allowframebreaks] \frametitle{Model}
  \begin{itemize}
    
  \item Risk-neutral forward-looking individual faces uncertain health shocks
    
  \item Prescriptions are defined by $(\theta ,\omega )$,
    
    \begin{itemize}
    \item $\theta >0$ is
      the prescription's (total)\ cost
    \item $\omega >0$ is the monetized cost of
      not taking the drug
      
    \item Arrive at weekly rate $\lambda $, drawn from $G(\theta ,\omega
      )=G_{2}(\omega |\theta )G_{1}(\theta )$
      
    \item $\lambda $ follows a Markov process $%
      H(\lambda |\lambda ^{\prime })$
    \end{itemize}
    
  \item Insurance defines $c(\theta ,x)$ -- the out-of-pocket cost
    associated with a prescription that costs $\theta $ when total
    spending so far is $x$

  \item Individuals choose to fill each prescription or not
  \item Flow utility 
    \[
    u(\theta ,\omega ;x)=\left \{ 
      \begin{array}{ll}
        -c(\theta ,x) & if\text{ }filled \\ 
        -\omega & if\text{ }not\text{ }filled%
      \end{array}%
    \right. 
    \]

   
  \item Bellman equation given by%
    \begin{eqnarray*}
      v(x,t,\lambda _{t+1}) &=&E_{\lambda |\lambda _{t+1}} \\
      &&\hspace{-1.35in}\left[ 
        \begin{array}{c}
          (1-\lambda )\delta v(x,t-1,\lambda )+ \\ 
          \lambda \int \max\left \{ 
            \begin{array}{l}
              -c(\theta ,x)+\delta v(x+\theta ,t-1,\lambda ), \\ 
              -\omega +\delta v(x,t-1,\lambda )%
            \end{array}%
          \right \} dG(\theta ,\omega )%
        \end{array}%
      \right]
    \end{eqnarray*}    
    with terminal condition $v(x,0)=0$ for all $x$ 
  \end{itemize}
\end{frame}%

\begin{frame}[shrink]
  \frametitle{Three key economic objects}  
  \begin{itemize}
  \item Statistical description of distribution of health shocks: $\lambda $
    and $G_{1}(\theta )$
  \item \textquotedblleft Primitive\textquotedblright \ price elasticity
    capturing substitution between health and income: $G_{2}(\omega |\theta )$
    \begin{itemize}
    \item If $\omega \geq \theta $, fill even if have to pay full cost
    \item If $\omega <\theta $, fill only if some portion of cost (effectively)
      paid by insurer
    \item Convenient to think about the ratio $\omega /\theta $
    \item Loosely, identified off the bunching
    \end{itemize}
  \item Extent to which individuals understand and respond to dynamic
    incentives in non-linear contract: $\delta \in \lbrack 0,1]$
    \begin{itemize}
    \item \textquotedblleft Full\textquotedblright \ myopia $(\delta =0)$: don't
      fill if $\omega <c(\theta ,x)$
    \item Dynamic response ($\delta >0$): utilization depends on both spot and
      future price
    \item $\delta $ is context specific! ... Captures salience, discounting, and
      perhaps liquidity constraints
    \item Loosely, identified off the timing patterns
    \end{itemize}
  \end{itemize}
\end{frame}%



\begin{frame}[allowframebreaks]
  \frametitle{Parameterization}
\begin{itemize}
\item $G_{1}(\theta )$ is lognormal: $\log \theta \sim N(\mu ,\sigma
^{2})$.

\item $\omega |\theta $ is stochastic, and is drawn from a mixture
distribution:

\begin{itemize}
\item $\omega \geq \theta $ with probability $1-p$ (prescription is filled
  for sure)
  
\item $\omega \sim U[0,\theta ]$ with probability $p$ (decision responds to
price)
\end{itemize}

\item $\lambda $ can obtain two values, and follows a 2-by-2
transition matrix
\item Heterogeneity modeled using a finite (5 types) mixture:

\begin{itemize}
\item Individual is of type $m$ with probability $\pi _{m}=\exp \left(
z_{i}^{\prime }\beta _{m}\right) /\sum \nolimits_{k=1}^{M}\exp \left(
z_{i}^{\prime }\beta _{k}\right) $

\item Almost all parameters vary with type: $\lambda _{m,low}$, $\mu _{m}$, $%
\sigma _{m}^{2}$, $p_{m}$ (exception:\ $\delta $, $\lambda _{high}/\lambda
_{low}$, $\lambda -$transition)

\item Baseline $z_{i}$: constant, risk score and 65-year-old indicator
\end{itemize}

\item Allowing for heterogeneity in both individual health ($\lambda ,\mu
,\sigma $) \ and in responsiveness of individual spending to cost-sharing ($%
p $)

\item Do not use panel nature across years (although risk scores introduce
  some serial correlation within individuals across years)
  
\end{itemize}
\end{frame}

\begin{frame}%
  \frametitle{Intuition for identification}  
  Three key objects:
  \begin{itemize}
  \item Claim sizes $\theta $ distributions identified from observed
    claims (panel data allows identification despite unobserved types
    and selection, similar to \cite{kasahara2009}, \cite{hu2012}, and
    \cite{sasaki2012} )
  \item Moral hazard ($p$'s and $\lambda $'s) identified from bunching
    described earlier.
% \item $\delta $ identified from spending pattern for 65 y.o. over join
%    months described earlier.
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Estimation}
  \begin{itemize}
  \item Moments chosen based on identification intuition
    \begin{itemize}
    \item Summary statistics (mean, standard deviation, $\Pr = 0$) of
      total spending
    \item Bunching: histogram of total spending near ICL
    \item Timing patterns for individuals around and below the kink
    \end{itemize}
  \item Calculate objective function using simulation
    \begin{enumerate}
    \item Given parameters solve for value function using backward
      induction
      \begin{itemize}
      \item Choose grid of values of $x_g$, set $v(x,0;\type) = 0 \forall x$
      \item Given $v(x,t-1;\type)$, calculate
        \[ v_{g,\type}  = (1-\lambda_\type) \delta v(x_g,t-1) +
        \lambda \Er\left[ \max 
          \begin{Bmatrix} 
            -oop(\theta,x_j) + \delta v(x_g+\theta,t-1;\type), \\ -\omega + \delta
            v(x_j,t-1;\type) 
          \end{Bmatrix} | \type \right] 
        \]
      \item Set $v(x,t;\type) = $ shape preserving cubic spline
        interpolation of $\{x_g, v_{g,\type} \}$, repeat until maximum $t$
      \item Backward induction can amplify approximation errors so
        important to calculate $\Er[\max]$ accurately (our
        distributional assumptions give an analytic expression
        conditional on $\theta$ and we use quadrature to integrate
        over $\theta$) and a good interpolation method of $v$
        (polynomials, Fourier series, and non shape preserving splines
        fail spectacularly here; linear interpolation is okay)
      \end{itemize}
    \item Draw $\type$, sequences of $\theta, \omega$ and simulate the
      model
    \item Compute moments from observed data and simulated data
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Poor approximation with polynomials}
  \begin{tabular}{ccc} 
    $\Pr(treat)$ & $v_g$ & $v(x,t)$ \\
    \multicolumn{3}{c}{$t=1$} \\
    \includegraphics[width=0.32\linewidth]{\folder/polyMC/ptreat1} & 
    \includegraphics[width=0.32\linewidth]{\folder/polyMC/vj1} & 
    \includegraphics[width=0.32\linewidth]{\folder/polyMC/vapprox1} \\
    \multicolumn{3}{c}{$t=2$} \\
    \includegraphics[width=0.32\linewidth]{\folder/polyMC/ptreat2} & 
    \includegraphics[width=0.32\linewidth]{\folder/polyMC/vj2} & 
    \includegraphics[width=0.32\linewidth]{\folder/polyMC/vapprox2} 
  \end{tabular}
\end{frame}

\begin{frame}\frametitle{Value function with  shape preserving cubic splines}
  \begin{tabular}{ccc} 
    $P(treat)$ & $v_g$ & $v(x,t)$ \\
    \multicolumn{3}{c}{$t=1$} \\
    \includegraphics[width=0.32\linewidth]{\folder/ptreat1} & 
    \includegraphics[width=0.32\linewidth]{\folder/vj1} & 
    \includegraphics[width=0.32\linewidth]{\folder/vapprox1} \\ 
    \multicolumn{3}{c}{$t=52$} \\
    \includegraphics[width=0.32\linewidth]{\folder/ptreat52} & 
    \includegraphics[width=0.32\linewidth]{\folder/vj52} & 
    \includegraphics[width=0.32\linewidth]{\folder/vapprox52} \\ 
  \end{tabular} 
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{Estimation}
  \begin{itemize}
  \item Objective function minimized using
    \href{http://www.lri.fr/~hansen/cmaesintro.html} {CMA-ES} 
    \begin{itemize}
    \item Uses quadratic approximation of objective to guide search,
      so converges more quickly than most random or global
      minimization algorithms (e.g.\ simulated annealing, genetic
      algorithms, pattern search)
    \item Introduces randomness so able to escape local minima unlike
      deterministic algorithms (e.g.\ Nelder-Mead, (Quasi)-Newton,
      conjugate gradient)
    \item Good performance, especially on non-convex problems compared
      to other algorithms 
    \end{itemize}
  \end{itemize}
\end{frame}

\subsubsection{Results}

\begin{frame}[shrink]
  \frametitle{Parameter estimates}
    \begin{center}
    \begin{scriptsize}
      \begin{tabular}{c cccccc}
        \input{\folder/paramSE.tex}
      \end{tabular}
    \end{scriptsize}
  \end{center}
\end{frame}

\begin{frame}[shrink]
  \frametitle{Parameter estimates: implied quantities}

  \begin{center}
    \begin{scriptsize}
      \begin{tabular}{c cccccc}
        \input{\folder/param2.tex}
      \end{tabular}
    \end{scriptsize}    
  \end{center}
\end{frame}%

\begin{frame}%
  \frametitle{Fit:\ Distribution of total costs}
  \begin{center}
    \includegraphics[height=0.8\textheight]{\folder/fit}
  \end{center}
\end{frame}%


\begin{frame}% %EndExpansion
  \frametitle{Fit:\ Total costs near ICL}
  \begin{center}
    \includegraphics[height=0.8\textheight]{\folder/bunchFit}
  \end{center}
\end{frame}% %EndExpansion



%\begin{frame}% %EndExpansion
%  \frametitle{Fit IV:\ Initial claims}
%  \input{\folder/initial}
%\end{frame}% %EndExpansion

\begin{frame}
  \frametitle{Fit: Risk score and total spending}
  \begin{tabular}{cc} First Quartile & Second Quartile \\
    \includegraphics[height=0.4\textheight]{\folder/spendRS-4} &
    \includegraphics[height=0.4\textheight]{\folder/spendRS-3} \\
Third Quartile & Fourth Quartile \\
    \includegraphics[height=0.4\textheight]{\folder/spendRS-2} &
    \includegraphics[height=0.4\textheight]{\folder/spendRS-1}
  \end{tabular}
\end{frame}

\begin{frame}
  \frametitle{Fit: Timing moments}
  \includegraphics[height=0.8\textheight]{\folder/timingAll}
\end{frame}

\begin{frame}
  \frametitle{Average weekly spending}
  \includegraphics[height=0.8\textheight]{\folder/spendWeek}
\end{frame}

\begin{frame} 
  \frametitle{Counterfactual: \textquotedblleft filling the
    gap\textquotedblright }
  \begin{itemize}
  \item Main counterfactual exercise considers \textquotedblleft filling the
    gap\textquotedblright \ as specified by ACA by 2020:
    \begin{itemize}
    \item Coinsurance rate in standard contract will remain at its pre-gap level
      (of 25\%) until out of pocket spending puts individual at CCL
    \end{itemize}
  \item First consider spending effect of \textquotedblleft filling the
    gap\textquotedblright \ in the 2008 standard benefit design
    \begin{itemize}
    \item On average, increases total spending by \$204 (11.5\%)
    \end{itemize}
  \end{itemize}
  \begin{center}
    \begin{tabular}{cccccccc}
      \hline
      &  & mean & sd & Q25 & Q50 & Q75 & Q90 \\ 
      \hline
      & baseline & 1760 & 1924 & 402 & 1413 & 2513 & 3632 \\ 
      & filled gap & 1964 & 2127 & 407 & 1455 & 2862 & 4450 \\ 
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}\frametitle{What has happened?}
  TODO: find data on spending per beneficiary by year (ideally leaving
  out subsidized beneficiaries, etc).
  
  \url{https://www.everycrsreport.com/reports/R40611.html}
\end{frame}

\begin{frame}
  \frametitle{Counterfactuals:\ change in spending from filling gap}
  \begin{center}
    \includegraphics[height=0.8\textheight]{\folder/std_nhDspendVspend}
  \end{center}
\end{frame}%

\begin{frame}
  \frametitle{Counterfactuals:\ change in weekly spending from filling
    gap}
  \includegraphics[height=0.8\textheight]{\folder/fillGap-timing}
\end{frame}

\begin{frame} \frametitle{Some subtle implications of non-linear contracts}
  \begin{itemize}
  \item Change in spending by people far from gap / endogeneity of people at
    risk of bunching
    \begin{itemize}
    \item Arises due to dynamic considerations
    \item Estimate that about 25\% of average \$204/person increase in annual
      spending comes from "anticipatory" response by people more than \$200 below
      kink location
    \end{itemize}
  \item \textquotedblleft Filling\textquotedblright \ donut hole causes some
    people to \textit{decrease }spending
    \begin{itemize}
    \item CCL held constant with respect to out of pocket (vs. total)\ spending,
      so for some people marginal price actually rises
    \item General point: with non-linear contracts, a given contract change can
      provide more coverage on margin to some individuals but less coverage to
      others
    \end{itemize}
  \end{itemize}
\end{frame}%

\begin{frame}
  \frametitle{Policy-relevant counterfactuals}
  \begin{itemize}
  \item Most people have more coverage than standard benefit
    \begin{itemize}
    \item So effect will be lower (people have some gap coverage already)
    \end{itemize}
  \item Analyze filling gap on existing contracts (ignore potential firm
    responses in contract design or beneficiary contract choice)
    \begin{itemize}
    \item Filling gap increases spending by about \$148 (8.5\%)
    \item But insurer ($\approx $Medicare) spending rise by \$253 (26\%); absent
      behavioral response, insurer spending would increase by only \$150
    \end{itemize}
  \end{itemize}  
  \begin{center}
    \includegraphics[width=\textwidth]{\folder/spending-actual}
  \end{center}
\end{frame}%

\begin{frame}\frametitle{Robustness}
\begin{itemize}
\item Results appear quite stable across a (limited) set of alternative
  specifications
\item Explore sensitivity to:
  \begin{itemize}
  \item Alternative number of discrete types (heterogeneity)
  \item Set of covariates (e.g. gap coverage indicator as "reduced form" way
    to capture potential plan selection)
  \item Adding risk aversion via recursive utility
  \item Letting $\omega $ vary more flexibly with $\theta $
  \end{itemize}
\end{itemize}
\end{frame}%

\begin{frame}\frametitle{Robustness}
  \begin{center}
    \includegraphics[width=\textwidth]{\folder/robustness}
  \end{center}
\end{frame}%

\begin{frame}\frametitle{Cross-year substitution}
  \begin{itemize}
  \item So far we treated each year of coverage in isolation
    \begin{itemize}
    \item Typical in the literature
    \end{itemize}
  \item But some of the effect could simply reflect substitution to next
    calendar year (as in Cabral, 2013)
  \item Consequences (for health and spending)\ may be less important if
    cross-year substitution is the main story
  \end{itemize}
\end{frame}%

\begin{frame}%
  \frametitle{Evidence of cross-year substitution}
  \begin{center}
    \includegraphics[width=\textwidth]{\folder/cross-sub}
  \end{center}
\end{frame}%

\begin{frame}\frametitle{But it does not explain all ...}
  \begin{center}
    \includegraphics[width=\textwidth]{\folder/adjustedBunching}
  \end{center}
\end{frame}

\begin{frame}\frametitle{Quantifying cross-year substitution}
  \begin{itemize}
  \item Back of the envelope: January effect is $\approx \$40$, so if applied
    to everyone would be about 25\% of estimated average response
  \item Extend (or \textquotedblleft tweak\textquotedblright !) the model by
    making terminal values depend on unfilled prescriptions, and allowing
    individual to fill in the beginning of next year
  \item Extension suggests\ overall effect may be reduced by 70\%, from
    \$153 to \$44
  \end{itemize}
  \begin{center}
    \includegraphics[width=\textwidth]{\folder/terminalV}
  \end{center}
\end{frame}\section{Summary}

\begin{frame}\frametitle{Summary}
  \begin{itemize}
  \item Spending response to non-linear health insurance contracts (vs "an
    elasticity"\ with respect to "the"\ price)
  \item Context:\ Medicare Part D (lots of current policy interest)
  \item Results:
    \begin{itemize}
    \item \textquotedblleft Filling the gap\textquotedblright \ (ACA) will
      increase Part D spending by about \$150 per beneficiary (8.5\%), and
      government spending by \$250 (26\%)
    \item Importance of non-linearity of insurance contracts
      \begin{itemize}
      \item Much of spending increase comes from "anticipatory" behavior of
        individuals whose predicted spending is below the gap (would not be captured
        in static model)
      \end{itemize}
    \item A big part of the effect (but not all)\ could be explained by
      cross-year substitution
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Normative analysis}
  \begin{itemize}
  \item Focus of paper has been entirely positive
  \item Normative implications are more tricky
  \item Some of our findings regarding nature of response to kink may be
    useful for informally beginning to assess normative implications. e.g.,
    \begin{itemize}
    \item Larger response by healthier individuals
    \item Larger response of chronic (vs. acute) drugs
    \item (Evidence on spillover effects to non-drug spending and health would
      also be useful)
    \end{itemize}
  \item Conceptual question:\ optimality of drug consumption in absence of
    insurance?\ 
    \begin{itemize}
    \item Prices marked up above social MC\ (patents)
    \item Failures of rationality may produce under-consumption w/o insurance?
    \end{itemize}
  \end{itemize}
\end{frame}%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}


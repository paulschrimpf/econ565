\input{../slideHeader}
\providecommand{\folder}{medD}
\providecommand{\omth}{\frac{\omega}{\theta}}
\providecommand{\clm}{{\mathrm{claim}}}
\providecommand{\jm}{{\iota}}
\providecommand{\type}{{m}}
\providecommand{\typeSet}{{\mathcal{M}}}
\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Single Agent Dynamic Models}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \cite{Aguirregabiria2012} chapters 6-7
    \item \cite{rust2008}
    \item \cite{am2010}
    \item \href{http://faculty.arts.ubc.ca/pschrimpf/628/dynamic.pdf}{My
        notes from 628}
    \end{itemize}
  \item Key papers:
    \begin{itemize}
    \item \cite{rust1987}, \cite{hotz1993}
    \end{itemize}
  \end{itemize}
\end{frame}

%\begin{frame}
%  \tableofcontents
%\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Introduction}

% \begin{frame}[allowframebreaks]\frametitle{Introduction}
%   The following slides are from \cite{aguirregabiria2011dps}
% \end{frame}

% \mode<all>

% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{1}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{2}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{3}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{4}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{5}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{6}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{7}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{8}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{9}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{10}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{11}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{12}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{13}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{14}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{15}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{16}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{17}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{18}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{19}
% \inputslide{bgse_lecture_notes_dynamic_sagent_2011}{20}

% \mode<all>{\usebackgroundtemplate{}}
% \mode*

\section{\cite{holmes2011}}
\begin{frame}
  \frametitle{\cite{holmes2011}: ``The diffusion of Wal-Mart and
    economies of density''}
  %\movie{\includegraphics[width=\linewidth,keepaspectratio=true]{walmart01}}
  \begin{center}
    \includegraphics[width=0.7\linewidth,keepaspectratio=true]{walmart01}
  \end{center}
  \href{http://www.econ.umn.edu/~holmes/papers/Wal1962-2004_nov_05.wmv}
  {Movie}
\end{frame}

\begin{frame}
  \frametitle{Spread of Walmarts}
  %\movie{\includegraphics[width=\linewidth,keepaspectratio=true]{walmart01}}
  \begin{center}
    \includegraphics[width=\textwidth,keepaspectratio=true]{holmes-fig1}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Spread of Walmart Supercenters}
  \begin{center}
    \includegraphics[width=\textwidth,keepaspectratio=true]{holmes-fig2}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Spread of Walmarts}
  \begin{center}
    \includegraphics[width=\textwidth,keepaspectratio=true]{holmes-tab2}
  \end{center}
\end{frame}

\subsection{Overview}

\begin{frame}[allowframebreaks] \frametitle{\cite{holmes2011} overview}
  \begin{itemize}
  \item Observation:
    Walmart\footnote{\href{https://www.cnbc.com/id/32403443}{Should
        it be ``Wal-Mart'' or ``Walmart''?}}  opens its new stores
    close to existing ones
  \item Benefit from high store density: distribution
    \begin{itemize}
    \item Shipping costs
    \item Rapid response to demand shocks
    \end{itemize}
  \item Question: how large are the benefits of density for Walmart?
  \item Challenge: Wal-Mart logistics data is confidential, even if
    detailed cost data available some benefits of density might not be
    reflected by it
  \item Solution: use revealed preference
    \begin{itemize}
    \item Walmart's choices reveal tradeoff between benefit and cost
      of density
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] \frametitle{\cite{holmes2011}}
  \begin{itemize}
  \item Cost of high store density: cannibalization
    \begin{itemize}
    \item Two Walmarts close together will take sales away from one
      another
    \item Can be inferred from demand estimates
    \end{itemize}
  \item Sequence of store openings important, so need a
    dynamic model
  \item Walmart's dynamic decisions:
    \begin{enumerate}
    \item\label{q1} How many new Walmarts and how many new
      supercenters should be opened?
    \item\label{q2} \alert{Where should the new Walmarts and supercenters be located?}
    \item\label{q3} How many new distribution centers should be opened?
    \item\label{q4}  Where should the new distribution centers be located?
    \end{enumerate}
    Focus on \ref{q2} and take \ref{q1}, \ref{q3}, and \ref{q4} as given
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}[allowframebreaks]
  \frametitle{Model: dynamic choice of locations}
  \begin{itemize}
  \item Complete information
  \item Take as given choice of number of stores, $N^{\mathrm{Wal}}_t$,
    and supercenters, $N^{\mathrm{Super}}_t$
  \item Choose new store locations to maximize discounted sum of
    profits
    \[ \max_a \sum_{t=1}^\infty (\rho_t \beta)^{t-1} \begin{bmatrix}
      \sum_{j \in
        \mathcal{B}_t^{\mathrm{Wal}}} (\pi_{jt}^g - c_{jt}^g - \tau
      d_{jt}^g) + \\ + \sum_{j \in
        \mathcal{B}_t^{\mathrm{Super}}} (\pi_{jt}^f - c_{jt}^f - \tau
      d_{jt}^f)
    \end{bmatrix}
    \]
    \begin{itemize}
    \item $g$, $f$ superscripts for goods and food
    \item $\pi$ is variable profits
      \[ \pi_{jt}^e = \mu \underbrace{R_{jt}^e}_{\text{revenue}} -
      \mathrm{Wage}_{jt} \mathrm{Labor}_{jt}^e - \mathrm{Rent}_{jt}
      \mathrm{Land}^e_{jt} - \mathrm{Other}_{jt}^e \]
  \item $R_{jt}^e = $ revenue comes from demand estimates; demand at
    store $j$ depends on whether there is a store at nearby location
    $k$ and through a distance term in consumers' utility of shopping
    at a store
    \item $c_{jt}$ is a fixed cost
      \[ c_{jt} = \omega_0 + \omega_1 \log(\mathrm{Popden}_{jt}) + \omega_2
      \log(\mathrm{Popden}_{jt})^2 \]
    \item $d_{jt}$ is distance to the nearest distribution center,
      $\tau d_{jt}$ is a (fixed) distribution cost
    \item $\mathcal{B}_t^{\mathrm{Wal}}$ is set of all Walmarts open
      at time $t$
    \item $\mathcal{B}_t^{\mathrm{Super}} \subset
      \mathcal{B}_t^{\mathrm{Wal}}$ is set of  Walmart Supercenters
    \item $a = (\mathcal{A}_1^{\mathrm{Wal}},
      \mathcal{A}_1^{\mathrm{Super}}, ... )$ is sequence of sets of
      new stores
    \item Stores never close
      \begin{align*}
        \mathcal{B}_t^{\mathrm{Wal}} = &
        \mathcal{B}_{t-1}^{\mathrm{Wal}} +
        \mathcal{A}_t^{\mathrm{Wal}} \\
        \mathcal{B}_t^{\mathrm{Super}} = &
        \mathcal{B}_{t-1}^{\mathrm{Super}} +
        \mathcal{A}_t^{\mathrm{Super}}
      \end{align*}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model: demand}
  \begin{itemize}
  \item Consumers make discrete choice among Walmarts within 25 miles
    and an outside option
    \begin{align*}
      u_{i0} = & \alpha_0 + \alpha_1 \log(\mathrm{Popden}_{l(i)}) + \alpha_2
      \log(\mathrm{Popden}_{l(i)})^2 + \epsilon_{i0} \\
      u_{ij} = & (\xi_0 + \xi_1
      \log(\mathrm{Popden}_{l(i)}))\mathrm{Distance}_{l(i)j} +
      \mathrm{StoreChar}_j \gamma + \epsilon_{ij}
    \end{align*}
    \begin{itemize}
    \item $l(i) = $ location of consumer $i$
    \item $\epsilon_i \sim $ logit
    \end{itemize}
  \item Revenue:
    \[ R_j^g  = \sum_{l} \underbrace{\lambda^g}_{\text{spending per
        $i$}} \times
    \underbrace{p_{jl}^g}_{\Pr(l\text{ shops at }j)} \times
    \underbrace{n_l}_{\text{number of consumers}} \]
  \item Revenue data is store-level sales estimate from Trade
    Dimensions, so must have measurment error
    \[ \log(R_j^{\mathrm{Data}}) = \log(R_j^{\mathrm{True}}) +
    \eta_j^{\mathrm{Sales}} \]
    where $\eta_j^{\mathrm{Sales}} \sim N$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Estimation strategy}
  \begin{enumerate}
  \item Estimate revenue using demand model and data on store sales
  \item Construct variable costs based on local wages and property
    values
  \item Estimate fixed costs ($\omega$) and densities of scale
    ($\tau$) using moment inequalities derived from profit
    maximization
  \end{enumerate}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item Store level sales and employment in 2005
  \item Store openings and locations
  \item Demographic data from census
  \item Local wages and land rents
  \item Information from Walmart's annual reports
  \end{itemize}
\end{frame}

\subsection{Results: demand estimation}
\begin{frame}
  \includegraphics[width=0.6\textwidth,keepaspectratio=true]{holmes1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{holmes2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{holmes3}
\end{frame}

%\begin{frame}
%  \includegraphics[width=\textwidth,keepaspectratio=true]{holmes4}
%\end{frame}

\begin{frame}\frametitle{Variable costs}
  \begin{itemize}
  \item Labor costs $=$ average employees per million dollars of sales
    (3.61) (measure in 2005) $ \times$ average retail wage in county
    in year
  \item Land value to sales ratio constructed from property values
    based on census data (for each year) and property tax data for
    Walmarts in Minnesota and Iowa
  \item Scale demand estimates from 2005 by average Walmart revenue
    in each year
  \end{itemize}
\end{frame}

\subsection{Dynamic estimation}
\begin{frame}[allowframebreaks]\frametitle{Dynamic estimation}
  \begin{itemize}
  \item Given demand estimates and variable costs only unknown
    parameters are fixed costs, $\omega$, and economies of density, $\tau$
  \item Total profits from action $a = $ variable profits plus fixed
    costs plus economics of density
    {\footnotesize{
    \begin{align*}
      \Pi^T(a) = & \sum_{t=1}^\infty (\rho_t \beta)^{t-1} \begin{bmatrix}
        \sum_{j \in
          \mathcal{B}_t^{\mathrm{Wal}}} (\pi_{jt}^g - (\omega_0
        + \omega_1 \log(\mathrm{Popden}_{jt}) + \omega_2
        \log(\mathrm{Popden}_{jt})^2) - \tau
        d_{jt}^g) + \\ + \sum_{j \in
          \mathcal{B}_t^{\mathrm{Super}}} (\pi_{jt}^f - (\omega_0 +
        \omega_1 \log(\mathrm{Popden}_{jt}) + \omega_2
        \log(\mathrm{Popden}_{jt})^2) - \tau
        d_{jt}^f)
      \end{bmatrix}
      \\
      = & \Pi(a) + \omega_0 + \omega_1 C_{1,a} + \omega_2 C_{2,a} +
      \tau D_a
    \end{align*}
    }}
  \item Profit maximization implies that
    {\footnotesize{
    \begin{align*}
      \Pi^T(a) \leq & \Pi^T(a^o) \\
      \underbrace{\omega_1 (C_{1,a} - C_{1,a^o}) +  \omega_2 (C_{2,a} - C_{2,a^o})
        + \tau (D_a - D_{a^o})}_{\equiv x_a'\theta} \leq &
      \underbrace{\Pi(a^o) - \Pi(a)}_{\equiv y_a}
    \end{align*}
    }}
    where $a^o$ is observed choice, $a$ is any other choice
  \item Estimation of demand and variable costs $\Rightarrow$ observe
    $y_a$ with error
  \item Assume measurement has zero mean given $x_a$, then conditional
    moment inequalities,
    \[ \Er[y_a - x_a'\theta | x_a] \geq 0 \]
    can be used to form objective function
  \item Must choose deviations $a$ and unconditional moment
    inequalities for estimation
    \begin{itemize}
    \item Uses pairwise resequencing deviations (i.e. change order a
      pair of stores opens)
    \item Group deviations according to their affect on density to
      aggregate conditional moment inequalities
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Dynamic results}
\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{holmes5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{holmes6}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item $\tau \approx \$3.50 = $ cost savings per year in thousands of dollars when a
    store is 1 mile closer to its distribution center
    \begin{itemize}
    \item Shipping costs $\approx \$0.85$
    \end{itemize}
  \item Results robust to splitting sample, changing revenues, labor,
    or rent, including/excluding supercenters
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Setup}

\begin{frame}
  \frametitle{General Setup}
  \begin{itemize}
  \item Discrete time $t$, maximum $\tau \leq \infty$
  \item State $s_{it} \in S$, follows a controlled Markov process
    \[ F(s_{it+1} | \mathcal{I}_t) = F(s_{it+1} | s_{it}, a_{it}) \]
  \item Action $a_{it} \in A$
  \item Preferences: $\sum_{j=0}^\infty \beta^j
    U(a_{i,t+j},s_{i,t+j})$
    \[ a_{it} \in \argmax_{a \in A} \Er\left[ \sum_{j=0}^\tau \beta^j
      U(a_{i,t+j},s_{i,t+j}) | a_{it}=a,s_{it} \right]. \]
  \item Bellman equation
    \[ V(s_{it} ) = \max_{a \in A} U(a,s_{it}) + \beta \Er[V(s_{i,t+1}) |
    a, s_{it} ]  \]
  \item Policy function
    \[ \alpha(s) = \argmax_{a \in A} U(a,s_{it}) + \beta
    \Er[V(s_{i,t+1}) | a, s_{it} ] \]
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Example: Retirement}
  \begin{example}[Retirement]\footnote{From \cite{am2010}.}
    Consider the choice of when to retire. Let $a_{it} = 1$ if an agent
    is working and $a_{it}= 0$ if retired.  Suppose $\tau$ is the age at
    death. The payoff function could be
    \[ U(a_{it},x_{it},\epsilon_{it}) = \Er[ c_{it}^{\theta_1} | a_{it},
    x_{it} ] \exp \left(\theta_2 + \theta_3 h_{it} + \theta_4
      \frac{t}{1+t} \right) - \theta_5 a_{it} + \epsilon(a_{it}) \]
    where $c_{it}$ is consumption, $\theta_1$ is the coefficient of
    relative risk aversion, $h_{it}$ is health, and the expression in
    the $\exp$ captures the idea that the marginal utility of
    consumption could vary with health and age. $-\theta_5 a_{it}$
    captures the disutility of working.
  \end{example}
\end{frame}

\begin{frame}\frametitle{Example: Entry / Exit}
  \begin{example}[Entry/Exit]\footnote{From \cite{am2010}.}
    A firm is deciding whether to operate in a market. Its per-period
    profits are
    \begin{align*}
      U(a_{it}) = a_{it} \left( \theta_R \log (S_t) - \theta_N
        \log\left( 1 + n_t \right) - \theta_F -
        \theta_E(1- a_{i,t-1}) + \epsilon_{it} \right)
    \end{align*}
    where $a_{it}$ is whether the firm operates at time $t$. $S_t$ is
    the size of the market, $n_t$ is the number of
    other firms operating. $\theta_F$ is a fixed operating cost, and
    $\theta_E$ is an entry cost.
  \end{example}
\end{frame}

\subsection{Identification}

\begin{frame}\frametitle{Identification: Setup}
  \begin{itemize}
  \item Panel data on $N$ individuals for $T$ periods
  \item Observe
    \begin{itemize}
    \item Actions $a_{it}$
    \item Some state variables $x_{it}$, $s_{it} =
      (x_{it},\epsilon_{it})$
    \end{itemize}
  \item i.e. observe joint distribution of $x_{i\cdot}$ and
    $a_{i\cdot}$
  \item Goal: recover $U$, $F(s_{it+1} | s_{it}, a_{it})$, $\beta$
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Non-identification without more restrictions}
  \begin{itemize}
  \item Value function
    \[ V(s) = \max_{a \in A} U(a,s) + \beta
    \Er[V(s'| a, s) ] \]
  \item Change $U(a,s)$ to
    \[ \tilde{U}_f(a,s) = U(a,s) + f(s) - \beta
    \Er[ f(s') | a, s] \], new value function
    \begin{align*}
      \tilde{V}(s) = & \max_{a \in A} U(a,s) + f(s) - \beta \Er[f(s')
      | a, s] + \beta \Er[ \tilde{V}(s') | a, s] \\
      \tilde{V}(s) - f(s) = & \max_{a \in A} U(a,s) + \beta \Er[
      \tilde{V}(s') - f(s') | a,s]
    \end{align*}
  \item So, $V(s) = \tilde{V}(s') - f(s')$
  \item Policy functions,
    \begin{align*}
      \alpha(s) = & \argmax_{a \in A} U(a,s) + \beta
      \Er[V(s'| a, s)] \\
      \tilde{\alpha}(s) = & \argmax_{a \in A} U(a,s) + \beta \Er[
      \tilde{V}(s') - f(s') | a,s]
    \end{align*}
    so $\alpha(s) = \tilde{\alpha}(s)$
  \item $U$ leads to same policy as $\tilde{U}_f$; they are
    observationally equivalent
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Identification: discrete $A$}
  \begin{itemize}
  \item Assume:
    \begin{itemize}
    \item $A$ is discrete and finite
    \item $U(a,x,\epsilon) = u(a,x) + \epsilon(a)$,
    \item $\epsilon$ has known CDF $G$, $\epsilon \indep x$ and
      $\epsilon_{it} \indep \epsilon_{is}$ for $t \neq s$
      \begin{itemize}
      \item Partial identification if $G$ unknown, see \cite{norets2013}
      \end{itemize}
    \item $\beta$ is known
    \item $u(0,x) = 0$
    \end{itemize}
  \item Then $u(a,x)$ is identified
  \end{itemize}
\end{frame}

\begin{frame}
  \newcounter{saveenumi}
  \frametitle{Identification -- discrete controls}
  \begin{itemize}
  \item Given $\{x_{it},a_{it}\}$ want to recover $U$
    \note{ Need some restrictions. Can add function of $x$ to $U$
      w/o changing policy function (Rust 1994). }
  \item Compare with discrete control identification from
    Magnac and Thesmar (2002) or Bajari, Chernozhukov, Hong, and
    Nekipelov (2009)
    \begin{itemize}
    \item Assume:
      \begin{enumerate}
      \item Transition distribution is identified
      \item Payoff additively separable in $\epsilon$, $U(x,a,\epsilon) =
        u(x,a) + \epsilon(i)$
      \item Distribution of $\epsilon$ known and $\epsilon_{it}$ independent
        across $f$ and $t$
      \item $u(x, a_0)$ is known for all $x \in \mathcal{X}$ and
        some $a_0 \in \mathcal{A}$
      \item Discount factor, $\delta$, is known
      \end{enumerate}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Identification -- discrete controls}
  \begin{itemize}
  \item Proof sketch:
    \begin{itemize}
    \item Additive separability and knowing distribution of $\epsilon$
      allows Hotz-Miller inversion to recover differences of choice
      specific value functions
    \item Given $u(x,a_0)$ and differences in choice specific
      value functions, can recover choice specific value functions
      from Bellman equation
    \item Given choice specific value functions, can get $u(x,a)$
      from Bellman equation
    \item See
      \href{http://faculty.arts.ubc.ca/pschrimpf/628/dynamic.pdf}{my
      notes from 628} and references therein for details
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Identification -- continuous controls}
  \begin{itemize}
  \item Key assumptions:
    \begin{enumerate}
    \item Transition density, $f_{x_{it+1} | x_{it}, a_{it}}$, is
      identified\note{Immediate if states observed, also possible with
        some unobserved states, see later.}
    \item Distribution of $\epsilon$, $F_\epsilon$, is normalized
      \begin{itemize}
      \item Not a restriction because $\epsilon$ enters $U(x,a,\epsilon)$
        without restriction
      \end{itemize}
      $\epsilon_{it}$ is independent across $f$ and $t$.
    \item Discount factor, $\delta$, is known
    \item For some $k$,
      \begin{itemize}
      \item $\frac{\partial
          U}{\partial x^{(k)}} \left( x,\alpha(x,\epsilon),\epsilon \right)$ is
        known
      \item There exists $\chi_k(a_0,x^{(-k)},\epsilon)$ such that $a_0 =
        \alpha\left (\chi_k(a_0,x^{(-k)},\epsilon),x^{(-k)},\epsilon\right)$
      \end{itemize}
    \item Initial condition: for some $a_0$,
      $U(x,a_0,\epsilon)$ is known
    %\item Exclusion: for some $j$, $x^{(j)}$ enters $\alpha(x,\epsilon)$, but not
    %  $U(x,a,\epsilon)$ directly
      \setcounter{saveenumi}{\theenumi}
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Identification -- continuous controls}
  \begin{itemize}
  \item Key assumptions (continued):
    \begin{enumerate} \setcounter{enumi}{\thesaveenumi}
    \item Completeness: let $\frac{\partial U}{\partial a} \in
      \mathcal{G}$, define
      {\footnotesize{
      \begin{align*}
        \mathcal{D}(g)(x,\epsilon) = & \frac{\partial}{\partial a_t}
        E\left[ \sum_{\tau=0}^\infty
          \delta^{\tau} g(x_{t+\tau},\epsilon_{t+\tau}) \vert x_t = x,
          a_t = \alpha(x,\epsilon) \right] \\
        \mathcal{L}(g)(x,\epsilon) = &
        \int_{\chi_k(a_0,x^{(-k)},\epsilon)}^{x^{(k)}}
        g(\tilde{x}^{(k)},x^{(-k)},\epsilon)
        \frac{\partial \alpha}{\partial x^{(k)}}
        (\tilde{x}^{(k)},x^{(-k)},\epsilon) d\tilde{x}^{(k)} \\
        \mathcal{K}(g)(x,\epsilon) = &
        \mathcal{D}\l(\mathcal{L}(g)\r)(x,\epsilon)
      \end{align*}
      }}
      The only solution in $\mathcal{G}$ to
      \begin{align*}
        0 = g(x,\epsilon) + \mathcal{K}(g)(x,\epsilon)
      \end{align*}
      is $g(x,\epsilon) = 0$
    \end{enumerate}
  \item Result: $U$ identified
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Proof sketch}
  \begin{itemize}
  \item Policy function: $F_\epsilon(\epsilon) =  F_{a|x}\left(
      \alpha(x,\epsilon)|x\right)$ \note{from monotonicty and identified
      transition densities}
  \item First order condition for $a_t$:
    {\footnotesize{
    \begin{align*}
      0 = & \frac{\partial U}{\partial a} (x_t,\alpha(x_t,\epsilon_t), \epsilon_t)
      + \\ & +
      \frac{\partial}{\partial a}  \sum_{\tau=1}^\infty \delta^\tau E\left[
        U\left(x_{t+\tau},\alpha(x_{t+\tau},\epsilon_{t+\tau}) ,\epsilon_{t+\tau}\right)
        \vert x_t, \alpha(x_t,\epsilon_t) \right]
    \end{align*}
    }}
\item Write payoff function in terms of its derivatives:
  {\footnotesize{
  \begin{align*}
    U(x,\alpha(x,\epsilon),\epsilon)
    = & \int_{\chi_k(a_0,x^{(-k)},\epsilon)}^{x^{(k)}} \Bigl( \frac{\partial
      U}{\partial a}(x,\alpha(x,\epsilon),\epsilon) \frac{\partial \alpha}{\partial
      x^{(k)}}(x,\epsilon) + \\ & \;\;  + \frac{\partial U}{\partial
      x^{(k)}}(x,\alpha(x,\epsilon),\epsilon) d\tilde{x}^{(k)}\Bigr) \\
    & +
    U\l(\chi_k(a_0,x^{-k},\epsilon),x^{-k},a_0,\epsilon\r)
    \notag
  \end{align*}
}}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Proof sketch}
  \begin{itemize}
\item Let
  {\footnotesize{
  \begin{align*}
    \varphi(x,\epsilon) = & U\l(\chi_k(a_0,x^{-k},\epsilon),x^{-k},a_0,\epsilon\r) + \\
    & + \int_{\chi_k(a_0,x^{(-k)},\epsilon)}^{x^{(k)}}\frac{\partial U}{\partial
      x^{(k)}}(x,\alpha(x,\epsilon),\epsilon) d\tilde{x}^{(k)}
  \end{align*}
}}
\item Substitute into first order condition:
  {\footnotesize{
  \begin{align*}
    0 = & \l(\mathbf{1} + \mathcal{K} \r) \l(\frac{\partial
      U}{\partial a} \r)
    + \mathcal{D}\l( \varphi \r)
  \end{align*}
}}
\item Integrate to recover $U(x,\alpha(x,\epsilon),\epsilon)$
%\item Exclusion gives $U(x,a,\epsilon)$
\end{itemize}
\end{frame}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Machine replacement models}

\begin{frame}\frametitle{Machine replacement models}
  \begin{itemize}
  \item Firm operates many machines independently, machines fail with
    some probability that increases with age, firm chooses when to
    replace machines to minimize costs of failure and replacement
  \item Classic \cite{rust1987} about bus-engine replacement
  \item Many follow-ups and extensions
    \begin{itemize}
    \item \cite{das1992}: cement kilns
    \item \cite{kennet1994}: aircraft engines
    \item \cite{rr1995}: nuclear power plants
    \item \cite{adda2000}: cars
    \item \cite{kasahara2009}: response of investment to tariffs
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Model}
  \begin{itemize}
  \item Choose: $a_{it} = 1$ (replace) or $0$ (don't replace)
  \item Machine age: $x_{it+1} = (1-a_{it}) x_{it} + \xi_{i,t+1}$
  \item Profits: $Y(x) - a RC(x) + \epsilon(a)$
  \item Firm's problem:
    \begin{align*}
      \max_{\mathbf{a}} \; & \Er_t \left[ \sum_{j=0}^\infty \beta^j \left(
          Y\left((1-a_{it}) x_{it}\right) -
          a_{it} RC(x_{it}) + \epsilon_{it}(a_{it})
        \right) \right] \\
      \text{ s.t. } & x_{i,t+1} = (1-a_{it}) x_{it} + \xi_{i,t+1}
    \end{align*}
  \item $\epsilon$ and $\xi$ i.i.d.
  \item Often $\xi$ non-stochastic, e.g. $x = $ age, $\xi = 1$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Value functions}
  \begin{itemize}
  \item Value function
    \[ V(x,\epsilon) = \max_a Y((1-a) x) - a RC(x) + \epsilon(a) +
    \beta \Er[ V(x',\epsilon') | x(1-a) ] \]
  \item Expected (or integrated) value function
    \[ \bar{V}(x) = \Er\left[ V(x',\epsilon') | x \right] \]
  \item Choice specific value function
    \begin{align*}
      v(x,a) = &  Y((1-a) x) - a RC(x) + \beta \Er\left[\max_{a'} v(x',a') +
        \epsilon(a') | x,a \right] \\
      = & Y((1-a) x) - a RC(x) + \beta \bar{V}(x(1-a))
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Identification}
  \begin{itemize}
  \item Observe: $\Pr(a|x)$
    {\footnotesize{
    \begin{align*}
      \Pr(a=1|x) = & \Pr\left( v(x,1) + \epsilon(1) \geq v(x,0)  +
        \epsilon(0) | x\right) \\
      = & \Pr\left(\epsilon(0) - \epsilon(1) \leq v(x,1) - v(x,0)
      \right) \\
      = & \Pr\left(\epsilon(0) - \epsilon(1) \leq Y(0) - Y(x) - RC(x) +
        \beta\left(\bar{V}(0) - \bar{V}(x) \right)  \right)
    \end{align*}
  }}
  \item Choice probabilities identify $v(x,1) - v(x,0) = \log \Pr(1|x) -
    \log \Pr(0|x)$
  \item Choice probabilities not enough to separately identify $RC(x)$
    and $Y(x)$, only identify the sum $RC(x) + Y(x)$
  \item Normalize $Y(x)$, solve for $v(x,0)$ from
    {\footnotesize{
        \begin{align*}
          v(x,0) = & Y(x) + \beta \Er\left[ \max_a v(x',a) - v(x',0) +
                     \epsilon(a) | x \right] + \beta \Er[v(x',0) | x] \\
          v(x,0) = & (I - \beta \mathcal{E})^{-1} \left( Y(\cdot)  + \beta \Er\left[ \max_a v(x',a) - v(x',0) +
                     \epsilon(a) | \cdot \right] \right)(x) \\
          = & (I - \beta \mathcal{E})^{-1} \left( Y(\cdot)  + \beta
              \Er\left[ \log\left(\sum_a e^{v(x',a) - v(x',0)} \right) | \cdot \right] \right)(x)
        \end{align*}
      }}
    where $\mathcal{E}(f)(x) = \Er[f(x')|x]$
  \item Then
    \[ v(x,1) = v(x,0) + \left[v(x,1) - v(x,0)\right] \]
    and
    \begin{align*}
      \bar{V}(x) = & \Er\left[\max_{a'} v(x',a') +
      \epsilon(a') | x \right] \\
      = & \Er\left[ v(x',0) + \max_{a'} v(x',a')-v(x',0) +
      \epsilon(a') | x \right] \\
      = & \Er\left[v(x',0) + \log\left(\sum_{a}
          e^{v(x',a')-v(x',0)} \right) | x \right]
    \end{align*}
    and
    \[ RC(x) = -v(x,1) + \beta \bar{V}(0) \]
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{(Non)-identification of discount factor}
  \begin{itemize}
  \item Given $\beta$ and $Y(x)$, above steps identify $RC(x)$, change
    $\beta$ and get a new observationally equivalent $RC(x)$
  \item Does it matter? Consider counterfactuals that change $Y$ or
    distribution of $\xi$. Check whether $\frac{\partial P}{\partial
      Y}$, $\frac{\partial v}{\partial Y}$ depend on $\beta$.
  \item Identify $\beta$ by having some components of $x$ affect
    $\Er[\cdot|x,a]$, but not $Y$ or $RC$
    \begin{itemize}
    \item Previous slide gives $RC$ as a function of $\beta$, identify
      $\beta$ from restriction on $RC$
    %   \begin{align*}
    %     RC(x) = & -v(x,1) + \beta \bar{V}(0) \\
    %     = & -v(x,0) - \left[v(x,1) - v(x,0)\right]
    %         + \beta \Er[v(x',0) + \log\left(\sum_{a}
    %       e^{v(x',a')-v(x',0)} \right) | 0 \right] \\
    %     = & -v(x,0) - \beta \Er[v(x',0)|x=0] - \left[v(x,1) -
    %         v(x,0)\right] + \beta \Er[\log\left(\sum_{a}
    %       e^{v(x',a')-v(x',0)} \right) | 0 \right] \\
    %     = &
    \end{itemize}
  \end{itemize}
\end{frame}

\nocite{kennet1994,rust1987,adda2000,kasahara2009}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}\frametitle{Other models of dynamic demand}
  \begin{itemize}
  \item Storable goods
    \begin{itemize}
    \item \cite{hendel2006}
    \item \cite{erdem2003}
    \end{itemize}
  \item Durable goods
    \begin{itemize}
    \item \cite{gowrisankaran2009}
    \end{itemize}
  \item Health care
    \begin{itemize}
    \item \cite{gilleskie1998}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Euler equations}

\subsection{\cite{am2013}}

\begin{frame}
  \frametitle{Euler Equations for the Estimation of Dynamic Discrete
    Choice Structural Models}
  \begin{itemize}
  \item Euler equations provide easier way to estimate dynamic
    continuous choice models than solving for value function
  \item Derive Euler equations for discrete choice model
    \begin{itemize}
    \item Write problem in terms of choice probability instead of
      policy to make differentiable
    \end{itemize}
  \item Reduces computation, but loses asymptotic efficiency
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Continuous choice}
  \begin{itemize}
  \item Exogenous state $z$ with density $f(z_{t+1}|z_t)$
  \item Endogenous state $y_{t+1} = Y(a_t,y_t,z_t,z_{t+1})$
  \item Action $a$
  \item Bellman equation
    \[ V(y,z) = \max_a \pi(a,y,z) + \beta \int V(Y(a,y,z,z'),z')
      dF(z'|z) \]
  \item FOC
    \[ 0 = \frac{\partial \pi}{\partial a} + \beta \int \frac{\partial
        V}{\partial y} \frac{\partial Y}{\partial a} dF(z'|z) \]
  \item Envelope theorem
    \begin{align*}
      \frac{\partial V}{\partial y} = & \frac{\partial \pi}{\partial y}
                                        + \beta \int \frac{\partial V}{\partial y} \frac{\partial
                                        Y}{\partial y} dF(z'|z)
    \end{align*}
  \item Present value approach (solving for $V$)
    \begin{itemize}
    \item
      \begin{align*}
        \frac{\partial V}{\partial y} = &  \frac{\partial \pi}{\partial y} + \beta \mathcal{E}
                                          (\frac{\partial V}{\partial y}) \\
        \frac{\partial V}{\partial y} = & (I - \beta \mathcal{E})^{-1}
                                        \frac{\partial \pi}{\partial
                                          y}
      \end{align*}
    \item substitute into FOC
      \[ 0 = \frac{\partial \pi}{\partial a} + \beta \int
        (I - \beta \mathcal{E})^{-1}\frac{\partial \pi}{\partial y}
        \frac{\partial Y}{\partial a} dF(z'|z) \]
      and use to estimate derivatives of $\pi$
    \item Downsides:
      \begin{itemize}
      \item Computational curse of dimensionality $(I - \beta
        \mathcal{E})$  is like $|\mathcal{Z}| \times |\mathcal{Z}|$
        matrix, so costly to invert
      \item Statistical curse of dimensionality: need to estimate
        expectations conditional on $z$
      \end{itemize}
    \end{itemize}
  \item Euler equation approach:
    \begin{itemize}
    \item Assume $\frac{\partial Y}{\partial y} = H(a,y,z) \frac{\partial
        Y}{\partial a}$
    \item Combine with envelope theorem and FOC to get
      \[ 0 = \frac{\partial \pi}{\partial a_t} + \beta \int
        \left(
          \frac{\partial \pi}{\partial y_{t+1}} - H(a_{t+1},y_{t+1},z_{t+1}) \frac{\partial
            \pi}{\partial a_{t+1}}
        \right)
        \frac{\partial Y}{\partial a_t} dF(z_{t+1}|z_t)
      \]
    \item Equivalently, solve
      \begin{align*}
        \max_{a_t,a_{t+1}} & \pi(a_t,y_t,z_t) + \beta \int
        \pi(a_{t+1}, Y(a_t,y_t,z_t,z_{t+1}),z_{t+1}) dF(z_{t+1}|z_t)
        \\
        \text{s.t.} &
                      Y(a_{t+1},Y(a_t,y_t,z_t,z_{t+1}),z_{t+1},z_{t+2})
                      = y^*_{t+2}(y_t,z_t,z_{t+1},z_{t+2})
      \end{align*}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Euler equation for discrete choice}
  \begin{itemize}
  \item Rewrite problem of choosing $a(x)$ to choosing $P(x)$
    using 1-1 mapping between probabilities and threshold decision
    rules
    \[
      a(x) = \mathbf{1}(v(a,x) - v(j,x) \geq \epsilon(j) - \epsilon(a))
    \]
    iff
    \[ P(a,x) = \tilde{G}(v) \]
  \item
    \[ W(x) = \max_P \sum_a P(a,x)\left(\pi(a,x) + \Er_P[\epsilon(a) | a]
        + \int W(x') dF(x'|x,a) \right)
    \]
  \item Constrained two-period problem to get Euler equation
    \begin{align*}
      \max_{P_t,P_{t+1}} & \pi^e(x_t,P_t) + \beta \int
                           \pi^e(x_{t+1},P_{t+1}) dF^e(x_{t+1}|x_t,P) \\
      \text{s.t.} & F^e(x_{t+2} | x_t,P_t,P_{t+1}) = F^{e}(x_{t+2} |
                    x_t,P_t^*,P_{t+1}^*)
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Application: cow replacement}
  \begin{itemize}
  \item Out of time, see paper
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}

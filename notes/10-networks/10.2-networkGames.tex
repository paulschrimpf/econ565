\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Games on Networks}
\author{Paul Schrimpf}
\institute{UBC \\ Vancouver School of Economics \\ 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{bkd2014}}

\begin{frame}[allowframebreaks]\frametitle{Games on networks}
  \begin{itemize}
  \item Model of \cite{bkd2014}
  \item $N$ agents, simultaneously choose $x_i \geq 0$
  \item Payoffs $U_i(x_i, x_{-i}; \delta, G)$
    \begin{itemize}
    \item Assume $U_i$ depends on $x_j$ iff they are linked
    \item $\delta \geq 0$ parameterizes how strongly payoffs depend on
      one another
    \end{itemize}
  \item Best reply $x_i = f_i(x_{-i};\delta,G)$
  \item Focus on games where $f_i(x_{-i};\delta,G)$ is linear in
    $x_{-i}$
    \begin{align*}
        f_i(x_{-i};\delta,G) = \max\{1 - \delta \sum_j g_{ij}
        x_j, 0 \}
    \end{align*}
    \begin{itemize}
    \item Public goods
      \begin{align*}
        U_i(x_i,x_{-i};\delta,G) = b_i(x_i + \delta \sum_j g_{ij} x_j)
        - \kappa_i x_i
      \end{align*}
    \item Negative externalities (e.g. congestion)
      \begin{align*}
        U_i(x_i,x_{-i};\delta,G) = \bar{x}_i x_i - \frac{1}{2}x_i^2 -
        \delta \sum_j g_{ij} x_i x_j
      \end{align*}
    \item Cournot competition with linear demand, network of substitutes
      \begin{align*}
        \Pi_i(x_i,x_{-i};\delta,G) = x_i\left(a - s \left(x_i + 2
            \delta \sum_J g_{ij} x_j \right) \right) - dx_i
      \end{align*}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Nash equilibria}
  \begin{itemize}
  \item Define $G_A = $ links among active agents with $x_i > 0$
  \item $G_{N-A,A}$ links connect active to inactive agents
  \item Actions $x$ is a Nash equilibrium iff
    \begin{enumerate}
    \item $(I+ \delta G_A) x_A = \mathbf{1}$
    \item $\delta G_{N-A,A} x_A \geq \mathbf{1}$
    \end{enumerate}
  \item Main results: equilibria depend on the minimal eigenvalue
    \begin{enumerate}
    \item If $\left\vert \lambda_{\min}(G) \right\vert < 1/\delta$,
      there is a unique Nash equilibrium
    \item An equilibrium is stable iff $\left\vert \lambda_{\min}(G_A)
      \right\vert < 1/\delta$
    \item If $\left\vert \lambda_{\min}(G) \right\vert > 1/\delta$,
      there may be multiple equilibria and all stable equilibria
      include inactive agents
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sketch of proof}
  \begin{itemize}
  \item Potential games: $\varphi(x_i, x_{-i})$ is a potential for a game
    with payoffs $v_i(x_i,x_{-i})$ if
    \[ \varphi(x_i,x_{-i}) - \varphi(x_i',x_{-i}) = v_i(x_i,x_{-i}) -
    v_i(x_i,x_{-i}) \]
  \item For these games: $\varphi(x;\delta,G) = x^T \mathbf{1} -
    \frac{1}{2} x^T (I + \delta G) x$
  \item Key observation: first order conditions for
    \[ \max_x x^T \mathbf{1} - \frac{1}{2} x^T (I + \delta G) x \text{
      s.t. } x \geq 0 \]
    are the same as the equilibrium conditions; in fact the set of
    equilibria are the maxima and any saddle points
  \item Unique maximum if $I + \delta G$ positive definite
    \begin{itemize}
    \item Eigenvalues of $I + \delta G = 1 + \delta \lambda(G)$, so
      unique equilibrium if $ 1 > -\delta \lambda_{\min}(G) $
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Application: R\&D free-riding}
  \begin{itemize}
  \item Empirical evidence of R\&D free-riding
    \begin{itemize}
    \item \cite{hp1996}: exploratory oil drilling
    \item \cite{fr1995}: new seeds in agriculture
    \end{itemize}
  \item Question: how does the network of firms affect free-riding?
    \begin{itemize}
    \item If $\left\vert \lambda_{\min}(G) \right\vert < 1/\delta$,
      unique equilibrium with all firms active
    \item Larger $|\lambda_{\min}(G)|$ implies stable equilibrium
      involves inactive firms
    \item Larger $|\lambda_{\min}(G)|$ means more global connections
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item \cite{acemoglu2012} is somewhat similar --- how does network
    structure of sectoral input-output relate to how sectoral shocks
    translate into aggregate shocks
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{konig2014}}

\begin{frame}
  \frametitle{\cite{konig2014}}
  ``R \& D Networks: Theory, Empirics and Policy Implications''
  \begin{itemize}
  \item Firms connected in two networks:
    \begin{itemize}
    \item R \& D network
    \item Competition network
    \end{itemize}
  \item Cournot game where firms choose R\&D expenditure and
    quantities
  \item OECD countries spend more than \$50 billion per year on R\&D
    subsidies and tax credits
  \item Identify key firms, analyze impact of R\&D subsidies
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}[allowframebreaks] \frametitle{Model}
  \begin{itemize}
  \item Firms $i \in \{1, ..., n\}$
  \item Partitioned product markets $\mathcal{M}_m$, $m=1,...,M$
  \item Consumer utility
    \[ U_m(q) = \alpha_m \sum_{i \in \mathcal{M}_m}  q_i - \frac{1}{2}
      \sum_{i \in \mathcal{M}_m} q_i^2 - \rho \sum_{i \in
        \mathcal{M}_m} \sum_{j \in \mathcal{M}_m \setminus \{i\}} q_i
      q_j \]
  \item Inverse demand
    \[ p_i = \underbrace{\sum_{m} \alpha_m 1_{i \in
          \mathcal{M}_m} }_{\bar{\alpha}_i} - q_i - \rho \sum_{j \in
        \mathcal{M}_m \setminus \{i\}} q_j \]
  \item R\&D collaboration network $A$
  \item Marginal cost:
    \[ c_i = \bar{c}_i - e_i -\varphi \sum_{j=1}^n a_{ij} e_j \]
  \item Profits:
    \[ \pi_i = (\bar{\alpha}_i - \bar{c}_i) q_i - q_i^2 - \rho
      \sum_{j=1}^n b_{ij} q_i q_j + q_i e_i + \varphi q_i \sum_{j=1}^n
      a_{ij} e_j - \frac{1}{2}e_i^2 \]
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Equilibrium}
  \begin{itemize}
  \item FOC for $e_i$: $q_i - e_i = 0$
  \item Substitute into $\pi(q):$
    \[
      \pi_i =
       (\bar{\alpha}_i - \bar{c}_i) q_i - \frac{1}{2} q_i^2 +
      \sum_{j=1}^n (\varphi a_{ij}  -\rho b_{ij}) q_i q_j
    \]
  \item FOC for $q$:
    \[ q_i =  (\bar{\alpha}_i - \bar{c}_i)  +
      \sum_{j=1}^n (\varphi a_{ij}  -\rho b_{ij})  q_j \]
  \item Matrix form:
    \[ (I_n + \rho B - \varphi A) q = (\bar{\alpha} - \bar{c}) \]
  \item Proposition 1: conditions for $ (I_n + \rho B - \varphi A)$ to
    be invertible given special structure of $A, B$ both in general
    and for special cases
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Network \& Market effects}
  \begin{itemize}
  \item Network effect: through $A$
    \begin{itemize}
    \item More connections $\rightarrow $ more $e$ \& $q$
    \end{itemize}
  \item Market effect: through $B$
    \begin{itemize}
    \item More connections (competitors) $\rightarrow $ less $e$ \& $q$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Network \& Market effects}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig1}
\end{frame}

\begin{frame}\frametitle{Welfare}
  \begin{itemize}
  \item Welfare $ = $ utility $+$ profits
    \[ W(G) = q^T q + \frac{\rho}{2} q^T B q \]
  \item Propositions 2-4 : characterize how welfare varies with
    network, competition effect ($\rho$) and spillovers ($\varphi$)
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Key firms}
  \begin{itemize}
  \item $G^{-i} = $ network without firm $i$
  \item Key firm:
    \[ i^* = \argmax_i W(G) - W(G^{-i}) \]
  \item Proposition 5: characterizes $i^*$
  \item Key firm need not have highest $q_i$ or highest of any
    conventional centrality measure (degree, Bonacich, etc)
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{R \& D subsidies}
  \begin{itemize}
  \item Subsidize R\&D at rate $s_i$ per $e_i$
    \[ \pi_i = (\bar{\alpha}_i - \bar{c}_i) q_i - q_i^2 - \rho
      \sum_{j=1}^n b_{ij} q_i q_j + q_i e_i + \varphi q_i \sum_{j=1}^n
      a_{ij} e_j - \frac{1}{2}e_i^2 + s_i e_i \]
  \item Proposition 6: optimal uniform subsidy
  \item Proposition 7: optimal firm specific subsidies
    \[ s^* = \argmax_{s \in \R^n_+} W(G,s) \]
    \begin{itemize}
    \item Key firm does not necessarily get the largest subsidy
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Empirical questions}
  \begin{itemize}
  \item Test proposition 1 and disentangle spillover and product
    rivalry effects of R\&D
  \item Determine key firms
  \item Estimate optimal subsidies
  \end{itemize}
\end{frame}

\subsection{Data}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item MERIT-CATI database on interfirm R\&D collaboration
    \begin{itemize}
    \item 1987-2006, some information going back to 1950
    \item 13040 companies
    \end{itemize}
  \item Matched by firm name with Compustat for balance sheets and
    income
    \begin{itemize}
    \item Profit, sales, R\&D, employees, capital
    \item $x_{it} = $ R\&D stock $ = $ perpetual inventory of past
      $R\&D$ expenditures with 15\% depreciation
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig5}
\end{frame}

\subsection{Econometric Model}
\begin{frame}\frametitle{Econometric Model}
  \begin{itemize}
  \item Marginal cost:
    \[ c_{it} = \eta_i - \epsilon_{it} - x_{it} \beta - e_{it} -
      \varphi \sum_{j=1}^n a_{ij,t} e_{jt} \]
  \item Inverse demand:
    \[ p_{it} = \alpha_m + \alpha_t - q_{it} - \rho \sum_{j=1}^n
      b_{ij} q_{jt} \]
  \item Best response of $q$
    \[ q_{t} = \varphi \sum_{j=1}^n a_{ij,t} q_{jt} - \rho \sum_{j=1}^n
      b_{ij} q_{jt}  + x_{it}  \beta + \eta_i +
      \kappa_t + \epsilon_{it} \]
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Econometric Model}
  \begin{itemize}
  \item Instruments:
  \begin{itemize}
    \item $\sum_{j=1}^n a_{ij,t} q_{jt}$, $\sum_{j=1}^n b_{ij} q_{jt}$
      endogenous
    \item Instrument with $\sum_{j=1}^n a_{ij,t} x_{it-1}$,
      $\sum_{j=1}^n b_{ij} x_{it-1}$ and/or $\sum_{j=1}^n a_{ij,t} taxcredits_{it-1}$,
      $\sum_{j=1}^n b_{ij} taxcredits_{it-1}$
    \item Instrument $a_{ij,t}$ with $a_{ij,t-s}$ and/or
      $\hat{a}_{ij,t-s}$ predicted using time $t-s$ firm
      characteristics
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Results}
\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab4new}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab5new}
\end{frame}


\begin{frame} \frametitle{Robustness: inter vs intra industry spillovers}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab4}
\end{frame}

\begin{frame}\frametitle{Optimal firm subsidies}
  \begin{itemize}
  \item Targeted subsidies have much larger welfare gain than uniform
  \item Optimal subsidies cyclical
  \item Firm subsidy ranking not same as market share or other simple
    observed firm characteristic
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig6new}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig6}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-fig8new}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab7}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{klz-tab8}
\end{frame}


\section{\cite{acemoglu2016b}}

\begin{frame}
  ``Network security and contagion'' \cite{acemoglu2016b}
  \begin{itemize}
  \item Setting:
    \begin{itemize}
    \item Agents $i$ connected in network
    \item Security investment $q_i$
    \item Probability of infection transmitted to $i$ is $1-q_i$
    \end{itemize}
  \item Results:
    \begin{itemize}
    \item Decompose payoff into own effect and externality
    \item Characterize relationship between network structure and
      security investments
    \item Compare Nash equilibrium to social optimum
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Agents $V = \{1, ..., n\}$
  \item Undirected network $G = (V, E)$
  \item Initially, one agent infected with probabilities $\Phi =
    (\phi_1, ..., \phi_n)$
  \item Before infection $i$ chooses $q_i \in [0,1]$, $q_i$ is
    probability of being immune
  \item Preferences
    \[ u_i(G, \mathbf{q}, \Phi) = \left(1 - \Pr_i(G,\mathbf{q},
        \Phi) \right) - c_i(q_i) \]
    $c_i$ continuously differentiable, increasing, convex, $c(0) =
    c'(0) = 0$ and $\lim_{q \to 1} c'(q) = \infty$
  \item Social welfare
    \[ W(G, \mathbf{q}, \Phi) = \sum_{i \in V} u_i(G, \mathbf{q},\Phi) \]
  \end{itemize}
\end{frame}

\subsection{Incentives}

\begin{frame}\frametitle{Network effect}
  \begin{proposition}
    Given network $G$, security profile $\mathbf{q}$, and attack decision $\Phi$, the
    infection probability of agent $i$ satisfies
    \[ \Pr_i(G,\mathbf{q}, \Phi) = (1-q_i) \tilde{\Pr}_i(G,
      \mathbf{q}_{-i}, \Phi) \]
    where $\tilde{\Pr}_i(G, \mathbf{q}_{-i}, \Phi)$ is the probability
    of the infection reaching agent i (including the probability of
    agent i being the seed).
  \end{proposition}

  \begin{itemize}
  \item $\tilde{\Pr}_i(G, \mathbf{q}_{-i}, \Phi) =$ ``network effect
    on agent $i$''
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Decomposition}
  \begin{proposition}
    Given network $G$, security profile $\mathbf{q}_{-j}$, and attack
    decision $\Phi$, the probability of the infection reaching agent
    $j$, $\tilde{\Pr}_j(G, \mathbf{q}_{-j}, \Phi)$, satisfies the
    following: For all $i \in V \setminus \{j\}$,
    \[ \tilde{\Pr}_j(G, \mathbf{q}_{-j}, \Phi) = \tilde{\Pr}_j(G_{-i},
      \mathbf{q}_{-\{j,i\}}, \Phi) + (1-q_i) Q_{ji}(G, q_{-\{j,i\}},
      \Phi) \]
    where $Q_{ji}(G, q_{-\{j,i\}}, \Phi)$ is the probability of
    infection reaching agent $j$ only through a path that contains
    agent $i$.
  \end{proposition}
  \begin{itemize}
  \item $(1-q_i) Q_{ji}(G, q_{-\{j,i\}}, \Phi)$ is the externality of $i$ on
    $j$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Strategic substitutes}
  \begin{itemize}
  \item Propositions 1 \& 2 imply
    \[ \frac{\partial^2 u_i}{\partial q_i \partial q_j} = -Q_{ji} (G,
      q_{-\{j,i\}}, \Phi) < 0\]
  \item  Agent $i$ invests more if others invest less
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] \frametitle{Best response}
  \begin{itemize}
  \item Using propositions 1 \& 2:
    \begin{align*}
      u_i(G, \mathbf{q}, \Phi) = & 1 - (1-q_i)\tilde{\Pr}_i(G,
                                   \mathbf{q}_{-i},\Phi) - c_i(q_i)
    \end{align*}
    and
    {\scriptsize{
    \begin{align*}
      W(G,\mathbf{q}) = & 1 - (1-q_i)\tilde{\Pr}_i(G,
                          \mathbf{q}_{-i},\Phi) - c_i(q_i) + \\
                        & + \sum_{j \neq i} 1 - (1 - q_j)\left( \tilde{Pr}_j(G_{-i},
                          \mathbf{q}_{-\{i,j\}},\Phi) + (1 - q_i) Q_{ji}(G,
                          \mathbf{q}_{-\{i,j\}},\Phi) \right) -
                          c_j(q_j)
    \end{align*}
    }}
  \item First order conditions for $q_i$
    \[ c_i'(B_i(\mathbf{q}_{-i})) = \tilde{\Pr}_i(G,
      \mathbf{q}_{-i},\Phi) \]
    and
    \[ c_i'(S_i(\mathbf{q}_{-i})) = \tilde{\Pr}_i(G,
      \mathbf{q}_{-i},\Phi) + \sum_{j \neq i} (1 - q_j)Q_{ji}(G,
      \mathbf{q}_{-\{i,j\}},\Phi) \]
  \item So (given $\mathbf{q}_{-i})$) equilibrium best response is less than social welfare
    maximizing best response, $B_i(\mathbf{q}_{-i}) \leq
    S_i(\mathbf{q}_{-i})$
    \begin{itemize}
    \item If network and costs are symmetric across agents, then $q^e
      \leq q^s$
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Equilibrium}

\begin{frame}\frametitle{Gatekeepers and protection}
  \begin{itemize}
  \item Define $a^{j}_{ik} = \mathbf{1}\{j \text{ included in all
      paths from } i \text{ to } k \}$ i.e. $j$ is a gatekeeper between
    $i$ and $k$
  \item Protection of $j$ for $i$ is $a_i^j = \frac{1}{n} \sum_{k}
    a_{ik}^j$
  \end{itemize}

  \includegraphics[width=\textwidth]{amo-fig1.jpg}
\end{frame}

\begin{frame}\frametitle{Equilibrium in non-symmetric networks}
  \begin{proposition}
    \[ \tilde{\Pr}(G, \mathbf{q}_{-i}) = 1 - \sum_{j \neq i} a_i^j q_j
      + o(\norm{\mathbf{q}}_\infty) \]
  \end{proposition}

  \begin{theorem}
    If $\alpha = c''(0)$ is large, then
    \begin{align*}
      q^e = & (A + \alpha I)^{-1} e + o(1/\alpha^2) \\
      = & \frac{1}{\alpha} e - \frac{1}{\alpha^2} A e + o(1/\alpha^2)
    \end{align*}
    where $A_{ij} = a_i^j$, $A_{ii} = 0$, and $e = (1, 1, ..., 1)$
  \end{theorem}
\end{frame}

\begin{frame}\frametitle{Protection centrality determines equilibrium investment}
  \begin{itemize}
  \item Theorem implies
    \[ q_i^e = \frac{1}{\alpha} (1 - \frac{1}{\alpha} a_i) +
      o(1/\alpha^2) \]
    where $a_i = \sum_j a_i^j$ is the \emph{protection centrality} of $i$
  \end{itemize}

  \includegraphics[width=\textwidth]{amo-fig2.jpg}
\end{frame}

\subsection{Social optimum}

\begin{frame}\frametitle{Gatekeeping centrality and separation}
  \begin{itemize}
  \item Gatekeeping centrality, $s_i = \sum_j a_j^i$
  \item $b_{kt}^{(i,j)} = 1 $ if $(i,j)$ is separating pair for $k,t$,
    i.e. if neither $i$ or $j$ is a gatekeeper for $k,t$, but removing
    both $i$ and $j$ disconnects $k$ and $t$
  \item Network separation of $i$ and $j$, $b_i^j = \sum_{k,t} (a^j_{kt}
    a^i_{kt} - b_{kt}^{(i,j)})$
  \end{itemize}
  \includegraphics[width=\textwidth]{amo-fig3.jpg}
\end{frame}

\begin{frame}\frametitle{Social optimum}
  \begin{theorem}
    \begin{align*}
      \mathbf{q}^s = & (B + \alpha I)^{-1} \mathbf{s} + o(\alpha^{-2}) \\
      = & \frac{1}{\alpha}\left(I - \frac{B}{\alpha}\right) \mathbf{s}  + o(\alpha^{-2})
    \end{align*}
    where $B_{ij} = b_i^j$
  \end{theorem}
  Implies
  \[ q_i^s \approx \frac{1}{\alpha} \left( s_i - \frac{1}{\alpha}
      \sum_{j\neq i} b_i^j s_j \right) \]
\end{frame}

\begin{frame}\frametitle{Optimum vs Equilibrium}
  \[ q_i^s - q_i^e \approx \frac{1}{\alpha} \sum_{j \neq i} a_j^i  -
    \frac{1}{\alpha^2} \left( \sum_{j \neq i} b_i^j s_j - a_i^j\right) \]
  \begin{itemize}
  \item Equilibrium security investments are smaller than
    socially optimal security investments
  \item The node with the largest gatekeeping centrality increases its
    investment the most in the socially optimal solution compared to
    the equilibrium
  \item For all nodes with the same gatekeeping centrality, the gap
    between socially optimal investment and equilibrium is
    proportional to $a_i - \sum_j b_i^j s_j$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Optimum vs Equilibrium}
  \includegraphics[width=\textwidth]{amo-fig4.jpg}
\end{frame}

\begin{frame}\frametitle{Approximation accuracy}
  \includegraphics[width=\textwidth]{amo-fig5.jpg}

  \footnotetext{Equilibrium security investment levels as a function
    of $\alpha$ for a barbell network of size 20. The solid line plots the
    exact equilibrium investment. The dashed line in (a) plots the
    approximation from Eq. (12) and the dashed line in (b) plots the
    approximation from Theorem 3.}
\end{frame}

\subsection{Extensions}

\begin{frame}\frametitle{Endogenous Connections}
  \begin{itemize}
  \item Allow agents to choose $\mathcal{E}_i \subseteq E$ connections
    to maintain
  \item $(i,j)$ maintained iff $(i,j) \in \mathcal{E}_i \cap
    \mathcal{E}_j$
  \item Let $\hat{G} = $ network of maintained connections,
    $C_i(\hat{G})=$ size of component connected to $i$
  \end{itemize}
  \begin{proposition}
    Suppose $c(q) = \frac{\alpha}{2}q^2$ and $\alpha$ large. Then
    agents choose connections to maximize $|\mathcal{E}_i \cap
    \mathcal{E}_j|(1 - C_i(\hat{G})/n)$
  \end{proposition}
\end{frame}

\begin{frame}\frametitle{Strategic Attacks}
  \begin{itemize}
  \item Instead of $\Phi$ fixed, strategic attacker chooses $\Phi$
    given $\mathbf{q}$
  \item Relevant for computer network or power transmission network
    security
  \item Maybe relevant for social networks and diseases if you are a
    pessimist
  \item New externality: investment by one agent shifts attack to
    others
  \item Possible to have more investment in equilibrium than in social
    optimum
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{\cite{fs2014}}
%\begin{frame}
%  \cite{fs2014}
%\end{frame}

% \cite{baqaee2018}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}


\end{document}

\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Network formation}
\author{Paul Schrimpf}
\institute{UBC \\ Vancouver School of Economics \\ 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}\frametitle{Network formation}
  \begin{itemize}
  \item Network formation: model of which nodes are connected
  \item Goal: parsimonious, tractable, and estimable model that
    matches features of observed networks
  \item Types of models
    \begin{itemize}
    \item Random network models: specify $\Pr(i \& j \text{ connect} |
      \text{other connections, node characteristics})$
    \item Strategic network formation: specify payoffs $u_i(G, \cdot)$
      and equilibrium concept (e.g. pairwise stability)
      \begin{itemize}
      \item $G$ is pairwise stable if for each link neither player
        would be better off without it, and there are no two players
        would both be better off by adding a link
      \item Payoffs could come from a subsequent game on the network
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{hsieh2016}}
\begin{frame}\frametitle{``Network Formation with Local Complements
    and Global Substitutes: The Case of R\&D Networks''
    \cite{hsieh2016}}
  \begin{itemize}
  \item Estimable model of R\&D network formation and production
  \item Estimate for chemical firms
  \item Examine key firms and R\&D collaboration subsidies
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}[allowframebreaks] \frametitle{Model}
  \begin{itemize}
  \item Profits
    \[ \pi_i(q,G) = \eta_i q_i - \nu q_i^2 - b q_i \sum_{j \neq i} q_j
      + \rho \sum \sum_{j=1}^n a_{ij} q_i q_j - \zeta d_i \]
    where
    \begin{itemize}
    \item $A$ is collaboration network
    \item $\rho\geq 0$ local complementarity
    \item $b>0$ global substitutability
    \item $d_i =$ number of collaborators
    \end{itemize}
  \item Potential function
    {\footnotesize{
        \[ \Phi(q,G) = \sum_{i=1}^n (\eta_i q_i - \nu q_i^2) -
          \frac{b}{2} \sum_i \sum_{j \neq i} q_i q_j + \frac{\rho}{2}
          \sum_i \sum_j a_{ij} q_i q_j - \zeta m \]
      }}
    is such that
    \begin{itemize}
    \item $\Phi(q, G \oplus (i,j) ) - \Phi(q,G) = \pi_i(q, G \oplus
      (i,j)) - \pi_i(q, G)$
    \item $\Phi(q_i', q_{-i}, G ) - \Phi(q,G) = \pi_i(q_i', q_{-i},
      G) - \pi_i(q, G)$
    \end{itemize}
    \framebreak
  \item Equilibrium:
    \begin{itemize}
    \item ``Natural'' equilibrium concepts (e.g. pairwise stable links
      $+$ Nash in $q$) difficult to characterize and typically not
      unique
    \item Instead, introduce time and stochastic move opportunities,
      solve for unique stationary distribution of $q$, $G$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Network formation process}
  \begin{itemize}
  \item Continuous time
  \item $q \in \mathcal{Q}$ a discrete and bounded set
  \item State of model $\omega_t = (q_t, G_t)$
  \item Move opportunities
    \begin{enumerate}
    \item Quantity adjustment, arrival rate $\chi$ firm $i$ chooses
      $q$ to maximize profits with some error
      {\footnotesize{
      \[ P(\omega_{t+\Delta t} = (q, q_{-it}, G_t) | \omega_t = (q_t,
        G_t)) = \chi \frac{e^{\vartheta \pi_i(q,q_{-it},G_t)}}{
          \int_\mathcal{Q} e^{\vartheta \pi_i(q',q_{-it},G_t)} dq'}
        \Delta t + o(\Delta t) \]}}
  \framebreak
     \item Link formation, arrival rate $\tau$ , $(i,j)$ choose whether
      to link
      {\footnotesize{
      \[ P(\omega_{t+\Delta t} = (q_t, G_t \oplus (i,j)) | \omega_t = (q_t,
        G_t)) = \tau \frac{ e^{\vartheta \Phi(q, G_t \oplus(i,j))} }
        { e^{\vartheta \phi (q, G_t \oplus(i,j))} +  e^{\vartheta
            \phi(q, G_t)} }  \Delta t + o(\Delta t) \]
      }}
      \begin{itemize}
      \item Linking if $\pi_i(q, G_t \oplus(i,j)) - \pi_i(q, G_t) +
        \epsilon_{i,j,t} > 0$ and $\pi_j(q, G_t \oplus(i,j)) - \pi_j(q,
        G_t) + \epsilon_{i,j,t} > 0$
      \item Difference in $\pi$ equal for $i$ and $j$, and $=\Phi(q,G
        \oplus(i,j)) - \Phi(q,G)$
      \end{itemize}
  \framebreak
    \item Link removal, arrival rate $\xi$, $(i,j)$ choose whether to
      remove link
      {\footnotesize{
      \[ P(\omega_{t+\Delta t} = (q_t, G_t \ominus (i,j)) | \omega_t = (q_t,
        G_t)) = \xi \frac{ e^{\vartheta \Phi(q, G_t \ominus(i,j))} }
        { e^{\vartheta \Phi(q, G_t \ominus(i,j))} +  e^{\vartheta
            \Phi(q, G_t)} } \Delta t + o(\Delta t) \]
      }}
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Stationary distribution}
  \begin{itemize}
  \item Model is continuous time, discrete state Markov chain
  \item Stationary distribution:
    \[ \mu^\vartheta(q,G) = \frac{e^{\vartheta \left(\Phi(q,G) - m
            \log(\xi/\tau) \right)} } {\sum_{G' \in \mathcal{G}^n}
        \int_{\mathcal{Q}^n} e^{\vartheta \left(\Phi(q,G') - m'
            \log(\xi/\tau) \right)} dq'}  \]
    where
    \begin{itemize}
    \item Potential function
      {\footnotesize{
      \[ \Phi(q,G) = \sum_{i=1}^n (\eta_i q_i - \nu q_i^2) -
        \frac{b}{2} \sum_i \sum_{j \neq i} q_i q_j + \frac{\rho}{2}
        \sum_i \sum_j a_{ij} q_i q_j - \zeta m \]
      }}
      is such that
      \begin{itemize}
      \item $\Phi(q, G \oplus (i,j) ) - \Phi(q,G) = \pi_i(q, G \oplus
        (i,j)) - \pi_i(q, G)$
      \item $\Phi(q_i', q_{-i}, G ) - \Phi(q,G) = \pi_i(q_i', q_{-i},
        G) - \pi_i(q, G)$
      \end{itemize}
    \end{itemize}
  \item Propositions 2-3 characterize stationary distribution
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Average degree and output}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig1}
\end{frame}

\begin{frame}\frametitle{Output and degree distributions}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig3}
\end{frame}


\begin{frame}\frametitle{Output and degree distributions with Pareto productivity}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig5}
\end{frame}

\begin{frame}\frametitle{Welfare}
  \begin{itemize}
  \item Proposition 5: with homogenous firms, efficient $G$ is either
    complete or empty depending on $\zeta$ (link cost)
  \end{itemize}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig6}
\end{frame}

\subsection{Data}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item CATI and SDC alliance database for R\&D collaborations
  \item Compustat and Orbis for other firm information
  \item PATSTAT for patents
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{R\&D Network}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig7}
\end{frame}

\begin{frame}\frametitle{R\&D Network}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-figf8}
\end{frame}

\begin{frame}\frametitle{Data}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-tab1}
\end{frame}

\begin{frame}\frametitle{Data}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-figf5}
\end{frame}

\begin{frame}\frametitle{Competition Network}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig8}
\end{frame}

\subsection{Estimation}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item MLE using stationary distribution?
    \[ \mu^\vartheta(q,G) = \frac{e^{\vartheta \left(\Phi(q,G) - m
            \log(\xi/\tau) \right)} } {\sum_{G' \in \mathcal{G}^n}
        \int_{\mathcal{Q}^n} e^{\vartheta \left(\Phi(q,G') - m'
            \log(\xi/\tau) \right)} dq'}  \]
    no, denominator too hard to compute
  \item Use MCMC instead
    \begin{itemize}
    \item Still difficult, reports results from 3 different algorithms
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}\frametitle{Estimates}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-tab2}
\end{frame}

\begin{frame}\frametitle{Patent Similarity}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-figf9}
\end{frame}


\begin{frame}\frametitle{Heterogeneous spillovers}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-tab3}
\end{frame}

\begin{frame}\frametitle{Model fit}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-fig9}
\end{frame}


\begin{frame}\frametitle{Key firms}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-tab4}
\end{frame}

\begin{frame}\frametitle{Mergers}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-tab5}
\end{frame}

\begin{frame}\frametitle{Collaboration subsidies}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{hkl-tab6}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{ahrs2011}}
\begin{frame}\frametitle{\cite{ahrs2011}: Network structure of
    production}
  \begin{itemize}
  \item Model of buyer-supplier network of US firms
  \item Common features of observed social \& economic networks: (see \cite{jackson2010})
    \begin{itemize}
    \item Scale-free: degree distribution is Pareto: $P(d) = c
      d^{-\gamma}$ i.e. $\log P(d)$ is linear function of $\log d$.
    \item Small worlds: the diameter \& average path length tends to
      be small even for a large number of nodes (e.g.\ 6 degrees of Kevin
      Bacon; Erd\:{o}s number)
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Background}
\begin{frame}[allowframebreaks]
  \frametitle{Preferential attachment}
  \begin{itemize}
  \item Growing random network model that is scale-free and has small
    worlds
  \item Model: nodes born over time and indexed by date of birth
    \begin{itemize}
    \item Begin with $m$ nodes fully connnected
    \item Time $t$ one node added and forms $m$ connections with
      existing nodes, connects to node $i$ with probability
      $\frac{d_i(t)}{\sum_j d_j(t)} = \frac{d_i(t)}{2tm}$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Mean-field approximation}
  \begin{itemize}
  \item Solving for degree distribution: ``mean-field
    approximation''
    \begin{itemize}
    \item $\Pr(i\text{ gets new link}) = m\frac{d_i(t)}{2tm} =
      \frac{d_i(t)}{2t}$
    \item Approximate time as continuous instead of discrete
      \begin{align*}
        \frac{d}{dt} d_i(t) = \frac{d_i(t)}{2t}
      \end{align*}
      and $d_i(1) = m$, implies
      \begin{align*}
        d_i(t) = m \left(\frac{t}{i}\right)^{1/2}
      \end{align*}
    \item Degree of older nodes $>$ degree of younger nodes, at time
      $t$ node born at time $ i = t\left(\frac{m}{d}\right)^2$ has
      degree $d$, so $F_t(d) = 1 - m^2 d^{-2}$, $P_t(d) = m^2
      d^{-3}$
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Model}

\begin{frame}\frametitle{Observed degree distribution}
  \includegraphics[width=\textwidth]{fig1A}
\end{frame}

\begin{frame}\frametitle{Model overview}
  \begin{itemize}
  \item Directed network of buyers and suppliers
  \item Mix of preferential attachment and random attachment
  \item Adds node death \& reattachment of survivors
  \item Better incorporate features of the actual firm network: firms
    often go out of business, and many suppliers actively prefer to
    work with less-connected downstream firms because of product
    specialization and long-term contracting issues
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Model}
  \begin{itemize}
  \item Notation:
    \begin{itemize}
    \item $N(t)$ firms at time $t$
    \item $n(k,t)$ firms with in-degree $k$ at time $t$
    \item $m(t) = \frac{\sum_k k n(k,t)}{N(t)}$ average in-degree
    \end{itemize}
  \item Each period:
    \begin{enumerate}
    \item Exit: each firm exists with probability $q$; destroys
      $q(2-q) N(t) m(t)$ edges, $q(1-q) N(t) m(t)$ of which have
      receiving vertex survive
    \item Reconnection: surviving firms whose connections were lost
      due to exit reconnect; $q(1-q) N(t) m(t)$ reconnections to make
      \begin{itemize}
      \item $r$ uniformly at random
      \item $1-r$ by preferential attachment
      \end{itemize}
    \item Entry:
    \end{enumerate}
  \item $(g+q)N(t)$ firms enter, each form $m(t)$ edges
    \begin{itemize}
    \item $\delta (1-r)$ by preferential attachment to existing firms
    \item $r\delta$ randomly to existing firms
    \item $1-\delta$ randomly to other entrants
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Mean-field approximation}
  \begin{align*}
    \frac{\partial}{\partial t} n(k,t)  + \frac{\partial}{\partial
      k}\left[n(k,t) \gamma(k,t) \right] = \beta(k,t) N(t) (q+g) - qn(k,t)
  \end{align*}
  \begin{itemize}
  \item $\gamma(k,t) = $ in-degree growth rate
    \begin{itemize}
    \item $= \frac{dk}{dt} = qr(m(t) - k) +
      \frac{\delta(k+r(m(t)-k))(q+g)}{1-q}$
    \end{itemize}
  \item $\beta(k,t) =$ in-degree distribution of entering vertices
    \begin{itemize}
    \item $=$ binomial $\left( (g+q) N(t) (1-\delta)m(t),
        \frac{1}{N(t)(g+q)}\right)$
    \item $\approx \frac{1}{m(t)(1-\delta)}
      e^{-\frac{k}{m(t)(1-\delta)}}$ (exponential)
    \end{itemize}
  \item Let $p(k,t) = \frac{n(k,t)}{N(t)}$,
    \begin{align*}
      \frac{\partial p(k,t)}{\partial t} + \frac{\partial}{\partial
        k}\left[p(k,t) \gamma(k,t) \right] = \beta(k,t)  (q+g) - qp(k,t)
    \end{align*}
  \item Solve for stead-state degree distribution, $p(k)$
    \begin{align*}
       \frac{\partial}{\partial
         k}\left[p(k) \gamma \right] = \beta(k)  (q+g) -
       qp(k)
    \end{align*}
    so
    \begin{align*}
      p(k) = \lambda (k+R)^{-1-S} \left(\Gamma[1+S, R/(m(1-\delta))] -
        \Gamma[1+S,(R+k)/(m(1-\delta))]\right)
    \end{align*}
    where $R$, $S$ and $\lambda$ are functions of $\delta$, $q$, $g$, $m$,
    and $r$
  \end{itemize}
\end{frame}

\subsection{Estimation}

\begin{frame}
  \frametitle{Data}
  \begin{itemize}
  \item Data yearly firm-level data from Compustat
  \item 1979-2007 publicly listed firms
  \item Link $=$ major customer $=$ firm that purchases $\geq$10\%
    of seller's revenue
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{atab1}
\end{frame}

\begin{frame}
  \frametitle{Estimation}
  \begin{itemize}
  \item 5 parameters
    \begin{itemize}
    \item $q = $ exit rate $=$ empirical average $=$ 0.24
    \item $m = $ edges per vertex $=1.06$
    \item $\delta = $ portion of new vertices to existing firms
      $=0.75$
    \item $g=$ growth rate of number of firms $=0.04$
    \item $r = $ fraction of edges assigned randomly estimated by MLE
      for probability a new link among surviving vertices given
      in-degree $=0.18$
    \end{itemize}
  \item Not fitting CDF directly
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Network}
  \includegraphics[width=\textwidth]{fig2}
\end{frame}

\begin{frame}
  \frametitle{Fit}
  \includegraphics[width=0.8\textwidth]{fig1B}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Strategic network formation}

\begin{frame}
  \begin{itemize}
  \item \cite{cfik2010}
  \item \cite{leeFong2013}
  \item \cite{chandrasekhar2013}
  \item \cite{leung2013}
  \item \cite{sheng2012}
  \item \cite{graham2014model}, \cite{graham2014id}
  \end{itemize}
\end{frame}

\subsection{\cite{cfik2010}}

\begin{frame}\frametitle{\cite{cfik2010}}
  \begin{itemize}
  \item Tractable empirical model of network formation
  \item Estimable from data on a single network
  \item Bayesian estimation
  \item Applied to social network of high school students
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item Sequential: $N$ nodes, $T$ periods
  \item Begin with no links
  \item Each period two nodes meet and have opportunity to form a link
  \item Payoff of $i$ from linking with $j$ at time $t$
    \[ U_i(j|\underbrace{X}_{\text{Node
        characteristics}},\underbrace{C}_{\text{link
        characteristics}}, G_{t-1}, t) \]
  \item Link formed if
    \[ g\left(U_i(j|X,C, G_{t-1}, t), U_j(i|X,C, G_{t-1}, t) \right)
    > 0 \]
  \item Myopic behavior:
    \[ U_i(j|X,C, G_{t-1}, t) = U_i(j|X,C,G_{t-1}) \]
    \begin{itemize}
    \item Individuals do no have to take expectation over future links
    \item Avoids multiple equilibria \& computational difficulties
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Empirical specification}
  \begin{itemize}
  \item Preferences:
    \begin{align*}
      U_i(j|X,C,G_{t-1}) = & \beta_0 + \beta_1' x_j - (x_i -
      x_j)'\Omega(x_i - x_j) + \\
      & + \alpha_1 d_{jt} + \alpha_2 d_{jt}^2 +
      \alpha_3 d(i,j;G_{t-1}) + \delta c_{ij} + \epsilon_{ij}
    \end{align*}
    Non-transferable:
    \[ g(u_i,u_j) = \mathbf{1}\{u_i \geq 0 \;\&\; u_j \geq 0 \} \]
  \item $\epsilon_{ij} \sim$ logistic, independent
  \item Sequence of meetings, $M$: assume $T = N(N-1)/2$, each potential
    pair meets exactly once, all sequences equally likely
  \item Parameter meanings:
    \begin{itemize}
    \item $\beta$ individual characteristics
    \item $\Omega$ captures homophily
    \item $\alpha$ network characteristics
    \item $\delta$ pair characteristics
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item Bayesian
  \item Likelihood
    \[ \mathcal{L}(\theta|G,X,C) = \Pr(G|X,C;\theta) = \sum_{M \in
      \mathbb{M}} \Pr(M|X,C;\theta) \Pr(G|M,X,C;\theta) \]
    \begin{itemize}
    \item $\Pr(G|M,X,C;\theta)$ is product of logit probabilities
    \item $|\mathbb{M}|=(N(N-1)/2)!$ is too large for MLE
    \end{itemize}
  \item Compute posterior using MCMC --- Metropolis-Hastings with data augmentation
    \begin{itemize}
    \item Draw $\theta_k | M_k$ from $\Pr(\theta|M_k,G,X,C) \propto
      \Pr(G|M_k,X,C,\theta) \Pr(\theta)$
    \item Draw $M_{k+1} | \theta_{k}$ from $\Pr(M|\theta_k,G,X,C) \propto
      \Pr(G|M_k,X,C,\theta) \Pr(M)$
    \end{itemize}
  \item Data from a single large network
    \begin{itemize}
    \item Properties of estimator as $N \to \infty$ unknown
    \item \cite{chandrasekhar2013}, \cite{leung2013} also have data
      from a single network and show consistency of their estimators
      (but models differ)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item Friendship network in single high school of 669 students, 1541
    links
  \item From AddHealth data set
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Summary statistics}
  \includegraphics[width=\textwidth]{cfik-tab1}
\end{frame}
\begin{frame}\frametitle{Summary statistics}
  \includegraphics[width=\textwidth]{cfik-tab2}
\end{frame}

\begin{frame}\frametitle{Estimates}
  \includegraphics[width=\textwidth]{cfik-tab6}
  \note{There are a couple of interesting observations from the
    posterior distributions. First, the link-specific variable, the
    number of classes potential friends have in common, with parameter
    δ is very important in explaining friendship patterns. Each
    additional class in common increases the log odds ratio of
    friendship by approximately 0.12. Note that friends have on
    average 2.1 classes in common, and non-friends have on average 0.6
    classes in common. Conditional on that grade, age, sex and sports
    participation are only moderately predictive of friendships.
    Second, the homophily effects as captured by Ω are substantial,
    for all four covariates.  Third, network effects are very
    important. The number of friends a potential friend already has, F
    jj , is somewhat important, with people preferring, everything
    else equal, friends who do not have many friends yet. Much more
    important though is the distance between individuals in the
    current network. If potential friends already have friends in
    common, the log odds go up by 2.66, and even if the degree of
    separation or geodesic distance is three (some friends of i have
    friends in common with j), the log odds goes up by 1.22. Compared
    to the effect of the covariates and the number of classes in
    common, these effects are large.}
\end{frame}


\begin{frame}\frametitle{Fit}
  \includegraphics[width=\textwidth]{cfik-tab3}
\end{frame}
\begin{frame}\frametitle{Fit}
  \begin{columns}[T]
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{cfik-fig1}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=\textwidth]{cfik-fig2}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}\frametitle{}
  \includegraphics[width=\textwidth]{cfik-tab7}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{\cite{balland2013}}
% \begin{frame}\frametitle{\cite{balland2013}}
%   \begin{itemize}
%   \item Dynamics of networks among video game firms
%   \item 3 mechanisms driving network formation:
%     \begin{enumerate}
%     \item Endogenous, path-dependent network effects
%     \item Proximity --- geographic, genre, etc
%     \item Firm characteristics
%     \end{enumerate}
%   \item Finds constant endogenous network effects and increasing role
%     of geographic and genre proximity over time
%   \end{itemize}
% \end{frame}

% \begin{frame}\frametitle{Data}
%   \begin{itemize}
%   \item Data from Mobygames (same as \cite{claussen2012}) and German Online Games Datenbank
%   \item Games for consoles
%   \item Firms linked if co-produces a game (>75\% of games involve
%     more than one company)
%   \end{itemize}
% \end{frame}

% \begin{frame}\frametitle{Data}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-tab1}
% \end{frame}

% \begin{frame}\frametitle{Data}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-fig1}
% \end{frame}

% \begin{frame}\frametitle{Data}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-tab2}
% \end{frame}

% \begin{frame}\frametitle{Data}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-tab3}
% \end{frame}

% \begin{frame}[shrink]\frametitle{Model}
%   \begin{itemize}
%   \item Broadly similar to \cite{cfik2010}, but actual sequence of
%     formation observed
%   \item ``Stochastic Actor Oriented Model'' SAOM
%   \item Continuous time
%   \item Move opportunities are Poisson with arrival rate $\lambda_i$
%   \item Network at time $t$ denoted $X(t)$
%   \item Links formed/removed with logistic probabilities
%     \begin{align*}
%       \Pr & \left(X(t)\text{ changes to } x | i \text{moves, }
%       X(t)=x^0\right) = \\
%       & = \frac{\exp(\sum_{k} \beta_k s_{ki}(x^0,x,v,w))}{\sum_{x' \in
%         C_i(x^0)} \exp(\sum_k \beta_k s_{ki}(x^0,x',v,w))}
%     \end{align*}
%     where
%     \begin{itemize}
%     \item $C_i(x^0) = $ possible new networks given $i$ moving
%     \item $s_{ki}(x^0,x,v,w)$ statistic involving old network, $x^0$,
%       new network $x$, firm characteristics $v$, and proximity $w$
%     \end{itemize}
%   \item Estimated by MCMC using SIENA software
%   \end{itemize}
% \end{frame}

% \begin{frame}\frametitle{$s_{ki}(x^0,x,v,w)$}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-tab4}
% \end{frame}

% \begin{frame}\frametitle{Proximity and firm characteristics}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-tab5}
% \end{frame}

% \begin{frame}\frametitle{Estimates}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-tab6}
% \end{frame}

% \begin{frame}
%   \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{balland-fig2}
% \end{frame}

% \begin{frame}\frametitle{Results}
%   \begin{itemize}
%   \item Negative effect of density -- cost of links?
%   \item Proximity meaures
%     \begin{itemize}
%     \item Geographic proximity $=$ distance, increased effect over time
%     \item Organizational proximity $=$ whether have common owner
%     \item Institutional proximity $=$ in the same country, reduced
%       importance over time
%     \item Cognitive $=$ same genres
%     \item Social $=$ number of previous games together
%     \end{itemize}
%   \end{itemize}
% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\cite{chandrasekhar2013}}

\begin{frame}[shrink]\frametitle{\cite{chandrasekhar2013}}
  \begin{itemize}
  \item Consistent and tractable network formation model
  \item Setup nests variant of \cite{cfik2010} model
  \item Starting point: exponential random graph (ERGM):
    \begin{itemize}
    \item Network $g \in G$
    \item Vector of statistics $S(g)$
    \item Likelihood:
      \[ P_\theta(g) = \frac{e^{\theta S(g)}} {\sum_{g' \in G}
        e^{\theta S(g')}} \]
    \item Broad class, can represent any random graph model
    \item Used in many applications
    \end{itemize}
  \item Challenges of ERGMs: set of networks, $G$ very large, typically
    estimated by MCMC, but consistency unknown and mixing time
    exponential in number of nodes
  \item This paper: propose a related class of models, give conditions
    for consistent and asymptotically normal estimation, give examples
    of strategic network formation models that fit into setup
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{SERGM}
  \begin{itemize}
  \item Statistical exponential random graph model
  \item Write model on space of statistic instead of network
    \[ \Pr_{\beta,K}(s) = \frac{K(s) e^{\beta s}}{ \sum_{s' \in A}
      K(s') e^{\beta s'}} \]
  \item Estimate $\beta$ by MLE or GMM
  \item Sum in denominator is over space of statistic instead of
    possible networks
  \item Sufficient conditions for consistent, asymptotically normal $\hat{\beta}$
    (loosely):
    \begin{itemize}
    \item Statistics are counts, e.g.\ of links, triangles, stars, etc
    \item Graph is not too dense
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{SUGM}
  \begin{itemize}
  \item Subgraph generation models
  \item List of subgraph types $G_\ell^n$, $\ell = 1, ..., k$
  \item Probabilities $p_\ell^n$ of each type
  \item Formation:
    \begin{itemize}
    \item Each subnetwork in $G^n_1$ formed with probability $p_1^n$
    \item Repeat for $\ell = 2, ..., n$
    \end{itemize}
  \item E.g. Erdos-Renyi: $G_1^n = $ all pairs of nodes
  \item $\hat{p}_\ell^n$ consistent and asymptotically normal if
    network is sparse
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Strategic network formation as SUGM}
  \begin{itemize}
  \item If payoff depends only on subgraph, then natural
  \item I.e. if $u_i(g)$ only depends on direct connection or direct
    connections $+$ friends of friends etc
  \item E.g. in \cite{cfik2010}
    \begin{align*}
      U_i(j|X,C,G) = & \beta_0 + \beta_1' x_j - (x_i -
      x_j)'\Omega(x_i - x_j) + \\
      & + \alpha_1 d_{j} + \alpha_2 d_{j}^2 + \delta c_{ij} + \\ & +
      \alpha_3 1\{d(i,j;G)= 2\} + \alpha_4 1\{d(i,j;G)= 3\} +  \epsilon_{ij}
    \end{align*}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\cite{leeFong2013}}

\begin{frame}\frametitle{\cite{leeFong2013}}
  \begin{itemize}
  \item Dynamic network formation model with transfers
  \item Applicable to bilateral contracting between firms, e.g.
    \begin{itemize}
    \item Manufacturers \& retailers
    \item Health insurers \& providers
    \item Hardware \& software
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Infinite horizon, discrete time
  \item Network $g \in G$
  \item Contracts (payments) $t_g = \{ t_{ij;g} \}_{ij \in g}$
  \item Per-period payoffs:  $\pi_i(g,t_g)$
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Model: each period}
  \begin{itemize}
  \item Start with network $g^{\tau-1}$
  \end{itemize}
  \begin{enumerate}
  \item Network formation:
    \begin{enumerate}
    \item Simultaneously announce links $a_i$ that want to negotiate,
      private payoff shock $\epsilon_{a_i,i}$ received
    \item Network of negotiations: $\tilde{g}(a)$
      \begin{itemize}
      \item If $i$ \& $j$ both announced link, $ij \in \tilde{g}(a)$,
      \item Everyeone pays cost $c_i(\tilde{g}(a) | g^{\tau-1} )$
      \end{itemize}
    \end{enumerate}
    \pause
  \item Bargaining:
    \begin{enumerate}
    \item Additive payoff shocks $\eta_{ij}$ observed
    \item Unstable links $ij \in \tilde{g}$ with no gains from trade
      (given rest of network) dissolves, repeat until no such pairs
      remain to get $g^\tau \subseteq \tilde{g}$
    \item Contracts $t_g^\tau$ determined by Nash bargaining, payoffs
      realized
      \[ \bar{\pi}_i(g^\tau, \eta, t_g^\tau) =
      \pi_i(g^\tau,t_g^\tau) + \sum_{ij \in g^\tau} \eta_{ij} \]
    \end{enumerate}
  \end{enumerate}
\end{frame}

\begin{frame}[shrink]\frametitle{Model - dynamics}
  \begin{itemize}
  \item Markov strategies $\sigma_i(g,\epsilon_i)$
  \item Conditional choice probabilities $P^\sigma_i(a|g) = \int
    \mathbf{1}\{\sigma_i(g,\epsilon_i) = a\}f(\epsilon_i) d\epsilon_i$
  \item $\Gamma(g;\eta,V^\sigma) = $ subnetwork $g' \subseteq g$ such
    that all pairs stable
  \item Negotiation network probabilities
    \[ q_i^\sigma(g'|a_i,g) = \sum_{a_{-i}} \prod_{j \neq i}
      P_j^\sigma(a_j|g) I\{\tilde{g}(a) = g'\} \]
  \item Choice-specific value function
    \begin{align*}
      v_i^\sigma(a,g) = \sum_{g'} q_i^\sigma(g'|a,g) \bigl( c_i(g'|g)
        + \Er_\eta \bigl[ & \bar{\pi}_i(g'',\eta,t_{g''}^\sigma) + \beta
          V_i^\sigma(g'') :  \\
        & : g'' = \Gamma(g;\eta,V^\sigma) \bigr]
      \bigr)
    \end{align*}
  \item Value function
    \[ V_i^\sigma(g) = \int \left(\max_{a} \epsilon_{a,i} +
      v_i^\sigma(a_i,g) \right) f(\epsilon_i) d\epsilon_i \]
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model - bargaining}
  \begin{itemize}
  \item Nash bargaining:
    \begin{itemize}
    \item Surplus of $i$ from trading with $j$
      \begin{align*}
        \Delta S^{\sigma}_{i,j} (g;\eta,\{t, t_{-ij:g}^\sigma\}) = &
        \left(\bar{\pi}_i(g,\eta,\{t,t_{-ij:g}\}) + V_i^\sigma(g) \right)- \\
        & - \left(
          \bar{\pi}_i(g-ij, \eta, t_{-ij:g}) + V_i^\sigma(g - ij) \right)
      \end{align*}
    \item Assumes if $ij$ do not link, other links unaffected today
      (but they could be in the future)
    \end{itemize}
    \begin{align*}
      t_{ij;g}(\eta) \in \argmax_{\tilde{t}} \Delta
      S^{\sigma}_{i,j}(g;\eta,\{\tilde{t}, t_{-ij:g}^\sigma\})^{b_{ij}} \Delta
      S^{\sigma}_{j,i}(g;\eta,\{\tilde{t}, t_{-ij:g}^\sigma\})^{b_{ji}}
    \end{align*}
  \item Equilibrium existence from Brouwer's fixed point theorem
  \item Equilibrium may not be unique
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example}
  \includegraphics[width=\linewidth]{lf-fig1}
  \begin{itemize}
  \item Contracting externalities
  \item Static model (or equivalently $\beta=0$) with
    equal bargaining power
    \begin{itemize}
    \item $t_{1,j}(g_2) = 6$, $t_{1,j}(g_3) = 4$
    \end{itemize}
  \item Dynamic model with $\beta = 0.9$, $c()=1$, $var(\epsilon) = \pi^2/8$
    \begin{itemize}
    \item $t_{1,j}(g_2) \approx 7.6$, $t_{1,j}(g_3) = 4.4$
      \begin{itemize}
      \item Chance of downstream firms being unlinked for multiple
        periods lowers value of their outside option
      \end{itemize}
    \item Distribution of states $[g_0,g_1,g_2,g_3]
      \approx[.00,.43,.43,.14]$, $P(g_1|g_2) = P(g_2|g_2) \approx 0.8$
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Estimation}
  \begin{itemize}
  \item Much like dynamic games
  \item Approaches:
    \begin{itemize}
    \item Constrained MLE: maximize pseudo-likelihood subject to equilibrium
      constraints
    \item Two-step:
      \begin{enumerate}
      \item Estimate policy functions: using Hotz-Miller inversion
        (e.g. with type I extreme value shocks)
        \[ \hat{\sigma}_i(g,\epsilon) = \argmax_a \log(\hat{P}_i(a|g))
        + \epsilon \]
      \item Let $\tilde{\sigma}_i(\cdot;\theta)$ be the best response
        of player $i$ when payoff parameters are $\theta$ and other
        players play $\hat{\sigma}_{-i}$, estimate $\theta$ to
        minimize
        \[ \hat{\theta} = \argmin \sum_{a,g,i}
        \left(P_i^{\tilde{\sigma}_i;\hat{\sigma}_{-i}}(a|g) -
            P_i^{\hat{\sigma}}(a|g) \right)^2 \]
      \end{enumerate}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Identification}
  \begin{itemize}
  \item ``Intuitively, if there are gains from trade between two agents
    who form a link (given the actions of others), a static model
    would predict that the link should form regardless of which agent
    obtains a larger share. However, in a dynamic model, different
    values of Nash bargaining parameters will change each agent’s
    respective outside options through their continuation values, and
    hence only certain parameter values will be consistent with a link
    forming in equilibrium.''
  \item What data is observed?
    \begin{itemize}
    \item Realized sequence of networks?
    \item Sequence of networks $+$ actions $=$ announcements (i.e. we
      see potential links where negotiations failed)
    \item 2-step estimator assumes the announcements observed, single
      step estimator allows only networks to be observed
    \item Section 4.2 about estimation of bargaining parameter assumes
      $(N, G, \pi, \beta, f, c)$ either observed, assumed, or can be
      separately estimated
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Identification if $\pi$, $c$ not known}
  \begin{itemize}
  \item Assuming announcements observed, usual dynamic decision model
    identifies per-period payoff:
    \[ \tilde{\pi}(a|g) = \sum_{g'} q^P(g'|a,g) \left( c_i(g'|g) +
      \Er_\eta[\pi_i(\Gamma(g',\eta),t_{\Gamma(g',\eta)}^P,\eta)] \right) \]
  \item $q^{P}(g'|a_i,g)$ is known, so variation in $a_i$ identifies
    \[ c_i(g'|g) + \Er_\eta[\pi_i(\Gamma(g',\eta),t_{\Gamma(g',\eta)}^P,\eta)]  \]
  \item Need restriction to separate $c_i$ and $\pi_i$, e.g. assume
    $c_i(g'|g) = 0$ if $g'=g$
  \item $\Gamma(g',\eta) = $ stable subnetwork of $g'$
    \[ \Gamma(g,\eta) = \begin{cases} g & \text{ if $\exists t$
        s.t. $\Delta S_{ij}(G;\eta, t) \geq 0$ for all $ij \in g$ } \\
      \Gamma(g',\eta) \text{ otherwise where $g' = g \setminus \{ij \in g :
        \Delta S_{ij}(G,\eta,t) < 0$}
    \end{cases}
    \]
  \item Need to untangle $\Gamma$, $\eta$, and $\pi$ from bargaining
  \item Estimator assumes $\eta$ degenerate
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Example: Insurer-Provider negotiations}
  \begin{itemize}
  \item Simulate version of model designed to reflect features of
    HMO-hospital network
  \item Look at performance of estimator
  \item Ignoring dynamics biases estimates of payoffs (table 2)
  \item Estimates of bargaining power appear unbiased and precise
    (table 3)
  \item Simulate hospital mergers
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{lf1}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{lf2}
\end{frame}
\begin{frame}
  \includegraphics[width=\textwidth]{lf3}
\end{frame}
\begin{frame}\frametitle{Merger simulation}
  \includegraphics[width=\textwidth]{lf4}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}


\end{document}

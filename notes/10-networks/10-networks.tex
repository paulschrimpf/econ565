\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Network structure and outcomes}
\author{Paul Schrimpf}
\institute{UBC \\ Vancouver School of Economics}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Overviews:
    \begin{itemize}
    \item \cite{jackson2010},
      \url{https://class.coursera.org/networksonline-001}
    \item \cite{goyal2012}
    \end{itemize}
  \item Network industries: \cite{economides1996}, \cite{ee1996}
  \end{itemize}
\end{frame}

\begin{frame}
  \tableofcontents
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}[allowframebreaks]
  \frametitle{Introduction}
  \begin{itemize}
  \item Network: nodes \& links between them
  \item Questions:
    \begin{itemize}
    \item How does network structure affect behavior?
    \item How are networks formed?
    \end{itemize}
  \item Focus on networks that are not owned by a single entity
    \begin{itemize}
    \item i.e. not on network industries where a single firm owns and
      controls its network (telecom, electricity, airlines, etc)
    \item Relatively new area, little empirical work
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Networks in IO}
  \begin{itemize}
  \item R\&D collaboration
  \item Trade
  \item Buyer-supplier
  \item Consumer information \& targeting
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Describing Networks}

\begin{frame} \frametitle{Basic notation}
  \begin{itemize}
  \item Nodes $\i \in \{1, ..., N \} = \mathcal{N}$
  \item Adjacency matrix $G$, $N \times N$ matrix with $g_{ij}$ representing
    connection between $i$ and $j$
  \item Graph $\equiv (\mathcal{N}, G)$
    \begin{itemize}
    \item Undirected $\equiv$ symmetric $G$
    \item Directed $\equiv$ asymmetric $G$
    \item Unweighted (or discrete) $\equiv g_{ij} \in \{0,1\}$
    \item Weighted $\equiv$ not discrete
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] \frametitle{Summary statistics}
  \begin{itemize}
  \item Distance between two nodes $=$ shortest path between them
    ($\infty$ if no connected path)
  \item Diameter $=$ largest distance between nodes
  \item Clustering
    \begin{itemize}
    \item of graph is portion of $j$ and $k$ connected given $j$ and
      $k$ both connected to $i$
    \item of node $i$ is the portion of time $j$ and $k$ are
      connected directly given $j$ and $k$ are connected to $i$
    \item average clustering of graph $=$ average across nodes of node
      clustering
    \end{itemize}
  \item Degree of a node $=$ number of links
    \begin{itemize}
    \item Directed graphs: in-degree \& out-degree
    \item Degree centrality $= \frac{\text{degree}}{N-1}$
    \item Network density $=$ fraction of possible links present $=
      \frac{\text{average degree}}{N-1}$
    \item Degree distribution $=$ CDF of node degree
    \end{itemize}
  \item Centrality measures:
    \begin{itemize}
    \item Degree, closeness, betweenness, decay
    \item Eigenvector, Katz, Bonacich
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Network structure \& outcomes}

\begin{frame}\frametitle{Network structure \& outcomes}
  \begin{itemize}
  \item Question: how does network structure affect some outcome?
  \item Reduced form work: regress outcome on node or network summary
    statistics
  \end{itemize}
\end{frame}


\subsection{\cite{fg2011}}

\begin{frame}
  \frametitle{Example: \cite{fg2011}}
  \begin{itemize}
  \item Knowledge spillovers in open-source projects
  \item Data:
    \begin{itemize}
    \item Sourceforge
    \item Contributor network: linked if participated in same project
    \item Project network: linked if have common contributors
    \end{itemize}
  \item Question: how important are project vs contributor spillovers
    for project success?
    \begin{itemize}
    \item Project spillover $=$ developers learn from working on a
      particular project
    \item Contributor spillover $=$ developers learn from working with
      other developers
    \end{itemize}
  \item Related paper: \cite{claussen2012}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example: \cite{fg2011}}
  \includegraphics[width=\textwidth]{fg-fig1.png}
\end{frame}

\begin{frame}
  \frametitle{Example: \cite{fg2011}}
  \begin{table}\caption{The Distribution of Contributors per Project and Projects per Contributor}
    \begin{tabular}{cc|cc}
      \multicolumn{2}{c}{Project Network} &
      \multicolumn{2}{c}{Contributor Network} \\
      Contributors & N Projects&Projects &N Contributors \\
      1& 77,571&1&123,562 \\
      2& 17,576&2& 22,690 \\
      3–4& 11,362&3–4& 10,347 \\
      5–9&  6,136&5–9&  3,161 \\
      10–19&  1,638&10–19&   317 \\
      20–49&   412&20–49&    26 \\
      $\geq$ 50&    56&$\geq $50&     1 \\
      Total projects&114,751&Total contributors&160,104
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}
  \frametitle{Example: \cite{fg2011}}
  \begin{table}\caption{Distribution of Component Size}
    \begin{tabular}{cc}
      Component Size (Contributors)&Components (Subnetworks) \\
      55,087&    1 \\
      196&    1 \\
      65–128&    2 \\
      33–64&   27 \\
      17–32&  152 \\
      9–16&  657 \\
      5–8& 2,092 \\
      3–4& 4,810 \\
      2& 8,287 \\
      1&47,787
    \end{tabular}
  \end{table}
\end{frame}


\begin{frame}
  \frametitle{Example: \cite{fg2011}}
  \begin{table}\caption{Distribution of Degree}
    \begin{tabular}{cc}
      Degree&Number of Contributors \\
      0&47,787 \\
      1&22,133 \\
      2&14,818 \\
      3–4&20,271 \\
      5–8&20,121 \\
      9–16&16,228 \\
      17–32&10,004\\
      33–64& 5,409\\
      65–128& 2,040\\
      129–256&  802\\
      257–505&  491
    \end{tabular}
  \end{table}
\end{frame}

\begin{frame}
  \frametitle{Empirical specification}
  \begin{itemize}
  \item Degree centrality as measure of direct connections
  \item Closeness centrality $= C_C(i) = \frac{N-1}{\sum_{j \in
        \mathcal{N}} d(i,j)}$ --- conditional on degree measures
    indirect connections
  \item $S_i = $ success $=$ number of downloads
  \end{itemize}
  \begin{align*}
    S_i  = \alpha + \gamma C_c(i) / (N-1) + \beta \mathrm{degree}_i +
    \text{controls}
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Results - project network}
  {\tiny{
  \begin{table}\caption{Regression Results: Dependent Variable:
      ldownloads}
    \begin{tabular}{lcc|cc}
    & Regression 1 & (All 66,511 Projects) & Regression 2 & (Giant
    Component: 18.697 Projects) \\
Independent variables & Coefficient & T-Statistic & Coefficient &
T-Statistic \\
Constant & 0.72 & 17.76 & 1.45 & 3.62 \\
lyears\_since & 1.42 & 60.66 & 1.68 & 31.08 \\
lcount\_topics & 0.23 & 9.07 & 0.18 & 3.59 \\
lcount\_trans & 0.35 & 11.73 & 0.45 & 8.15 \\
lcount\_aud & 0.36 & 10.44 & 0.44 & 5.85 \\
lcount\_op\_sy & 0.11 & 5.95 & 0.18 & 5.00 \\
ds\_1 & −1.96 & −60.57 & −2.01 & −31.90 \\
ds\_2 & −0.60 & −17.58 & −0.78 & −11.50 \\
ds\_3 & 0.89 & 25.83 & 0.66 & 9.95 \\
ds\_4 & 1.86 & 57.21 & 1.80 & 29.27 \\
ds\_5 & 2.72 & 79.97 & 2.61 & 40.96 \\
ds\_6 & 2.12 & 27.07 & 2.03 & 15.35 \\
inactive & 0.45 & 6.11 & 0.39 & 2.75 \\
lcpp & 0.46 & 18.71 & 0.87 & 29.34 \\
ldegree & 0.19 & 9.45 & 0.079 & 2.10 \\
giant\_comp & −0.21 & −3.86 &   &   \\
lgiant\_cpp & 0.44 & 12. 05 &   &   \\
lgiant\_degree & −0.05 & −1.26 &   & \\
lcloseness &   &   & 0.69 & 3.21 \\
Number of observations & 66,511 & & 18,697 \\
Adjusted R$^2$ & 0.41  & & 0.40
\end{tabular}
\end{table}
}}
\end{frame}

\begin{frame}\frametitle{Contributor effects}
  \begin{itemize}
  \item Regress downloads on average contributor degree and average
    contributor closeness centrality (and controls)
  \item Result:
    \begin{itemize}
    \item Coefficient on log average closeness $= 0.12$, with $t=1.59$
    \item Coefficient on log average degree $= -0.019$, with $t=-0.72$
    \end{itemize}
  \item Including both contributor and project measures, project ones
    significant, contributor ones not
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Robustness}
  {\tiny{
  \begin{tabular}{l|cc|cc|cc}

    Dept Variable: Ldownloads & $\geq 2$yr, & $\geq 200$dl & $<3.6$yr, & $\geq 200$dl & $\geq 2$yr, & $\geq 200$dl \\
    Independent variables &Coef &T‐Stat &Coef &T‐Stat &Coef &T‐Stat \\
    \hline
Constant &5.75 &13.35 &6.21 &14.55 &8.51 &16.29 \\
lyears\_since &1.08 &11.18 &0.91 &11.40 &1.06 &10.97\\
lcount\_topics &−0.06 &−1.31 &−0.06 &−1.12 &−0.06 &−1.23\\
lcount\_trans &0.42 &9.61 &0.44 &8.83 &0.41 &9.39\\
lcount\_aud &0.21 &2.65 &0.46 &5.62 &0.18 &2.28\\
lcount\_op\_sy &0.26 &7.91 &0.26 &6.62 &0.26 &7.94\\
ds\_1 &−0.46 &−6.83 &−0.75 &−6.23 &−0.46 &−6.88\\
ds\_2 &−0.57 &−7.31 &−0.52 &−4.80 &−0.57 &−7.68\\
ds\_3 &−0.27 &−4.35 &−0.23 &−2.72 &−0.28 &−4.44\\
ds\_4 &0.19 &3.45 &0.18 &2.41 &0.18 &3.24\\
ds\_5 &0.75 &12.99 &0.54 &6.92 &0.73 &12.80\\
ds\_6 &0.73 &7.00 &0.45 &3.06 &0.72 &6.89\\
Inactive &−0.018 &−0.12 &0.02 &0.11 &−0.041 &−0.28\\
lcpp &0.76 &22.21 &0.63 &19.30 &0.59 &15.38\\
ldegree &0.19 &5.07 &0.0038 &0.09 &−0.019 &−0.43\\
lcloseness &0.71 &3.28 &0.54 &2.37 &0.45 &2.08\\
lbetweenness &&&&&0.30 &9.21\\
    Number of observations & 6,397 & & 4,086& &6,397\\
    Adjusted R$^2$ & 0.28 & & 0.25 & & 0.29
  \end{tabular}
  }}
\end{frame}

\begin{frame}
  \frametitle{Limitations}
  \begin{itemize}
  \item How to interpret results?
    \begin{itemize}
    \item Network structure affects downloads
    \item Downloads affect contributions, which affects network structure
    \end{itemize}
  \item Why closeness centrality and degree? (they do explore
    robustness to other measures, but none of them theoretically
    motivated)
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\cite{claussen2012}}

\begin{frame}\frametitle{\cite{claussen2012}}
  \begin{itemize}
  \item Developer networks in electronic games
  \item Panel data 1972-2007 on games \& developers
  \item Construct network of developers 1995-2007
  \item Developers linked in year $t$ if worked together anytime
    between 1972 and $t$
  \item Look at relationship between revenue (or rating) \& degree centrality \&
    closeness centrality
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{claussen2012}}
  \begin{equation*}
    S_{igdpt} =  \alpha_i + \alpha_d + \alpha_p + \alpha_t + \beta_1 D_{igdpt-1}
    + \beta_2 C_{igdpt-1} + CV_{igdpt} \gamma + \epsilon_{igdpt}
  \end{equation*}
  \begin{itemize}
  \item Developer $i$
  \item Game $g$
  \item Developing firm $d$
  \item Publisher $p$
  \item Year $t$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{claussen2012}}
  \includegraphics[width=\linewidth]{cfg-tab1}
\end{frame}

\begin{frame}\frametitle{\cite{claussen2012}}
  \includegraphics[height=\textheight]{cfg-tab3}
\end{frame}

\subsection{Related Subsequent Work}

\begin{frame}
  \frametitle{\cite{gandal2016}}
  ``Network dynamics and knowledge transfer in virtual organisations''
  \begin{itemize}
  \item Panel data of Sourceforge contributions
  \item Look for direct \& indirect spillovers
  \item Programmers who work on many projects positively impact
    success beyond their effect on connectivity in the network
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\cite{athey2014}}
  ``Dynamics of Open Source Movements''
  \begin{itemize}
  \item Dynamic model of open source contributions and commercial
    competitors
  \item Theory paper, not empirical
  \end{itemize}
\end{frame}

%--------------------------------------------------------------------------------
\subsection{\cite{zacchia2019}}

\begin{frame}
  \frametitle{Knowledge Spillovers through Networks of Scientists}
  \cite{zacchia2019}
  \begin{itemize}
  \item Weighted network of publicly traded companies
  \item Links = proportion of firms' inventors that have former patent
    collaborations
  \item Main endogeneity concern: common unobservables
  \item IV motivated by model of firm interaction
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Network Among Inventors}
  \begin{itemize}
  \item Inventor $m$ and $n$ linked ($p_{(mn)t}=1$) if $m$ and $n$
    collaborated on any past patent
  \end{itemize}
  \includegraphics[width=\textwidth]{z-graph2}
\end{frame}

\begin{frame}
  \frametitle{Network Among Firms}
  {\scriptsize{
  $$
  c_{(ij)t}^f = f\left(\frac{\text{inv. of i connectected to j at t} +
    \text{inv. of j connectected to i at t}}
    {\text{inv. of i at t} + \text{inv. of j at t}}\right)
  $$
  }}
  \begin{itemize}
  \item Symmetric
  \item $0 \leq c_{(ij)t}^f \leq 1$
  \item In empirical results, $f(\cdot) = \sqrt{\cdot}$ and $g_{(ij)t}
    = c_{(ij)t}^{\sqrt{\cdot}}$
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{z-fig1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{z-fig2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{z-fig3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{z-fig4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{z-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{z-fig5}
\end{frame}

\begin{frame}\frametitle{Econometric Model}
  $$
  \log Y_{it} =\alpha_i + \sum_{q=1}^Q \beta_q \log X_{itq} + \gamma
  \log S_{it}+\delta \sum_{j=1}^N g_{(ij)t} \log S_{jt} + \tau_t +
  \nu_{it}
  $$
  \begin{itemize}
  \item Output $Y_{it}$
  \item Inputs $X_{itq}$
  \item R\&D stock $S_{it}$ (depreciated past sum of R\&D
    expenditures)
  \item $\delta = $ strength of R\&D spillovers
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Endogeneity}
  \begin{itemize}
  \item $\Er[\log S_{jt} \nu_{it} ] \neq 0$ from e.g. $\nu_{jt}$
    correlated with $\nu_{it}$, or $S_{jt}$ chosen with some knowledge
    of $\nu_{it}$
  \item Endogenous connections $\Er[g_{(ij)t} \nu_{it} ] \neq 0$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Analytic Framework}
  \begin{itemize}
  \item Firms $\mathcal{I}$ with connection $\mathcal{G}$
  \item Knowledge capital
    $$
    \tilde{S}_i = S_i^\gamma \left(\prod_j S_j^{g_{ij}}\right)^\delta
    $$
  \item R\&D cost $e^{\bar{\omega}_i} S_i$
  \item Cobb-Douglas Production as above
  \item Firms maximize profits (output minus linear input costs minus
    R\&D costs)
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Equilibrium}
  \begin{itemize}
  \item Unique Bayes-Nash equilibrium with
    {\scriptsize{
        \begin{align*}
    \log S_i^\ast & = \frac{\log \gamma + \sum_q \beta_q (\log \beta_q
                    - \log \xi_q - \log \gamma)}{1 - \gamma - \sum_q
                    \beta_q} b_i^*(\mathcal{G};\vartheta) +
                    s_i^* (\Omega_i;\mathcal{G})
                 \end{align*}
                 }}
    where
    \begin{itemize}
    \item $\vartheta = \frac{\delta}{1 - \gamma - \sum_q
        \beta_q}$
    \item $b_i^*(\mathcal{G};\vartheta)$ is Katz-Bonacich network
      centrality
    \item $\Omega_i$ is firm's information set and
      $$
      s_i^*(\Omega_i;\mathcal{G}) = \frac{\omega_i - (1 - \sum_q
        \beta_q \bar)\omega_i + \log \Er[\prod_j e^{g_{ij}\delta
          s_j^*(\Omega_j,\mathcal{G})} | \Omega_i]}{1 - \gamma - \sum_q
        \beta_q}
      $$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Identifying Assumptions}
  \begin{itemize}
  \item Correlation of $\log S_{jt}$ with unobservables of $i$ happens
    through $s_i^*(\Omega_i;\mathcal{G})$ due to $\omega_{it}$ and
    $\omega_{jt}$ possibly being correlated and $\Omega_i$
    potentially being informative about $\omega_{jt}$
  \item Assumption 1: $\exists C > 0$ such that if distance from $i$
    to $j$ is greater than $C$, then $Cov(\omega_{i}, \omega_j |
    d_{ij} > C) = 0$ and $Cov(\bar\omega_{i}, \bar\omega_j |
    d_{ij} > C) = 0$
  \item Assumption 2: $\exists L > 0$ such that $d_{ij}>L$ implies
    $(\omega_j, \bar\omega_j) \not\in \Omega_i$
  \item Implies:
    \begin{align*}
      Cov(\omega_i, \log S_j | d_{ij} > C+ L ) & = 0 \\
      Cov(\log S_i \log S_j | d_{ij} \leq C + 2L) & \lesseqgtr 0 \\
      Cov(\log S_i \log S_j | d_{ij} > C + 2L) & = 0
    \end{align*}
  \item Use $\log S_k$ as instrument for $\log S_j$ when $k$ and $j$
    are distance $C+L < D \leq C + 2L$ apart
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{z-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{z-tab3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{z-tab4}
\end{frame}

\begin{frame}\frametitle{Conclusions}
  \begin{itemize}
  \item $D=2$ IV near OLS implies $D=2$ might be too small, i.e. $C=2$
  \item Marginal social return
    $=(\gamma + \delta \bar g_i)\frac{Y_i}{S_i} = 114\%$ considerably
    greater than marginal private return
    $= \gamma \frac{Y_i}{S_i} = 102\%$
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\cite{acemoglu2016}}

\begin{frame}\frametitle{\cite{acemoglu2016}}
  ``Innovation network''
  \begin{itemize}
  \item Directed network of patent citations, 1975-2004
  \item Results:
    \begin{itemize}
    \item Network stable over time
    \item Past innovations (patents) in connected industries predict
      current patents
    \item Impact of innovations are localized
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{aak-fig1}
\end{frame}

\begin{frame}\frametitle{1975-1984}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{aak-fig2a}
\end{frame}

\begin{frame}\frametitle{1985-1994}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{aak-fig2b}
\end{frame}

\begin{frame}\frametitle{1995-2004}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{aak-fig2c}
\end{frame}

\begin{frame}\frametitle{Predicting patents}
  \begin{itemize}
  \item Predicted patents in industry $j$ from past citations:
    \[ \hat{P}_{j,t} = \sum_{k \neq j} \sum_{a = 1}^{10}
      \frac{Citations_{j \to k, a}}{Patents_k} P_{k,t-a} \]
    where
  \item  $Citations_{j \to k, a} = $ citations of a patent in
    industry $k$ that is $a$ years old from $j$
  \item $Patents_k = $ total patents in $k$
  \item Both estimated using 1975-1994 data
  \item Predictions for 1995-2004
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{aak-fig7}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{aak-tab2}
\end{frame}


\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}


\end{document}


    @article{2004,
     jstor_articletype = {research-article},
     title = {Market Sharing Agreements and Collusive Networks},
     author = {Belleflamme, Paul and Bloch, Francis},
     journal = {International Economic Review},
     jstor_issuetitle = {},
     volume = {45},
     number = {2},
     jstor_formatteddate = {May, 2004},
     pages = {pp. 387-411},
     url = {http://www.jstor.org/stable/3663525},
     ISSN = {00206598},
     abstract = {We analyze reciprocal market sharing agreements by which firms commit not to enter each other's territory in oligopolistic markets and procurement auctions. The set of market sharing agreements defines a collusive network. We characterize stable collusive networks when firms and markets are symmetric. Stable networks are formed of complete alliances, of different sizes, larger than a minimal threshold. Typically, stable networks display fewer agreements than the optimal network for the industry and more agreements than the socially optimal network. When firms or markets are asymmetric, stable networks may involve incomplete alliances and be underconnected with respect to the social optimum.},
     language = {English},
     year = {2004},
     publisher = {Wiley for the Economics Department of the University of Pennsylvania and Institute of Social and Economic Research -- Osaka University},
     copyright = {Copyright © 2004 Economics Department of the University of Pennsylvania},
    }

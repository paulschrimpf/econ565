library(igraph)

n <- 25

g <- barabasi.game(n, power=1,
                   m=2,directed=FALSE,out.pref=TRUE)
#g <- growing.random.game(nas,m=1,directed=FALSE,citation=TRUE)
plot(g, layout=layout.fruchterman.reingold, vertex.size=3, frame=TRUE)
degree.distribution(g)
eigen(get.adjacency(g))$values

g.er <- erdos.renyi.game(n, 0.1)
plot(g.er, layout=layout.fruchterman.reingold, vertex.size=3, frame=TRUE)
eigen(get.adjacency(g.er))$values

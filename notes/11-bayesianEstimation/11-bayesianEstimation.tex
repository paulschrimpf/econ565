\input{../slideHeader}
\def\Pb{\mathbf{P}}
\def\Lb{\mathbf{\Lambda}}

\providecommand{\J}{{\mathcal{J}}}
\def\Y{\mathcal{Y}}
\def\A{\mathcal{A}}
\def\L{\mathcal{L}}
\def\C{\mathcal{C}}

\title{Bayesian Estimation Introduction}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \begin{itemize}
  \item Brief introductions
    \begin{itemize}
    \item \cite{mikusheva2007} lectures 23-26 (starting slides based off of)
    \item \cite{geweke1999}, \cite{geyer2011}    
    \end{itemize}
  \item Textbooks
    \begin{itemize}
    \item Widely recommended: \cite{gelman2013}
    \item Econometrics focused: \cite{geweke2005},
      \cite{lancaster2004}, \cite{greenberg2012}
    \item Computational \cite{brooks2011}, \cite{marin2007}, \cite{bolstad2011}
    \end{itemize}
  \item Bayesian estimation in IO
    \begin{itemize}
    \item \cite{jiang2009}: BLP
    \item \cite{imai2009}: dynamic discrete choice
    \item \cite{ghk2012}: dynamic game
    \item \cite{norets2013}: dynamic binary choice
    \item \cite{dube2010}: consumer inertia
    \end{itemize}
  \end{itemize} 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame} \frametitle{Bayesian econometrics}
  \begin{itemize}
  \item Bayesian econometrics is based on two pieces:
    \begin{enumerate}
    \item A parametric model, giving a distribution, $f(\Y_T|\theta)$, for the data given
      parameters
    \item A prior distribution for the parameters, $p(\theta)$
    \end{enumerate}
  \item Implies
    \begin{itemize}
    \item Joint distribution of the data and
      parameters
      \[ p(\Y_T,\theta) = f(\Y_T|\theta)p(\theta) \]
    \item Marginal distribution of the data
      \[ p(\Y_T) = \int f(\Y_T|\theta)p(\theta) d\theta \]
    \item Posterior distribution of parameters
      \[ p(\theta|\Y_T) = \frac{f(\Y_T|\theta)p(\theta)}{p(\Y_T)} \]
    \end{itemize}
  \item Inference based on posterior
    \begin{itemize}
    \item Report posterior mean (or mode or median) as point estimate
    \item Credible set $=$ set of posterior measure $1-\alpha$  
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Differences between Bayesian and Frequentist Approaches}
  \begin{columns}[T] % align columns
    \begin{column}{.48\textwidth}
      \textbf{Frequentist}
      \begin{itemize}
      \item $\theta$ fixed 
      \item Sample random 
      \item Uncertainty from sampling 
      \item Probability about sampling uncertainty 
      \end{itemize} 
    \end{column}
    \begin{column}{.48\textwidth}
      \textbf{Bayesian}
      \begin{itemize}
      \item $\theta$ random 
      \item  Sample fixed once observed
      \item  Uncertainty from beliefs about parameter 
      \item Probability about parameter uncertainty
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Reasons to be Bayesian}
  \begin{enumerate}
  \item Philosophical
  \item Bayesian methods asymptotically valid from frequentist
    perspective
  \item Decision theory---leads to admissible decision rules
  \item Nuisance parameters easily integrated out
  \item Sometimes easier to implement (main reason for this course)
  \end{enumerate} 
\end{frame}

\subsection{OLS}

\begin{frame}[allowframebreaks] \frametitle{OLS}
  \begin{itemize}
  \item Model $ y_t = x_t \theta + u_t $, $u_t \sim iid N(0,1)$.  
  \item Distribution of data
  \[ f(Y|X,\theta) = (2\pi)^{-T/2} \exp\left(-\frac{1}{2}
    (Y-X\theta)'(Y-X\theta) \right) \]
  \item Conjugate prior (posterior \& prior in same family)
    \begin{itemize}
    \item $\theta \sim N(0,\tau^2 I_k)$,
      \[ p(\theta) = (2\pi \tau^2)^{-k/2} \exp\left(\frac{-1}{2\tau}
        \theta'\theta\right) \]
    \end{itemize}
  \item Posterior
    {\footnotesize{
    \begin{align*}
      p(\theta|Y,X) \propto & \exp\left(-\frac{1}{2} \left[-Y'X\theta -
          \theta'X'Y + \theta'X'X\theta +
          \frac{1}{\tau^2}\theta'\theta\right]\right) \\
      \propto & \exp\left(-\frac{1}{2} \left[-Y'X\theta -
          \theta'X'Y + \theta'(X'X + \frac{I_k}{\tau^2})\theta 
        \right]\right) \\ 
      \propto & \exp\left(-\frac{1}{2}
        \left[ \left(\theta-(X'X+\frac{I_k}{\tau^2})^{-1}X'Y\right)' 
          (X'X+\frac{I_k}{\tau^2})^{-1}
          \left(\theta-(X'X+\frac{I_k}{\tau^2})^{-1}X'Y\right)
        \right]\right) 
    \end{align*}
  }}
    so $\theta|Y,X \sim N(\tilde{\theta},\tilde{\Sigma})$ with 
    \begin{align*}
      \tilde{\theta} = & (X'X+\frac{I_k}{\tau^2})^{-1}X'Y \\
      \tilde{\Sigma} = & (X'X+\frac{I_k}{\tau^2})^{-1}
    \end{align*}
  \item Fix $\tau$ and $T \rightarrow \infty$ with $\frac{X'X}{T} \rightarrow Q_{XX}$, then
    $\tilde{\theta} \rightarrow \theta_0$
  \item Uninformative prior, $\tau \rightarrow \infty$,  $\tilde{\theta}
    \rightarrow (X'X)^{-1}X'Y = \hat{\theta}^{ML}$
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{MCMC}

\begin{frame}\frametitle{MCMC}
  \begin{itemize}
  \item Posterior 
    \[ p(\theta|\Y_T) = \frac{f(\Y_T|\theta)p(\theta)}{p(\Y_T)} =
    \frac{f(\Y_T|\theta) p(\theta)} {\int f(\Y_T|\tilde{\theta}) d\tilde{\theta} }  \]
  \item Closed form posterior is rare, often impossible 
  \item Sample $\theta_i \sim p(\theta|\Y_T)$ instead
  \item Markov Chain Monte-Carlo
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Acceptance-Rejection}
  \begin{itemize}
  \item Want $\xi \sim \pi(x)$, can calculate $f(x) \propto \pi(x)$
  \item Find distribution with pdf $h(x)$ such that $f(x) \leq
    c h(x)$ 
  \item Accept-reject
    \begin{enumerate}
    \item Draw $z \sim h(x)$, $u \sim U[0,1]$
    \item If $u \leq \frac{f(z)}{c h(z)}$, then $\xi = z$.  Otherwise
      repeat (1)
    \end{enumerate}
  \item Let $\rho$ be the probability of rejecting a single draw.  Then,
    {\footnotesize{
        \begin{align*}
          P(\xi \leq x) = & P(z_1 \leq x, u_1 \leq \frac{z_1}{ch(z_1)})
          (1+\rho + \rho^2 + ...) & \\
          = & \frac{1}{1-\rho} P(z_1 \leq x, u_1 \leq \frac{z_1}{ch(z_1)}) &
          \\ 
          = & \frac{1}{1-\rho} E_z \left[P(u \leq
            \frac{z}{ch(z)}|z)\mathbf{1}_{\{z\leq x\}}\right] &
          \\
          = & \frac{1}{1-\rho} \int_{-\infty}^x \frac{f(z)}{ch(z)} h(z) dz &
          \\
          = & \int_{-\infty}^x \frac{f(z)}{c(1-\rho)} dz &
          \\
          = & \int_{-\infty}^x \pi(z) dz &
        \end{align*}
      }}
  \item Advantage: directly gives independent draws from $\pi$
  \item Downside: if $h$ too far from $f$, then will reject many draws
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] 
  \frametitle{Markov Chains}
  \begin{itemize}
  \item  Transition kernel $P(x,A) = $ probability of moving from $x$
    into the set $A$.  
  \item Distribution of $x^k$ is $\pi^*$, then the distribution of $y=x^{k+1}$ is 
    \[ \tilde{\pi}(y) dy = \int_{\Re} \pi^*(x) P(x,dy) dx \]
  \item Invariant measure if $\tilde{\pi} = \pi^*$
  \item Invariant measure exists iff:
    \begin{itemize}
    \item Irreducible: every state can be reached from any other
    \item Positive recurrent: $\Er[$ time until $x$ again $|x]$ finite
    \end{itemize}
  \item Conditions for chain to converge to invariant measure from any
    initial measure:
    \begin{itemize}
    \item Irreducible
    \item Positive recurrent
    \item Aperiodic: greatest common denominator of $\{n: y \text{ can be
        reached from } x \text{ in } n \text{ steps}\}$ is 1
    \end{itemize}
  \item Easier sufficient condition for convergence:
    \begin{itemize}
    \item Reversible: if $\pi(x) p(x,y) = \pi(y)
      p(y,x)$ (aka detailed balance)
    \end{itemize}
  \item Goal: construct a Markov chain, which we can simulate, that
    has the posterior as its invariant measure and has fast mixing
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Metropolis-Hastings}
  \begin{itemize}
  \item General purpose method to sample from $\pi$
    \begin{enumerate}
    \item Draw $y \sim q(x^j,\cdot)$
    \item Calculate $\alpha(x^j,y) = \min \{1, \frac{\pi(y) q(y,x)}{\pi(x) q(x,y)}\}$
    \item Draw $u \sim U[0,1]$
    \item If $u < \alpha(x^j,y)$, then $x^{j+1} = y$.  Otherwise $x^{j+1}
      = x^j$
    \end{enumerate}
  \item Invariant measure is $\pi$
    \begin{itemize}
    \item Proof: detailed balance condition
      \[ \pi(x) q(x,y) \alpha(x,y) = \pi(y) q(y,x) \alpha(y,x) \]
    \end{itemize}
  \item Proposal density $q$
    \begin{itemize}
    \item Too disperse $\implies$ many rejections
    \item Too concentrated $\implies$ high autocorrelation and slow
      mixing (slow convergence to $\pi$)
    \item Common choices:
      \begin{itemize}
      \item Random walk chain: $q(x,y) = q_1(y-x)$, e.g. $y = x +
        \epsilon$, $\epsilon \sim N(0,s)$
      \item Independence chain: $q(x,y) = q_1(y)$
      \item Autocorrelated $y = a + B(x-a) + \epsilon$ with $B<0$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Gibbs sampling}
  \begin{itemize}
  \item Break $x$ into blocks $x
    = (x_1,x_2,...,x_d)$ such that we can draw from
    \[ \pi(x_k|x_1,...,x_{k-1},x_{k+1},...,x_d) \;\; \forall k \]
  \item Simulate
    \begin{itemize}
    \item $x_1^{(j+1)}$ from $\pi(x_1^{(j+1)}|x_2^{(j)},...,x_d^{(j)})$ 
    \item $x_2^{(j+1)}$ from
      $\pi(x_2^{(j+1)}|x_1^{(j+1)},x_3^{(j)},...,x_d^{(j)})$ 
    \item $x_3^{(j+1)}$ from
      $\pi(x_3^{(j+1)}|x_1^{(j+1)},x_2^{(j+1)},x_4^{(j)},...,x_d^{(j)})$ 
    \item ...
    \end{itemize}
  \item Can be viewed as Metropolis-Hastings with $q = \pi(\cdot |
    \cdot)$
  \item Pros:
    \begin{itemize}
    \item Usually fast
    \item No need to choose candidate distribution
    \item Sometimes less autocorrelation
    \item Easy to incorporate latent variables (data augmentation)
    \end{itemize}
  \item Cons:
    \begin{itemize}
    \item Not possible for all models \& priors
    \item Can lead to slow mixing (especially with many blocks)
    \item ``many naive users still have a preference for Gibbs updates that is
      entirely unwarranted. If I had a nickel for every time someone had
      asked for help with slowly converging MCMC and the answer had been to
      stop using Gibbs, I would be rich. Use Gibbs updates only if the
      resulting sampler works well. If not, use something else.''
      \cite{geyer2011}
    \end{itemize}
  \end{itemize}
\end{frame}
\begin{frame}[allowframebreaks]
  \frametitle{Gibbs sampling}
  \begin{itemize}
  \item Example: probit
    \begin{itemize}
    \item $d = \{ x\beta + \epsilon > 0\}$, $\epsilon \sim N(0,1)$
    \item Prior: $\beta \sim N(0, I \tau^2)$
    \item Posterior: $\prod_i \Phi(x_i\beta)^{d_i} \left(1 -
        \Phi(x_i\beta)\right)^{1-d_i}$
    \item Data augmentation: draw $y_i = x_i \beta + \epsilon_i$
      conditional on data and $\beta$
    \item Gibbs sampler:
      \begin{itemize}
      \item Draw $y_i \sim $ truncated $N(x_i\beta, 1; d_i)$
      \item Draw $\beta \sim N\left((X'X+\frac{I_k}{\tau^2})^{-1}X'Y ,
          (X'X+\frac{I_k}{\tau^2})^{-1}\right)$
      \end{itemize}
    \end{itemize}
  \item Example: random coefficients probit (in Bayesian stats,
    random coefficients $\approx$ multilevel model)
    \begin{itemize}
    \item $d_{it} = \{ x_{it}\beta_i + \epsilon_{it} > 0\}$,
      $\epsilon_{it} \sim N(0,1)$, $\beta_i \sim N(\beta, \Sigma)$
    \item Prior: $\beta \sim N(0, I\tau^2)$, $\Sigma^{-1} \sim $
      Wishart $(V)$
    \item Data augmentation: draw $y_{it} = x_{it} \beta_i + \epsilon_{it}$
      conditional on data and $\beta_i$
    \item Gibbs sampler:
      \begin{itemize}
      \item Draw $y_{it} \sim $ truncated $N(x_{it}\beta_i, 1; d_i)$
      \item Draw $\beta_i \sim N\left(X_i'X_i+ \Sigma)^{-1}X'Y ,
          (X_i'X_i+\Sigma)^{-1}\right)$
      \item Draw $\beta \sim N\left( 1/n \sum_i \beta_i , S \right)$
      \item Draw $\Sigma^{-1} \sim $ Wishart(something)
      \end{itemize}
    \end{itemize}
  \item Software: OpenBUGS, WinBUGS, JAGS 
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{Hamiltonian MCMC}
  \begin{itemize}
  \item Improve Metropolis-Hastings through better choice of candidate density
    \begin{itemize}
    \item Avoid high autocorrelation
    \end{itemize}
  \item Overview: \cite{neal2011}
  \item Software: STAN, Turing.jl, DynamicHMC.jl
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Hamiltonian MCMC}
  \begin{itemize}
  \item Hamiltonian dynamics:
    \begin{itemize}
    \item Parameters $=$ position $= x$
    \item Momentum $=m$
    \item Hamiltonian $H(x,m) = U(x) + K(m) = $ potential $+$ kinetic
      energy 
    \item $U(x) = -\log(\pi(x))$
    \item Conservation of energy $\rightarrow$ 
      \begin{align*}
        \frac{dx}{dt} = & \frac{\partial H}{\partial m} \\
        \frac{dm}{dt} = & -\frac{\partial H}{\partial x}
      \end{align*}
      Given $H$ can accurately compute $x(t)$, $m(t)$
    \item Useful properties:
      \begin{itemize}
      \item Symmetrically invertible: $(x^*,m^*) = T_s(x,m) \iff (x,-m) = T_s(x^*,-m^*)$
      \item Conserved: $\frac{dH}{dt}=0$
      \item Volume preserved: mapping $T_s: (x(t),m(t)) \to (x(t+s),m(t+s))$
        has jacobian, $B_s$, with determinant $1$
      \end{itemize}
    \item Use Hamiltonian dynamics for candidate density
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Hamiltonian MCMC}
  \begin{itemize}  
  \item Hamiltonian MC for drawing from $\pi$
    \begin{itemize}
    \item Set $U(x) = -\log pi(x)$, $K(m) = m^T M^{-1} m / 2$
    \item Each step of chain: 
      \begin{enumerate}
      \item Draw $m \sim N(0, M)$
      \item Simulate dynamics $(x^*, m^*) = T_s(x,m)$
      \item Accept $x^*$ with probability 
        \[ \alpha(x^*,m^*;x,m) = \min\left \lbrace 1, \exp\left(-U(x^*) + U(x) -
            K(m^*) + K(m) \right) \right \rbrace \]
      \end{enumerate}
    \end{itemize}
  \item Detailed balance: 
    {\footnotesize{
    \begin{align*}
      p(x_1;x_0) \propto & \begin{cases} 0 & \text{ if } (x_1,m_1)
        \neq T_s(x_0,m_0) \text{ for any } m_0,m_1  \\
        e^{-{m_0}^T M^{-1} m_0 / 2} \alpha(x_1,m_1;x_0,m) & \text{ if }
        (x_1,m_1) = T_s(x_0,m_0)  
      \end{cases} \\
      \propto & \begin{cases} 0 &  \text{ if } (x_1,m_1)
        \neq T_s(x_0,m_0) \text{ for any } m_0,m_1  \\
        e^{-K(m_0)} \min\{ 1 , e^{-U(x_1) + U(x_0) - K(m_1) + K(m_0)} \} & \text{ if }
        (x_1,m_1) = T_s(x_0,m_0)  
      \end{cases} 
    \end{align*}
    $\pi(x) = \exp(-U(x))$, and $(x_1,m_1) = T_s(x_0,m_0)$ implies
    $(x_0,m_0) = T_s(x_1,-m_1)$, so
    \begin{align*}
      \pi(x_0) p(x_1;x_0) = & e^{-U(x_0)-K(m_0)} \min\{ 1 ,
      e^{-U(x_1) + U(x_0) - K(m_1) + K(m_0)} \} \\
      = &  e^{-U(x_1)-K(-m_1)} \min\{ 1 ,
      e^{-U(x_0) + U(x_1) - K(m_0) + K(-m_1)} \} \\
      = & \pi(x_1,m_1) p(x_0,m_0;x_1,m_1) 
    \end{align*}
  }}
\end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Hamiltonian MCMC}
  \begin{itemize}
  \item Tuning choices:
    \begin{itemize}
    \item Length of path to simulate, $s$, which in practice is
      discretized into $L$ steps of size $\epsilon$
    \item Variance of momentum, $M$
    \item Various methods to automate e.g.\ NUTS (used by Stan)
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Example: Linear Regression}
  \href{https://ubcecon567.github.io/ols}{Julia Code and Notes}
\end{frame}

\end{frame}  

\begin{frame}\frametitle{Example: Probit}
  \href{https://bitbucket.org/paulschrimpf/econ565/src/master/notes/11-bayesianEstimation/mcmc-probit.R?at=master}
  {R code}
  
  \includegraphics[width=\linewidth]{chains}
  
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
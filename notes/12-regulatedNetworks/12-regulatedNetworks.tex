\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Network Industries}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item Network industries
    \begin{itemize}
    \item Telecommunications, natural gas, electric power, railroads,
      etc
    \item Natural monopolies
    \end{itemize}
  \item Long-term trend from state-owned or highly regulated monopolies to
    vertically segmented with parts deregulated
  \end{itemize}
\end{frame}

\begin{frame}
  \tableofcontents
\end{frame}

\section{\cite{chen2023}}

\begin{frame}
  \frametitle{Network Structure and Efficiency Gains from Mergers:
    Evidence from US Freight Railroads}
  \cite{chen2023}

  \includegraphics[width=\textwidth]{chen-fig1}
\end{frame}

\subsection{Introduction}

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item Model of oligopolistic competition on transportation network
  \item Firms choose: pricing, routing, maintenance
  \item Estimate model
  \item Simulate effects of each merger from 1985-2004 (no mergers
    since then)
  \item On average after merger shipment cost reduces by 12.9\%,
    shipment price reduces by 8.8\%, and the additive markup increases
    by 7.2\%
    \begin{itemize}
    \item Increased markup from other firms allocating away from
      region where merged firm operates
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Data}
  \begin{itemize}
  \item 3 data sets
    \begin{enumerate}
    \item confidential Carload Waybill Sample
    \item Class I Railroad Annual Report
    \item Commodity Flow Survey
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}

  Rail network and ownership changes over time

  \url{https://yanyouchen.com/american-railroads/}

\end{frame}

\begin{frame}
  \frametitle{Data}
  \includegraphics[width=\textwidth]{chen-tab1}

  \includegraphics[width=\textwidth]{chen-tab2}
\end{frame}

\begin{frame}
  \frametitle{Reduced Form Evidence}
  \includegraphics[width=\textwidth]{chen-tab3}
\end{frame}

\begin{frame}
  \frametitle{Reduced Form Evidence}
  \includegraphics[width=\textwidth]{chen-tab4}
\end{frame}

\subsection{Model}

\begin{frame}
  \frametitle{Demand}
  \begin{itemize}
  \item Utility of customer $i$, service $s$ between origin-destination $od$ at
    time $t$
    \begin{align*}
      u_{is, odt} = \alpha p_{s, odt} + \beta_1 \log Miles_{s,odt} +
      \underbrace{\alpha_{os} + \alpha_{ds} + \alpha_{od} +
      \alpha_t}_{\text{fixed effects} \equiv \Xi_{s, odt}} + \epsilon_{is, odt}
    \end{align*}
    $\epsilon ~ $ extreme value
  \item Demand
    \begin{align*}
      Q_{s,odt} = M_{odt} \frac{e^{\alpha p_{s,odt} + \beta_1
      Miles_{s,odt} + \xi_{s, odt}}}{1 + \sum_{s'} e^{\alpha p_{s',odt} + \beta_1
      Miles_{s',odt} + \xi_{s', odt}} }
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Firm's Problem}
  \begin{itemize}
  \item Each period, firm $j$ chooses
    \begin{itemize}
    \item Prices $\{p_{s,o_j, d_j}\}_{(o_j,d_j) \in \mathcal{Z}_j^2}$
    \item Routing $\{\mathcal{R}_{j,o_j,d_j}\}_{(o_j,d_j) \in \mathcal{Z}_j^2}$
    \item Maintenance $\{I_{j,ab}\}_{ab \in \mathcal{A}_j}$
    \end{itemize}
  \item Profits
    \begin{align*}
      \pi_j  = & \max_{\{p\}, \{\mathcal{R}\}, \{I\}} \sum_{s \in S(j)}
                 p_{s,od} Q_{s,od}(p_{s,od}, p_{-s,od}) - C(Q_j, R_j, I_j) \\
               & \text{ s.t. } \sum_{ab \in \mathcal{A}_j} I_{j,ab} \leq K_j \\
               & \;\;\; \underbrace{\sum_{a \in \mathcal{Z}_j(z)} Q_{s,od} 1\{(a,z)
                 \in \mathcal{R}_{j,o,d} + D_{j,z} = \sum_{b \in
                 \mathcal{Z}_j(z)} Q_{s, od} 1\{(z,b) \in
                 \mathcal{R}_{j,o,d}}_{\text{balanced flow}}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Firm's Problem}
  \begin{itemize}
  \item Costs
    \begin{align*}
      C(Q_j,R_j,I_j) = \sum_{od(s)} Q_{j,od(s)} \sum_{j' \in od(s)} \sum_{(a,b) \in R_{j',od(s)}} \delta
      \frac{Dist_{j',ab}} {I_{j',ab}^\gamma}
    \end{align*}
  \item Firm $j$ chooses $I_{j,ab}$ to minize its own costs, but does
    not take into account costs for other firms using its rails
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Equilibrium}
  \begin{itemize}
  \item No congestion, constant marginal costs makes choice of
    $\mathcal{R}$ given $p$ and $I$ a linear program
  \item Assumption of no cross $od$ demand elasticity and no effect of
    $Q_{s,o'd'}$ on cost of $Q_{s,o,d}$ helps keep price computation
    fast
  \item Equilibrium may not be unique
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}\frametitle{Demand}
  \includegraphics[width=\textwidth]{chen-tab5}
\end{frame}

\begin{frame}\frametitle{Costs}
  \begin{itemize}
  \item Cost parameters:
    \begin{itemize}
    \item $\eta = $ cost of interchange
    \item $\delta = $ scaling factor
    \item $\gamma = $ economies of scope in maintenance (important for
      merger efficiency changes)
    \end{itemize}
  \item Estimated by indirect inference / simulated method of moments
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Cost Moment Fits}
  \includegraphics[width=\textwidth]{chen-tab6}
\end{frame}

\begin{frame}\frametitle{Cost Parameter Estimates}
  \includegraphics[width=\textwidth]{chen-tab7}
\end{frame}

\begin{frame}\frametitle{Implied Merger Effects}
  \includegraphics[width=\textwidth]{chen-tab8}
\end{frame}

\begin{frame}\frametitle{Mergers change resource allocation}
  \begin{itemize}
  \item Example: Burlington Northern \& Santa Fe merged to BNSF
  \end{itemize}

  \includegraphics[width=\textwidth]{chen-fig5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{chen-fig6}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Incentive regulation}

\begin{frame}
\begin{itemize}
\item This section is based largely on \cite{joskow2014}
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Downsides of rate of return regulation}
  \begin{itemize}
  \item Gives no incentive to control costs
    \begin{itemize}
    \item Overinvestment
    \item Too little managerial effort
    \end{itemize}
  \item Gives no incentive for high quality
    \begin{itemize}
    \item In transportation networks, quality $\approx$ lack of
      congestion
    \item Uniformly applied rate of return does not give incentive
      about where to invest in network, e.g.\
      \url{http://faculty.arts.ubc.ca/pschrimpf/565/gasSlides.pdf}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Incentive regulation}
  \begin{itemize}
  \item Incentive regulation : regulate prices such that regulated
    firm is the residual claimant on cost reductions and/or quality
    improvements
  \item If regulator knows the costs of an efficiently run
    firm, set prices such that revenues of any firm equals the costs
    of an efficient one
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Theoretical framework}
  \begin{itemize}
  \item Allow revenues, $R = a + (1-b) C$, where $C = $
    realized costs
    \begin{itemize}
    \item Rate of return / cost of service : $a = 0$, $b = 0$, so $R = C$.
    \item Fixed price / price cap : $a = C^*$, $b = 1$
    \item Sliding scale : $0 < a < C^*$, $0<b<1$
    \end{itemize}
  \item $C$ depends on type of firm and managerial effort
    \begin{itemize}
    \item Rate of return pricing gives no incentive for cost reducing
      effort
    \item Fixed price fully incentives effort, but for all firms to be
      viable, $C^*$ must be set to cost of the highest cost type firm
    \item Faced with distribution of cost types, optimal for regulator
      to offer menu of contract such that lowest cost firm chooses
      fixed price, others sliding scale getting closer to rate of
      return as cost type increases
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Practical issues}
  \begin{itemize}
  \item How does regulator know $C^*$?
    \begin{itemize}
    \item $C$ from cost accounting data like in FERC Form 2 for natural gas
      pipelines
    \item $C^*$ typically based on either historical performance $+$
      expected improvements, and/or performance of similar firms
    \item $C^*$ usually reset periodically (``ratchet'') as regulator gains information
    \end{itemize}
  \item Should a menu be used?
    \begin{itemize}
    \item Explicit menus rarely offered, but negotiations between
      firms and regulator could be serving a similar purpose
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Examples}

\begin{frame}[allowframebreaks]
  \frametitle{Examples}
  \begin{itemize}
  \item England 1855-1930ish gas distribution : sliding scale
    mechanism, see \cite{hammond2002}
  \item US electric power : some states adopted rate freezes and price
    caps since mid-1990s
  \item Price cap mechanisms : since mid 1980s UK, New Zealand,
    Australia, and Latin America electric, gas, water, and telecom ;
    US telecom
    \begin{itemize}
    \item Initial price cap chosen, then each year changes by inflation
      minus target productivity growth
      \[ p_{t+1} = p_t (1 + RPI - x) \]
    \item Periodic ratchets tradeoff incentives, rent extraction, and
      firm viability constraints
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]
  \frametitle{UK electric distribution}
  \begin{itemize}
  \item OFGEM - Office of Gas and Electricity Markets
  \item Operating costs - price cap with 5 year ratchet -- measure of
    $C^*$ relatively easy and well-understood
  \item Capital costs - at price review, next price cap depends on
    future capital costs
    \begin{itemize}
    \item Difficult to have efficient benchmark for capital costs
      because of variation in time and space
    \item OFGEM offers menu of sliding scale contracts
      \begin{itemize}
      \item Lower capital allowance with higher powered incentive and
        higher expected return on investment
      \end{itemize}
    \end{itemize}
  \item Price also affected by reaching quality of service targets
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth]{joskow-tab52}
\end{frame}

\begin{frame}[shrink]\frametitle{National Grid Company}
  \begin{itemize}
  \item Electricity transmission in England and Wales
  \item Price cap with 5 year ratchets
  \item There is only one firm, so $C^*$ determined by historical
    data and engineering studies
  \end{itemize}
  {\footnotesize{`` there are many similarities here with the way
      cost-of-service regulation works in practice in the United
      States. Indeed, perhaps the greatest difference is
      philosophical. OFGEM takes a view that recognizes that by
      providing performance based incentives for regulated utilities
      to reduce costs, it can yield consumer benefits in the long run
      by making it profitable for the firm to make efficiency
      improvements. If the firm overperforms against the target,
      consumers eventually benefit at the next price review. It has
      generally (though not always) been willing to allow the
      regulated firms to earn significantly higher returns than their
      cost of capital when these returns are achieved from cost
      savings beyond the benchmark, knowing that the next “ratchet”
      will convey these benefits to consumers. Under traditional US
      regulation, the provision of incentives through regulatory lag
      is more a consequence of the impracticality of frequent price
      reviews and changing economic conditions than by design.''}}
\end{frame}

\begin{frame}\frametitle{Empirical work}
  \begin{itemize}
  \item Far more theory than empirical work
  \item Little to no structural empirical work about impact of
    incentive regulation
  \item Mostly case studies and some reduced form
  \item See \cite{joskow2014} for references
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Cable television regulation and quality}

\subsection{\cite{crawford2007}}

\begin{frame}
  ``Monopoly Quality Degradation and Regulation in Cable Television''
  \cite{crawford2007}
\end{frame}

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item Firms with market power
    \begin{itemize}
    \item Charge higher prices
    \item If quality endogenous, provide lower quality
    \end{itemize}
  \item Usual approach to measuring market power wrt prices : BLP -
    estimate demand and use optimality condition for prices to recover
    marginal costs
  \item This paper : optimality conditions for quality choice to
    measure quality degradation
  \item Relate variation in quality degradation to variation in local
    regulatory oversight
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Quality choice model}
  \begin{itemize}
  \item \cite{mussa1978}
  \item Consumer types $t_0 < t_1 < t_2$, probabilities $f_j$
  \item Firm chooses two qualities and prices:
    \[ \max_{p,q} \sum_{i = 1}^2 f_i \left[P(q_i) - C(q_i)\right] \]
    s.t.
    \[ q_i = \argmax q \in \{q_1, q_2\} v(q,t_i) - P(q) \]
    \[ v(q_i,t_i) - P(q_i) \geq 0 \]
    \[ q_i \geq \underline{q} \]
  \item FOC:
    \[ v_q(q_1,t_1) - C_q(q_1) + \lambda = \frac{1-F_1}{f_1}[v_q(q_1,t_2) -
      v_q(q_1,t_1) ] \text{ and } v_q(q_2,t_2) - C_q(q_2) = 0 \]
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-fig1}
\end{frame}

\begin{frame}
  \frametitle{Data}
  \begin{itemize}
  \item US cable systems in 1995
  \item Quality $=$ basic vs expanded basic service
  \item Regulation : 1992 cable act required price per channel
    reduction by 17\% if local franchise authority or consumers
    complained to FCC
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab3}
\end{frame}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item Functional forms $u(q,t) = tq - p$, $C(q) = q^2/2$
    \begin{itemize}
    \item Implies socially optimal $q_i^{**} = t_i$
    \end{itemize}
  \item Market shares $= \hat{f}_i$
  \item Prices $p_i = t_i q_i - u_i(q_i)$
  \item Quality $q_i = \begin{cases} t_n & \text{ if } i = n \\
      t_i = \frac{1-F_i}{f_i} (t_{i+1} - t_i) & \text{ otherwise} \end{cases}$
  \item Utilities $u_i = \sum_{i'=1}^{i-1} (t_{i'+1} - t_{i'})
    q_{i'}$, $u_1 = 0$
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab6}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{cs-fig3}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth]{cs-fig4}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab7}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth]{cs-tab8}
\end{frame}


\subsection{\cite{crawford2015}}

\begin{frame}
  ``The Welfare Eﬀects of Endogenous Quality Choice in
  Cable Television Markets'' \cite{crawford2015}
\end{frame}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Firms with market power
    \begin{itemize}
    \item Charge higher prices
    \item If quality endogenous, provide non-optimal quality
    \end{itemize}
  \item Compared with \cite{crawford2007}
    \begin{itemize}
    \item More flexible preferences
    \item Marginal social benefit of quality can be higher or lower
      than marginal cost
    \item Find quality is distorted upward
    \end{itemize}
  \item Decompose welfare loss from monopoly into price distortion
    and quality distortion
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Quality markups}
  \begin{itemize}
  \item Inverse demand
    $P(\underbrace{s}_{\text{quantity}},\underbrace{q}_{\text{quality}})$
  \item Cost $c(q)s$
  \item Social planner
    \[
      \max_{s,q} \int_0^s P(s',q)ds' -c(q)s \]
    \begin{align*}
      [s]:&& P(s^{SP},q^{SP}) = & c(q^{SP}) \\
      [q]:&& \int_0^{s^{SP}} P_q(s',q^{SP})ds' = & s^{SP} c_q(q^{SP})
    \end{align*}
  \item Price markup $PM(s,q) = P(s,q) - c(q)$
  \item Quality markup $QM(s,q) = \int_0^{s} P_q(s',q^{SP})ds' - s c_q(q)$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Welfare effect decomposition}
  \begin{itemize}
  \item Total surplus $TS(s,q) = \int_0^s P(s',q)ds' -c(q)s$
  \item Total welfare loss $\Delta TS(s,q) = TS(s^{SP},q^{SP}) -
    TS(s,q)$
  \item Given quality, $p$ and $s$ one-to-one, so let
    $TS(p,q) = TS(s(p,q),q)$
  \item Welfare loss from market power over quality
    \[ MPQ = TS(p,q^{SP}(p)) - TS(p,q) \]
  \item Welfare loss from market power over price
    \[ MPP = TS(p^{SP}, q^{SP}) - TS(p,q^{SP}(p)) \]
  \item $\Delta TS(p,q) = MPP(p,q) + MPQ(p,q)$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item Annual data on 3931 cable systems from 1997-2006
  \item Prices and market shares of cable and satellite tiers
  \item Quality $=$ sum of average cost of channels offered
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-tab3}
\end{frame}

\begin{frame}
  \frametitle{Model}
  \begin{itemize}
  \item Consumers : choose among cable, satellite, product
    \begin{align*}
      u_{ijgn} = & \alpha_g + (a_{i0} + a_y I_n + a_h H_n + a_u U_n)
                   p_{jgn} + \\
      + & (b_{i0} + b_y I_n + b_h H_n + b_u U_n)
          q_{jgn} + \xi_{gn} + \epsilon_{ijgn}
    \end{align*}
  \item Supply :
    \begin{itemize}
    \item assume satellite price is fixed (wrt counterfactual
      prices and qualities of cable systems)
    \item FOC for cable systems :
      \begin{align*}
        [p]: && s_{jcn} + \sum_r (p_{rcn} - mc_{rcn})\frac{\partial
                s_{rcn}}{\partial p_{jcn}} = & 0 \\
        [q]: && -\frac{\partial mc_{jcn}}{\partial q_{jcn}} s_{jcn} +
                \sum_r (p_{rcn} - mc_{rcn}) \frac{\partial
                s_{rcn}}{\partial q_{jcn}} = & 0
      \end{align*}
    \item Functional form :
      \[ mc_{jcn} = \exp\left(z_{jn} \theta_{s0} +
        v_{0jn} + (z_{jn} \theta_{s1} + v_{1jn}) \right) \]
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Moment conditions and instruments}
  \begin{itemize}
  \item $\Er[\xi_{gn}|Z_{gn}] = 0$
  \item Average price and quality of other local cable systems owned
    by same multi-system operator
  \item Total number of subscribers of multi-system operator (shifts
    bargaining power)
  \item Average channel capacity of multi-system
  \item Total length of coaxial lines
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-tab4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-fig3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-tab5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-fig4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-tab6}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-tab7}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{css-fig5}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Dynamic Oligopoly: Additional Issues}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \cite{an2010}
    \item \cite{Aguirregabiria2012} chapters 
    \item \cite{ackerberg2006structural} section 3
    \item \cite{am2010}
    \end{itemize}
  \end{itemize} 
\end{frame}

\begin{frame}
  \tableofcontents  
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item There are not many applied papers that estimate dynamic games
    (less true now than 4 years ago)
  \item Reasons:
    \begin{enumerate}
    \item Estimating dynamic games is computationally intensive
    \item Assumption that the only unobserved heterogeneity are i.i.d\
      shocks is not plausible 
      \begin{itemize}
      \item Why bother estimating a complicated
        model if the results are not credible?
      \item Should add some permanent and/or autocorrelated unobserved
        heterogeneity 
      \end{itemize}
    \end{enumerate}
  \item Today we will look at recent research addressing these two
    issues 
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Computation}

\begin{frame}[allowframebreaks]\frametitle{Computation}
  \begin{itemize}
  \item Estimation involves maximizing some objective function subject
    to equilibrium conditions (or nested fixed point with equilibrium
    condition substituted into objective)
  \item Estimation methods:
    \begin{itemize}
    \item Maximum likelihood 
      \begin{align*}
        \max_{\theta \in \Theta, \Pb \in [0,1]^N} & \sum_{m=1}^M \sum_{t=1}^{T_m} \sum_{i=1}^N
        \log \Lambda\left(a_{imt} | v_i^{\Pb}(\cdot, x_{mt};\theta) \right) \\
        \text{s.t.} & \Pb = \Lb(v^{\Pb}(\theta)) 
      \end{align*}      
    \item 2-step estimators: estimate $\hat{\Pr}(a|x)$ from observed
      actions and then 
      \begin{align*}
        \max_{\theta \in \Theta} & \sum_{m=1}^M \sum_{t=1}^{T_m} \sum_{i=1}^N
        \log \Lambda(a_{imt} | v_i^{\hat{\Pb}}(\cdot, x_{mt};\theta) )
      \end{align*}
    \item Nested pseudo likelihood (NPL) \citep{am2007}: after 2-step
      estimator update $\hat{\Pb}^{(k)} =
      \Lb(v^{\hat{\Pb}^{(k-1)}}(\hat{\theta}^{(k-1)}))$, re-maximize
      pseudo likelihood to get $\hat{\theta}^{(k)}$ and repeat
    \end{itemize}
  \item Computation time: 2-step $<$ NPL $\leq$ MLE
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Computation}
  \begin{itemize}
  \item Possible reductions in computation 
    \begin{itemize}
    \item Improve calculation of $\Lb(v^{\Pb}(\theta))$ (main problem
      is $v^{\Pb}$)
    \item Improve maximization
      \begin{itemize}
      \item Better maximization algorithm (MPEC \cite{su2012}); same
        issues with starting values and local optima as in BLP models 
      \item Bayesian method (MCMC) instead of maximization, 
        \cite{imai2009} and \cite{ghk2012} 
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Improving calculation of value function} 
\begin{frame}[allowframebreaks]\frametitle{Improving calculation of value function} 
  \begin{itemize}
  \item For finite state space can compute $v^\Pb$ as 
    \[ V^\Pb(\theta) = (I - \delta \Mb_c)^{-1} \Mb_c \left[
      \pib(\theta) + g(\pb,\theta) \right] \] 
  \item Inverting matrix takes $O(S^3)$ operations where $S$ is size
    of state space
    \begin{itemize}
    \item State space can be very large even for models that appear
          simple 
          \begin{itemize}
          \item E.g.\ entry/exit game with $N$ firms whose identities matter
            $S = 2^N |X|$
          \end{itemize}
    \item Matrix inversion becomes prohibitively slow for surprisingly
      moderate $S$
    \item On my desktop in 2013, (AMD-FX8150 cpu) $S = 1000$ takes 1.15
      seconds, $S=2000$ takes 11.1 seconds, $S=3000$ takes 38.6
      seconds , $S=4000$ takes 91.2 seconds
    \item Faster hardware can cut these times by a constant factor,
      but still face cubic growth
    \item Using GPU instead of CPU for matrix inversion can be much
      faster
      \begin{itemize}
      \item On my desktop in 2013, the GPU (NVidia GeForce GTX 560) takes about
        a hundredth as long to invert large matrices as the CPU
      \item Scientific computing using GPUs is a new and active field
      \item Programming for GPUs can be difficult
      \item Fast GPUs are not part of most servers (this has changed
        since these slides were first written)
      \end{itemize}
    \item Inverting sparse matrices can take much less than $O(S^3)$ 
      operations 
      \begin{itemize}
      \item $I - \delta \Mb_c$ is often sparse
      \item Exact complexity  of inversion depends on number of
        non-zero entries and their locations (sparsity pattern)
      \end{itemize}
    \item Some papers iterate value equation instead of explicitly
      inverting 
      \[ V^\Pb(\theta) =  \Mb_c \left[
        \pib(\theta) + g(\pb,\theta) + \delta V^\Pb(\theta) \right]  \]
      \begin{itemize}
      \item Simulation often used
      \item Still solving same equation, if solving accurately has to
        take $O(S^3)$ 
      \item If iterating is faster must be either (i) implicitly
        exploiting sparsity or (ii) solving inaccurately
      \item Estimation can proceed with approximate solutions that
        only become exact at estimated $\theta$, e.g.\
        \cite{kasahara2011} 
      \end{itemize}
    \end{itemize}
  \item $\Mb$ depends on $\Pb$, so for 2-step methods only need to
    compute inverse once
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] 
  \frametitle{Reducing the size of the state space}
  \begin{itemize}
  \item Economically motivated restrictions can reduce the size of the
    state space
  \item[R1] assume homogenous players and symmetric equilibrium
    \begin{itemize}
    \item E.g.\ entry game, assume:      
      \begin{enumerate}
      \item Only number of competitors and not their identities
        affects profits
      \item Firms have the same profit function
      \item Symmetric equilibrium 
      \end{enumerate}
      then state space size is $2 (N+1) |X|$
    \end{itemize}
  \item[R2] Inclusive values \citep{nevo2008}
    \begin{itemize}
    \item Inclusive value $ = $ in discrete choice model the expected
      utility of a consumer from facing several options before
      observing the shocks \citep{mcfadden1978}
      \[ \Er\left[ \max_{j} u_j + \epsilon_j \right] \]        
    \item Adjusted inclusive value $\approx$ inclusive value minus
      firm's marginal costs; denote by $i_f$
    \item With appropriate assumptions, profits can be written as
      function of adjusted inclusive values,
      \[ \pi_f\left(\text{all state variables}  \right) = \pi_f(i_f,
      i_{-f} ) \]
    \item Assume strategies only depend on adjusted inclusive values, 
      \[ \Pr\left(i_{f,t+1}, i_{-f,t+1} | \text{state}_t \right) =
      \Pr\left(i_{f,t+1}, i_{-f,t+1} \left| i_{f, t}, i_{-f,t}, \sum
          \text{invest}_{f,t} \right. \right) \]
      possible justifications:
      \begin{itemize}
      \item Strong assumptions about investment process
      \item Limited information of firms
      \item Bounded rationality: firms have as hard a time computing
        strategies as we do
      \end{itemize}
    \item Then value function only depends on inclusive values
    \end{itemize}
  \item[R3] Oblivious equilibrium (\citet{wbr2008}, \citet{wbr2010},   
    \citet{farias2012}) 
    \begin{itemize}
    \item Oblivious equilibrium: firms make decisions conditional only
      on their own state variables and long-run industry average state 
    \item In Markov equilibrium, firms make decisions based on all
      state variables
    \item \cite{wbr2008} show that oblivious equilibrium approximates
      Markov perfect equilibrium as number of firms increases
    \item \cite{krusell1998} use similar idea in dynamic macro model
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Finite dependence}
  \begin{itemize}
  \item Value function: 
    \begin{align*}
      V^\Pb(\theta) = & \Mb_c \left[
        \pib(\theta) + g(\pb,\theta) + \delta V^\Pb(\theta) \right] \\
      = & \Mb_c \left[
        \pib(\theta) + g(\pb,\theta) + \delta \Mb_c \left[
          \pib(\theta) + g(\pb,\theta) + \delta V^\Pb(\theta) \right]
      \right] \\
      = & \left(\sum_{t=0}^T \Mb_c^{t+1} \delta^t \right) \left[
        \pib(\theta) + g(\pb,\theta) \right] + \Mb_c^{T+2}
      \delta^{T+1} V^\Pb(\theta)
    \end{align*}
  \item \cite{arcidiacono2011}: if $\Mb_c^{T+2}
    \delta^{T+1} V^\Pb(\theta)$ is identical across actions then it
    will drop out of $v^p(a,x) - v^p(a',x)$, avoiding inversion
  \item Examples:
    \begin{itemize}
    \item Renewal action: bus engine replacement
    \item Terminal choice
    \end{itemize}
  \end{itemize}    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Unobserved heterogeneity}

\begin{frame}[allowframebreaks]
  \frametitle{Unobserved heterogeneity}
  \begin{itemize}
  \item Only unobservables in basic dynamic model are i.i.d.\ shocks
  \item More plausible to allow richer unobserved heterogeneity i.e.\
    unobserved state variables
  \item Panel data can identify fairly rich unobserved heterogeneity
    \begin{itemize}
    \item E.g.\ in linear models:
      \begin{itemize}
      \item Random effects
      \item Fixed effects
      \item (Dynamic factors)
      \item Fixed effects with additional autocorrelation (dynamic
        panel models as in \citet{blundell2000})
      \end{itemize}
    \item Variants of methods for linear models can be applied to
      dynamic games, but not straightforward because of 
      \begin{itemize}
      \item Nonlinearity -- requires different identification
        arguments; complicates fixed effects estimation 
      \item Computation -- introducing unobserved state variables
        makes computing the model more complex
      \end{itemize}
    \end{itemize}
  \item Types of unobserved heterogeneity 
    \begin{itemize}
    \item Permanent firm or market unobserved heterogeneity
      \begin{itemize}
      \item Similar to random or fixed effects
      \item \cite{am2007}, \cite{collard2008}, \cite{ah2012}
      \end{itemize}
    \item Unobserved states that follow a controlled Markov process
      \begin{itemize}
      \item Identification of transition probabilities:
        \cite{ks2009}, \cite{hu2012}, \cite{allman2009}, \cite{huShum2013}
      \item \cite{arcidiacono2011}, \cite{kasahara2011}
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Permanent unobserved heterogeneity}

\begin{frame}[allowframebreaks]\frametitle{Permanent unobserved heterogeneity}
  \begin{itemize}
  \item Here we go over approach of \cite{am2007} as described in
    \cite{an2010} 
  \item Market specific random effect in profits
    \[ \pi_{imt} = \Pi(a_{mt},x_{imt}, \theta) + \theta_i(a) + \sigma_i \xi_m +
    \epsilon_{imt} \]
    \begin{itemize}
    \item $\epsilon_{imt}$ i.i.d.
    \item $\xi_m$ unobserved, discrete with finite support, known mean
      and variance (absorbed by $\theta_i$ and $\sigma_i$), known
      support, $\{\xi^\ell\}_{\ell=1}^L$, pmf $\lambda$ 
    \item $\theta_i$ and $\sigma_i$ varying with $i$ requires large
      $T$ (or large $M$ and same firms across markets)
    \end{itemize}
  \item Conditional choice probabilities different for each $\ell$,
    denote by $\Pb_\ell$
  \item Equilibrium for each $\ell$:
    \[ \Pb^\ell = \Lb(v^{\Pb_\ell}(\theta),\ell)  \]
  \item Pseudo-likelihood integrates over distribution of
    $\xi_m$
    \begin{align*}
      \max_{\theta, \Pb^\ell,\lambda} & \sum_{m=1}^M \sum_{t=1}^{T} \sum_{i=1}^N
      \log \left( \sum_{\ell=1}^L \lambda_{\ell|x}
        \Lambda\left(a_{imt} | v_i^{\Pb_\ell}(\cdot,
          x_{mt};\theta,\ell) \right) \right) \\ 
      \text{s.t.} & \Pb_\ell = \Lb(v^{\Pb_\ell}(\theta),\ell) 
    \end{align*}
    where 
    \[ \lambda_{\ell|x} = \Pr(\xi_m = \xi^\ell | x_{m1}) \]
  \item Initial conditions problem
    \begin{itemize}
    \item $\xi_m$ will be correlated with initial values of endogenous
      state variables (markets with high $\xi_m$ will start with a
      large number of firms)
    \item  One solution: assume stationary, find stationary
      distribution of $x|\xi$,
      \begin{align*}
        \Pr(x_t | \xi_m = \xi^\ell) = \sum_{x_{t-1}} \Pr(x_{t-1} | \xi_m =
        \xi^\ell) \Pr(x_t | x_{t-1}, \xi_m = \xi^\ell) 
      \end{align*}
      Bayes' rule
      \begin{align*}
        \lambda_{\ell|x} = \frac{ \lambda_\ell \Pr(x | \xi_m =
          \xi^\ell) }{\sum_{j=1}^L \lambda_j \Pr(x | \xi_m = \xi^j)}
      \end{align*}
    \end{itemize}
  \item To apply 2-step estimators need to first consistently estimate
    transition probabilities conditional on unobserved $\xi_m$ and
    $\Pr^\ell(\cdot | x_{mt})$ 
    \begin{itemize}
    \item Can use \cite{ks2009}, but \cite{an2010} say estimation is
      difficult
    \item Identification argument is constructive, based on singular
      value decomposition, can mimic for estimation , e.g. \cite{hu2010}
    \item \cite{levine2011} 
    \end{itemize}
  \item NPL can be used but must iterate to convergence unless started
    from consistent $\hat{\Pr}^\ell(\cdot | x_{mt})$
    \begin{itemize}
    \item Start with arbitrary $\Pr^\ell(\cdot | x_{mt})$
    \item Maximize pseudo likelihood to get $\hat{\theta}$,
      distribution of $\xi_m$
    \item Update $\Pr^\ell(\cdot | x_{mt})$ 
    \item Repeat until convergence
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Unobserved autocorrelated state variables}

\begin{frame}[allowframebreaks]\frametitle{Unobserved autocorrelated
    state variables}
  \begin{itemize}
  \item Suppose state $x_{mt} = (x^o_{mt}, x^u_{mt})$ where only
    $x^o_{mt}$ is observed
  \item \cite{ks2009} (for finite) and \cite{hu2012} (for
    continuous) give conditions for identification of transition
    probabilities $\Pr(\cdot | x^o_{mt}, x^u_{mt})$
  \item Given consistent $\hat{\Pr}(\cdot | x^o_{mt}, x^u_{mt})$ can
    apply 2-step estimator or NPL
    \begin{itemize}
    \item Estimation is difficult
    \end{itemize}
  \end{itemize}
\end{frame}

\subsubsection{\cite{arcidiacono2011}}

\begin{frame} \frametitle{\cite{arcidiacono2011}}
  \begin{itemize}
  \item Computationally tractable estimation with unobserved state
    variables
  \item Two innovations:
    \begin{itemize}
    \item Avoid matrix inversion in value function computation through
      finite dependence
    \item Modified EM algorithm to integrate out distribution of
      unobserved states
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] \frametitle{EM algorithm}
  \begin{itemize}
  \item ``Expectation-Maximization''
    \begin{itemize}
    \item Expectation: conditional probabilities of unobserved state
      given observables and parameters updated
    \item Maximization: maximize likelihood as though unobserved state
      observed
    \item Repeated until convergence
    \end{itemize}
  \item Setup: observe $x$, missing $s$, complete $z=(x,s)$
  \item Joint likelihood $p(x,z|\theta)$
  \item Marginal likelihood $L(\theta;x) = p(x|\theta) =
    \Er[p(x,z|\theta) | x, \theta]$
  \item Difficulty: 
    \[ \Er[p(x,s|\theta) | x, \theta] = \int_S p(x,s|\theta)
    p(s|x;\theta) ds \]
    might be hard to compute
  \item Steps:
    \begin{itemize}
    \item Initial $\theta^0$
    \item Expectation: calculate $p(s | x;\theta^0)$
    \item Maximization: $\theta^1 = \argmax_{\theta} \int_S p(x,s|\theta)
      p(s|x;\theta^0) ds$
    \item Iterate to convergence
    \end{itemize}
  \item Pros: stable --- each iteration guaranteed to increase
    likelihood
  \item Cons: slow?
  \item \cite{arcidiacono2011}:
    \begin{itemize}
    \item Not slow  when using finite dependence so that maximization
      step fast
    \item Show 2-step version of EM algorithm possible, i.e. can
      estimate $p(s|x;\theta)$ without estimating $\theta$
      \[ p(s|x;\theta) = \frac{p(x,s|\theta)}{\sum_{s'}
        p(x,s'|\theta)} \]
      Can use empirical probabilities in place of model
      probabilities within EM algorithm
    \end{itemize}
  \end{itemize}
\end{frame}






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
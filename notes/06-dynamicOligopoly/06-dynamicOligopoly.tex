
\input{../slideHeader}
\newcommand{\Pb}{\boldsymbol{\mathrm{P}}}
\newcommand{\Lb}{\boldsymbol{\Lambda}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Dynamic Oligopoly}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \textbf{\cite{aguirregabiria2021}}
    \item \cite{Aguirregabiria2012} chapter 8
    \item \cite{am2010}
    \item \href{http://faculty.arts.ubc.ca/pschrimpf/628/dynamic.pdf}{My
        notes from 628}
    \end{itemize}
  \item Key papers:
    \begin{itemize}
    \item \cite{ericson1995}, \cite{am2007}, \cite{bbl2007}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \tableofcontents
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}\frametitle{Introduction}

\end{frame}

\section{Model}

\begin{frame}[allowframebreaks]\frametitle{Model primitives}
  \begin{itemize}
  \item $N$ players indexed by $i$
  \item Discrete time index by $t$
  \item Player $i$ chooses action $a_{it} \in A$; actions of
    all players $a_t = (a_{1t}, ..., a_{Nt})$
  \item State $x_t = (x_{1t}, ..., x_{Nt}) \in X$ observed by econometrician
    and all players at time $t$
  \item Private shock $\epsilon_{it} \in \mathcal{E}$
  \item Payoff of player $i$ is $U_i(a_t,x_t,\epsilon_{it}) = u(a_t,
    x_t) + \epsilon_{it}(a_{it})$
  \item Discount factor $\beta$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Strategies}
  \begin{itemize}
  \item Strategies $\alpha:(X \times \mathcal{E})^N \to A^N$
    \begin{itemize}
    \item $\alpha_i$ is the strategy of player $i$
    \item $\alpha_{-i}$ is the strategy of other players
    \end{itemize}
  \item Equilibrium: each player's strategy maximizes that player's
    expected payoff given other player's strategies
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Value function}
  \begin{itemize}
  \item Value function given strategies:
    $$
    V_i^{\alpha}(x_t,\epsilon_{it}) =
  \Er_{\epsilon_{-i}}\left[u(\alpha_i(x_t,\epsilon_{i}),
    \alpha_{-i}(x_t, \epsilon_{-i}),x_{t}) + \epsilon_i(a_i) +
    \beta \Er[V_i^{\alpha}(x_{t+1}, \epsilon_{it+1}) | x_t, a_t] |
    x_t, \epsilon_{it} \right]
  $$
\item Integrated (over $\epsilon$) value function given strategies:
  \begin{align*}
    \bar{V}^\alpha(x) = & \int V_i^\alpha(x_t,\epsilon_{it})
                          dG(\epsilon_{it})
  \end{align*}
\item Choice specific value function
  \begin{align*}
    v_i^\alpha(a_{it},x_t) = & \Er_{\epsilon_{-i}}\begin{bmatrix}
                                                    u(a_{it},\alpha_{-i}(x_t,\epsilon_{-it}),x_t) + \\ + \beta
          \Er_x[\bar{V}_i^\alpha(x_{t+1}) | a_{it},
                                                    \alpha_{-i}(x_t,\epsilon_{-it}),x_t ]
                                                  \end{bmatrix}
  \end{align*}
\end{itemize}
\end{frame}



\begin{frame}\frametitle{Equilibrium}
  \begin{itemize}
  \item Markov perfect equilibrium: given $\alpha_{-i}$, $\alpha_i$
    maximizes $v_i$
    \begin{align*}
      \alpha_i(x_t,\epsilon_{it}) \in \argmax_{a_i} \Er_{\epsilon_{-i}}\begin{bmatrix}
        u\left(a_i,\alpha_{-i}(x_t,\epsilon_{-it}),x_t\right) +
        \epsilon_{it}(a_i) + \\ + \beta \Er_x\left[ \bar{V}_i^\alpha(x_{t+1}) |
          a_{it}, \alpha_{-i}(x_t,\epsilon_{-it}),x_t \right]
      \end{bmatrix}
    \end{align*}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]\frametitle{Equilibrium in conditional
    choice probabilities}
  \begin{itemize}
  \item Conditional choice probabilities
    \begin{align*}
      \Pr^\alpha_i(a_i|x) = & \Pr\left( a_i = \argmax_{j \in A}
        v_i^\alpha(j,x) + \epsilon_{it}(j) | x \right) \\
      = & \int 1\left\lbrace a_i = \argmax_{j \in A}
        v_i^\alpha(j,x) + \epsilon_{it}(j)  \right\rbrace
      dG(\epsilon_{it}).
    \end{align*}
  \item Choice specific value function with $\Er_{\epsilon_{-i}}$ replaced
    with $\Er_{a_{-i}}$
    \begin{align}
      v_i^P(a_{it},x_t) = & \sum_{a_{-i} \in
        A^{N-1}} \Pr_{-i}(a_{-i} | x_t) \begin{pmatrix} u(a_{it},a_{-i},x_t) +
        \\ + \beta \Er_x[\bar{V}_i^\alpha(x_{t+1}) | a_{it},a_{-i},x_t ]
      \end{pmatrix}
      \notag
    \end{align}
    where
    \[ \Pr_{-i}(a_{-i}|x) = \prod_{j \neq i}^N \Pr(a_j|x). \]
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Equilibrium in conditional
    choice probabilities}
  \begin{itemize}
  \item Let
    \[ \Lambda(a|v_i^P(\cdot,x_t)) = \int 1\left\lbrace a_i = \argmax_{j
          \in A}
        v_i^P(j,x) + \epsilon_{it}(j)  \right\rbrace
      dG(\epsilon_{it}). \]
    Then the equilibrium condition is that
    \[ \Pr_i(a|x) = \Lambda(a|v_i^P(\cdot,x)) \]
    or in vector form $\Pb = \Lb(v^{\Pb})$
  \item Fixed point equation in $\Pb$
  \item Generally not a contraction mapping, so analysis and
    computation more difficult than in single agent models
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Equilibrium Existence}
  \begin{itemize}
  \item If $\Lb: [0,1]^{N|X|} \to [0,1]^{N|X|}$ is continuous, then
    by Brouwer's fixed point theorem, there exists at least one equilibrium
  \item $\Lb$ need not be continuous, see
    \cite{gowrisankaran1999} and \cite{doraszelski2010}
  \item Equilibrium not unique except in special cases
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Identification}

\begin{frame}[allowframebreaks]\frametitle{Assumptions}
  \begin{enumerate}
  \item $A$ is a finite set
  \item Payoffs additively separable in $\epsilon_{it}$,\
    \[ U_i(a_t,x_t,\epsilon_{it}) = u(a_t,x_t) +
    \epsilon_{it}(a_{it}) \]
  \item $x_t$ follows a controlled Markov process
    \[ F(x_{t+1}|\underbrace{\mathcal{I}_t}_{\begin{array}{c}
        \text{all information} \\ \text{at time } t \end{array}}) =
    F(x_{t+1}|a_t,x_t) \]
  \item The observed data is generated by a single Markov Perfect
    equilibrium
  \item $\beta$ is known
  \item\label{iid} $\epsilon_{it}$ i.i.d. with CDF $G$, which is known up to a
    finite dimensional parameter
  \end{enumerate}
  Each of these assumptions could be (and in some papers has been)
  relaxed; relaxing \ref{iid} is probably most important empirically
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Identification -- expected payoff}
  \begin{itemize}
  \item As in single-agent dynamic decision problems given $G$, $\beta$, and
    $\Er_\epsilon[u(0,\alpha_{-i}(x,\epsilon_{-i}),x_t) ]  = 0$, we can
    identify the expectation over other player's actions of the payoff
    function,
    \begin{align*}
      \Er_\epsilon[u(a_i,\alpha_{-i}(x,\epsilon_{-i}),x) ] =
      \sum_{a_{-i}} \Pr(a_{-i}|x) u(a_i,a_{-i},x)
    \end{align*}
  \item See \cite{bchn2009}, which builds on \cite{hotz1993}  and
    \cite{magnacThesmar2002}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Identification -- expected
    payoff (details)}
  {\small{
  \begin{itemize}
  \item \cite{hotz1993} inversion shows
    \[ v_i^{\alpha^*}(a,x) - v_i^{\alpha^*}(0,x) =
      q(a,\Pr(\cdot|x);G) \]
    for some known function $q$
  \item Use normalization and Bellman equation to recover
    $v_i^{\alpha^\ast}$
    \begin{align*}
      v_i^{\alpha^*}(0,x) = &
                              \underbrace{\Er[u(0,\alpha_{-i}^*(x,\epsilon_{-i}),x)]}_{= 0}
                              + \\ & + \beta \Er[ \max_{a' \in A} v_i^{\alpha^*}(a',x') +
                                     \epsilon(a') | a, x] \\
      = &  \underbrace{\beta \Er[ \max_{a' \in A} v_i^{\alpha^*}(a',x') - v_i^{\alpha^*}(0,x') +
          \epsilon(a') | 0, x]}_{\equiv q(x,\Pr(\cdot|x),G)} + \\ & + \beta \Er[v_i^{\alpha^*}(0,x')|0,x]
    \end{align*}
    $q$ is known; can solve this equation for $v_i^{\alpha^*}(0,x)$,
    then
    \[ v_i^{\alpha^*}(a,x) = v_i^{\alpha^*}(0,x) + q(a,\Pr(\cdot|x);G) \]
  \item Recover $\Er[u(a_i,\alpha_{-i}^*(x,\epsilon_{-i}),x)]$ from
    $v_i^{\alpha^*}$ using Bellman equation
    \begin{align*}
      \Er\left[u(a_i,\alpha_{-i}^*(x,\epsilon_{-i}),x)\right] = &
                                                                  v_i^{\alpha^*}(a_i,x) - \\ & - \beta \Er\left[ \max_{a' \in A}
                                                                                               v_i^{\alpha^*}(a',x')+\epsilon(a') | a , x\right]
    \end{align*}
  \end{itemize}
  }}
\end{frame}

\begin{frame}\frametitle{Identification of $u(a,x)$}
  \begin{itemize}
  \item Separating $u(a,x)$ from
    $\Er_\epsilon[u(a_i,\alpha_{-i}(x,\epsilon_{-i}),x) ]$ is new step
    compared to single-agent model
  \item Need exclusion to identify $u(a,x)$
  \item Without exclusion order condition fails
    \begin{align*}
      \Er_\epsilon[u(a_i,\alpha_{-i}(x,\epsilon_{-i}),x) ] =
      \sum_{a_{-i}} \Pr(a_{-i}|x) u(a_i,a_{-i},x)
    \end{align*}
    Left side takes on $|A| |X|$ identified values, but $u(a,x)$ has
    $|A|^N |X|$ possible values
  \item Assume $u(a,x) = u(a,x_i)$ where $x_i$ is some sub-vector of
    $x$. $u$ identified if
    \begin{align*}
      \Er_\epsilon[u(a_i,\alpha_{-i}(x,\epsilon_{-i}),x) ] =
      \sum_{a_{-i}} \Pr(a_{-i}|x) u(a_i,a_{-i},x_i)
    \end{align*}
    has a unique solution for $u$
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Estimation}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Can use similar methods as in single agent dynamic models
  \item Maximum likelihood
    \begin{align*}
      \max_{\theta \in \Theta, \Pb \in [0,1]^N} & \sum_{m=1}^M \sum_{t=1}^{T_m} \sum_{i=1}^N
      \log \Lambda\left(a_{imt} | v_i^{\Pb}(\cdot, x_{mt};\theta) \right) \\
      \text{s.t.} & \Pb = \Lb(v^{\Pb}(\theta))
    \end{align*}
    \begin{itemize}
    \item Nested fixed point: substitute constraint into objective and
      maximize only over $\theta$
      \begin{itemize}
      \item For each $\theta$ must solve for equilibrium --
        computationally challenging
      \item $\Lb$ not a contraction
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation approaches}
  \begin{itemize}
  \item MPEC \citep{su2012}: use high quality optimization software to
    solve constrained optimization problem
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation approaches}
  \begin{itemize}
  \item 2-step estimators: estimate $\hat{\Pr}(a|x)$ from observed
    actions and then
    \begin{align*}
      \max_{\theta \in \Theta} & \sum_{m=1}^M \sum_{t=1}^{T_m} \sum_{i=1}^N
      \log \Lambda(a_{imt} | v_i^{\hat{\Pb}}(\cdot, x_{mt};\theta) )
    \end{align*}
    \begin{itemize}
    \item Can replace pseudo-likelihood with GMM \citep{bbl2007}
      or least squares \citep{psd2008} objective
    \item Unlike single agent case, efficient 2-step estimators do not have same
      asymptotic distribution as MLE\footnote{In single agent models
        efficient 2-step and ML estimators have the same asymptotic
        distribution but different finite sample properties.}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation approaches}
  \begin{itemize}
  \item Nested pseudo likelihood \citep{am2007}: after 2-step
    estimator update $\hat{\Pb}^{(k)} =
    \Lb(v^{\hat{\Pb}^{(k-1)}}(\hat{\theta}^{(k-1)}))$, re-maximize
    pseudo likelihood to get $\hat{\theta}^{(k)}$
    \begin{itemize}
    \item Asymptotic distribution depends on number of iterations; if
      iterate to convergence, then equal to MLE
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Incorporating static parameters}
  \begin{itemize}
  \item Often some portion of payoffs can be estimated without
    estimating the full dynamic model
    \begin{itemize}
    \item E.g.\ \cite{holmes2011} estimates demand and revenue from
      sales data, costs from local wages, and only uses dynamic model
      to estimate fixed costs and sales
    \end{itemize}
  \item \cite{bbl2007} and \cite{pob2007} incorporate a similar ideas
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Examples}

\subsection{\cite{dunne2011}}

\begin{frame}[allowframebreaks]
  \frametitle{\cite{dunne2011} ``Entry, Exit and the
    Determinants of Market Structure''}
  \begin{itemize}
  \item Market structure $=$ number and relative size of firms
  \item Classic question in IO: how does market structure affect
    competition?
  \item Here: how is market structure determined? Entry and exit
    \begin{itemize}
    \item Sunk entry costs
    \item Fixed operating costs
    \item Expectations of profits (nature of competition)
      \begin{itemize}
      \item Like \cite{br1991} summarize with profits as a function
        of number of firms, $\pi(n)$
      \end{itemize}
    \end{itemize}
  \item Estimate dynamic model of entry and exit to determine relative
    importance of factors affecting market structure
  \item Context: dentists and chiropractors
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Model Setup}
  \begin{itemize}
  \item State variables $s = (n, z)$
    \begin{itemize}
    \item $n=$ number of firms, $z=$
      exogenous profit shifters
    \item Follow a finite state Markov process
    \end{itemize}
  \item Parameters $\theta$
  \item Profit $\pi(s;\theta)$ (leave $\theta$ implicit henceforth)
  \item Fixed cost $\lambda_i \sim G^\lambda = 1 - e^{-\lambda_i/\sigma}$
  \item Discount factor $\delta$
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Existing Firms}
  \begin{itemize}
  \item Value function
    \[ V(s;\lambda_i) = \pi(s) + \max\{ \delta
    VC(s) - \delta \lambda_i, 0\} \]
   where $VC$ is expected next period's value function
   \begin{align*}
     VC(s) = & \Er_{s'}^c\left[ \pi(s') + \Er_{\lambda'} \left[ \max \{
         \delta VC(s') - \delta \lambda', 0 \} | s\right] |s\right]
   \end{align*}
 \item Probability of exit:
   \[ p^x(s) = \Pr(\lambda_i > VC(s)) = 1 - G^\lambda
   (VC(s)). \]
 \item Assume $\lambda$ exponential, $G^\lambda = 1 - e^{-(1/\sigma)
     \lambda}$, then
   \[ VC(s) = \Er_{s'}^c\left[ \pi(s') + \delta VC(s') - \delta \sigma
     \left(1- p^x(s')\right) | s \right] \]
 \item Let $\Mb_c$ be the transition matrix, then
   \begin{align}
     \VCb = & \Mb_c \left[ \pib + \delta \VCb - \delta \sigma (1 -
       \pb^x) \right] \notag \\
     \VCb = & (I - \delta \Mb_c)^{-1} \Mb_c \left[ \pib - \delta \sigma (1 -
       \pb^x) \right] \label{e9}
   \end{align}
   \begin{itemize}
   \item Use non parametric estimate of $\Mb_c$ and form $\VCb$
     by solving
     \[ \VCb = \Mb_c \left[ \pib + \delta \VCb - \delta \sigma
         G^\lambda(\VCb) \right] \]
   \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Entrants}
  \begin{itemize}
  \item Potential entrants:
    \begin{itemize}
    \item Expected value after entering
     \[ VE(s) = \Er_{s'}^e\left[ \pi(s') + \delta VC(s') - \delta \sigma
     \left(1- p^x(s')\right) | s \right] \]
   \item Cost of entry $\kappa_i \sim G^\kappa$
   \item Entry probability
     \[ p^e(s) = \Pr\left(\kappa_i < \delta VE(s) \right) = G^\kappa \left(\delta
       VE(s) \right) \]
   \item As before can use Bellman equation in matrix form to solve
     for $VE$
   \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Empirical specification}
  \begin{itemize}
  \item Data: U.S.\ Census of Service Industries and Longitudinal
    Business Database
    \begin{itemize}
    \item 5 periods -- 5 year intervals from 1982-2002
    \item 639 geographic markets for dentists; 410 for chiropractors
    \item Observed average market-level profits $\pi_{mt}$
    \item Number of firms $n_{mt}$, entrants, $e_{mt}$, exits
      $x_{mt}$, potential entrants $p_{mt}$
    \item Market characteristics $z_{mt} = (pop_{mt}, wage_{mt},
      inc_{mt})$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Empirical specification}
  \begin{itemize}
  \item Profit function
    \begin{align*}
      \pi_{mt} = & \theta_0 + \sum_{k=1}^5 \theta_k 1\{n_{mt}=k\} +
      \theta_6 n_{mt} + \theta_7 n_{mt}^2 + \\
       & + \text{quadratic polynimal in } z_{mt} + \\
      & + f_m + \epsilon_{mt}
    \end{align*}
    Key assumption: $\epsilon_{mt}$ independent over time
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Empirical specification}
  \begin{itemize}
  \item Transition matrix $\Mb_c$
    \begin{itemize}
    \item Define $\hat{z}_{mt} = $ estimate value polynomial in
      $z_{mt}$ in profit function
    \item Discretize $\hat{z}_{mt}$ into 10 categories and use sample
      averages to estimate transition probabilities
    \end{itemize}
  \item Fixed ($G^\lambda$) and entry costs ($G^\kappa$)
    \begin{itemize}
    \item $\widehat{VC}(\sigma)$ and $\widehat{VE}(\sigma)$ as
      described above
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Likelihood}
  \begin{itemize}
  \item Log-likelihood
    \begin{align*}
      L(\sigma,\alpha) = & \sum_{m,t} \begin{array}{l}
          (n_{mt}-x_{mt}) \log \left(G^\lambda \left(
              \widehat{VC}_{mt}(\sigma);\sigma \right)\right) + \\
          + x_{mt} \log\left(1 - G^\lambda \left(
              \widehat{VC}_{mt}(\sigma);\sigma \right) \right) + \\
          + e_{mt} \log \left(G^\kappa\left(\widehat{VE}_{mt}(\sigma)
            ;\alpha\right) \right) + \\
          + (p_{mn} - e_{mt} ) \log \left(1 -
            G^\kappa\left(\widehat{VE}_{mt}(\sigma)
              ;\alpha\right) \right)
                                      \end{array}
    \end{align*}
  \end{itemize}
\end{frame}

\subsubsection{Data}
\begin{frame}\frametitle{Data}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab1}
\end{frame}

\begin{frame}\frametitle{Entrants}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab2}
\end{frame}

\subsubsection{Results}
\begin{frame}\frametitle{Results}
  Profit function:
  \begin{itemize}
  \item Decreasing with $n$ increasing in $w$, $inc$, $pop$
  \item Compare fixed effects and OLS estimates
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab3}
\end{frame}

\begin{frame}\frametitle{$\hat{\pi}(n)$}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-fig1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab4}
\end{frame}


\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab6}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-fig2}
\end{frame}

\begin{frame}\frametitle{Fit}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab7}
\end{frame}

\begin{frame}\frametitle{Fit}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab8}
\end{frame}


\begin{frame}\frametitle{Subsidies to entry and fixed costs}
  \begin{itemize}
  \item Health Professional Shortage Areas (HPSA) have entry subsidies
  \item Entry cost subsidy $=$ change distribution of entry costs for
    all markets to the distribution estimated for HPSA markets
  \item Fixed cost subsidy $=$ reduce mean of fixed cost by 8\%
    (chosen to generate similar number of firms as HPSA subsidy)
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab9}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab10}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,keepaspectratio=true]{figs/dkrx-tab11}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\cite{lin2015}}

\begin{frame}\frametitle{\cite{lin2015}}
  \textbf{Quality choice and market structure: a dynamic analysis of
    nursing home oligopolies}

  \begin{itemize}
  \item Poor quality common in nursing homes
    \begin{itemize}
    \item 30\% of nursing homes violated federal regulations in 2006
    \end{itemize}
  \item Policies designed to inform consumers about nursing home quality
    \begin{itemize}
    \item Nursing Home Quality Initiative began in 2002 in US
    \item
      \href{http://www.npr.org/sections/health-shots/2016/11/27/503040600/on-hospital-exit-patients-don-t-learn-about-nursing-home-quality}
      {NPR: Rule Change Could Push Hospitals To Tell Patients About
        Nursing Home Quality}
    \item
      \href{https://www.cihi.ca/en/health-system-performance/performance-reporting/international/performance-of-1000-canadian-long-term}
      {Performance of 1,000 Canadian long-term care facilities now
        publicly available}
    \item
      \href{https://www.thestar.com/news/canada/2017/03/10/ontario-nursing-homes-feed-seniors-on-833-a-day.html}
      {Ontario nursing homes feed seniors on \$8.33 a day}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{\cite{lin2015}}
  \begin{itemize}
  \item Dynamic model of quality choice
  \item Effect of eliminating low quality nursing homes
    \begin{itemize}
    \item Raises quality, but reduces supply and alters competition
    \end{itemize}
  \item Effect of competition
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item 1996-2005 Online Survey Certification and Reporting System
    (OSCAR)
  \item Not his paper, but if you wanted similar, more recent data see
    \href{https://www.cms.gov/Research-Statistics-Data-and-Systems/Downloadable-Public-Use-Files/Provider-of-Services/index.html}
    {Provider of Services (POS) files from CMS}
    \begin{itemize}
    \item Annual (possibly quarterly) 2006-2016
    \item Very detailed staff and service information
    \end{itemize}
  \item Market $=$ county
  \item Limit sample to counties with 6 or fewer nursing homes
  \item Quality $=$ nurses$/$beds above or below median
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{lin-tab12}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Common knowledge state $x_t = (\underbrace{M_t}_{market size},
    \underbrace{I_t}_{market income}, \underbrace{\tau}_{market type}, \underbrace{\mathbf{s}_t}_{
      firm states})$
    \begin{itemize}
    \item All variables are market (county) specific, but suppressed
      from notation
    \end{itemize}
  \item $s_{it} = \begin{cases} 0 & \text{ if out of market} \\
      1 & \text{ if low quality } \\
      2 & \text{ if high quality}
    \end{cases}$
  \item Private info of firm $i$, $\epsilon_{it}$
  \item Action $a_{it} = s_{it+1}$
  \item Assumptions (same as general setup):
    \begin{enumerate}
    \item Additive separability: $\pi_{it}(x_t, a_t, \epsilon_t) =
      \pi_{it}(x_t,a_t) + \epsilon_{it}(a_{it})$
    \item Conditional independence: $F(x_{t+1},\epsilon_{t+1} | x_t,
      \epsilon_t, a_t) = F_t(x_{t+1} | x_t, a_t)
      F_\epsilon(\epsilon_{t+1})$
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Market type}
  \begin{itemize}
  \item Market type used to capture unobserved market heterogeneity
  \item Market type estimation:
    \begin{itemize}
    \item Fixed effects regressions
      \begin{align*}
        N_{high quality, mt} = & \theta_{m,H} +  \beta_{1,H} M_{mt} +
                                 \beta_{2,H} I_{mt} + u_{mt} \\
        N_{low quality, mt} = & \theta_{m,L} +  \beta_{1,L} M_{mt} +
                                 \beta_{2,L} I_{mt} + u_{mt}
      \end{align*}
    \item Market m, type $H_L$ if $\hat{\theta}{m,H}$ below its median
    \item Similarly define $H_H$, $L_L$, $L_H$, to get 4 types
    \end{itemize}
  \item Ad-hoc? similar to \cite{collard2008}
    \begin{itemize}
    \item Method of \cite{bonhomme2015} could be better way to capture
      similar idea
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{lin-tab3}
\end{frame}

\begin{frame}\frametitle{Payoff function}
  \includegraphics[width=\textwidth,
  keepaspectratio=true]{lin-eq6}
  with
  \includegraphics[width=\textwidth,
  keepaspectratio=true]{lin-eq7}
  and similar for $g_H$
\end{frame}

\begin{frame}[shrink]\frametitle{Estimation}
  \begin{itemize}
  \item Estimate $\tilde{P}(a|x)$ by multinomial logit
  \item Form value function
    \begin{align*} \hat{V}(x,a;\theta,\tilde{P}) = & \pi(x,a;\theta) + (I-\beta
      F^{\tilde{P}})^{-1} \left( \sum_{a}
      \tilde{P}(a|x)\pi(x,a;\theta) \right) +  \\
      & + (I-\beta
      F^{\tilde{P}})^{-1}  \left( \sum_{a}
      \tilde{P}(a|x) \Er[\epsilon|a,x] \right)
    \end{align*}
    $\pi$ linear in $\theta$, so
    \[  \hat{V}(x,a;\theta,\tilde{P}) = Z(a) \theta +
      \hat{\epsilon}(a|\tilde{P}) \]
  \item Model predicted probabilities: $\hat{P}(a|x;\theta,\tilde{P})
    = \frac{e^{Z(a)\theta + \hat{\epsilon}(a|\tilde{P})} }
    {\sum_{a'} e^{Z(a')\theta + \hat{\epsilon}(a'|\tilde{P})}}$
  \item Moments:
    \[ \Er\left[ \left(\hat{P}(a|x;\theta,\tilde{P}) - P^0(a|x) \right) X
      \right] = 0 \]
  \item Estimate $\theta$ by GMM
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{lin-tab4}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{lin-tab5}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{lin-tab6}
\end{frame}

\begin{frame}\frametitle{Counterfactuals}
  \begin{itemize}
  \item Simulate beginning in 2000 for markets with 4 or fewer firms
    (2195 markets)
  \item[I] Baseline
  \item[II] Elderly populations grows 3\% faster years 6-15
  \item[III] Low quality forbidden
  \item[IV] Lower entry cost
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]{lin-tab8}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Generalizations and extensions}

\begin{frame}\frametitle{Generalizations and extensions}
  \begin{itemize}
  \item Unobserved state variables
  \item Multiple equilibria
  \item Continuous time
  \end{itemize}
\end{frame}


%\subsection{Unobserved state variables}
%Permanent unobserved heterogeneity \cite{arcidiconoMiller}
%Markov types \cite{kasaharaShimotsu}, \cite{huShum}
%Single equilibria \cite{????}

%\subsection{Multiple equilibria}

%\subsection{Preferences additively separable over time}
%\cite{fangXXXX}

\begin{frame}\frametitle{Additional Applications}
  \begin{itemize}
  \item Hard disk drives: \cite{igami2017}, \cite{igami2018}, \cite{igami2019}
  \item Hamburger chains: \cite{igami2016}
  \item Electricity: \cite{doraszelski2018}
  %\item Railroads: \cite{chen2023}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
\input{../slideHeader}

\title{Estimating Production Functions}
\subtitle{Methodology Extensions}

\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{Mistakes have been made}
  Control function estimators of production functions have repeatedly
  been used without fully thinking through the underlying model and
  assumptions
  \begin{enumerate}
  \item Colinearity of flexible inputs with each other
    \begin{itemize}
    \item Pointed out by \cite{ackerberg2006structural}
    \end{itemize}
  \item Lack of relevant instrument for flexible input
    \begin{itemize}
    \item Pointed out by \cite{gandhi2009identifying}
    \end{itemize}
  \item Heterogeneous markups are incompatible with the monotonicity
    assumption
    \begin{itemize}
    \item Mistake in \cite{deloecker2012} (1337 citations), repeated
      in \cite{deloecker2020} (1268 citations)
    \item Pointed out by \cite{doraszelski2019}, \cite{doraszelski2021}
    \end{itemize}
  \end{enumerate}
\end{frame}


\begin{frame}
  \frametitle{Extensions}
  \begin{itemize}
  \item \cite{levinsohn2003}: investment often zero, so use other
    inputs instead of investment to form control function
  \item \cite{ackerberg2006structural}: control function often
    collinear with $l_{it}$ --- for it not to be must be firm specific
    unobervables affecting $l_{it}$ (but not investment / other input
    or else demand not invertible and cannot form control function)
  \item \cite{gandhi2009identifying}: relax scalar unobservable in
    investment / other input demand
  \item \cite{wooldridge2009}: more efficient joint estimation
  \item \cite{maican2006} and \cite{doraszelski2009}: endogenous
    productivity
  \end{itemize}
\end{frame}

\section{\cite{ackerberg2006structural}}

\begin{frame}\frametitle{\cite{ackerberg2006structural}: contributions}
  \begin{itemize}
  \item Document collinearity problem in OP and \citet{levinsohn2003}
    \begin{itemize}
    \item Need $l_{it}$, $f_{it}(k_{it}, i_{it})$ not collinear, i.e.\
      something causes variation in $l$, but not $k$
    \end{itemize}
  \item Propose alternative estimator
  \item Relates estimator to dynamic panel \citep{blundell2000}
    approach
  %\item Illustrates estimator using Chilean data
  \end{itemize}
  \footnotetext{$^*$These slides are based on the working paper version \cite{ackerberg2006wp}.}
\end{frame}

\subsection{Collinearity in OP}

\begin{frame}[allowframebreaks]
  \frametitle{Collinearity in OP}
  \begin{itemize}
  \item OP assume $i_{it} = I_t(k_{it}, \omega_{it})$
  \item Symmetry, parsimony suggest $l_{it} = L_t(k_{it},
    \omega_{it})$
  \item Then $l_{it} = L_t(k_{it}, I_t^{-1}(k_{it}, i_{it}) ) =
    g_t(k_{it}, i_{it})$
    \[ y_{it} = \beta_l l_{it} + f_t(k_{it}, i_{it}) +
    \epsilon_{it} \]
    $l_{it}$ collinear with $f_t(k_{it}, i_{it})$
  \item Worse in \cite{levinsohn2003}
    \begin{itemize}
    \item Uses other input $m_{it}$ to form control function
      \begin{align*}
        y_{it} = & \beta_l l_{it} + \beta_k k_{it} + \beta_m m_{it} +
        \omega_{it} + \epsilon_{it} \\
        m_{it} = & M_t(k_{it}, \omega_{it})
      \end{align*}
    \item Even less reason to treat labor demand differently than other
      input demand
    \end{itemize}
  \item Collinearity still problem with parametric input demand
  \item Plausible models that do not solve collinearity
    \begin{itemize}
    \item Input price data
      \begin{itemize}
      \item Must include in control function to preserve scalar unobservable
      \item Same logic above implies $m$ and $l$ are functions of both prices,
        so still collinear
      \end{itemize}
    \item Adjustmest costs in labor
      \begin{itemize}
      \item Need to add $l_{it-1}$ to control function
      \end{itemize}
    \item Change in timing assumptions
    \item Measurement error in $l$ (but not $m$)
      \begin{itemize}
      \item Solves collinearity, but makes $\hat{\beta}_l$
        inconsistent
      \end{itemize}
    \end{itemize}
  \item Potential model change that removes collinearity
    \begin{itemize}
    \item Optimization error in $l$ (but not $m$)
    \item $m$ chosen, $l$ specific shock revealed, $l$ chosen
    \item OP only: $l_{it}$ chosen at $t-1/2$, $l_{it} =
      L_t(\omega_{it-1/2},k_{it})$, $i_{it}$ chosen at $t$
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{ACF estimator}
\begin{frame}[shrink]
  \frametitle{ACF estimator}
  \begin{itemize}
  \item Idea: like capital, labor is harder to adjust than other
    inputs
  \item Model: $l_{it}$ chosen at time $t-1/2$, $m_{it}$ at time $t$
    \begin{itemize}
    \item Implies $m_t = M_t(k_{it},l_{it}, \omega_{it})$
    \end{itemize}
  \item Estimation:
    \begin{enumerate}
    \item $y_{it} = \underbrace{\beta_k k_{it} + \beta_l l_{it} +
        f_t(m_{it},k_{it},l_{it})}_{\equiv
        \Phi_t(m_{it},k_{it},l_{it})} + \epsilon_{it}$ gives
      \[ \hat{\omega}_{it}(\beta_k,\beta_l) = \hat{\Phi}_{it} -
      \beta_k k_{it} - \beta_l l_{it} \]
    \item Moments from timing and Markov process for $\omega_{it}$
      assumptions:
      \[ \omega_{it} = \Er[\omega_{it} | \omega_{it-1} ] + \xi_{it} \]
      \begin{itemize}
      \item $\Er[\xi_{it} | k_{it}]=0 $ as in OP
      \item $\Er[\xi_{it} | l_{it-1}] = 0$ from new timing assumption
      \item $\hat{\xi}_{it}(\beta_k,\beta_l)$ as residual from
        nonparametric regression of $\hat{\omega}_{it}$ on
        $\hat{\omega}_{it-1}$
      \item Can add moments based on $\Er[\epsilon_{it} |
        \mathcal{I}_{it}] = 0$
      \end{itemize}
    \end{enumerate}
  \end{itemize}
\end{frame}

\subsection{Relation to dynamic panel}
\begin{frame}
  \frametitle{Relation to dynamic panel estimators}
  \begin{itemize}
  \item Both derive moment conditions from assumptions about timing
    and information set of firm
  \item Dealing with $\omega$
    \begin{itemize}
    \item Dynamic panel: AR(1) assumption allows quasi-differencing
    \item Control function: makes $\omega$ estimable function of
      observables
    \end{itemize}
  \item Dynamic panel allows fixed effects, does not make assumptions
    about input demand
  \item Control function allows more flexible process for $\omega_{it}$
  \end{itemize}
\end{frame}

\subsection{Simulations}
\begin{frame}
  \frametitle{Simulations}
  \begin{itemize}
  \item DGPS:
    \begin{enumerate}
    \item Consistent with their model, but not LP
    \item Consistent with both
    \item Combination that consistent with neither
    \end{enumerate}
  \item Add measurement error to materials
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Simulation Results}
  \includegraphics[width=\textwidth]{acf-tab1}
\end{frame}

%\frame[plain]{\includegraphics[page=39,width=\textwidth]{ACF20withtables}}
%\frame{\includegraphics[page=40,height=\textheight]{ACF20withtables}}
%\frame[plain]{\includegraphics[page=40,width=\textwidth]{ACF20withtables}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{gandhi2009identifying}}

\begin{frame}\frametitle{\cite{gandhi2009identifying}}
  \begin{itemize}
  \item Show that control function method is not nonparametrically
    identified when there are flexible inputs
  \item Propose alternate estimate that uses data on input shares and
    information from firm's first order condtiion
  \item Show that value-added and gross output production functions
    are incompatible
  \item Application to Colombia and Chile
  \end{itemize}
\end{frame}

\subsection{Identification problem}

\begin{frame}\frametitle{Assumptions}
  \begin{enumerate}
  \item Hicks neutral productivity $Y_{jt} = e^{\omega_{jt} +
      \epsilon_{jt}} F_t(L_{jt},K_{jt},M_{jt}) $
  \item $\omega_{jt}$ Markov, $\epsilon_{jt}$ i.i.d.
  \item $K_{jt}$ and $L_{jt}$ determined at $t-1$, $M_{jt}$ determined
    flexibly at $t$
    \begin{itemize}
    \item $K$ and $L$ play same role in the model, so after this slide
      I will drop $L$
    \end{itemize}
  \item $M_{jt} = \mathbb{M}_t(L_{jt}, K_{jt}, \omega_{jt})$, monotone
    in $\omega_{jt}$
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Reduced form}
  \begin{itemize}
  \item Let $h(\omega_{jt-1}) = \Er[\omega_{jt}|\omega_{jt-1}]$,
    $\eta_{jt} = \omega_{jt} - h(\omega_{jt-1})$
  \item log output
    \begin{align*}
      y_{jt} = & f_t(k_{jt},m_{jt}) + \omega_{jt} + \epsilon_{jt} \\
      = & f_t(k_{jt},m_{jt}) +
      \underbrace{h(\mathbb{M}_{t-1}^{-1}(k_{jt-1},m_{jt-1}))}_{=h_{t-1}(k_{jt-1},m_{jt-1})}
      + \eta_{jt} + \epsilon_{jt}
    \end{align*}
  \item Assumptions imply
    \[ \Er[\eta_{jt} | \underbrace{k_{jt}, k_{jt-1}, m_{jt-1}, ... k_{j1}, m_{j1}}_{=\Gamma_{jt}}]
    = 0 \]
  \item Reduced form
    \begin{align}
      \Er[y_{jt} |\Gamma_{jt}] = & \Er[f_t(k_{jt}, m_{jt}) |
      \Gamma_{jt}] +  h_{t-1}(k_{jt-1},m_{jt-1}) \label{eq:id}
    \end{align}
  \item Identification: given observed $ \Er[y_{jt} |\Gamma_{jt}] $ is
    there a unique $f_t$, $h_{t-1}$ that satisfies (\ref{eq:id})?
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Example: Cobb-Douglas}
  \begin{itemize}
  \item Let $f_t(k,m) = \beta_k k + \beta_m m$
  \item Assume firm is takes prices as given
  \item First order condition for $m$ gives
    \[ m = constant + \frac{\beta_k}{1-\beta_m} k +
    \frac{1}{1-\beta_m} \omega \]
  \item Put into reduced form
    \begin{align}
      \Er[y_{jt} |\Gamma_{jt}] = & C + \frac{\beta_k}{1-\beta_m}
      k_{jt} + \frac{\beta_m}{1-\beta_m} \Er[\omega_{jt} | \Gamma_{jt}] +
      h_{t-1}(k_{jt-1},m_{jt-1}) \label{eq:id}
    \end{align}
  \item $\omega$ Markov and $\omega_{jt-1} =
    \mathbb{M}^{-1}_{t-1}(k_{jt-1},m_{jt-1})$ implies
    \begin{align*}
      \Er[\omega_{jt} | \Gamma_{jt}] = & \Er[\omega_{jt} |
      \omega_{jt-1} = \mathbb{M}^{-1}_{t-1}(k_{jt-1},m_{jt-1})] = \\
      = &  h_{t-1}(k_{jt-1},m_{jt-1})
    \end{align*}
  \item Which leaves
    \begin{align}
      \Er[y_{jt} |\Gamma_{jt}] = & constant + \frac{\beta_k}{1-\beta_m}
      k_{jt} + \frac{1}{1-\beta_m} h_{t-1}(k_{jt-1},m_{jt-1}) \label{eq:id}
    \end{align}
    from which $\beta_k$, $\beta_m$ are not identified
  \item Rank condition fails, $\Er[m_{jt} | \Gamma_{jt}]$ is colinear
    with $h_{t-1}(k_{jt-1}, m_{jt-1})$
  \item After conditioning on $k_{jt}, k_{jt-1}, m_{jt-1}$, only
    variation in $m_{jt}$ is from $\eta_{jt}$, but this is
    uncorrelated with the instruments
  \end{itemize}
\end{frame}

% \begin{frame}[allowframebreaks]
%   \frametitle{Proof of non-identification}
%   \begin{itemize}
%   \item I do not doubt the claim of non-identification, but I do not
%     understand the proof in the paper
%   \item Lemma 2 incorrect?
%   \item Alternate proof
%   \end{itemize}
% \end{frame}

\subsection{Identification from first order conditions}

\begin{frame}[allowframebreaks]
  \frametitle{Identification from first order conditions}
  \begin{itemize}
  \item Since $m$ flexible, it satisfies a simple static first order
    condition,
    \begin{align*}
      \rho_t = & p_t \frac{\partial F_t}{\partial M}
      \Er[e^{\epsilon_{jt}}] e^{\omega_{jt}} \\
      \log \rho_t = & \log p_t + \log \frac{\partial F_t}{\partial
        M}(k_{jt},m_{jt}) + \log \Er[e^{\epsilon_{jt}}] + \omega_{jt}
    \end{align*}
  \item Problem: prices often unobserved, endogenous $\omega$
  \item Solution: difference from output equation to eliminate
    $\omega$, rearrange so that it involves only the value of materials
    and the value of output (which are often observed)
    \begin{align*}
      \underbrace{s_{jt}}_{\equiv \log \frac{\rho_t M_{jt}}{p_t Y_{jt}}} = & \log \underbrace{G_t(k_{jt},
      m_{jt})}_{\equiv \left(M_t \frac{\partial F_t}{\partial
          M}\right)/F_t} + \log \underbrace{\Er[e^{\epsilon_{jt}}]}_{\mathcal{E}} - \epsilon_{jt}
    \end{align*}
  \item Identifies elasticity up to scale, $G_t \mathcal{E}$ and
    $\epsilon_{jt}$ which identifie $\mathcal{E}$
  \item Integrating,
    \[ \int_{m_0}^{m_{jt}} G_t(k_{jt},m)/m =
    f_t(k_{jt},m_{jt}) + c_t(k_{jt}) \]
    identifies $f$ up to location
  \item Output equation
    \begin{align*}
      y_{jt} = & \int_{m_0}^{m_{jt}}
      \tilde{G}_t(k_{jt},m)/m - c_t(k_{jt}) + \omega_{jt} + \epsilon_{jt} \\
      -c_t(k_{jt}) +
      \omega_{jt} = & \underbrace{y_{jt} - \int_{m_0}^{m_{jt}} \tilde{G}_t(k_{jt},m)/m
        - \epsilon_{jt}}_{\equiv \mathcal{Y}_{jt}}
    \end{align*}
    where the things on the right have already been identified
  \item Identify $c_t$ from
    \begin{align*}
      \mathcal{Y}_{jt}= & -c_t(k_{jt}) +
      \tilde{h}_t( \mathcal{Y}_{jt-1},k_{jt-1}) + \eta_{jt}
    \end{align*}
  \end{itemize}
\end{frame}

\subsection{Value added vs gross production}

\begin{frame}\frametitle{Value added vs gross production}
  \begin{itemize}
  \item Value added:
    \begin{align*}
      VA_{jt} = & p_t Y_{jt} - \rho_t M_{jt} \\
      = & p_t F_t(K_{jt},
      \mathbb{M}_t(K_{jt},\omega_{jt}))e^{\omega_{jt}+\epsilon_{jt}} -
      \rho_t \mathbb{M}_t(K_{jt},\omega_{jt})
  \end{align*}
\item Envelope theorem implies $\text{elasticity}_{e^\omega}^Y \approx
    \text{elasticity}_{e^\omega}^{VA} (1 -  \frac{\rho_t M_{jt}}{p_t
      Y_{jt}})$
  \end{itemize}
  Problems
  \begin{itemize}
  \item Production Hicks-neutral productivity does not imply
    value-added Hicks-neutral productivity
  \item Ex-post shocks $\epsilon_{jt}$ not accounted for
    in approximation
  \end{itemize}
\end{frame}

\subsection{Empirical results}
\begin{frame}\frametitle{Empirical results}
  \begin{itemize}
  \item Look at tables
  \item Value-added estimates imply much more productivity dispersion
    than gross (90-10) ratio of 4 vs 2
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\section{Non-Hicks neutral productivity}
% Li & Sasaki, Fox et al, Navarro & Rivers

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}

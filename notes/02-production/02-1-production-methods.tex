\input{../slideHeader}

\title{Estimating Production Functions}
\subtitle{Introduction}

\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Why estimate production functions?}
  \begin{itemize}
  \item Primitive component of economic model
  \item Gives estimate of firm productivity --- useful for
    understanding economic growth
    \begin{itemize}
    \item Stylized facts to inform theory, e.g. \cite{foster2001}
    \item Effect of deregulation, e.g. \cite{olley1996dynamics}
    \item Growth within old firms vs from entry of new firms,
      e.g. \cite{foster2006}
    \item Effect of trade liberalization, e.g. \cite{amiti2007}
    \item Effect of FDI \cite{javorcik2004}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{General references:}
  \begin{itemize}
  \item \textbf{\cite{aguirregabiria2012} chapter 3}
  \item \cite{deloecker2021}
  \item \cite{ackerberg2007econometric} section 2
  \item \cite{vanBeveren2012}
  \end{itemize}
\end{frame}

\section{Setup}

\begin{frame}
  \frametitle{Setup}
  \begin{itemize}
  \item Cobb Douglas production
    \[ Y_{it} = A_{it} K_{it}^{\beta_k} L_{it}^{\beta_\ell} \]
  \item In logs,
    \[ y_{it} = \beta_k k_{it} + \beta_\ell l_{it} +
    \omega_{it} + \epsilon_{it} \]
    with $\log A_{it} = \omega_{it} + \epsilon_{it}$, $\omega_{it}$
    known to firm, $\epsilon_{it}$ not
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Empirical Challenges}
  \begin{enumerate}
  \item Simultaneity: if firm has information about $\log A_{it}$
    when choosing inputs, then inputs correlated with
    $\log A_{it}$
  \item Selection: firms with low productivity will exit sooner
  \item Others: measurement error, specification
  \end{enumerate}
\end{frame}

\section{Simultaneity}

\begin{frame}
  \frametitle{Simultaneity}
  \begin{itemize}
  \item Firm's inputs will be correlated with firm's knowledge of
    productivity
  \item E.g.\ output price $p$, wage $w$, choosing $L$ given $K$
    \[ \max_L p \Er[A] K^\beta_k L^\beta_\ell - w L \]
    implies
    \[ L = \left( \frac{p}{w} \beta_\ell \Er[A]
        K^{\beta_k}\right)^{\frac{1}{1-\beta_\ell}}  \]
    or in logs,
    \[ l = \frac{1}{1-\beta_\ell} \log
      \left(\frac{p}{w}\beta_\ell\right) +
      \frac{\beta_k}{1-\beta_\ell} k + \frac{1}{1-\beta_\ell} \log
      \left( \Er[A] \right) \]
  \item $l$ correlated with productivity through correlation of $\log
    A$ and $\log(\Er[A])$
  \item Exercise: calculate bias of $\hat{\beta}_\ell^{OLS}$ (with some
    further assumptions)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Simultaneity solutions}
  \begin{enumerate}
  \item IV
  \item Control functions
  \item Panel data and timing assumptions
  \end{enumerate}
\end{frame}

\subsection{Instrumental variables}

\begin{frame}
  \frametitle{Instrumental variables}
  \begin{itemize}
  \item Instrument must be
    \begin{itemize}
    \item Correlated with $k$ and $l$
    \item Uncorrelated with $\omega + \epsilon$
    \end{itemize}
  \item Possible instrument: input prices
    \begin{itemize}
    \item Correlated with $k$, $l$ through first-order condition
    \item Uncorrelated with $\omega$ if input market competitive
    \end{itemize}
  \item Other possible instruments: output prices (more often
    endogenous), input supply or output demand shifter (hard to find)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Problems with input prices as IV}
  \begin{itemize}
  \item Not available in many data sets
  \item Average input price of firm could reflect quality as well as
    price differences
  \item Need variation across observations
    \begin{itemize}
    \item If firms use homogeneous inputs, and operate in the same
      output and input markets, we should not expect to find any
      significant cross-sectional variation in input prices
    \item If firms have different input markets, maybe variation in
      input prices, but different prices could be due to different
      average productivity across input markets
    \item Variation across time is potentially endogenous because
      could be driven by time series variation in average productivity
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Control functions}

\begin{frame} \frametitle{Control functions}
  \begin{itemize}
  \item From \cite{olley1996dynamics} (OP)
  \item {\bfseries{Control function}}: function of data conditional on
    which endogeneity problem solved
    \begin{itemize}
    \item E.g. usual 2SLS $y = x\beta + \epsilon$, $x = z \pi + v$,
      control function is to estimate residual of reduced form,
      $\hat{v}$ and then regress $y$ on $x$ and $\hat{v}$. $\hat{v}$
      is the control function
    \end{itemize}
  \item Main idea: model choice of inputs to find a control function
  \end{itemize}
\end{frame}


\begin{frame}[shrink]\frametitle{OP assumptions}
  \[ y_{it} = \beta_k k_{it} + \beta_\ell l_{it} + \omega_{it} + \epsilon_{it} \]
  \begin{enumerate}
  \item $\omega_{it}$ follows exogenous first order Markov process,
    \[ p(\omega_{it+1} | \mathcal{I}_{it}) = p(\omega_{it+1} |
    \omega_{it}) \]
    \note{Define first order Markov. \\
      Econometric assumption about evolution of $\omega$. \\
      Economic assumption about firm information.}
  \item Capital at $t$ determined by investment at time $t-1$,
    \[ k_t = (1-\delta) k_{it-1} + i_{it-1} \]
  \item Investment is a function of $\omega$ and other observed
    variables
    \[
    i_{it} = I_t(k_{it}, \omega_{it}),
    \]
    and is strictly increasing in $\omega_{it}$
    \note{
    \begin{itemize}
    \item $I_t()$ depends on $t$, so can have common unobserved
      input prices that change with time
    \item Unobserved variables that change across firms are not
      allowed to affect investment
    \end{itemize}
    }
  \item Labor flexible and non-dynamic, i.e.\ chosen each $t$,
    current choice has no effect on future (can be relaxed)
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{OP estimation of $\beta_\ell$}
  \begin{itemize}
  \item Invertible investment implies $\omega_{it} =
    I_t^{-1}(k_{it}, i_{it})$
    \begin{align*}
      y_{it} = & \beta_k k_{it} + \beta_\ell l_{it} +
      I_t^{-1}(k_{it}, I_{it}) + \epsilon_{it} \\
      = & \beta_\ell l_{it} + f_t(k_{it}, i_{it}) + \epsilon_{it}
    \end{align*}
  \item Partially linear model
    \begin{itemize}
    \item Estimate by e.g.\ regress $y_{it}$ on $l_{it}$ and series
      functions of $t, k_{it},i_{it}$
    \item Gives $\hat{\beta}_l$, $\hat{f}_{it} = \hat{f}_t(k_{it}, i_{it})$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{OP estimation of $\beta_k$}
  \begin{itemize}
  \item Note: $\hat{f}_t(k_{it}, i_{it}) = \hat{\omega}_{it} +
    \beta_k k_{it}$
  \item By assumptions, $ \omega_{it} = \Er[\omega_{it} |
    \omega_{it-1}] + \xi_{it} = g(\omega_{it-1}) + \xi_{it}$ with
    $\Er[\xi_{it} | k_{it} ] = 0$
  \item Use $\Er[\xi_{it} | k_{it} ] = 0$ as moment to estimate
    $\beta_k$.
    \begin{itemize}
    \item OP: write production function as
      \begin{align*}
        y_{it} - \beta_\ell l_{it} = & \beta_k k_{it} +
        g(\omega_{it-1}) + \xi_{it} + \epsilon_{it} \\
        = &  \beta_k k_{it} +
        g\left(f_{it-1} - \beta_k k_{it-1} \right) +
        \\
        & + \xi_{it} + \epsilon_{it}
      \end{align*}
      Use $\hat{\beta}_l$ and $\hat{f}_{it}$ in equation above and
      estimate $\hat{\beta}_k$ by e.g.\
      semi-parametric nonlinear least squares
    \item \cite{ackerberg2006structural}: use $\Er \left[
        \hat{\xi}_{it}(\beta_k) k_{it} \right] = 0$
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Panel data}

\subsubsection{Fixed effects}

\begin{frame} \frametitle{Fixed effects}
  \begin{itemize}
  \item Have panel data, so should consider fixed effects
  \item FE consistent if:
    \begin{enumerate}
    \item $\omega_{it} = \eta_i + \delta_t + \omega_{it}^\ast$
    \item $\omega_{it}^\ast$ uncorrelated with $l_{it}$ and $k_{it}$,
      e.g.\ $\omega_{it}^\ast$ only known to firm after choosing
      inputs
    \item $\omega_{it}^\ast$ not serially correlated and is strictly
      exogenous
    \end{enumerate}
  \item Problems:
    \begin{itemize}
    \item Fixed productivity a strong assumption
    \item Estimates often small in practice
    \item Worsens measurement error problems
      \[ \mathrm{Bias}(\hat{\beta}^{FE}_k) \approx -\frac{\beta_k
        \var(\Delta \epsilon)} {\var(\Delta k) + \var(\Delta
        \epsilon)} \]
    \end{itemize}
  \end{itemize}
\end{frame}

\subsubsection{Dynamic panel}
\begin{frame}[allowframebreaks]\frametitle{Dynamic panel: motivation}
  \begin{itemize}
  \item General idea: relax fixed effects assumption, but still
    exploit panel
  \item Collinearity problem: Cobb-Douglas production, flexible labor
    and capital implies log labor and log capital are linear functions
    of prices and productivity (\cite{bond2005})
  \item If observed labor and capital are not collinear then there
    must be something unobserved that varies across firms
    (e.g. prices), but that could invalidate monotonicity assumption
    of control function
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Dynamic panel: moment conditions}
  \begin{itemize}
  \item See \cite{blundell2000}
  \item Assume $\omega_{it} = \gamma_t + \eta_i + \nu_{it}$ with
    $\nu_{it} = \rho \nu_{i,t-1} + e_{it}$, so
    \[ y_{it} = \beta_\ell l_{it} + \beta_k k_{it} + \gamma_t + \eta_i +
    \nu_{it} + \epsilon_{it} \]
    subtract $\rho y_{i,t-1}$ and rearrange to get
    \begin{align*}
      y_{it} = & \rho y_{i,t-1} + \beta_\ell ( l_{it} - \rho l_{i,t-1}) + \beta_k (k_{it} -
      \rho k_{i,t-1}) + \\
      & + \gamma_t - \rho \gamma_{t-1}
      + \underbrace{\eta_i(1-\rho)}_{=\eta_{i}^\ast} + \underbrace{e_{it} + \epsilon_{it} - \rho
        \epsilon_{i,t-1}}_{= w_{it}}
    \end{align*}
  \item Moment conditions:
    \begin{itemize}
    \item Difference: $\Er[x_{i,t-s} \Delta w_{it}]=0$ where
      $x=(l,k,y)$
    \item Level: $\Er\left[ \Delta x_{i,t-s} (\eta_i^\ast +
        w_{it})\right] = 0$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Dynamic panel: economic model}
  \begin{itemize}
  \item Adjustment costs
    \begin{align*}
      V(K_{t-1},L_{t-1}) = \max_{I_t,K_t,H_t,L_t} & P_t F_t(K_t,L_t) -
      P^K_t\left(I_t + G_t(I_t,K_{t-1}) \right) - \\
      & - W_t \left(L_t +
        C_t(H_t,L_{t-1}) \right) + \\
      \psi \Er\left[V(K_t,L_t) |
        \mathcal{I}_t\right]  \\
      \text{s.t. } & K_t = (1-\delta_k)K_{t-1} + I_t \\
      & L_t = (1-\delta_l) L_{t-1} + H_t
    \end{align*}
    Implies
    \begin{align*}
      P_t \frac{\partial F_t}{\partial L_t} - W_t \frac{\partial
        C_t}{\partial L_t} = & W_t + \lambda_t^L\left(1 - (1-\delta_l)
        \psi \Er\left[\frac{\lambda^L_{t+1}}{\lambda_{t}^L} |
          \mathcal{I}_t \right]  \right) \\
      P_t \frac{\partial F_t}{\partial K_t} - P^K_t \frac{\partial
        G_t}{\partial K_t} = & \lambda_t^K\left(1 - (1-\delta_k)
        \psi \Er\left[\frac{\lambda^K_{t+1}}{\lambda_{t}^K} |
          \mathcal{I}_t \right]  \right)
    \end{align*}
    \begin{itemize}
    \item Current productivity shifts $\frac{\partial F_t}{\partial
        L_t}$ and (if correlated with future) the shadow value of
      future labor $\Er\left[\frac{\lambda^L_{t+1}}{\lambda_{t}^L} |
        \mathcal{I}_t \right]$
    \item Past labor correlated with current because of adjustment costs
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Dynamic panel data: problems}
  \begin{itemize}
  \item Problems:
    \begin{itemize}
    \item Sometimes imprecise (especially if only use difference
      moment conditions)
    \item Differencing worsens measurement error
    \item Weak instrument issues if only use difference moment
      conditions but levels stronger (see \cite{blundell2000})
      \begin{itemize}
      \item Level moments require stronger stationarity assumption -
        $\eta_i$ uncorrelated with $\Delta x_{it}$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Dynamic panel vs control function}
  \begin{itemize}
  \item Both derive moment conditions from assumptions about timing
    and information set of firm
  \item Dealing with $\omega$
    \begin{itemize}
    \item Dynamic panel: AR(1) assumption allows quasi-differencing
    \item Control function: makes $\omega$ estimable function of
      observables
    \end{itemize}
  \item Dynamic panel allows fixed effects, does not make assumptions
    about input demand
  \item Control function allows more flexible process for
    $\omega_{it}$
  \item \cite{ackerberg2023} for detailed comparison
  \end{itemize}
\end{frame}

\subsection{Applications}

\begin{frame}\frametitle{Applications}
  \begin{itemize}
  \item \cite{olley1996dynamics}: productivity in telecom after
    deregulation
  \item \cite{sth2006}: productivity and exit of African manufacturing
    firms, uses IV
  \item \cite{levinsohn2003}: compare estimation methods using Chilean
    data
  \item \cite{javorcik2004}: FDI and productivity, uses OP
  \item \cite{amiti2007}: trade liberalization in Indonesia, uses OP
  \item \cite{aw2001}: productivity differentials and firm turnover in
    Taiwan
  \item \cite{kortum2000}: venture capital and innovation
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Selection}

\begin{frame} \frametitle{Selection}
  \begin{itemize}
  \item Let $d_{it} = 1 $ if firm in sample.
    \begin{itemize}
    \item Standard conditions imply $d = 1\{\omega
      \geq\omega^\ast(k)\}$
    \end{itemize}
  \item Messes up moment conditions
    \begin{itemize}
    \item All estimators based on $\Er[ \omega_{it} \text{Something} ] =
      0$, observed data really use $\Er[ \omega_{it} \text{Something} |
      d_{it} = 1]$
    \item E.g.\ OLS okay if $\Er[ \omega_{it} |l_{it}, k_{it} ] = 0$,
      but even then,
      \begin{align*}
        \Er[ \omega_{it} | l_{it}, k_{it} , d_{it} = 1 ] =
        & \Er[\omega_{it} | l_{it}, k_{it} , \omega_{it} \geq
        \omega^\ast( k_{it}) ] \\
        = & \lambda( k_{it} ) \neq 0
      \end{align*}
    \end{itemize}
  \item Selection bias negative, larger for capital than labor
  \end{itemize}
\end{frame}

\subsection{OP and selection}

\begin{frame}
  \frametitle{Selection in OP model}
  \begin{itemize}
  \item Estimate $\beta_\ell$ as above
  \item Write $d_{it} = 1\{ \xi_{it} \leq \omega^\ast(k_{it})
    - \rho (f_{i,t-1} - \beta_k k_{it-1}) =
    h(k_{it},f_{it-1},k_{it-1}) \}$
  \item Propensity score $P_{it} \equiv \Er[d_{it} | k_{it},
    f_{it-1},k_{it-1}]$
  \item Similar to before estimate $\beta_k$, from
    \begin{align*}
      y_{it} - \beta_\ell l_{it} = & \beta_k k_{it} +
      \tilde{g}\left(f_{it-1} - \beta_k k_{it-1},
        P_{it}\right) + \\ & + \xi_{it} + \epsilon_{it}
    \end{align*}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}

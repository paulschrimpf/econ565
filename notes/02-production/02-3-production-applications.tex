\input{../slideHeader}

\title{Estimating Production Functions}
\subtitle{Applications}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 567}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{amiti2007}}

\begin{frame}
  \frametitle{Overview}
  \begin{itemize}
  \item Effect of reducing input and output tariffs on productivity
  \item Reducing output tariffs affects productivity by increasing competition
  \item Reducing input tariffs affects productivity through learning,
    variety, and quality effects
  \item Previous empirical work focused on output tariffs; might be
    estimating combined effect
  \item Input tariffs hard to measure; with Indonesian data on
    plant-level inputs can construct plant specific input tariff
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Trade Liberalization in Indonesia}
  \includegraphics[width=\textwidth]{ak-tab1}
\end{frame}


\begin{frame}
  \frametitle{Data and tariff measure}
  \begin{itemize}
  \item Indonesian annual manufacturing census of $20+$ employee
    plants 1991-2001, after cleaning $~15,000$ firms per year
  \item Input tariffs:
    \begin{itemize}
    \item Data on tariffs on goods, $\tau_{jt}$, but also need to know inputs
    \item 1998 only: have data on inputs, use to construct input
      weights at industry level, $w_{jk}$
    \item Industry input tariff $= \sum_{j} w_{jk} \tau_{jt}$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Summary Statistics}
  \includegraphics[width=0.5\textwidth]{ak-tab3}
\end{frame}

\begin{frame}
  \frametitle{Productivity Estimates}
  \begin{equation*}
    \hspace*{-6em}
    \tikzmarknode{y}{y_{it}} = \beta_l^{\tikzmarknode{ind}{k}} l_{it}
    + \beta_k^k k_{it} + \beta_m^k \tikzmarknode{m}{m_{it}} +
    \underbrace{tfp_{it}^k}_{\omega_{it} + \epsilon_{it}}
  \end{equation*}
  % \vspace*{0.8\baselineskip}
  \begin{tikzpicture}[overlay,remember
    picture,>=stealth,nodes={align=left,inner ysep=1pt},<-]
    % For "N"
    \path (y.north) ++ (0,1.0em) node[anchor=south
    east] (ytext){\textsf{\footnotesize log revenue}};
    \draw(y.north) |-
    ([xshift=-0.3ex]ytext.south west);
    % For "ind"
    \path (ind.north) ++ (0,2.0em) node[anchor=north
    west]
    (indtext){\textsf{\footnotesize industry k}};
    \draw(ind.north) |-
    ([xshift=-0.3ex]indtext.south east);
    % For "m"
    \path (m.north) ++ (0,1.9em)
    node[anchor=north west]
    (mtext){\textsf{\footnotesize cost of materials}};
    \draw(m.north) |- ([xshift=-0.3ex]mtext.south east);
  \end{tikzpicture}

  \begin{itemize}
  \item Output measure is revenue $\Rightarrow$ may confound
    productivity and markups
  \item Materials measured as cost $\Rightarrow$ may confound quality
    and productivity
  \item Estimate TFP using Olley-Pakes
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Production Function Estimates}
  \includegraphics[width=\textwidth]{ak-tab2}
\end{frame}


\begin{frame}\frametitle{Productivity and Tariffs}
  \begin{itemize}
  \item Estimate relation between TFP and tariffs
    \begin{align}
      \log(TFP_{it}) = & \gamma_0 + \alpha_i + \alpha_{tl(i)} + \gamma_1
      (\text{output tariff})_{tk(i)} + \notag \\ & + \gamma_2 (\text{input
        tariff})_{tk(i)} + \epsilon_{it} \label{tfpeq}
    \end{align}
    \begin{itemize}
    \item $k(i)=$
      \href{http://unstats.un.org/unsd/cr/registry/regcst.asp?Cl=27&Lg=1}{5-digit
        (ISIC)} industry of plant $i$
    \item $l(i)=$ island of plant $i$
    \end{itemize}
  \item Explore robustness to:
    \begin{itemize}
    \item Different productivity measure
    \item Specification of \ref{tfpeq}
    \item Endogeneity of tariffs
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Productivity and Tariffs}
  \includegraphics[width=0.7\textwidth]{ak-tab4}
\end{frame}


\begin{frame}
  \frametitle{Results}
  \begin{itemize}
  \item Input tariffs have larger effect than output, $\hat{\gamma}_1
    \approx -0.07$, $\hat{\gamma}_2 \approx -0.44$
  \item Robust to:
    \begin{itemize}
    \item Productivity measure
    \item Tariff measure
    \item Including/excluding Asian financial crisis
    \end{itemize}
  \item Less robust to instrumenting for tariffs
    \begin{itemize}
    \item Qualitatively similar, but larger coefficient estimates
    \end{itemize}
  \item Explore channels for productivity change
    \begin{itemize}
    \item Markups (maybe), product switching/addition (no), foreign
      ownership (no), exporters (no)
    \end{itemize}
  \end{itemize}
\end{frame}

% \begin{frame} \frametitle{Criticism}
%   \begin{itemize}
%   \item Methodology:
%     \begin{itemize}
%     %\item Could use LP, ACF, or dynamic panel methods to estimate
%       TFP
%     \item Standard errors in productivity regression appear to ignore
%       uncertainty from estimating TFP
%     \item Measurement error in tariffs could be taken more seriously
%       (is this why IV estimates are larger?)
%     \end{itemize}
%   \item
%   \end{itemize}
% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{grieco2017}}

\begin{frame}\frametitle{\cite{grieco2017}}
  \url{https://www.ftc.gov/sites/default/files/documents/public_events/fifth-annual-microeconomics-conference/grieco-p_0.pdf}
\end{frame}

\begin{frame}\frametitle{Model details}
  \begin{itemize}
  \item Timing:
    \begin{enumerate}
    \item Quality chosen $q_{it} = q(k_{it},\ell_{it},
      x_{it},\omega_{i,t-b})$
    \item Production occurs, $\omega_{it}$ revealed to firm
    \item Hiring chosen $\ell_{i,t+1} - \ell_{it} = h_{it} =
      h(k_{it},\ell_{it},x_{it}, \omega_{it})$
    \end{enumerate}
  \item $\omega$ follows Markov process:
    \[ \Er[\omega_{i,t-b} | \mathcal{I}_{i,t-b} ] =
      \Er[\omega_{i,t-b}|\omega_{i,t-1}] \text{ \& } \Er[\omega_{it}
      | \mathcal{I}_{i,t} ] = \Er[\omega_{it} | \omega_{i,t-b} \]
    and $\omega_{it} = \Er[\omega_{it} | \omega_{i,t-1}] + \eta_{it} =
    g(\omega_{i,t-1}) + \eta_{it}$
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Moment conditions}
  \begin{itemize}
  \item Control function assumption: hiring is a monotonic function of $\omega$
    \[ h_{it} = h(k_{it},\ell_{it},x_{it}, \omega_{it}) \]
    so
    \[ \omega_{it} = h^{-1} (k_{it},\ell_{it},x_{it},h_{it} ) \]
  \item Substitute into production function:
    \begin{align*}
      y_{it} = & \alpha_q q_{it} + \beta_k k_{it} + \beta_\ell
                 \ell_{it} + h^{-1} (k_{it},\ell_{it},x_{it},h_{it} ) +
                 \epsilon_{it} \\
      y_{it} = & \alpha_q q_{it} + \Phi(k_{it},\ell_{it},x_{it},h_{it}
                 ) + \epsilon_{it}
    \end{align*}
  \item Evolution of $\omega$
    \begin{align*}
      \omega_{it} = & y_{it} - \alpha_q q_{it} - \beta_k k_{it} -
                      \beta_\ell \ell_{it} - \epsilon_{it} = g(\omega_{i,t-1}) +
                      \xi_{it} \\
      = & g(\Phi(k_{it-1},\ell_{it-1},x_{it-1},h_{it-1}) - \beta_\ell
          \ell_{it-1} - \beta_k k_{it-1}) + \xi_{it}
    \end{align*}
  \item Moment conditions:
    \begin{align*}
      \Er[\epsilon_{it} | q_{it}, k_{it}, \ell_{it}, x_{it}, h_{it} ]
      & = 0 \\
      \Er[\xi_{it} | k_{it}, \ell_{it}, x_{it},  k_{it-1},
      \ell_{it-1}, x_{it-1} ] & = 0
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation}
  \begin{enumerate}
  \item Estimate, $\alpha_q$, $\Phi$ from
    \[ y_{it} =  \alpha_q q_{it} + \Phi(k_{it},\ell_{it},x_{it},h_{it}
      ) + \epsilon_{it}
    \]
    by semiparametric regression
  \item Estimate $\beta_k$, $\beta_\ell$
    \begin{itemize}
    \item Let $\omega(\beta)_{it} =
      \hat{\Phi}(k_{it},\ell_{it},x_{it},h_{it}) - \beta_k k_{it} -
      \beta_\ell \ell_{it}$
    \item For each $\beta$ estimate $g()$
      \[ y_{it} - \hat{\alpha} q_{it} - \beta_k k_{it} - \beta_\ell
        \ell_{it} = g(\omega(\beta)_{it-1}) + \underbrace{\xi_{it} + \epsilon_{it}}_{\equiv \eta_{it}(\beta)} \]
      by nonparametric regression
    \item Minimize empirical moment condition for $\eta$
      \[ \hat{\beta} = \argmin (\frac{1}{NT} \sum_{it} k_{it} \eta_{it}(\beta))^2 +
        (\frac{1}{NT} \sum_{it} \ell_{it} \eta_{it}(\beta))^2 \]
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{}
  \begin{itemize}
  % \item Incentive shifters may be correlated with quality and
  %   productivity
  %   \begin{itemize}
  %   \item Time since inspection
  %     \begin{itemize}
  %     \item[+] Inspect recently $\Rightarrow$ higher quality incentive
  %     \item[-] Low productivity $\Rightarrow$ low quality
  %       $\Rightarrow$ more inspections
  %     \end{itemize}
  %   \item Referral rate
  %   \item Section 5.2: incentive shifters can be correlated with
  %     productivity, only need that shape of production possibilities
  %     frontier is invariant
  %   \end{itemize}
  \item Should hemoglobin level be controlled for when measuring
    quality?
    \begin{itemize}
    \item Anemia (low hemoglobin) is risk-factor for infection
    \item Anemia can be treated through diet, iron supplements (pills
      or IV), EPO, etc
      \begin{itemize}
      \item Are dialysis facilities responsible for this treatment?
      \item In 2006-2014 data average full-time dieticiens = 0.5,
        average part-time = 0.6
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]\frametitle{Measurement error}
  \begin{itemize}
  \item Simplified setup:
    \[ y = \alpha \tilde{q} + \epsilon \]
    $\tilde{q}$ unobserved, observe $q = \tilde{q} + \epsilon^q$ with
    $\Er[\epsilon^q | \tilde{q} = 0]$
  \item Then $\plim \hat{\alpha}^{OLS} = \alpha
    \frac{\var(\tilde{q})}{\var(\tilde{q}) + \var(\epsilon^q)}$
  \item If $d = d(\tilde{q}) + \epsilon^d$  with $\Er[\epsilon^d |
    \tilde{q}] = 0$ and $\Er[\epsilon^d \epsilon^q ] = 0$, then
    \[ \plim \hat{\alpha}^{IV} = \alpha \]
  \item Is $\Er[\epsilon^d \epsilon^q ] = 0$ a good assumption?
    \begin{itemize}
    \item Paper argues $\Er[\epsilon^d \epsilon^q ] =
      O(1/(\text{patients per facility}))$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{}
  \begin{itemize}
  \item Estimation details:
    \begin{itemize}
    \item[Step 1:]   Estimate $\alpha_q$
      \[ y_{jt} − \hat{E}[y|h_{jt} , i_{jt} , k_{jt} , \ell_{jt} ,
        x_{jt} ] = −\alpha_q (q jt - \hat{E}[q|h_{jt} , i_{jt} ,
        k_{jt} , \ell_{jt} , x_{jt} ]) + \epsilon_{jt} \]
      \begin{itemize}
      \item Drop observations with $h_{jt} = 0$ (not invertible)
      \item Okay here, because selecting on $\omega$, and residual,
        $\epsilon_{jt}$ is uncorrelated with $\omega$
      \item Problematic in last step? No, see footnote 49
      \end{itemize}
    \item[Step 2:]  Estimate $\beta_k$, $\beta_\ell$ from
      \[ y_{jt} + \hat{\alpha_q } + \beta_k k_{jt} + \beta_\ell
        \ell_{jt} = g(\hat{\omega}_{jt-1}(\beta)) + \eta_{jt} +
        \epsilon_{jt} \]
      \begin{itemize}
      \item Only have $\hat{\omega}_{jt-1}(\beta)$ when
        $h_{jt-1}\neq 0$, okay because $\epsilon_{jt}$ and $\eta_{jt}$
        are uncorrelated with $\omega_{jt-1}$, would be problem if
        using $\hat{\omega}_{jt}$
      \end{itemize}
    \item Nothing about selection --- number of centers, 4270, vs
      center-years, 18295, implies there must be entry and exit
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item Estimate implications:
    \begin{itemize}
    \item Holding inputs constant, reducing infections by one per
      year requires reducing output by 1.5 patients
      \begin{itemize}
      \item Cost of treatment $\approx \$50,000$, so one infection
        $\approx \$75,000$
      \end{itemize}
    \item Holding output constant, reducing infections by one per year
      requires hiring 1.8 more staff
      \begin{itemize}
      \item Cost of staff $\approx \$42,000$, so one infection
        $\approx \$75,000$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{}
  \begin{itemize}
  \item Would like to see some results related to productivity
    dispersion e.g.\
    \begin{itemize}
    \item Decompose variation in infection rate into: productivity
      variation, incentive variation, quality-quantity choices, and
      random shocks
    \item Compare strengthening incentives vs closing least productive
      facilities as policies to increase quality
    \end{itemize}
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{doraszelski2009}}
\begin{frame}
  \frametitle{Overview}
  \begin{itemize}
  \item Estimable model of endogenous productivity, which combines:
    \begin{itemize}
    \item Knowledge capital model of R\&D
    \item OP \& LP productivity estimation
    \end{itemize}
  \item Application to Spanish manufacturers focusing on R\&D
    \begin{itemize}
    \item Large uncertainty (20\%-60\% or productivity
      unpredictable )
    \item Complementarities and increasing returns
    \item Return to R\&D larger than return to physical capital
      investment
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model (simplified)}
  \begin{itemize}
  \item Cobb-Douglas production:
    \[
    y_{it} = \beta_l l_{it} + \beta_k k_{it} +  \omega_{it} + \epsilon_{it}
    \]
  \item Controlled Markov process for productivity, $p(\omega_{it+1} |
    \omega_{it}, r_{it})$,
    \[ \omega_{it} = g(\omega_{it-1}, r_{it-1}) + \xi_{it} \]
  \item Labor flexible and non-dynamic
  \item Value function
    \begin{align*}
      V(k_t, \omega_t, u_t) = \max_{i,r} & \Pi(k_t,\omega_t) - C_i(i,u_t) -
      C_r(r,u_t) + \\ & +  \frac{1}{1+\rho} \Er\left[ V(k_{t+1}, \omega_{t+1},u_{t+1}) |
        k_t, \omega_t, i, r,  u_t \right]
    \end{align*}
    \begin{itemize}
    \item $u$ scalar or vector valued shock
    \item $u$ not explicitly part of model, but identification
      discussion (especially p10 and footnote 6) implicitly adds it
    \item $u$ independent of? $k$, $l$? across time?
    \end{itemize}
  \item Control function incorporating Cobb-Douglas assumption (and
    perfect competition):
    \[ \omega_{it} = h(l_{it}, k_{it}, w_{it} - p_{it}; \beta) =
    \lambda_0 + (1-\beta_l) l_{it} - \beta_k k_{it} + (w_{it} -
    p_{it}) \]
  \item Form moments based on
    \[
    y_{it} = \beta_l l_{it} + \beta_k k_{it} + g\left(h(l_{it-1}, k_{it-1},
      w_{it-1} - p_{it-1}; \beta), r_{it-1} \right) + \xi_{it} + \epsilon_{it}
    \epsilon_{it}
    \]
  \item No collinearity because:
    \begin{itemize}
    \item Parametric $h$
    \item Variation in $k$, $r$ due to $u$
    \end{itemize}
  \item Estimated model adds
    \begin{itemize}
    \item Material input instead of labor for control function
    \item $h$ based on imperfect competition
    \end{itemize}
  \item Comparison to OP, LP, ACF
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item Look at tables and figures
  \item Large uncertainty (20\%-60\% or productivity
    unpredictable )
  \item Complementarities and increasing returns
  \item Return to R\&D larger than return to physical capital
  \end{itemize}
\end{frame}

% \begin{frame} \frametitle{Criticism}
%   \begin{itemize}
%   \item
%     \begin{itemize}
%     \item Methodological: describe estimator, discuss relation to OP,
%       LP, ACF,
%     \item Empirical: R\& D and uncertainty
%     \end{itemize}
%   \item Figures, elasticity estimates, and other results that are
%     function of parameter estimates do not have standard errors (very
%     common but bad)
%   \item
%   \end{itemize}
% \end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}
\graphicspath{{snapshots/}}

\title{Market entry}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}

\part{Overview of market entry}

\frame{\partpage}

\begin{frame}
  \tableofcontents
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews:
    \begin{itemize}
    \item \cite{Aguirregabiria2012} chapter 5
    \item \cite{sutton1991} theory
    \item \cite{aradillas2020}, \cite{kline2021} econometrics
    \item \cite{levin2009}
    \end{itemize}
  \item Key papers:
    \begin{itemize}
    \item \cite{br1991}
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{frame}[allowframebreaks]\frametitle{Introduction}
  \begin{itemize}
  \item Models of entry:
    \begin{itemize}
    \item Dependent variable $=$ firm decision to operate or not in a
      market
      \begin{itemize}
      \item Enter industry, open new store, introduce new product,
        release a new movie, bid in an auction
      \end{itemize}
    \item Sunk cost from being active in market
    \item Payoff of being active depends on how many other firms are
      in the market (game)
    \end{itemize}
    \[ a_{im} = 1\left\lbrace
      \Pi_{im}(N_m,X_{im},\epsilon_{im}) \geq 0 \right\rbrace \]
  \item Estimate $\Pi$ using revealed preference
  \item Static models: entry $\approx$ being in active in market; not
    transition in/out
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Why estimate models of entry?}
  \begin{itemize}
  \item Why not just estimate payoff function using demand and production
    estimation techniques?
    \begin{itemize}
    \item Answers new questions: \alert{source of market power}
    \item \alert{Efficiency}: entry conditions provide additional
      information about payoffs, so using them can give us more precise
      estimates
    \item \alert{Identification}: some parameters (e.g.\ fixed costs)
      can only be identified from entry
    \item \alert{Requires less data}: price and quantity data not needed
      for some entry models
    \item \alert{Controlling for selection}
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{\cite{starc2014}}

\begin{frame}[allowframebreaks]
  \frametitle{\cite{starc2014}}
  \begin{itemize}
  \item What are the \alert{sources} and consequences of insurer
    market power?
  \item \cite{sutton1991}:
    \begin{itemize}
    \item Model with price competition \& fixed costs implies number
      of firms $\to \infty$ as market size $\to \infty$
    \item Model with price competition \& \alert{endogenous} fixed
      costs implies number of firms $\to$ constant as market size $\to
      \infty$
    \item Illustrative simplified model from \cite{schmalansee1992}
      \begin{itemize}
      \item Exogenous, $p, c$, endogenous $A_i$ (advertising)
        \[ \pi_i = (p-c) S \frac{A_i^e}{\sum_{j=1}^N A_j^e} -
          A_i - \sigma \]
      \item Symmetric Nash equilibrium:
        \[ 0 = (1/N^*)(1-e) + (1/N^*)^2e - (\sigma/S)(1/(P-c))  \]
        if $e \in (1,2]$, then $N^* \to e/(e-1)$ as $S \to \infty$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Entry model:
    \begin{itemize}
    \item Mutual of Omaha: fixed cost of entry (including advertising)
      in market $m$ is $\Theta_{Mm}$
    \item Assume:
      \begin{enumerate}
      \item Mutual of Omaha is profitable $\Pi_{Mm}(1,1) - \Theta_{Mm}
        \geq 0$
      \item It is not profitable for another firm to mimic Mutual of
        Omaha and enter
        $\Pi_{Mm}(1,2) - \Theta_{Mm} \leq 0$
      \end{enumerate}
      implies $ \Er[\Pi_{Mm}(2,1)] \leq \Er[\theta_{Mm}] \leq
      \Er[\Pi_{Mm}(1,1)]$
    \item Similar for United Health, but they pay a single national
      suck cost $\Phi_U$ each year and
      \[ \Er[\sum_{m} \Pi_{Um}(2,1)] \leq \Er[\Phi_U] \leq
        \Er[\sum_{m} \Pi_{Um}(1,1)] \]
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{Source of market power}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {../03-diffProd/starc-tabA7}

    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {../03-diffProd/starc-tabA8}
\end{frame}

\section{\cite{br1991}}

\begin{frame} \frametitle{\cite{br1991}}
  \begin{itemize}
  \item Can learn a lot from market entry with very limited data
  \item Cross-section of isolated markets where we observe
    \begin{itemize}
    \item Number of firms
    \item Some market characteristics (prices and quantities not
      needed)
    \end{itemize}
  \item Identify:
    \begin{itemize}
    \item Fixed costs
    \item Degree of competition: payoffs $=f(\text{number of firms})$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Motivating theory}
  \includegraphics[width=\textwidth]{br-fig1}
\end{frame}

\begin{frame}\frametitle{Motivating theory}
  \begin{itemize}
  \item Demand $=d(P)\underbrace{S}_{\text{market size}}$
  \item Monopolist entry:
    \begin{align*}
    0 = & (P_1 - AVC(q_1))d(P_1)S_1 - F \\
    S_1 = & \frac{F}{(P_1 - AVC(q_1))d(P_1)}
    \end{align*}
  \item Symmetric market with $n$ firms, demand per firm = $d(P)S/n$,
    entry threshold for $n$th firm
    \begin{align*}
      S_n = & \frac{F}{(P_n - AVC(q_n))d(P_n)}
    \end{align*}
  \item $P_n$, $q_n$, depend on ``competitive conduct'' (form of
    competition, residual demand for firm who deviates from
    equilibrium $P_n$)
  \item As $n\to \infty$, $S_n/n \to s_\infty =$ minimal market size
    per firm to support entry when $P, q$ competitive
  \item $S_{n+1}/S_n$ measures how competitive conduct changes with
    with number of firms
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Setting}
  \begin{itemize}
  \item Questions:
    \begin{itemize}
    \item Degree of competition: how fast profits decline with $n_m$
    \item How many entrants needed to achieve competitive equilibrium
      (contestable markets)
    \end{itemize}
  \item Data:
    \begin{itemize}
    \item Retail and professional industries (doctors, dentists,
      pharmacies, car dealers, etc.), treat each industry separately
    \item $M$ markets
    \item $n_m$ firms per market
    \item $S_m$ market size
    \item $x_m$ market characteristics
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{Model}
  \begin{itemize}
  \item $N$ potential entrants
  \item Profit of each firm when $n$ active $= \Pi_m(n)$
    \begin{itemize}
    \item $\Pi_m$ decreasing in $n$
    \end{itemize}
  \item Equilibrium:
    \[ \Pi_m(n_m) \geq 0 \;\text{ and }\; P_m(n_m+1) < 0 \]
  \item Profit function:
    \begin{align*}
      \Pi_m(n) = & \underbrace{V_m(n)}_{\text{variable}} -
      \underbrace{F_m(n)}_{\text{fixed}}  \\
      = & S_m v_m(n) - F_m(n) \\
      = & S_m \left(x_m^D \beta - \alpha(n) \right) -
      \left(x_m^c\gamma + \delta(n) + \epsilon_m \right)
    \end{align*}
    where
    \begin{itemize}
    \item $\alpha(1) \leq \alpha(2) \leq \cdots \leq \alpha(N)$
    \item $\delta(1) \leq \delta(2) \leq \cdots \leq \delta(N)$
      \begin{itemize}
      \item Entry deterrence, firm heterogeneity, real estate prices
      \end{itemize}
    \item Key difference between variable and fixed profits is that
      variable depend on $S_m$, fixed do not
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Parameters $\theta = (\beta,\gamma,\alpha,\delta)$
  \item MLE
    \[ \hat{\theta} = \argmax_\theta \sum_{m=1}^M \log \Pr(n_m | x_m,
    S_m; \theta) \]
  \item Assume $\epsilon_m \sim N(0,1)$, independent of $x_m,S_m$
    {\footnotesize{
    \begin{align*}
      \Pr(n | x_m, S_m; \theta)
      = & \Pr\left(\Pi_m(n) \geq 0 >
        \Pi_m(n+1) \right) \\
      = & \Pr\begin{pmatrix} S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n) - \delta(n) \geq \epsilon \\ \epsilon > S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n+1) - \delta(n+1)
      \end{pmatrix}    \\
      =
      & \includegraphics[width=0.3\textwidth,height=0.12\textwidth]{br-illustration} \\
      = & \Phi\left(S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n) - \delta(n) \right) -
      \\ & - \Phi\left(S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n+1) - \delta(n+1)\right)
    \end{align*}
  }}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item 202 isolated local markets
    \begin{itemize}
    \item Population 500-75,000
    \item $\geq$ 20 miles from nearest town of 1,000$+$
    \item $\geq$ 100 miles from city of 100,000$+$
    \end{itemize}
  \item 16 industries: retail and professions, each estimated
    separately
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=0.7\textwidth]{br-tab3}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{br-fig2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{br-fig3}
\end{frame}


\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item For most industries, $\alpha(n)$ and $\delta(n)$ increase with
    $n$
  \item Define $S(n)=$ minimal $S$ such that $n$ firms enter
    \[ S(n) = \frac{x_m^C\gamma + \delta(n)}{x_m^D\beta - \alpha(n)} \]
    \begin{itemize}
    \item Varies across industries
    \end{itemize}
  \item $\frac{S(n)}{n} \approx$ constant for $n \geq 5$
    \begin{itemize}
    \item Contestable markets \citep{baumol1982} : an industry can be
      competitive even with few firms if there is easy entry
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{br-thresholdTable}
\end{frame}

\begin{frame}
  \includegraphics[height=\textheight]{br-thresholdFigure}
\end{frame}

\begin{frame}\frametitle{Further evidence - prices}
  \includegraphics[width=\textwidth]{br-tab10}
\end{frame}

\begin{frame}\frametitle{Further evidence - prices}
  \includegraphics[width=0.7\textwidth]{br-tab11}
\end{frame}

\section{\cite{magnolfi2024}}

\begin{frame}\frametitle{``The Rise of Urgent Care Centers: Implications for
  Competition and Access to Health Care''}
\cite{magnolfi2024}
\end{frame}

\begin{frame}\frametitle{Growth in Urgent Care Centers}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-fig1}
\end{frame}

\begin{frame}\frametitle{Introduction}
  \begin{itemize}
  \item Questions:
    \begin{enumerate}
    \item to what degree UCCs compete with each other and with hospitals and their affiliated UCCs
    \item whether UCCs’ location decisions expand access in underserved markets
    \end{enumerate}
  \item Entry model of hospitals and UCCs
    \begin{itemize}
    \item in spirit of \cite{br1991}
    \item Variation in Certificate-of-Need laws to identify
      effect of hospitals
    \item Compare entry thresholds in typical to underserved markets
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Background and Data}

\begin{frame}[shrink]\frametitle{What are Urgent Care Centers?}
  \begin{itemize}
  \item Urgent Care Centers:
    \begin{itemize}
    \item Walk-in
    \item Extended hours
    \item imaging, testing, diagnostics, screening
    \item physicians, nurses, radiology technicians
    \item Entry requirements: physician licensing, malpractice insurance
    \end{itemize}
  \item Hospitals:
    \begin{itemize}
    \item Emergency: $2/3$ of visits for conditions also treated by
      UCCs
    \item Certificate-of-Need required for entry (regulations vary by state)
    \end{itemize}
  \item Retail clinics:
    \begin{itemize}
    \item Respiratory infections, vaccinations
    \item Within retail store (CVS, Walmart) with normal business
      hours
    \item Nurse practitioners
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item \href{https://wisconsinbdrc.org/ye-time-series/}{YE Time Series}
  establishments for US since 1997
\item
  \href{https://www.cms.gov/medicare/quality/initiatives/hospital-quality-initiative/hospital-compare}{Hospital
  Compare database from CMS}
\item Demographics from ACS
\end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-fig2}
\end{frame}

\subsection{Entry Model}

\begin{frame}\frametitle{UCC Entry}
  \begin{itemize}
  \item UCC profits:
  \begin{align*}
    \pi_{\tikzmarknode{t}{t}}(\tikzmarknode{nt}{n_t},
    \tikzmarknode{nth}{n_t^h})
    = & \overbrace{\tikzmarknode{S}{S_t} v(n_t, n_t^h, \tikzmarknode{x}{x_t},
        )}^{\text{variable profits}}
        - \overbrace{F(n_t, \tikzmarknode{w}{w_t})}^{\text{fixed costs}}
  \end{align*}
  \begin{tikzpicture}[overlay,remember
    picture,>=stealth,nodes={align=left,inner ysep=1pt},<-]
    \path (t.south) ++ (0,-0.2em) node[anchor=north east,color=gray] (scalep){market};
    \draw (t.south) |- (scalep.north);
    \path (nt.north) ++ (0,1em) node[anchor=south east,color=gray]
    (scalep){number of UCCs};
    \draw (nt.north) |- (scalep.south);
    \path (nth.north) ++ (0,1.5em) node[anchor=south west,color=gray]
    (scalep){number of hospitals};
    \draw (nth.north) |- (scalep.south);
    \path (x.south) ++ (0,-1.2em) node[anchor=north east,color=gray]
    (scalep){demand and cost shifters};
    \draw (x.south) |- (scalep.north);
    \path (S.south) ++ (0,-2.5em) node[anchor=north west,color=gray]
    (scalep){market size};
    \draw (S.south) |- (scalep.north);
    \path (w.south) ++ (0,-1em) node[anchor=north west,color=gray]
    (scalep){cost shifters};
    \draw (w.south) |- (scalep.north);
  \end{tikzpicture}
\end{itemize}
\end{frame}


\begin{frame}\frametitle{Hospital Entry}
  \begin{itemize}
  \item Hospital profits:
  \begin{align*}
    \pi_{t}^h(\tikzmarknode{nth}{n_t^h})
    = & \overbrace{V^h(n_t^h, \tikzmarknode{x}{x_t},
        \tikzmarknode{S}{S_t})}^{\text{variable profits}}
        - \overbrace{F^h(n_t^h, \tikzmarknode{w}{w_t}, \tikzmarknode{z}{z_t})}^{\text{fixed costs}}
  \end{align*}
  \begin{tikzpicture}[overlay,remember
    picture,>=stealth,nodes={align=left,inner ysep=1pt},<-]
    \path (nth.north) ++ (0,1.0em) node[anchor=south east,color=gray]
    (scalep){number of hospitals};
    \draw (nth.north) |- (scalep.south);
    \path (x.south) ++ (0,-1.2em) node[anchor=north east,color=gray]
    (scalep){demand and cost shifters};
    \draw (x.south) |- (scalep.north);
    \path (S.south) ++ (0,-2em) node[anchor=north west,color=gray]
    (scalep){market size};
    \draw (S.south) |- (scalep.north);
    \path (w.south) ++ (0,-1em) node[anchor=north west,color=gray]
    (scalep){UCC cost shifters};
    \draw (w.south) |- (scalep.north);
    \path (z.north) ++ (0,2em) node[anchor=south east,color=gray]
    (scalep){hospital cost shifters};
    \draw (z.north) |- (scalep.south);
  \end{tikzpicture}
\item Hospital entry does not respond to UCCs
\item Cost shifter for hospitals excluded from UCC cost
\end{itemize}
\end{frame}

\begin{frame}\frametitle{Equilibrium and Entry Thresholds}
  \begin{itemize}
  \item Number of UCCs
    $$
    \pi(n_t, n_t^h) \geq 0 \geq \pi(n_t+1, n_t^h)
    $$
  \item $n_t$ firms requires size
    $$
    S_t \geq \frac{F(n_t, w_t)}{v(n_t, n_t^h,x_t)}
    $$
  \item Minimal size per firm for $n$ firms in market with average characteristics:
    $$
    \tau_n = \frac{1}{n} \frac{ F(n,\bar{w}_n)}{v(n,\bar{n}^h, \bar{x}_n)}
    $$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Estimation}
  \begin{itemize}
  \item Profit Function parameterization
    \begin{align*}
    \pi_t(n_t, n_t^h) & = S_t \left(x_t \theta_x + n_t^h \delta +
                        \theta_1 - \sum_{i=2}^{n_t} \theta_i \right)
                        -w_t \gamma_w - \gamma_1 + \\
      & - \sum_{i=2}^{n_t} \gamma_i  + \epsilon_t \\
    \pi_t^h(n_t^h) = & S_t \left(x_t \theta_x^h + \theta_1^h \right) -
                       w_t \gamma_w^h - z_t \gamma_z^h - \gamma_1^h +
                       \epsilon_t^h \\
    \begin{pmatrix} \epsilon_t \\ \epsilon_t^h \end{pmatrix} \sim &
    N\left( 0, \begin{pmatrix} 1 & \rho \\ \rho & 1 \end{pmatrix} \right)
    \end{align*}
  \item $z_t = CON_t$ important for identification, especially $\rho$ vs $\delta$
    (exclusion in nonlinear simultaneous equations)
  \item Estimate by maximum likelihood
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Sample restricted to isolated markets}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-tab2a}
\end{frame}


\subsection{Results}
\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-tab2}
\end{frame}

\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item Column (1) takes hospital entry as fixed, column (5) models
    hospital entry
  \item Column (3) as first stage
  \item Column (7) percent change in number UCCs from 1 standard
    deviation change in variables
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Entry Thresholds}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-tab3}
\end{frame}

\begin{frame}\frametitle{Entry Thresholds}
  \begin{itemize}
  \item Ratios decreasing, but $> 1$, implies more entry increases
    competition, but even with 3 still have market power
  \item Hospitals make UCCs more competitive (next table)
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-tab4}
\end{frame}

\begin{frame}\frametitle{Access to Care}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {mmss-tab5}
\end{frame}

\begin{frame}\frametitle{Access to Care}
  \begin{itemize}
  \item Entry thresholds about the same in subsamples
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Robustness}
  \begin{itemize}
  \item Market definition
  \item Model hospital affiliated UCCs separately
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Conclusions}
  \begin{itemize}
  \item Growth of UCCs has expanded access to care
  \item Evidence that UCCs have market power
  \item Future work: quality, cost savings, welfare
  \end{itemize}
\end{frame}


\section{\cite{eliason2021}}

\begin{frame}
\frametitle{``Price Regulation and Market Structure: Evidence from the
  Dialysis Industry''}

\begin{itemize}
\item \cite{eliason2021}, revised version of \cite{eliason2017}
\end{itemize}

\end{frame}

\begin{frame}
  \begin{itemize}
  \item 80\% of dialysis patients in Medicare
  \item Medicare price regulation affects:
    \begin{itemize}
    \item Short run: quality competition
    \item Longer run: entry \& investment (market structure)
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Motivating Evidence}

\begin{frame}\frametitle{Dialysis Growth}
  \includegraphics[width=\textwidth]{esrd-fig1.png}
\end{frame}

\begin{frame}\frametitle{Spatial Dispersion}
  \includegraphics[width=\textwidth]{esrd-fig2.png}
\end{frame}

\begin{frame}\frametitle{Measuring Quality}
  \begin{align*}
    y_{ijt} = & X_{it}\beta + \mu_{jt} + \epsilon_{ijt} \\
    Q_{jt} = & \bar{X} \hat{\beta} + \hat{\mu}_{jt}
  \end{align*}
\end{frame}

\begin{frame}\frametitle{Quality Variation}
  \includegraphics[height=\textheight]{esrd-tab1.png}
\end{frame}

\begin{frame}\frametitle{Competition Increases Quality}
  \includegraphics[height=\textheight]{esrd-tab2.png}
\end{frame}

\subsection{Model}

\begin{frame}\frametitle{Model}
  \includegraphics[width=\textwidth]{esrd-fig3.png}
\end{frame}

\begin{frame}\frametitle{3. Demand}
  \begin{itemize}
  \item Patient $i$ chooses facility $j \in \tilde{\mathcal{J}}_i$
    from firm $f$
    \begin{align*}
      j = \argmax_{j \in \tilde{\mathcal{J}}_i} u_{ijf}+ \epsilon_{ijf}
    \end{align*}
    where
    \begin{align*}
      u_{ijf} = \begin{cases}
        g(\tikzmarknode{d}{d_{ijf}}, \mathcal{I}_i)
        + \Gamma(\tikzmarknode{I}{\mathcal{I}_i}) \tikzmarknode{H}{\mathcal{H}_{jf}} +
        \xi_{jf} & \text{ if } j \neq 0 \\
        \lambda(\mathcal{I}_i) & \text{otherwise}
      \end{cases}
    \end{align*}
    \begin{tikzpicture}[overlay,remember
      picture,>=stealth,nodes={align=left,inner ysep=1pt},<-]
      \path (d.north) ++ (0,0.5em) node[anchor=south east,color=gray]
      (scalep){distance};
      \draw (d.north) |- (scalep.south);
      \path (I.south) ++ (0,-2.2em) node[anchor=north west,color=gray]
      (scalep){individual characteristics};
      \draw (I.south) |- (scalep.south);
      \path (H.north) ++ (0,0.5em) node[anchor=south west,color=gray]
      (scalep){facility characteristics};
      \draw (H.north) |- (scalep.south);
    \end{tikzpicture}
  \item Market shares
    $$
    S_{jf} = \sum_i \underbrace{\frac{e^{u_{ijf}}}{\sum_{j' \in
          \mathcal{J}_i} e^{uij',f'}}}_{\equiv s_{ijf}(\mathcal{H},\mathcal{M},\xi;\beta)}
    $$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{2. Quality Competition}

  \begin{itemize}
  \item Firm chooses quality of its facilities
    $$
    \max_{\mathcal{Q}_{jf} \in [0, \bar{\mathcal{Q}}]} \sum_{r \in
        \mathcal{J}_f} \sum_i \left (P_i - MC_{rf}(\mathcal{H}_{rf},
        \nu_{rf};\alpha) \right) s_{irf}(\mathcal{H}, \mathcal{M},
      \xi;\beta)
    $$
  \item Expected profits $= \tilde{\pi}_{jf}(\mathcal{Q}^*_{jf})$
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{3. Entry \& Capacity}
  \begin{itemize}
  \item Firm has set of potential facilities $\mathcal{J}_f$
  \item Chooses capacity $\mathcal{K}_{rf} \geq 0$ for each potential facility
  \item Non-entry $\equiv$ $\mathcal{K}_{rf} = 0$
  \item Capacity affects marginal costs and demand (included in
    $\mathcal{H}_{jf}$)
  \end{itemize}
  \begin{align*}
    \max_{\{\mathcal{K}_{jf}\}_{j \in \mathcal{J}_f}} \Er\left[
    \sum_{r \in \mathcal{J}_f} \tilde{\pi}_{rf}(\mathcal{Q}^*_{rf}) -
    fc(\kappa_{rf};\gamma) + \eta_r^{\kappa_{rf}} \right]
  \end{align*}
\end{frame}


\subsection{Estimation}

\begin{frame}\frametitle{Demand Estimation}
  \begin{itemize}
  \item Estimate demand (micro-BLP)
    \begin{enumerate}
    \item Individual level multinomial logit to estimate
      $$
      u_{ijf} = \mathcal{I}_i \beta \mathcal{H}_{jf} + \delta_{jf}
      $$
    \item 2SLS with predicted patients based on geography as
      instruments for quality and congestion
      $$
      \hat{\delta}_{jt} = \mathcal{H}_{jf} \alpha + \xi_{jf}
      $$
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Marginal costs}
  \begin{itemize}
  \item Specify linear marginal cost function, use IV on firm first
    order conditions
  \item Predicted patients based on geography as instruments for
    quality and congestion
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Fixed Costs}
  \begin{itemize}
  \item Model gives probability of entry and capacity choice
  \item Estimate fixed costs parameters by Pseudo-MLE
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}
  \includegraphics[height=\textheight]{esrd-tab4.png}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{esrd-tab5.png}
\end{frame}

\begin{frame}
  \includegraphics[height=\textheight]{esrd-tab6.png}
\end{frame}

\begin{frame}
  \includegraphics[height=\textheight]{esrd-tab7.png}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{esrd-tab8.png}
\end{frame}

\begin{frame}
  \includegraphics[height=\textheight]{esrd-tab9.png}
\end{frame}


\section{Other applications}
\begin{frame}\frametitle{Other applications}
  \begin{itemize}
  \item Supermarkets:
    \begin{itemize}
    \item \cite{bronnenberg2009}
    \item \cite{jia2008happens}
    \item \cite{ellickson2007}
    \end{itemize}
  \item Airlines:
    \begin{itemize}
    \item \cite{berry1992}
    \item \cite{ciliberto2009}
    \end{itemize}
  \item Radio: \cite{sweeting2009}
  \item Urgent care: \cite{magnolfi2024}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}

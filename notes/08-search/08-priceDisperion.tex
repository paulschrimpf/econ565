\input{../slideHeader}
\newcommand{\Pb}{{\boldsymbol{\mathrm{P}}}}
\newcommand{\Lb}{{\boldsymbol{\Lambda}}}

\providecommand{\Mb}{{\boldsymbol{\mathrm{M}}}}
\providecommand{\VCb}{{\boldsymbol{\mathrm{VC}}}}
\providecommand{\pb}{{\boldsymbol{\mathrm{p}}}}
\providecommand{\pib}{{\boldsymbol{\pi}}}

\graphicspath{{figures/}}
%\usepackage{multimedia}

\title{Price dispersion and search}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\newcommand{\inputslide}[2]{{
    \usebackgroundtemplate{
      \includegraphics[page={#2},width=\paperwidth,keepaspectratio=true]
      {{#1}}}
    \frame[plain]{}
  }}


\begin{document}

\frame{\titlepage}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Theory and overview of reduced form empirical work:
    \cite{baye2006} 
  \item Structural empirical papers:
    \begin{itemize}
    \item \cite{hong2006}, \cite{gw2008}, \cite{santos2012},
      \cite{wildenbeest2011} 
    \end{itemize}
  \end{itemize} 
\end{frame}

\begin{frame}
  \tableofcontents  
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame}
  \begin{itemize}
  \item Homogenous product and competitive market: 
    \begin{itemize}
    \item Theory $\Rightarrow$ one price
    \item Reality $\Rightarrow$ price dispersion
    \end{itemize}
  \item Explanations:
    \begin{itemize}
    \item Unobserved product heterogeneity
      \begin{itemize}
      \item Likely part of explanation, but it is largely tautological  
      \end{itemize}
    \item {\bfseries{Imperfect information and search costs}} 
      \begin{itemize}
      \item \cite{stigler1961}
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{bms-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{bms-tab1b}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Models of price dispersion}

\begin{frame}[shrink] \frametitle{Models of price dispersion}
  \begin{itemize}
  \item Key point: relationship between price dispersion and
    primitives (search cost, market size, number of firms, demand
    elasticity) depends on modeling assumptions
  \item Types:
    \begin{enumerate}
    \item Search
      \begin{enumerate}
      \item Fixed : gather $n$ prices, choose lowest price
      \item Sequential : sequentially gather prices, stop when price
        low enough 
      \item Information clearinghouse : some consumers loyal to one
        firm, others buy from lowest price
      \end{enumerate}
    \item Bounded rationality: small departure from Nash equilibrium
      in firms' pricing game can give large price dispersion
      \begin{itemize}
      \item Quantal response equilibrium, 
        $\epsilon$-equilibrium, mistaken beliefs about price
        distribution 
      \end{itemize}
    \end{enumerate}
  \end{itemize}
\end{frame}

\subsection{Fixed search}
\begin{frame}\frametitle{Fixed search}
  \begin{itemize}
  \item \cite{stigler1961} 
  \item Assumptions:
    \begin{enumerate}
    \item Distribution of prices on $[\underline{p},\overline{p}]$,
      non-degenerate CDF
      $F(p)$, known by consumers    
    \item Each consumer wants to buy $K$ units
    \item Search process: optimally choose fixed number of price
      quotes, $n$; buy from firm with lowest price
    \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Fixed search - model implications}
  \begin{itemize}
  \item Number of price quotes:
    \[ \left(\Er[ p^{(1:n^*-1)} ] - \Er[ p^{(1:n^*)} ] \right)K \geq c
    \geq \left(\Er[ p^{(1:n^*)} ] - \Er[ p^{(1:n^*+1)} ] \right)K \]
    $n^*$ increasing in $K$
  \item Firm expected demand:
    \[ Q(p) = \mu n^* K (1 - F(p))^{n^* - 1} \]
  \item Transaction costs decrease with price dispersion
    \begin{itemize}
    \item If $G$ is a mean preserving spread of $F$, then
      $\Er_G[p^{(1:n)}] < \Er_F[p^{(1:n)}]$ for $n>1$
    \end{itemize}
  \item Expected total costs are lower with greater price dispersion
    \begin{itemize}
    \item If $G$ is a mean preserving spread of $F$, then
      $\Er_G[p^{(1:n^*_G)}] K - c n^*_G < \Er_F[p^{(1:n^*_F)}] K - c
      n^*_F$ for $n>1$ 
    \end{itemize}    
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Fixed search - critique}
  \begin{itemize}
  \item \cite{rothschild1973} critique: 
    \begin{enumerate}
    \item distribution of prices is not endogenous
    \item\label{fixed} fixed search may not be optimal for consumers
    \end{enumerate}
  \item For (\ref{fixed}) need to be more specific about search
    environment
    \begin{itemize}
    \item Fixed search optimal if e.g.\ waiting time to obtain each
      price quote 
    \end{itemize}
  \item \cite{diamond1971} in sequential or fixed search model with
    homogenous firms and consumers, there is an equilibrium where all
    firms charge the monopoly price
  \item Can obtain non-degenerate equilibrium distribution of prices
    by introducing firm heterogeneity or consumer search cost
    heterogeneity   
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Fixed search - endogenous price dispersion}
  \begin{itemize}
  \item \cite{burdett1983} : equilibrium price dispersion with ex-ante
    identical consumers and firms
  \item Assumptions:
    \begin{enumerate}
    \item Consumers: unit demand with reservation price $v$
    \item Fixed sample search
    \item Firms: constant marginal cost $m$, optimal monopoly price
      $p^*$
    \item Consumer utility given price $p^*$ and $n=1$ is positive
    \end{enumerate} 
  \item Equilibrium: price distribution, $F(p)$, and search distribution,
    $\Pr(n=i)$ for $i=1,2, ...$
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Fixed search - endogenous price dispersion}
  \begin{itemize}
  \item Implications:
    \begin{itemize}
    \item If $F(p)$ non-degenerate, then $\Pr(n=1), \Pr(n=2) > 0$ and
      $\Pr(n>2) = 0$, let $\theta = \Pr(n=1)$, $1-\theta = \Pr(n=2)$
    \item Firm profits: 
      \begin{align*} 
        \pi(p) = & \begin{cases} (v-m) \theta & \text{ if } p = v \\
          (p - m) \Pr(\text{consumer purchases})  & \text{ if } p <
          v 
        \end{cases}
        \\
        = & \begin{cases} 
          (v-m) \theta & \text{ if } p = v \\
          (p-m) \left[ \theta + (1-\theta)(1-F(p)) \right] & \text{ if } p <
          v 
        \end{cases}
      \end{align*}
    \item Firms indifferent among prices implies:
      \[ F(p) = 1 - \frac{v-p}{p-m}\frac{\theta}{1-\theta} \]
    \item Consumers indifferent between $n=1$ and $n=2$ pins down
      $\theta$ (generally two equilibria with $\theta \in (0,1)$
      (there's also an equilibrium where firms charge monopoly price
      and $n=1$ for all consumers))  
    \end{itemize}
  \end{itemize} 
\end{frame}

\subsection{Sequential search}
\begin{frame}[allowframebreaks]
  \frametitle{Sequential search}
  \begin{itemize}
  \item Sequential search: consumer pays cost $c$ to obtain price $p
    \sim F$; can either buy at price $p$ (or any previous price) or
    search again
  \item Optimal strategy $=$ reservation price $p^* =
    \min\{\overline{p}, z^* \}$ where
    \[ c = \int_{\underline{p}}^{z^*} (z^* - p) f(p) dp =
    \int_{\underline{p}}^{z^*} F(p) dp \]
  \item With homogeneous firms and consumers unique equilibrium is for
    firms to charge the monopoly price
  \item Equilibrium price dispersion with:
    \begin{itemize}
    \item Heterogeneous firm marginal cost and elastic demand
      (i.e.\ not unit demand); or
    \item Heterogeneous search costs (and assumptions about
      distribution of search costs) 
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Information clearinghouse}
\begin{frame}\frametitle{Information clearinghouse}
  \begin{itemize}
  \item Finite number $n>1$ of homogeneous firms
    \begin{itemize}
    \item Constant marginal cost $c$
    \item Clearinghouse charges $\phi \geq 0$ to firms to list their
      prices
    \end{itemize}
  \item Consumers with unit demand and reservation price $v$
    \begin{itemize}
    \item $S > 0$ ``shoppers'' consult clearinghouse, buy at lowest
      price if $< v$, else visits one other firm buys if price $<v$,
      else does not buy
    \item $L > 0$ ``loyal'' consumers visit firm $i$, buy if $p_i < v$
    \end{itemize}
  \item Equilibrium with price dispersion if $L > 0$ or $\phi>0$
    \begin{itemize}
    \item Non-clearinghouse prices all $= v$
    \item Distribution of clearinghouse prices $\leq v$
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{\cite{hong2006}}

\begin{frame}\frametitle{\cite{hong2006}: ``Using price distributions
    to estimate search costs''}
  \begin{itemize}
  \item Goal: estimate consumer search costs
  \item Environment: online booksellers
    \begin{itemize}
    \item Homogeneous product
    \item Homogeneous firm costs
    \end{itemize}
  \item Data: distribution of prices
  \item Method: use distribution of prices $+$ assumption about form
    of search to estimate distribution of consumer costs
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hs-fig1}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{Fixed search}
  \begin{itemize}
  \item Firm marginal cost $r$, continuum of firms with equilibrium
    price distribution $F_p$
  \item Consumer search cost $c_i \sim F_c$
  \item Number of searches
    \[ \left(\Er[ p^{(1:n(c_i)-1)} ] - \Er[ p^{(1:n(c_i))} ] \right)K \geq c_i
    \geq \left(\Er[ p^{(1:n(c_i))} ] - \Er[ p^{(1:n(c_i)+1)} ] \right)K \]    
  \item Define $\Delta_n = \Er[ p^{(1:n-1)} ] - \Er[ p^{(1:n)} ]$;
    $F_P$ observed, so $\Delta_n$ identified 
  \item Let $\tilde{q}_n = F_c(\Delta_{n-1}) - F_c(\Delta_n) = $
    portion of consumers who obtain $n$ prices
    \begin{itemize}
    \item $\tilde{q}_n$ not observed
    \item Assume $F_c$ such that $\tilde{q}_n = 0$ for all $n > K$
      (could be relaxed, but complicates econometrics)
    \end{itemize}
  \item Firms indifferent among prices $p \in
    [\underline{p},\overline{p}]$, so
    \[ (\overline{p} - r) \tilde{q}_1  = (p-r) \left[
      \sum_{k=1}^K \tilde{q}_{k} k \left(1 - F_p(p) \right)^{k-1}
    \right] \]
  \item Observed prices $p_j$, $j = 1, ..., n_f$
    \[ (\overline{p} - r) \tilde{q}_1  = (p-r) \left[
      \sum_{k=1}^K \tilde{q}_{k} k \left(1 - \hat{F}_p(p_j) \right)^{k-1}
    \right] \]
    identifies $\tilde{q}_1, ..., \tilde{q}_{K}$ and $r$
  \item Knowing $\tilde{q}_1, ..., \tilde{q}_K$ can solve for
    $F_c(\Delta_1), ..., F_c(\Delta_{K})$
  \item Estimate using empirical likelihood ($\approx$ efficiently
    weighted GMM) 
  \end{itemize}
\end{frame}
\begin{frame}
  \begin{columns}
    \begin{column}{0.3\textwidth}
      \begin{itemize}
      \item Estimates for Billingsley using 20 prices, $K=3$, and 5
        moments
      \item $\tilde{q}_1 = 0.633$, $\tilde{q}_2 = 0.309$, $\tilde{q}_3 =0.058$
      \end{itemize}
    \end{column}
    
    \begin{column}{0.7\textwidth}
      \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hs-fig2a}    
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Sequential search}
  \begin{itemize}
  \item Consumer search cost $c_i \sim F_c$
  \item Reservation price, $p_i^* = \overline{p}(c_i) = \min\{z(c_i),\overline{p}\}$
    where
    \[ c_i = \int_{\underline{p}}^{z(c_i)} (z(c_i) - p) f(p) dp =
    \int_{\underline{p}}^{z(c_i)} F(p) dp \]    
    Let $G(p) = $ CDF of $p_i^*$
  \item Firm indifference:
    \[ (\overline{p} - r) \left(1 - G(\overline{p}) \right) = (p-r)
    \left(1 - G(p) \right) \] 
  \item Data: $n_f$ prices, but $n_f - 1$ indifference conditions, so
    need some restriction 
    \begin{itemize}
    \item Parametric assumption about $F_c$ 
    \item (in fixed search model, assumption about $K$ played a
      similar role)
    \item Or fix $r$ and estimate $F_c$ nonparametrically 
    \end{itemize}
  \item Estimate by MLE
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hs-tab1}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hs-tab2}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hs-tab3}
\end{frame}

\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item For text books: Stokey-Lucas, Lazear, Billingsley, Duffie
  \item Fixed search model:
    \begin{itemize}
    \item Median search cost $\approx \$2.50$ (quantiles above median
      not identified)
    \item 25\%tile \$0.68 - \$2.50
    \item Selling cost $r \approx 65\%$ of median price
    \end{itemize}
  \item Sequential search model:
    \begin{itemize}
    \item Median search cost \$9.22-\$29.40
    \item Search cost such that $z(c_i) = \overline{p}$, $\$4.56 - \$19.19$
    \item Selling cost $r \approx 40\%$ of median price
    \end{itemize}
  \item Check whether parametric assumption driving sequential
    results: fix $r$ and estimate nonparametrically
  \end{itemize}
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{hs-tab4}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Additional empirical work}
\begin{frame}[allowframebreaks]\frametitle{Additional empirical work}
  \begin{itemize}
  \item \cite{gw2008}: 
    \begin{itemize}
    \item Oligopoly version of \cite{hong2006} fixed search model
    \item MLE instead of nonparametric EL
    \end{itemize}
  \item \cite{chen2007}: 
    \begin{itemize}
    \item Model selection test to choose between fixed
      and sequential search 
    \item Test is inconclusive
    \end{itemize}
  \item \cite{msw2012}
    \begin{itemize}
    \item \cite{hong2006}/\cite{gw2008} fixed search model with multiple markets
    \item Data: multiple markets with common search cost distribution,
      but different reservation prices, firm costs, and/or number of
      firms 
    \item Semi-nonparametric estimator
    \item Application: memory chips
    \end{itemize}
  \item \cite{santos2012}
    \begin{itemize}
    \item Data on web browsing and purchases to test sequential vs
      fixed search
    \item Key difference: behavior in sequential model depends on
      prices observed so far; in fixed model it does not
    \item Context: online book stores
    \item Results: favor fixed search model; also evidence of
      unobserved product heterogeneity (store loyalty)
    \end{itemize}
  \item \cite{hortacsu2004}
    \begin{itemize}
    \item Context: mutual funds
    \item Model with search frictions and product heterogeneity 
    \item Results: 
      \begin{itemize}
      \item Investors value observable nonportfolio product attributes
      \item Small search costs can rationalize price dispersion
      \end{itemize}
    \end{itemize}
  \item \cite{wildenbeest2011}
    \begin{itemize}
    \item Vertical product differentiation and search frictions
    \item Fixed search model
    \item ML estimation
    \item Context: grocery items
    \item Results: supermarket heterogeneity more important than
      search frictions
    \end{itemize}
  \item \cite{honka2014}: search \& switching costs in auto insurance
    \begin{itemize}
    \item Fixed search model 
    \item Consumer knows price of current insurer, and prices of $k$
      others
    \item Pays switching cost if change insurer
    \item Finds search costs more important than switching costs for
      customer retention \& consumer welfare
    \end{itemize}
  \item Search with learning: \cite{shw2012}, \cite{koulayev2013}
  \end{itemize}
\end{frame}

\section{\cite{koulayev2013}}

\begin{frame}\frametitle{\cite{koulayev2013}}
  \begin{itemize}
  \item Search model with unknown distribution
  \item Model based on \cite{rothschild1974} 
  \item Applied to S\&P 500 mutual funds
  \item Highlights differences with search model with known price
    distribution 
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item $N$ products with utilities $S_N = \{u_1, ..., u_N\}$, where
    $u_1 > u_2 > \cdots > u_N$
  \item Consumer believes possible utilities $S_G = \{ \tilde{u}_1,
    ..., \tilde{u}_G\}$ with $S_N \subseteq S_G$
  \item Search technology: each search independent and $\tilde{u}_g$
    drawn with probability $p_g$
  \item Consumer does not know $p_g$, has Dirichlet prior with
    parameters $\alpha_1, ..., \alpha_G$, 
    \[ f(\tilde{p}_1, ..., \tilde{p}_g) = \frac{\Gamma(\sum \alpha_g
      )}{\prod \gamma(\alpha_g)} \prod \tilde{p}_g^{\alpha_g - 1} \]
    which implies
    \[ \Er[\tilde{p}_j] = \frac{\alpha_j}{\sum \alpha_g} \]
  \item Bayesian updating: after seeing $\tilde{u}_g$, $n_g$ times,
    \begin{align*}
      f(p|n) \propto & f(n|p) f(p) \\
      \propto & \frac{(\sum n_g)!}{\prod n_g!} \prod p_g^{n_g} \frac{\Gamma(\sum \alpha_g
      )}{\prod \gamma(\alpha_g)} \prod \tilde{p}_g^{\alpha_g - 1} \\
      \propto & \frac{\Gamma(\sum \alpha_g + n_g
      )}{\prod \gamma(\alpha_g + n_g)} \prod \tilde{p}_g^{\alpha_g +
                n_g - 1} 
    \end{align*}
    so 
    \[ \Er[\tilde{p}_j | n_1, ..., n_g] = \frac{\alpha_j + n_j}{\sum \alpha_g
      + n_g} \]
  \item Sequential search and at end buy best good found
  \item Search cost $c$, best good found so far $u_{r^*}$
  \item Continue searching if 
    \begin{align*} 
      \Er[\max\{\tilde{u}, u_{r^*} \}|n] - u_{r^*} & > c \\
                                               \sum_{\tilde{u}_g>u_{r^*}} (\tilde{u}_g
      - u_{r^*})\Er[\tilde{p}_g|n]  &  > c 
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Market shares}
  \begin{itemize}
  \item Observe: market shares, product characteristics
  \item Consumers have different search costs $c_i \sim F(c)$
  \item Challenge: many search histories can lead to the same choice;
    need to integrate over all search histories to compute market
    shares
    \begin{itemize}
    \item Define $\bar{k}_{r} = $ longest a consumer with best draw
      $u_{r}$ will continue searching 
      \[ \bar{k}_r = \max\left\lbrace 1, \left \lceil \frac{1}{c}  \sum_{\tilde{u}_g>u_{r^*}} (\tilde{u}_g
      - u_{r^*})\alpha_g - \sum_g \alpha_g\right\rceil \right\rbrace \]
    \item Show that market shares can be written as a function of just
      the $\bar{k}_1 , ...., \bar{k}_N$
    \item $\bar{k}_r$ is integer valued and decreasing in $c$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Example}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{k-fig1a}
\end{frame}

\begin{frame}\frametitle{Example}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{k-fig1b}
\end{frame}

\begin{frame}
  \begin{itemize}
  \item Paper has simulations comparing price elasticity in search
    with learning versus search without learning models
  \item Simulations also show that ignoring learning can lead to bias
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Application: S\&P 500 mutual funds}
  \begin{itemize}
  \item $u_g = -$price (fixed fee per \$10,000 invested)
  \item $\log c_i \sim N(\mu_0 + \mu_1 t, \delta_0 + \delta_1 t)$
  \item Search probabilities depend on fund age:
    \[ \rho_{jt} = \frac{A_{jt}^\gamma}{\sum A_{kt}^\gamma} \]
  \item Rational prior: $\alpha_{jt} = \rho_{jt} N_0$ 
    \begin{itemize}
    \item Consumers' prior not identified from market share data alone
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Application: S\&P 500 mutual funds}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{k-fig4}
\end{frame}


\begin{frame}\frametitle{Application: S\&P 500 mutual funds}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{k-tab1}
\end{frame}

\begin{frame}\frametitle{Application: S\&P 500 mutual funds}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{k-fig5}
\end{frame}

\begin{frame}\frametitle{Application: S\&P 500 mutual funds}
  \includegraphics[width=\textwidth,height=\textheight,keepaspectratio=true]{k-fig6}
\end{frame}





\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}